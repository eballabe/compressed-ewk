from src.latex_utils import MakePdfofDirectory
Regions = ["DF0J_CRVV", "DF0J_VRVV", "DF0J_CRtop", "DF0J_VRtop", "SR_DF0J_binnedInclusive"]
Regions = ["DF0J_CRVVnew"]


for Region in Regions:
    MakePdfofDirectory("Output/" + Region)

vsav
variables_to_plot = [
    "lep1pT",
    "lep2pT",
    "MET",
    "MT2",
    "mll",
    "METsig",
    "dPhiLL",
    "dPhiMETL1",
    "dPhiMETL2",
    "cosTstar",
    "DPhib",
    "BDTDeltaM100_90",
    "BDTVVDeltaM100_90",
    "BDTtopDeltaM100_90",
    "BDTothersDeltaM100_90"
]

from src.latex_utils import make_pdf_3_plots
dirs = ["Output/SR_DF0J_inclusive", "Output/DF0J_VRVV", "Output/DF0J_CRVV"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_DF0J_inclusive", "Output/DF0J_VRVV", "Output/DF0J_CRVV"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_SF0J_inclusive", "Output/SF0J_VRVV", "Output/SF0J_CRVV"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_DF1J_inclusive", "Output/DF1J_VRVV", "Output/DF1J_CRVV"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_SF1J_inclusive", "Output/SF1J_VRVV", "Output/SF1J_CRVV"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


#top
dirs = ["Output/SR_DF0J_inclusive", "Output/DF0J_VRtop", "Output/DF0J_CRtop"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_SF0J_inclusive", "Output/SF0J_VRtop", "Output/SF0J_CRtop"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_DF1J_inclusive", "Output/DF1J_VRtop", "Output/DF1J_CRtop"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)


dirs = ["Output/SR_SF1J_inclusive", "Output/SF1J_VRtop", "Output/SF1J_CRtop"  ]

make_pdf_3_plots(dirs[0], dirs[1], dirs[2], variables_to_plot)