# Download, setup and compile the code the first time

To setup SusySkimAna the first time do
<pre>mkdir myArea
cd myArea
mkdir source build
cd source
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-higgsino/SusySkimHiggsino.git
git clone ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimMaker.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-wg/Common/Ext_RestFrames.git
git clone ssh://git@gitlab.cern.ch:7999/jshahini/NearbyLepIsoCorrection.git
git clone ssh://git@gitlab.cern.ch:7999/shion/TruthDecayContainer.git</pre>
and be sure to git checkout the same branches for at least the SusySkimMaker, SusySkimDriver, SusySkimHiggsino and TruthDecayContainer packages. Then, in the same source directory do
<pre>source SusySkimMaker/scripts/setupRelease.sh --release21
build </pre>
and everything should be fine. In case you experience errors with the Ext_RestFrames package after setting up the environment do as follows.
1) Remove the following lines if they are duplicated in myArea/source/CMakeLists.txt.
<pre>#Include the externals configuration:
include( Ext_RestFrames/externals.cmake )</pre>
2) Comment all the following lines in myArea/source/CMakeLists.txt.
<pre># RestFrames requires additions to CMakeList after each asetup
# (see https://gitlab.cern.ch/atlas-phys-susy-wg/Common/Ext_RestFrames.git)
if [ -d "Ext_RestFrames" ]; then
  sed -i 's/find_package( AnalysisBase )/find_package( AnalysisBase ) \
  \n\n# Include the externals configuration: \
  \ninclude( Ext_RestFrames\/externals.cmake )/g' CMakeLists.txt
fi </pre>
3) Repeat the setup by simply typing 
<pre>source setup.sh</pre>

# On every login or if you make any change
<pre>cd myArea/source
source setup.sh
build </pre>

# To produce new ntuples from DAODs
<pre>run_xAODNtMaker -d /path/to/the/output/directory -s /path/to/the/sample/directory -writeSkims 0 -writeTrees 1 -deepConfig SusySkimHiggsino/deepConfig_file -selector selector </pre>
- If it is AFII simulation add the `--isAf2` flag
- If it is data add the `--isData` flag
- If it is MC DAOD_TRUTH add the `--noCutBookkeepers` and the `--isTruthOnly` flags

Currently, the displaced track ntuples are produced using the MonojetSelector and the SusySkimHiggsino/SusySkimHiggsino_Rel21_DT.config.
