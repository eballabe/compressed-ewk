#include "SusySkimDriver/MergeWrapper.h"
#include "SusySkimMaker/MergeTool.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/SampleSet.h"
#include "SusySkimMaker/BaseUser.h"
#include "SusySkimMaker/DuplicateEventChecker.h"

// Root includes
#include "TChain.h"

// ------------------------------------------------------------------------------------------------------ //
MergeWrapper::MergeWrapper() :
  m_mergeDir(""),
  m_selector(""),
  m_minEvent(-1),
  m_maxEvent(-1),
  m_nEventsPerMergeJob(5000000),
  m_disableMetaDataCheck(false),
  m_resubmitMergeJobs(false),
  m_noSubmit(false)
{
}
// ------------------------------------------------------------------------------------------------------ //
bool MergeWrapper::init(TString mergeDir,TString selector,bool resubmit)
{

  m_mergeDir           = mergeDir;
  m_selector           = selector;
  m_resubmitMergeJobs  = resubmit;
  //
  return true;
}

// ------------------------------------------------------------------------------------------------------ //
void MergeWrapper::merge(TString process,TString treeName,SampleHandler::BATCH_TYPE batch_type)
{

  //
  auto sampleSet = getSampleSet(process);
  if( batch_type==SampleHandler::BATCH_TYPE::LSF ){
    merge_submitToLSF(sampleSet,treeName);
  }
  else{
    merge(sampleSet,treeName);
  }


}
// ------------------------------------------------------------------------------------------------------ //
void MergeWrapper::merge(SampleSet* sampleSet,TString treeName)
{

  TString outFileName = m_mergeDir + "/" + sampleSet->process + "_merged.root";;
  if(m_minEvent>=0 && m_maxEvent>0){
    TString bounds = TString::Format("_%i_%i",m_minEvent,m_maxEvent);
    outFileName = m_mergeDir + "/" + sampleSet->process + bounds + "_merged.root";
  }

  TString finalOutputName = SampleHandler::formatMergeOutputFileName(outFileName,treeName,sampleSet->isTruth);
  gSystem->Exec("rm -rf " + finalOutputName );

  auto parentChains = sampleSet->getTChainList(treeName);
  auto cbkChains = sampleSet->getTChainList("CutBookkeepers");

  TChain* cbkChain = 0;
  if( cbkChains.size()!=1 ){
    MsgLog::WARNING("MergeWrapper::merge","Could not find any CutBookkeepers TTree, cannot use these for normalization!!!");
  }
  else{
    MsgLog::INFO("MergeWrapper::merge","Found CutBookkeepers TTree, this will be used for the normalization");
    cbkChain = cbkChains[0];
  }


  for( auto& pChain : parentChains ){

    TString outTreeName = SampleHandler::formatOutputTTreeName( TString(pChain->GetName()), sampleSet->process, sampleSet->isData );
    TFile* out = TFile::Open(finalOutputName.Data(), "UPDATE");

    int maxEvent = m_maxEvent;
    if( maxEvent>pChain->GetEntries() ){
       MsgLog::INFO("MergeWrapper::merge","Max event %i is larger than input file %i. Fixing to input file!",m_maxEvent,pChain->GetEntries());
       maxEvent = pChain->GetEntries();
    }

    MergeTool* mergeTool = new MergeTool(pChain,cbkChain);
    mergeTool->setLumi(sampleSet->lumi);
    mergeTool->init(sampleSet->isData, sampleSet->isAF2,m_minEvent,maxEvent);
    mergeTool->addClone(outTreeName, out);

    // User analysis code, if requested
    BaseUser* baseUser = 0;
    if( m_selector!="" ){
      baseUser = getAnalysisSelector(m_selector);
      baseUser->init_merge(mergeTool);
    }

    // Loop over all events
    while( mergeTool->next() ){

      // Run normalization, etc
      mergeTool->calculateCommon();

      // User methods: calculate other variables,
      // or skim out events
      if( baseUser ){
        if( !baseUser->execute_merge(mergeTool) ){
          continue;
        }
      }

      // Fill output tree event
      mergeTool->fill();

    } // Loop over entries in each tree

    // Check metadata
    if( !m_disableMetaDataCheck ){
      for( auto& cTree : mergeTool->getChildTrees() ){
        checkMerge( sampleSet, cTree.second->tree, cbkChain );
      }
    }

    out->Write();
    out->Close();
    delete mergeTool;

  } // Loop over trees in file

}
// ------------------------------------------------------------------------------------------------------ //
bool MergeWrapper::checkMerge(const SampleSet* sampleSet, TTree*& tree, TChain* cbkChain)
{


  DuplicateEventChecker* dec = new DuplicateEventChecker();

  TString duplicateEventLog = "DuplicateEventLog_" + sampleSet->process + "_"+TString(tree->GetName());
  dec->setOutputInfo( m_mergeDir , duplicateEventLog );

  //
  SampleHandler::countDuplicateEvents(dec,sampleSet, tree);
  SampleHandler::checkMissingEvents(dec,sampleSet, cbkChain);

  // Finalize
  dec->collectResults();

  delete dec;

  return true;

}
// ------------------------------------------------------------------------------------------------------ //
SampleSet* MergeWrapper::getSampleSet(TString process)
{

  // Open file
  TString filename =  m_mergeDir + "/SampleSet_" + process;
  if( m_minEvent > -1 || m_maxEvent > -1)
    filename += TString::Format("_%d_%d", m_minEvent, m_maxEvent);
  TFile* file = new TFile( filename + ".root" ,"READ");

  if( !file->IsOpen() ){
    MsgLog::ERROR("MergeWrapper::getSampleSet","Cannot get SampleSet for %s and therefore cannot  merge!", process.Data() );
    return NULL;
  }

  // Retrieve SampleSet
  SampleSet* sampleSet = dynamic_cast<SampleSet*>( file->Get( process.Data() ) );
  if( !sampleSet ){
    MsgLog::ERROR("MergeWrapper::getSampleSet","Cannot get SampleSet from %s!!", file->GetName() );
    return NULL;
  }
  if( sampleSet->getMergeInputs().size()==0 ){
    MsgLog::ERROR("MergeWrapper::getSampleSet","Sample set %s has no merge inputs, nothing to merge!!",process.Data() );
    return NULL;
  }

  //
  return sampleSet;

}
// ----------------------------------------------------------------------------------------------------------- //
void MergeWrapper::merge_submitToLSF(SampleSet* sampleSet,TString treeName)
{

  MsgLog::INFO("MergeWrapper::merge_submitToLSF","Submitting process %s to LSF batch system...", sampleSet->process.Data() );

  // Submit one job for each tree
  // for cases of systematics
  for( auto& sys : sampleSet->getListOfTrees() ){

    // User doesn't want to process all trees
    if(treeName!="" && sys!=treeName ){
      MsgLog::INFO("MergeWrapper::merge_submitToLSF","Ignoring tree name %s, at the request of the user",sys.Data() );
      continue;
    }

    auto parentChains = sampleSet->getTChainList(treeName);
    if( parentChains.size()==0 ) continue;
    auto parentChain = parentChains[0];
    MsgLog::INFO("MergeWrapper::merge_submitToLSF","Total number of events in file %i ",parentChain->GetEntries() );

    // If the user wants to divide jobs, check bounds,
    // otherwise one job for entire tree
    float maxEvents = m_nEventsPerMergeJob;
    if( maxEvents<0 ||  maxEvents > parentChain->GetEntries() ){
      maxEvents = parentChain->GetEntries();
    }

   // Systematic tree might have different number of events!?

    // Loop over all jobs
    for( double evt=0; evt<=parentChain->GetEntries(); evt+=maxEvents ){
      double minEvent = evt;
      double maxEvent = minEvent+maxEvents-1;
      if( maxEvent > parentChain->GetEntries() ) maxEvent = parentChain->GetEntries();
      MsgLog::INFO("MergeWrapper::merge_submitToLSF","Submitting job with min event %f and max event %f ",minEvent,maxEvent );

      // Open new file to write a batch script
      TString batchScript_fullPath = TString::Format("%s/mergeScript_%s_%s_%i_%i.sh",m_mergeDir.Data(),sampleSet->process.Data(),sys.Data(),int(minEvent),int(maxEvent) );

      // Check if job was successful, if resubmitting jobs
      if( !checkJob(sampleSet,sys,minEvent,maxEvent) ) continue;

      std::ofstream batchScript( batchScript_fullPath.Data() );
      if( !batchScript.is_open() ){
        MsgLog::ERROR("MergeWrapper::merge_submitToLSF","Could not open batch script file for writing!!");
        continue;
      }
      // Sumbission command
      batchScript << "#!/bin/bash \n \n ";
      batchScript << "#BSUB -W2000 \n \n ";
      batchScript << "cd " << m_mergeDir << " \n \n";
      batchScript << "run_merge ";
      batchScript << "  -d " << sampleSet->baseDir;
      batchScript << "  -groupSet " << sampleSet->groupSet;
      batchScript << "  -tag " << sampleSet->tag;
      batchScript << "  -process " << sampleSet->process;
      batchScript << "  -treeName " << sys;
      batchScript << "  -minEvent " << int(minEvent);
      batchScript << "  -maxEvent " << int(maxEvent);
      batchScript << "  -lumi "+std::to_string(sampleSet->lumi);
      batchScript << "  -deepConfig " << sampleSet->deepConfig;
      batchScript << "  --skipMergePrep ";
      batchScript << "  --disableMetaDataCheck ";

      if( m_selector!="" ){
        batchScript << " -selector " << m_selector;
      }

      batchScript.close();

      TString log = TString::Format("%s/MergeLogs_%s_%s_%i_%i",m_mergeDir.Data(),sampleSet->process.Data(),sys.Data(),int(minEvent),int(maxEvent));

      TString cmd = "bsub -R \"select[centos7]\" ";
      cmd += " -o " + log + ".log ";
      cmd += " -e " + log + ".err ";
      cmd += " < " + batchScript_fullPath;
      if( !m_noSubmit ) gSystem->Exec( cmd.Data() );
      else{
	MsgLog::INFO("SampleHandler::merge_submitToLSF","Submission command \n  -> %s \n ",cmd.Data() );
      }

    }
  }

}
// ------------------------------------------------------------------------------------------------------ //
bool MergeWrapper::checkJob(SampleSet* sampleSet,TString sys,int minEvent,int maxEvent)
{

  // Running in normal submission mode
  if( !m_resubmitMergeJobs ) return true;

  // Add in path
  TString bounds = TString::Format("_%i_%i",int(minEvent),int(maxEvent));
  TString outFileName = m_mergeDir + "/" +  sampleSet->process + "_merged.root";;
  if(minEvent>=0 && maxEvent>0){
    outFileName = m_mergeDir + "/" +  sampleSet->process + bounds + "_merged.root";
  }
  TString finalOutputName = SampleHandler::formatMergeOutputFileName(outFileName,sys,sampleSet->isTruth);


  TFile* f = TFile::Open(finalOutputName.Data(),"READ");
  bool resubmit = false;
  if( !f || !f->IsOpen() ){
    MsgLog::INFO("SampleHandler::merge_submitToLSF","Could not open output root file, resubmitting %s",finalOutputName.Data());
    resubmit = true;
  }

  if( f && !resubmit ){
    TString outTreeName = SampleHandler::formatOutputTTreeName(sys, sampleSet->process, sampleSet->isData );
    TTree* tree = dynamic_cast<TTree*>(f->Get(outTreeName));
    if( !tree ){
      MsgLog::INFO("SampleHandler::merge_submitToLSF","Could not get TTree, resubmitting...");
      resubmit = true;
    }

    /*
    if( !resubmit ){
      int nEntries  = tree->GetEntries();
      int nExpected = maxEvent-minEvent;
      if( (nEntries<nExpected) || nEntries>nExpected ){
	MsgLog::INFO("SampleHandler::merge_submitToLSF","Detected %i events when expecting %i... resubmitting....",nEntries,nExpected);
	resubmit = true;
      }
    }
    */
  }

  if( f ) f->Close();

  // Looks like the job was successful!! Yay!
  if( !resubmit ) return false;
  MsgLog::INFO("SampleHandler::merge_submitToLSF","Resubmitting job...");
  return true;



}
