##############################################
#                                            
#   TruthDecayContainer Continuous Integration       
#      
##############################################

stages:
  - build 
  - package
  - run 

variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: none
  AB_FIXED_RELEASE: 21.2.130
  AB_NEXT_RELEASE: latest
  SRC_DIR_ABS: "${CI_PROJECT_DIR}/../"
  BUILD_DIR_ABS: "${CI_PROJECT_DIR}/../../build/"
  TESTSAMPLE_HOME: "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-susy/EWK0L_ANA-SUSY-2018-41/test_DAOD/"

before_script:
  # Install ssh-agent if not already installed, it is required by Docker.
  # (change apt-get to yum if you use a CentOS-based image)
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

  # Run ssh-agent (inside the build environment)
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY"
  - echo "$SSH_PRIVATE_KEY" > tmpKey
  - chmod 600 tmpKey
  - ssh-add tmpKey
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  - echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
  - klist
  - echo -e "Host svn.cern.ch lxplus.cern.ch\n\tUser ${CERN_USER}\n\tStrictHostKeyChecking no\n\tGSSAPIAuthentication yes\n\tGSSAPIDelegateCredentials yes\n\tProtocol 2\n\tForwardX11 no\n\tIdentityFile ~/.ssh/id_rsa" >> ~/.ssh/config 
  - more ~/.ssh/config
  - set +e
  - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
  - pwd
  - echo "Project Directory    ${CI_PROJECT_DIR}"
  - echo "Source Directory     ${SRC_DIR_ABS}"
  - echo "Build Directory      ${BUILD_DIR_ABS}"


################################
#  Build
################################

.build_template:
  stage: build
  tags:
    - cvmfs
  variables:
    GIT_STRATEGY: fetch
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - pwd;ls
    - echo "Make sure cvmfs is visible"
    - ls /cvmfs/atlas.cern.ch/
    
    # Set up the AB release
    - cd ${SRC_DIR_ABS}
    - source /home/atlas/release_setup.sh

    # Build
    - mkdir ${BUILD_DIR_ABS}
    - cd ${BUILD_DIR_ABS}
    - sudo cp ${CI_PROJECT_DIR}/CMakeLists_topLvl.txt  ${SRC_DIR_ABS}/CMakeLists.txt
    - cmake ${SRC_DIR_ABS}
    - make -j8
    - source "${AnalysisBase_PLATFORM}/setup.sh"
    - if ! (type TruthDecayContainerTester > /dev/null 2>&1); then echo "Compile failed. Exit."; exit 1; fi
    - echo "Make sure the env/paths are correctly set."; TruthDecayContainerTester
    - echo "Generate RPM..."; cpack -G RPM
    - ls -lavh
    - mv WorkDir_*.rpm ${CI_PROJECT_DIR}/truthdecaycontainer.rpm
    - ls ${CI_PROJECT_DIR}
    - ls ${BUILD_DIR_ABS}
    - ls ${SRC_DIR_ABS}
  artifacts:
    name: rpm
    paths:
      - truthdecaycontainer.rpm
      - Dockerfile
    expire_in: 1 day

################################
# build RPMs of the analysis code to install in docker image

compile:
  extends: .build_template
  image: atlas/analysisbase:$AB_FIXED_RELEASE

################################
#  Image building
################################

# This is a package template that all package jobs use to package the RPMs.
.package_template:
  stage: package
  dependencies: [compile]
  variables:
    BUILD_ARG_1: CI_COMMIT_SHA=$CI_COMMIT_SHA
    BUILD_ARG_2: CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
    BUILD_ARG_3: CI_COMMIT_TAG=$CI_COMMIT_TAG
    BUILD_ARG_4: CI_JOB_URL=$CI_JOB_URL
    BUILD_ARG_5: CI_PROJECT_URL=$CI_PROJECT_URL
  tags:
    - docker-image-build
  script:
    - ignore

#####################################
# Push image to latest in docker-registry
# This is always run.
# If tagged, CI_COMMIT_REF_SLUG is the tag name with dashes

package_image:
  extends: .package_template
  stage: package
  variables:
    TO: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  dependencies: [compile]


################################
# Run  
################################
# Anything that wants to use the provided analysis image (with set up) should
# extend this template.  This specifies the built image, as well as the correct
# initialization to get MBJ_run.py and kinit access.
.analysis_image:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - ls -lavh /eos/
    - source /home/atlas/release_setup.sh
    - echo $SERVICE_PASS | kinit $CERN_USER

# This template is used for testing the decayHandle functions
.test_DAOD:
  extends: .analysis_image
  stage: run
  script:
    - ls -lavh
    - ls -dl ./
    - echo "CI_JOB_NAME=${CI_JOB_NAME}"
    - TruthDecayContainerTester -inputFile ${INPUTFILE}
    - ls -lavh

# This template is used for testing the polarization reweighting tool
.test_polReweight:
  extends: .analysis_image
  variables:
    NEVENTS: 2000
  stage: run
  script:
    - ls -lavh
    - ls -dl ./
    - echo "CI_JOB_NAME=${CI_JOB_NAME}"
    - EWKinoPolReweighting -inputFile ${INPUTFILE} -maxEvents ${NEVENTS}
    - ls -lavh
  artifacts:
    name: costhStar
    paths:
      - test_polReweight.pdf
    expire_in: 1 day

############
# Test DecayHandle
############
test_mc16e_Wenu_MAXHTPTV280_500_BFilter_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.deriv.DAOD_SUSY1.e5340_s3126_r9364_p3990/DAOD_SUSY1.19547804._000016.pool.root.1
  stage: run
############
test_mc16e_Znunu_PTV500_1000_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.364222.Sherpa_221_NNPDF30NNLO_Znunu_PTV500_1000.deriv.DAOD_SUSY1.e5626_s3126_r9364_p3990/DAOD_SUSY1.19539335._000012.pool.root.1
  stage: run
############
test_mc16e_Znunu_PTV140_280_MJJ0_500_CVetoBVeto_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.366032.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_CVetoBVeto.deriv.DAOD_SUSY1.e7033_s3126_r10724_p3990/DAOD_SUSY1.19534419._000043.pool.root.1
  stage: run
############
test_mc16e_ttbar_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY1.e6337_s3126_r10724_p3990/DAOD_SUSY1.19501657._000196.pool.root.1
  stage: run
############
test_mc16e_WqqZvv_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_SUSY1.e5525_s3126_r9364_p3990/DAOD_SUSY1.19497100._000041.pool.root.1
  stage: run
############
test_mc16e_C1C1_WW_800_200_nofilt_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.397694.MGPy8EG_A14N23LO_C1C1_WW_800_200_nofilt.deriv.DAOD_SUSY1.e7354_s3126_r10724_p3990/DAOD_SUSY1.20788927._000001.pool.root.1
  stage: run
############
test_mc16e_C1N2_WZ_800_200_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.397744.MGPy8EG_A14N23LO_C1N2_WZ_800_200.deriv.DAOD_SUSY1.e7354_s3126_r10724_p3990/DAOD_SUSY1.20788877._000001.pool.root.1
  stage: run
############
run_mc16e_C1N2_Wh_hall_800_200_had_SUSY1_p3990:
  extends: .test_DAOD
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.397642.MGPy8EG_A14N23LO_C1N2_Wh_hall_800_200_had.deriv.DAOD_SUSY1.e7354_s3126_r10724_p3990/DAOD_SUSY1.20788590._000001.pool.root.1
  stage: run
############


############
# Test polarization reweighting
############
test_polReweight_mc16e_C1C1_WW_800_200_nofilt_SUSY1_p3990:
  extends: .test_polReweight
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.397694.MGPy8EG_A14N23LO_C1C1_WW_800_200_nofilt.deriv.DAOD_SUSY1.e7354_s3126_r10724_p3990/DAOD_SUSY1.20788927._000001.pool.root.1
  stage: run
############
test_polReweight_mc16e_C1N2_WZ_800_200_SUSY1_p3990:
  extends: .test_polReweight
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.397744.MGPy8EG_A14N23LO_C1N2_WZ_800_200.deriv.DAOD_SUSY1.e7354_s3126_r10724_p3990/DAOD_SUSY1.20788877._000001.pool.root.1
  stage: run
############
run_mc16e_C1N2_Wh_hall_800_200_had_SUSY1_p3990:
  extends: .test_polReweight
  variables:
    INPUTFILE: ${TESTSAMPLE_HOME}/mc16_13TeV.397642.MGPy8EG_A14N23LO_C1N2_Wh_hall_800_200_had.deriv.DAOD_SUSY1.e7354_s3126_r10724_p3990/DAOD_SUSY1.20788590._000001.pool.root.1
  stage: run
############
