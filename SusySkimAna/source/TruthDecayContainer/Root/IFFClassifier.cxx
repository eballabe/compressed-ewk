/// Based on https://twiki.cern.ch/twiki/pub/AtlasProtected/IsolationFakeForum/MakeTruthClassification.hxx

#include "TruthDecayContainer/DecayHandle.h"

//////////////////////////////////////////////////////
bool DecayHandle::isPromptRealEle(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  int firstEgMotherPdgId         = cacc_TDC_firstEgMotherPdgId.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherPdgId(*mcp)       : 0;
  // -------------------------

  bool isPrompEle = false;
  isPrompEle = (type==2 || 
		(type==4 && origin==5 && fabs(firstEgMotherPdgId) == 11) || 
		/// bkg electrons from ElMagDecay with origin top, W or Z, higgs, diBoson
		(type==4 && origin==7 && firstEgMotherType==2 && 
		 (firstEgMotherOrigin==10||firstEgMotherOrigin==12||firstEgMotherOrigin==13||
		  firstEgMotherOrigin==14||firstEgMotherOrigin==43) &&
		 fabs(firstEgMotherPdgId) == 11) || 
		/// unknown electrons from multi-boson (sherpa 222, di-boson)
		(type==1 && firstEgMotherType==2 && firstEgMotherOrigin==47 && fabs(firstEgMotherPdgId) == 11));
     
  if(isPrompEle && (firstEgMotherPdgId*((mcp->status()&2)?1:-1))<0) return true;
  
  /// bkg photons from photon conv fomr FSR (must check!!)
  if(type==4 && origin==5 && firstEgMotherOrigin==40) return true;  
  /// non-iso photons from FSR for the moment but we must check!! (must check!!)
  if(type==15 && origin==40) return true;  
  /// mainly in Sherpa Zee, but some also in Zmumu
  if(type==4 && origin==7 && firstEgMotherType==15 && firstEgMotherOrigin==40) return true; 

  return false;
}

//////////////////////////////////////////////////////
bool DecayHandle::isPromptRealMuon(xAOD::TruthParticle* mcp)
{
  // -----------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  // -----------------------------

  if(type==6 && (origin==10 || origin==12 || origin==13 || origin==14 || origin==15 || 
			       origin==22 || origin==43)) return true;

  return false;
}
//////////////////////////////////////////////////////
bool DecayHandle::isPromptRealLep(xAOD::TruthParticle* mcp)
{
  if(std::abs(mcp->pdgId())!=11 && std::abs(mcp->pdgId())!=13){
    ERROR("DecayHandle::isPromptRealLep", "Input xAOD::TruthParticle (pdg: %i) is not electron or muon . Return false.", mcp->pdgId());
    return false;
  }

  return  std::abs(mcp->pdgId())==11 ? isPromptRealEle(mcp) : isPromptRealMuon(mcp); 
}
//////////////////////////////////////////////////////
bool DecayHandle::isChargeFlip(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  int firstEgMotherPdgId         = cacc_TDC_firstEgMotherPdgId.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherPdgId(*mcp)       : 0;
  // -------------------------

  bool isPrompEle = false;
  isPrompEle = (type==2 || 
		(type==4 && origin==5 && fabs(firstEgMotherPdgId) == 11) || 
		/// bkg electrons from ElMagDecay with origin top, W or Z, higgs, diBoson
		(type==4 && origin==7 && firstEgMotherType==2 && 
		 (firstEgMotherOrigin==10||firstEgMotherOrigin==12||firstEgMotherOrigin==13||
		  firstEgMotherOrigin==14||firstEgMotherOrigin==43) &&
		 fabs(firstEgMotherPdgId) == 11) || 
		/// unknown electrons from multi-boson (sherpa 222, di-boson)
		(type==1 && firstEgMotherType==2 && firstEgMotherOrigin==47 && fabs(firstEgMotherPdgId) == 11));
  
  if(isPrompEle && (firstEgMotherPdgId*((mcp->status()&2)?1:-1))>0) return true;
  else return false;
}

//////////////////////////////////////////////////////
bool DecayHandle::isConversion(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  int chFlipEl = isChargeFlip(mcp);
  // -------------------------

  /// iso photon from a prompt photon sau Higgs
  if(type==4 && origin==5 && firstEgMotherType==14 && 
     (firstEgMotherOrigin==37||firstEgMotherOrigin==14)) return true;  
  if(type==14 && origin==37) return true;  
  
  /// bkg electrons from ElProc from a prompt photon
  if(type==4 && origin==7 && firstEgMotherType==14 && firstEgMotherOrigin==37) return true;  
  
  /// bkg photon from UndrPhot; don't have many when chFlipElLep!=0 (Here there is a generator level photon (not gen electron ) that later converts)
  if(chFlipEl==0 && type==4 && origin==5 && firstEgMotherType==16 && (firstEgMotherOrigin==38))return true;
  if(type==16 && origin==38) return true; /// allow to select photons from parton shower, i.e. brem photons not isolated
  
  
  return false;
}

//////////////////////////////////////////////////////
bool DecayHandle::isHadronDecay(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  // -------------------------

  /// bkg electrons from DalitzDec, LightMeson and StrangeMeson
  ///
  /// type = 4 origin = 6 (DalitzDecay) firstEgMotherType = 4 firstEgMotherOrigin = 6 (DalitzDecay) ==>
  /// here it worths to check the first eg mother and the contact but again seems to not be a generator level electron.
  ///
  if(type==4 && (origin==6 || origin==23 || origin==24)) return true;
  
  /// bkg electrons / bkg photons from pi0, LightMeson and StrangeMeson
  if(type==4 && origin==5 && 
     firstEgMotherType==16 && (firstEgMotherOrigin==42||firstEgMotherOrigin==23||firstEgMotherOrigin==24)) return true; 
  if(type==16 && (origin==42 || origin==23)) return true; 
  
  /// bkg electrons from ElProc from StrangeMeson
  if(type==4 && origin==7 && firstEgMotherType==4 && firstEgMotherOrigin==24) return true; 
  /// bkg electrons from ElProc from pi0
  if(type==4 && origin==7 && firstEgMotherType==16 && firstEgMotherOrigin==42) return true; 
     
  /// bkg muons from PionDecay, KaonDecay, LightMeson and StrangeMeson
  if(type==8 && (origin==34 || origin==35 || origin==23 || origin==24)) return true; 
  
  /// hadrons
  if(type==17) return true;
  
  return false;
}

//////////////////////////////////////////////////////
bool DecayHandle::isHF_nonPrompt_tau(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  // -------------------------

  /// non iso electrons from tau decays
  if(type==3 && origin==9) return true;
  
  /// non-iso photons from tau decays
  if(type==15 && origin==9) return true;
  if(type==4 && origin==5 && firstEgMotherType==15 && firstEgMotherOrigin==9) return true;
  
  /// non iso muons from tau decays
  if(type==7 && origin==9) return true;

  return false;
}

//////////////////////////////////////////////////////
bool DecayHandle::isHF_nonPrompt_mu(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  // -------------------------

  /// non iso electron/photon from muon
  if((type==3 || type==15)&& origin==8) return true;
  if(type==4 && origin==7 && firstEgMotherType==3 && firstEgMotherOrigin==8) return true;
  if(type==4 && origin==5 && firstEgMotherType==15 && firstEgMotherOrigin==8) return true;
  
  return false;
}

//////////////////////////////////////////////////////
bool DecayHandle::isHF_nonPrompt_B(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  // -------------------------

  /// electrons from BottomMeson, BBbarMeson and BottomBaryon
  if(type==3 && (origin==26||origin==29||origin==33)) return true; 
  if(type==4 && origin==7 && firstEgMotherType==3 && firstEgMotherOrigin==26) return true;  
  
  /// bkg photons from BottomMeson
  if(type==16 && origin==26) return true;  
  if(type==4 && origin==5 && firstEgMotherType==16 && firstEgMotherOrigin==26) return true;  
  
  /// iso / nonIso muons from BottomMeson, BBbarMeson and BottomBaryon
  if((type==6 || type==7) && (origin==26 || origin==29 || origin==33)) return true;  
  
  return false;
}

/////////////////////////////////////////////////////////////////////
bool DecayHandle::isHF_nonPrompt_C(xAOD::TruthParticle* mcp)
{
  // --------------------------
  int origin = cacc_TDC_truthOrigin.isAvailable(*mcp) ? cacc_TDC_truthOrigin(*mcp) : 0;
  int type   = cacc_TDC_truthType.isAvailable(*mcp)   ? cacc_TDC_truthType(*mcp)   : 0;
  int firstEgMotherType     = cacc_TDC_firstEgMotherType.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherType(*mcp)   : 0;
  int firstEgMotherOrigin   = cacc_TDC_firstEgMotherOrigin.isAvailable(*mcp) ? 
    cacc_TDC_firstEgMotherOrigin(*mcp) : 0;
  // -------------------------

  /// non iso electrons from CharmedMeson, CharmedBaryon and CCbarMeson
  if(type==3 && (origin==25||origin==32||origin==27)) return true; 
  
  /// bkg electrons from CCbarMeson
  if(type==4 && origin==27) return true; /// JPsi
  
  /// bkg photons from CharmedMeson and CCbarMeson
  if(type==16 && (origin==25 || origin==27)) return true;
  if(type==4 && origin==5 && 
     firstEgMotherType==16 && (firstEgMotherOrigin==25 || firstEgMotherOrigin==27)) return true;
  
  
  /// non iso muons from CharmedMeson, CharmedBaryon and CCbarMeson
  if(type==7 && (origin==25 || origin==32 || origin==27)) return true; 
  
  /// iso or bkg muons from CCbarMeson
  if((type==6 || type==8) && origin==27) return true; /// JPsi
  
  return false;
}
/////////////////////////////////////////////////////////////////////
