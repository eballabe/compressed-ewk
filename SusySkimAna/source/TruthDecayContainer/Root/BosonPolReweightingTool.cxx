#include "TruthDecayContainer/BosonPolReweightingTool.h"

// System include(s):
#include <memory>
#include <cstdlib>
#include <vector>
#include <algorithm>

// ROOT include(s):
#include <TError.h>
#include <TString.h>
#include <TFile.h>
#include <TKey.h>
#include <TObject.h>

// EDM include(s):
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

// Other include(s):
#include "AsgTools/StatusCode.h"
#include "AsgTools/AsgMetadataTool.h"
#include "TruthDecayContainer/ProcClassifier.h"
#include "TruthDecayContainer/TruthDecayUtils.h"
#include "PathResolver/PathResolver.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
BosonPolReweightingTool::BosonPolReweightingTool() : 
  m_convertFromMeV(0.001),
  m_func_cosThetaStar(0),
  m_decayHandle(0)
{}
//////////////////////////////////////////////////////////////////////////////////////////////////////
BosonPolReweightingTool::~BosonPolReweightingTool()
{}
//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode BosonPolReweightingTool::init(int dsid)
{  
  // Initialize DecayHandle instance 
  m_decayHandle = new DecayHandle();
  if(dsid)
    m_decayHandle->init(dsid,true,"Unknown"); // DSID=0, useGeV=true, generator="Unknown"
  else
    m_decayHandle->init(true); // useGeV=true

  // Load spin maps
  std::string path = PathResolverFindCalibFile("TruthDecayContainer/bosonSpinReweighting/spinMap.root");
  TFile *fin = new TFile(path.c_str(),"read");
  loadHistograms(fin, m_spinMap);

  // Initialize cosThetaStar shape function form
  m_func_cosThetaStar = new TF1("func_cosThetaStar","[0]*((3/4.)*[1]*(1-x^2) + (3/8.)*[2]*(1-x)^2 + (3/8.)*[3]*(1+x)^2)/( (3/4.)*[1]+(3/8.)*[2]+(3/8.)*[3]   )",-1,1);
    
  // Return gracefully
  std::cout << "BosonPolReweightingTool:init()  INFO  Initialization succeeded." << std::endl;

  return StatusCode::SUCCESS;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////
void BosonPolReweightingTool::loadHistograms(TFile* fin, std::map<TString, TH2F*> &histDict) {
  
  std::vector<TString> objNameList;   
  TList* list = fin->GetListOfKeys() ;
   if (!list) { printf("<E> No keys found in file\n") ; exit(1) ; }
   
   TIter next(list) ;
   TKey* key ;
   TObject* obj;
   
   do{    
     key = (TKey*)next();  if(!key) break;     
     try {
       obj = key->ReadObj() ;     
       if ( TString(obj->IsA()->GetName()) != TString("TH2F")  ) continue;
       TString name = obj->GetName();
       if(!name.EndsWith("intp") ) continue;
       std::cout << "BosonPolReweightingTool::loadHistograms  INFO  Loading " << name << std::endl;

       TH2F* hist = (TH2F*) fin->Get(name);
       histDict.insert(std::pair<TString, TH2F*>(name,hist));

     } catch (std::exception& e)
       {
	 std::cout << e.what() << std::endl;
	 std::cout << "BosonPolReweightingTool::loadHistograms  ERROR  Hit the loading limit of TFile. Do split into small files. Exit." << std::endl;
	 return;
       }       
     
   } while (key) ;
   
   std::cout << "BosonPolReweightingTool::loadHistograms  INFO  Loaded " << histDict.size() << " maps." << std::endl;

   return;
}

//////////////////////////////////////////////////
float BosonPolReweightingTool::fetchCoefficients_fixedTanb(const TString &proc, 
							   const float &mHeavy, const float &mLight, 
							   const int &tanb_i, const int &sgnMu){

  // Sanity check on the input tanb
  if(tanb_i!=2 && tanb_i!=5 && tanb_i!=10 && tanb_i!=30){
    std::cout << "BosonPolReweightingTool::fetchCoefficients_fixedTanb  ERROR  Input tanb (" << tanb_i << ") has invalid value! It has to be either 2/5/10/30. Return -1." << std::endl;
    return -1;
  }
    
  // Get corresponding histogram
  TString histName = "F0_"+proc+"_winoBino_tanb"+std::to_string(tanb_i)+"_"+(sgnMu>0 ? "posMu" : "negMu")+"_dM_intp";

  std::map<TString,TH2F*>::iterator it = m_spinMap.find(histName);	
  if(it==m_spinMap.end()){
    std::cout << "BosonPolReweightingTool::fetchCoefficients_fixedTanb  ERROR  No hist " << histName << " is found " << std::endl;
    return -999.;
  }

  // Fall back if dM is too small
  float dM = mHeavy-mLight;
  if(dM<100){
    std::cout << "BosonPolReweightingTool::fetchCoefficients_fixedTanb  WARNING  dM(" << dM << ")<100GeV is not supported. Fill dM=100GeV values as the approximation. " << std::endl;
    dM = 100.;
  }

  // Get value from histogram
  TH2F* hist = m_spinMap[histName];
  float F0 = hist->GetBinContent(hist->FindBin(mHeavy,dM));

  return F0;

}

//////////////////////////////////////////////////
float BosonPolReweightingTool::getPolWeight(const TString &proc, 
					    const float &mHeavy, 
					    const float &mLight, 
					    const float &cosThetaStar,
					    bool verbose){
  
  // Skip bosons with pathological kinematic configuration
  if(fabs(cosThetaStar)>1.) return 1.;

  // Ignore off-shell bosons
  if(mHeavy-mLight<75.) return 1.;

  // Only the nominal setup used in the simplified model generation is considered for now
  const int tanb = 10; 
  const float sgnMu = 1; 

  // Extract the longitudinal fraction of the boson polarization
  float F0 = fetchCoefficients_fixedTanb(proc,mHeavy,mLight,tanb,sgnMu);

  // Ignore the diff between L vs R (assuming L=R)
  float FL = (1-F0)/2.;  
  float FR = (1-F0)/2.; 

  // Calculate the weight
  m_func_cosThetaStar->SetParameters(1.,F0,FL,FR);  
  m_func_cosThetaStar->SetParameter(0, 1./m_func_cosThetaStar->Integral(-1.,1.));  

  float weight = m_func_cosThetaStar->Eval(fabs(cosThetaStar)) / (1./2);  // Default cosThetaStar: flat -> f(x)=1/2 (x=[-1,1])

  if(verbose)
    std::cout << " Process_decayingBoson: " << proc 
	      << " mHeavy: " << mHeavy 
	      << " mLight: " << mLight 
	      << " cosThetaStar: " << cosThetaStar
	      << " weight: " << weight 
	      << std::endl;

  return weight;
}

//////////////////////////////////////////////////
std::vector<std::pair<float,float> > BosonPolReweightingTool::getPolWeights(TruthEvent_XX *evt, bool verbose){

  TString proc = 
    evt->prodLabel==22 && std::abs(evt->B1.pdg)==24 && std::abs(evt->B2.pdg)==24  ? "C1C1_WW" :
    evt->prodLabel==21 && std::abs(evt->B1.pdg)==24  && evt->B2.pdg==23  ? "C1N2_WZ" :
    evt->prodLabel==21 && evt->B1.pdg==23  && std::abs(evt->B2.pdg)==24  ? "C1N2_WZ" :
    evt->prodLabel==21 && std::abs(evt->B1.pdg==24)  && evt->B2.pdg==25  ? "C1N2_Wh" :
    evt->prodLabel==21 && evt->B1.pdg==25  && std::abs(evt->B2.pdg==24)  ? "C1N2_Wh" :
    "";

  std::vector<std::pair<float,float> >  output; // pair of (cosThetaStar, weight)
  if(proc=="") return output;

  // Re-weight the first boson
  if(std::abs(evt->B1.pdg)==24)
    output.push_back(std::pair<float,float>(evt->costh_q11_B1, 
		     getPolWeight(proc+"_W", evt->pchi1.M(), evt->pN1A.M(), evt->costh_q11_B1, verbose)));

  else if(evt->B1.pdg==23)
    output.push_back(std::pair<float,float>(evt->costh_q11_B1, 
		     getPolWeight(proc+"_Z", evt->pchi1.M(), evt->pN1A.M(), evt->costh_q11_B1, verbose)));

  // Re-weight the second boson
  if(std::abs(evt->B2.pdg)==24)
    output.push_back(std::pair<float,float>(evt->costh_q21_B2, 
		     getPolWeight(proc+"_W", evt->pchi2.M(), evt->pN1B.M(), evt->costh_q21_B2, verbose)));

  else if(evt->B2.pdg==23)
    output.push_back(std::pair<float,float>(evt->costh_q21_B2, 
		     getPolWeight(proc+"_Z", evt->pchi2.M(), evt->pN1B.M(), evt->costh_q21_B2, verbose)));

  return output;
}

//////////////////////////////////////////////////
std::vector<std::pair<float,float> > BosonPolReweightingTool::getPolWeights(const xAOD::TruthParticleContainer* truthParticles, bool verbose){

  std::vector<std::pair<float,float> > weights;

  if(!truthParticles) {
    std::cout << "BosonPolReweightingTool::getCombinedPolWeight  WARNING  Input TruthParticleContainer is null. Return 1." << std::endl;
    return weights;  
  }

  m_decayHandle->loadContainers(truthParticles,0,0,0,0,0,0,0,0,0,0);
  
  TruthEvent_XX *evt = new TruthEvent_XX();
  m_decayHandle->GetDecayChain_XX(evt);

  weights = getPolWeights(evt,verbose);
  delete evt;

  return weights;
}
//////////////////////////////////////////////////
float BosonPolReweightingTool::getCombinedPolWeight(TruthEvent_XX *evt, bool verbose){

  float weight=1.;
  for(auto w : getPolWeights(evt,verbose))  weight *= w.second;
 
  return weight;
}
//////////////////////////////////////////////////
float BosonPolReweightingTool::getCombinedPolWeight(const xAOD::TruthParticleContainer* truthParticles, bool verbose){

  float weight=1.;
  for(auto w : getPolWeights(truthParticles,verbose))  weight *= w.second;

  return weight;
}
//////////////////////////////////////////////////
