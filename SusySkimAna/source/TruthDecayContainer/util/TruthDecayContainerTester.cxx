// System include(s):
#include <memory>
#include <cstdlib>
#include <string>
#include <iostream>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TStopwatch.h>
#include <TSystem.h>
#include "TObjArray.h"
#include "TObjString.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TStore.h"
#ifdef ROOTCORE
#include "xAODRootAccess/TEvent.h"
#else
#include "POOLRootAccess/TEvent.h"
#endif // ROOTCORE
#include "PATInterfaces/CorrectionCode.h"
#include <AsgMessaging/MessageCheck.h>

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODJet/JetContainer.h"

// Local include(s):
#include "TruthDecayContainer/DecayHandle.h"
#include "TruthDecayContainer/ProcClassifier.h"


int main( int argc, char* argv[] ) {
  
  // makes macros from AsgMessaging/MessageCheck.h available
  using namespace asg::msgUserCode;
  // don't return 0 on failures in main()
  ANA_CHECK_SET_TYPE (int)

  // The application's name:
  const char* APP_NAME = argv[ 0 ];

  // Unload the command line arguments
  TString inFilePath="";
  
  for(int k=0; k<argc; ++k) {
    TString key = argv[k];
    if(key=="-inputFile")   inFilePath  = argv[k+1];
  }

  // Check if we received a file name:
  if ( inFilePath=="" ) {
    Error( APP_NAME, "No file name received!" );
    Error( APP_NAME, "  Usage: %s -inputFile [full path to the xAOD file]", APP_NAME );
    return 1;
  }

  // Open the input file:
  Info( APP_NAME, "Opening file: %s", inFilePath.Data() );
  std::auto_ptr< TFile > ifile( TFile::Open( inFilePath, "READ" ) );
  if( !ifile.get() ) return EXIT_FAILURE;


  // Create a TEvent object:
#ifdef ROOTCORE
  xAOD::TEvent event( xAOD::TEvent::kAthenaAccess );
#else
  POOL::TEvent event( POOL::TEvent::kAthenaAccess );
#endif

  ANA_CHECK( event.readFrom( ifile.get() ) );
  Info( APP_NAME, "Number of events in the file: %i",
        static_cast< int >( event.getEntries() ) );

  xAOD::TStore store;
  
  // Get DSID
  const xAOD::EventInfo* eventInfo = 0;
  event.getEntry(0);
  ANA_MSG_INFO ("Running TruthDecayContainerTester ...");
  ANA_CHECK( event.retrieve( eventInfo, "EventInfo") );

  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
    Error( APP_NAME, "This is a data file. Exit. ");
    return 0;
  }
  
  int dsid = eventInfo->mcChannelNumber();
  if(dsid<=0){
    Error( APP_NAME, "Could not retrieve DSID from the file. Exit. ");
    return 1;
  }

  // Create the tool(s) to test:
  DecayHandle *decayHandle = new DecayHandle();

  bool convertFromMeV=true; // display everything in GeV
  decayHandle->init(dsid, convertFromMeV);

  TruthEvent_Vjets *truthEvent_Vjets=0;
  TruthEvent_VV    *truthEvent_VV=0;
  TruthEvent_TT    *truthEvent_TT=0;
  TruthEvent_XX    *truthEvent_XX=0;
  TruthEvent_GG    *truthEvent_GG=0;

  std::cout << "  DSID: " << dsid << std::endl;

  if(ProcClassifier::IsWjets_Sherpa(dsid) || ProcClassifier::IsZjets_Sherpa(dsid) || ProcClassifier::IsGammaJets_Sherpa(dsid)){   // V+jets
    Info(APP_NAME,"Going to test truthEvent_Vjets.");
    truthEvent_Vjets = new TruthEvent_Vjets();
  }
  if(ProcClassifier::IsVV(dsid) || ProcClassifier::IsWt(dsid) || ProcClassifier::IsZt(dsid)){   // VV (incl. t+W/Z)
    Info(APP_NAME,"Going to test truthEvent_VV.");
    truthEvent_VV    = new TruthEvent_VV();
  }
  if(ProcClassifier::IsTT(dsid) || ProcClassifier::IsTTPlusX(dsid)){ // ttbar (+X)
    Info(APP_NAME,"Going to test truthEvent_TT.");
    truthEvent_TT    = new TruthEvent_TT();
  }
  if(ProcClassifier::IsXX(dsid)){                                    // EWKino pair production (gauge boson mediated, direct decay only)
    Info(APP_NAME,"Going to test truthEvent_XX.");
    truthEvent_XX    = new TruthEvent_XX();
  }
  if(ProcClassifier::IsGG(dsid)){                                    // Gluino pair production (only for gauge boson mediated decay, upto 1-step)
    Info(APP_NAME,"Going to test truthEvent_GG.");
    truthEvent_GG    = new TruthEvent_GG();   
  }


  // ------------------------- Loop over the events ------------------------- //

  int nevt = 50;  // Just the first 50 events
  for ( int entry = 0; entry < nevt; ++entry ) {

    // Tell the object which entry to look at:
    event.getEntry( entry );

    // Retrieve containers
    const xAOD::TruthEventContainer*    truthEvents    = 0;
    const xAOD::TruthParticleContainer* truthParticles = 0;
    const xAOD::TruthParticleContainer* truthElectrons = 0;
    const xAOD::TruthParticleContainer* truthMuons     = 0;
    const xAOD::TruthParticleContainer* truthTaus      = 0;
    const xAOD::TruthParticleContainer* truthPhotons   = 0;
    const xAOD::TruthParticleContainer* truthNeutrinos = 0;
    const xAOD::TruthParticleContainer* truthBoson     = 0;
    const xAOD::TruthParticleContainer* truthTop       = 0;
    const xAOD::TruthParticleContainer* truthBSM       = 0;
    const xAOD::TruthParticleContainer* truthTauWDP    = 0;
    const xAOD::JetContainer*           truthJets      = 0;
    ANA_CHECK( event.retrieve( truthEvents,    "TruthEvents"    ));
    ANA_CHECK( event.retrieve( truthParticles, "TruthParticles" ));
    ANA_CHECK( event.retrieve( truthElectrons, "TruthElectrons" ));
    ANA_CHECK( event.retrieve( truthMuons,     "TruthMuons"     ));
    ANA_CHECK( event.retrieve( truthTaus,      "TruthTaus"      ));
    ANA_CHECK( event.retrieve( truthPhotons,   "TruthPhotons"   ));
    ANA_CHECK( event.retrieve( truthBoson,     "TruthBoson"     ));
    ANA_CHECK( event.retrieve( truthTop,       "TruthTop"       ));
    ANA_CHECK( event.retrieve( truthBSM,       "TruthBSM"       ));
    ANA_CHECK( event.retrieve( truthJets, "AntiKt4TruthDressedWZJets" ));

    decayHandle->loadContainers(truthParticles, truthElectrons, truthMuons, truthTaus, truthPhotons, truthNeutrinos, truthBoson, truthTop, truthBSM, truthTauWDP,truthJets);

    Info(APP_NAME, "////////////////////// Event: %i //////////////////////////////", entry);

    decayHandle->GetFakeLeptons();

    // Fill if the sample is W+jets ...
    if     (truthEvent_Vjets){ 
      decayHandle->GetDecayChain_Vjets(truthEvent_Vjets); 
      truthEvent_Vjets->print();
      
    // Fill if the sample is WW or t+W (t->bW){ ...
    }else if(truthEvent_VV){    
      decayHandle->GetDecayChain_VV(truthEvent_VV);
      truthEvent_VV->print();

    // Fill if the sample is ttbar (+X){ ...
    }else if(truthEvent_TT){    
      decayHandle->GetDecayChain_TT(truthEvent_TT);          
      truthEvent_TT->print();        

    // Fill if the sample is EW gaugino pair prod.
    }else if(truthEvent_XX){    
      decayHandle->GetDecayChain_XX(truthEvent_XX); 
      truthEvent_XX->print();            

    // Fill if the sample is gluino pair prod. 
    }else if(truthEvent_GG){    
      decayHandle->GetDecayChain_GG(truthEvent_GG);        
      truthEvent_GG->print();            
    }
    

  }

  return 0;
}
