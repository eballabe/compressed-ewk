#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup "asetup AnalysisBase,21.2.221,here" "rucio -w" pyami
echo "Creating an alias "build". In order to compile you just need to type build"
alias build='cmake --build $TestArea/../build; source $TestArea/../build/*/setup.sh'

# RestFrames requires additions to CMakeList after each asetup
# (see https://gitlab.cern.ch/atlas-phys-susy-wg/Common/Ext_RestFrames.git)
#if [ -d "Ext_RestFrames" ]; then
#  sed -i 's/find_package( AnalysisBase )/find_package( AnalysisBase ) \
#  \n\n# Include the externals configuration: \
#  \ninclude( Ext_RestFrames\/externals.cmake )/g' CMakeLists.txt
#fi

source set_groupset_env.sh

cd $TestArea/../build

cmake $TestArea

cd $TestArea
