#include "SusySkimHiggsino/VBF2020Selector.h"

// includes needed for RJR variable computation
#include "RestFrames/RestFrames.hh"
#include <TLorentzVector.h>
#include <TVector3.h>
#include <vector>

#include "SusySkimMaker/MsgLog.h"

//Initally this is just to be used for the 0 lepton channel
//New Variables are calculated here but can be moved to Common

//Variables with prefix "new_" already exist in Common

/*
Notes on new variables 
JetC = highest non vbf tagged jet
JetD = second highest non vbf tagged jet
etc
*/


// ------------------------------------------------------------------------------------------ //
VBF2020Selector::VBF2020Selector() : BaseUser("SusySkimHiggsino","VBF2020Selector")
{

}
// ------------------------------------------------------------------------------------------ //
void VBF2020Selector::setup(ConfigMgr*& configMgr)
{

    //For Running Indepenetly of Common 
    /*

    ///////////////// Object Selection /////////////////////

    // Use object def from SUSYTools
    configMgr->obj->useSUSYToolsSignalDef(true);

    // Bad muon veto
    configMgr->obj->applyBadMuonVeto(true);

    // Muon cuts
    configMgr->obj->setBaselineMuonPt(3.0);
    configMgr->obj->setBaselineMuonEta(2.7);
    configMgr->obj->setSignalMuonPt(3.0);
    configMgr->obj->setSignalMuonEta(2.7);

    // Electrons
    configMgr->obj->setBaselineElectronEt(4.5);
    configMgr->obj->setBaselineElectronEta(2.47);
    configMgr->obj->setSignalElectronEt(4.5);
    configMgr->obj->setSignalElectronEta(2.47);

    // Taus
    configMgr->obj->setSignalTauPt(20.0);
    configMgr->obj->setSignalTauEta(2.5);

    // Jets
    configMgr->obj->setCJetPt(20.0);
    configMgr->obj->setCJetEta(2.80);
    configMgr->obj->setFJetPt(30.0);
    configMgr->obj->setFJetEtaMin(2.80);
    configMgr->obj->setFJetEtaMax(4.50);

  ///////////////// Triggers /////////////////////

  // Defined by run number <start,end>; -1 means ignore that bound
  // Trigger bit is filled by m_configMgr->doTriggerAna() in SusySkimDriver::xAODEvtLooper::execute

  // Single electron trigger
  configMgr->addTriggerAna("HLT_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose",276262,284484, "singleElectronTrig"); // 2015
  configMgr->addTriggerAna("HLT_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",297730,-1, "singleElectronTrig"); // 2016-

  // Single muon trigger
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu40",276262,284484,"singleMuonTrig"); // 2015
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",297730,-1,"singleMuonTrig");        // 2016-

  // Met trigger
  configMgr->addTriggerAna("HLT_xe70",                    276262, 284484,"metTrig"); // 2015
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",         296939, 302872,"metTrig"); // 2016 A-D3
  configMgr->addTriggerAna("HLT_xe100_mht_L1XE50",        302919, 303892,"metTrig"); // 2016 D4-F1
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",        303943, 320000,"metTrig"); // 2016 F2-
  configMgr->addTriggerAna("HLT_xe110_pufit_L1XE55",      320000, 348884,"metTrig"); // 2017
  configMgr->addTriggerAna("HLT_xe110_pufit_xe70_L1XE50", 348885, 350013,"metTrig"); // 2018 B-C5
  configMgr->addTriggerAna("HLT_xe110_pufit_xe65_L1XE50", 350067,     -1,"metTrig"); // 2018 C6-


  configMgr->cutflow->defineCutFlow("cutFlow", configMgr->treeMaker->getFile("tree"));
  */

    //Call VBF setup in Common (adds VBF variables)
    com.vbfSetup(configMgr);

    //Call Common Setup (defines cutFlow, triggers etc) 
    com.setup(configMgr, false);



    ///////////////////// Cut flow definition //////////////////////

    // Make a cutflow stream
    //configMgr->cutflow->defineCutFlow("cutFlow", configMgr->treeMaker->getFile("tree")); //This is done in Common! 

    // Toggle this on if you want to run the cutFlow mode 
    // where dedicated cut flows are performed instead of ntuple making
    m_cutFlowOnly = false;

    if(m_cutFlowOnly){

    // Non-sequential cut flows
    configMgr->cutflow->defineCutFlow("cutFlow_genWeight",        configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_eventWeight",      configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_pileupWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_leptonWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_bTagWeight",       configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_jvtWeight",        configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_triggerWeight",    configMgr->treeMaker->getFile("tree"));
    
    // Sequential cut flows
    configMgr->cutflow->defineCutFlow("cutFlow_sq",               configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_weight",        configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_genWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_eventWeight",   configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_pileupWeight",  configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_leptonWeight",  configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_bTagWeight",    configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_jvtWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_triggerWeight", configMgr->treeMaker->getFile("tree"));
  }


}
// ------------------------------------------------------------------------------------------ //
bool VBF2020Selector::doAnalysis(ConfigMgr*& configMgr)
{
    //This is the main method, which is called for each event


    // Perform a dedicated cut flow and end the event if cutFlowOnly mode.
    if(m_cutFlowOnly){
        doCutFlow(configMgr);
        return false;
    }

    //Compute variables
    com.computeVBFVariables (configMgr);
    com.writeVBF(configMgr);
    //com.computeVBFJigSawVariables(configMgr);

    // Skims events by imposing any cuts you define in this method below
    if( !passCuts(configMgr) ) return false;

    com.doAnalysis(configMgr); //Common Variables
    com.VBF_computeTruthVariables(configMgr);

    // Fill the output tree
    configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

    return true;
}

// ------------------------------------------------------------------------------------------ //
bool VBF2020Selector::passCuts(ConfigMgr*& configMgr)
{

    /*
    This method is used to apply any cuts you wish before writing the output trees
    */
    
    // If no further skim is not needed
    if(m_disableCuts) return true;


    //double weight = configMgr->objectTools->getWeight(configMgr->obj);

    float weight = 1; // pb-1
    if(!configMgr->obj->evt.isData()){

        // Note that Rel20.7 2015+2016 is actually 36097.56 pb-1, but this is close enough
        weight = 140000; // pb-1
    }
    weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::GEN);
    weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::EVT);
    weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::LEP);
    weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::JVT);
    weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::PILEUP);

    
    ////////////////////////////////////////
    //// begin ntuple preselection cuts ////
    ////////////////////////////////////////
    
    // Kinematic variables
    const int nLep_base = configMgr->obj->baseLeptons.size();
    const int nLep_signal = configMgr->obj->signalLeptons.size();
    const float lep1Pt  = nLep_base >= 1 ? configMgr->obj->baseLeptons[0]->Pt() : 0.;
    const float lep2Pt  = nLep_base >= 2 ? configMgr->obj->baseLeptons[1]->Pt() : 0.;
    //const float lep3Pt  = nLep_base >= 3 ? configMgr->obj->baseLeptons[2]->Pt() : 0.;
    //const float lep4Pt  = nLep_base >= 4 ? configMgr->obj->baseLeptons[3]->Pt() : 0.;

    const int nJet30    = getNJets ( configMgr->obj, 30.0 );
    const float jet1Pt  = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Pt() : 0.;
    const float met     = configMgr->obj->met.Et;
    const float met_LepInvis     = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et;
    const float minDPhi_met_allJets30 = getminDPhiJetMet(configMgr->obj, -1, 30); //only central jets

    bool passMetTrig = configMgr->treeMaker->getBoolVariable("trigMatch_metTrig");
    bool pass1ElTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_singleElectronTrig");
    bool pass1MuTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_singleMuonTrig");
    bool pass2ElTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_diElectronTrig");
    bool pass2MuTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_diMuonTrig");

    // Fill cutflow histograms
    configMgr->cutflow->bookCut("cutFlow","allEvents",weight );


    //Basic VBF ntuple Selection

    // Apply all recommended event cleaning cuts
    if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;
    
    // Trigger
    
    //TODO: better implementation of truthOnly for dilepton triggers
    bool passTrig = m_truthOnly ? (met>200 || lep1Pt>28 || (lep1Pt > 20 && lep2Pt > 20)) :passMetTrig || pass1ElTrig || pass1MuTrig || pass2ElTrig || pass2MuTrig;
    if( !passTrig ) return false;
    configMgr->cutflow->bookCut("cutFlow","Pass Trigger", weight );  

    //
    // 0L
    //
    if( !( (met > 200) || (met_LepInvis > 200)) ) return false;
    configMgr->cutflow->bookCut("cutFlow","MET || MET(LepInvis) >200", weight );

    //VBF cuts
    if ( !(com.validVBFtags == 1) ) return false;
    configMgr->cutflow->bookCut("cutFlow","validVBFtags==1", weight );

    if ( !(com.vbfjjM > 500)) return false;
    configMgr->cutflow->bookCut("cutFlow","vbfjjM>500", weight );


    //if( !(jet1Pt > 125) ) return false;
    //configMgr->cutflow->bookCut("cutFlow","0L: nJet(125GeV)", weight );

    //if( !(minDPhi_met_allJets30 > 0.4) ) return false;
    //configMgr->cutflow->bookCut("cutFlow","0L: minDPhi_met_allJets30>0.4", weight );

    return true;


    //return false; 

}

// ------------------------------------------------------------------------------------------ //
void VBF2020Selector::doCutFlow(ConfigMgr*& configMgr)
{

  /*
   This method fills all cut flow histograms when cutOnlyMode is on.
  */

  // Fill cutflow histograms
  fillCutFlows(configMgr, "cutFlow", "allEvents");
  fillCutFlows(configMgr, "cutFlow_sq", "allEvents");

  // Apply all recommended event cleaning cuts
  fillCutFlows(configMgr, "cutFlow", "passEventCleaning");
  fillCutFlows(configMgr, "cutFlow_sq", "passEventCleaning");
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "dummy", 1. ) ) return;

  // ------------------------------------------------------
  // Non-sequential cuts 
  // i.e. events not discarded after each cut
  // ------------------------------------------------------

  const int nLeptons_base     = configMgr->obj->baseLeptons.size();   
  /*
  const int nLeptons_signal   = configMgr->obj->signalLeptons.size();   
  const int nElectrons_base   = configMgr->obj->baseElectrons.size();  
  const int nElectrons_signal = configMgr->obj->signalElectrons.size();  
  const int nMuons_base       = configMgr->obj->baseMuons.size();    
  const int nMuons_signal     = configMgr->obj->signalMuons.size();   
  const int nJet30            = getNJets ( configMgr->obj, 30.0 );
  const int nBJet30           = getNBJets( configMgr->obj, 30.0 );
  const int nFatJet           = configMgr->obj->cFatjets.size();
  */
  if(nLeptons_base   == 0) fillCutFlows(configMgr, "cutFlow", "== 0 Baseline lepton");
  if(nLeptons_base   == 1) fillCutFlows(configMgr, "cutFlow", "== 1 Baseline lepton");

  // ------------------------------------------------------
  // Sequential cuts 
  // i.e. events discarded after each cut
  // ------------------------------------------------------

  
  return;

}
// ------------------------------------------------------------------------------------------ //
// ------------------------------------------------------------------------------------------ //
void VBF2020Selector::fillCutFlows(ConfigMgr*& configMgr, TString cutFlowName, TString description)
{
  /*
   This method is used to fill an entry into cut flows defined in setup() 
  */

  float genWeight     = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::GEN);
  float eventWeight   = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::EVT);
  float pileupWeight  = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::PILEUP);
  float leptonWeight  = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::LEP);
  float bTagWeight    = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::BTAG);
  float jvtWeight     = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::JVT);  
  float triggerWeight = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::TRIG);  

  // Fill for cleaning cuts
  if(description=="passEventCleaning"){
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName                 , 1.      );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_genWeight"    , genWeight     );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_eventWeight"  , eventWeight   );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_pileupWeight" , pileupWeight  );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_leptonWeight" , leptonWeight  );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_bTagWeight"   , bTagWeight    );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_jvtWeight"    , jvtWeight     );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_triggerWeight", triggerWeight );        
  }
  // Fill for the other cuts
  else{
    configMgr->cutflow->bookCut(cutFlowName                 , description, 1.      );

    configMgr->cutflow->bookCut(cutFlowName+"_genWeight"    , description, genWeight     );
    configMgr->cutflow->bookCut(cutFlowName+"_eventWeight"  , description, eventWeight   );
    configMgr->cutflow->bookCut(cutFlowName+"_pileupWeight" , description, pileupWeight  );
    configMgr->cutflow->bookCut(cutFlowName+"_leptonWeight" , description, leptonWeight  );
    configMgr->cutflow->bookCut(cutFlowName+"_bTagWeight"   , description, bTagWeight    );
    configMgr->cutflow->bookCut(cutFlowName+"_jvtWeight"    , description, jvtWeight     );
    configMgr->cutflow->bookCut(cutFlowName+"_triggerWeight", description, triggerWeight );
  }

}
// ------------------------------------------------------------------------------------------ //



// ------------------------------------------------------------------------------------------ //
void VBF2020Selector::finalize(ConfigMgr*& configMgr)
{
    (void)configMgr;
  /*
   This method is called at the very end of the job. Can be used to merge cutflow histograms 
   for example. See CutFlowTool::mergeCutFlows(...)
  */

}
// ------------------------------------------------------------------------------------------ //



// ------------------------------------------------------------------------------------------ //







/*
JetVector VBF2020Selector::FilterJets(JetVector JETs, double pt_cut, double eta_cut) {
  //filters input jets based on pt and eta cuts 
  JetVector newJETs;
  //for(JetVariable* jet : configMgr->obj->aJets){
  for(JetVariable* jet : JETs){
    //JET.SetPtEtaPhiM( jet->Pt(), jet->Eta(), jet->Phi(), jet->M() );
    if((jet->Pt() >= pt_cut) && (fabs(jet->Eta()) < eta_cut || eta_cut < 0)){
       newJETs.push_back(jet);
       }
    }

    return newJETs;
}



void VBF2020Selector::addJetVariables(const ConfigMgr* configMgr, TString prefix) {
    configMgr->treeMaker->addFloatVariable(prefix+"_Pt",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_Eta",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_Phi",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_M",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_tileEnergy",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_jvt",0.0);
//    configMgr->treeMaker->addFloatVariable(prefix+"_EMFrac",0.0);
//    configMgr->treeMaker->addFloatVariable(prefix+"_HECFrac",0.0);
//    configMgr->treeMaker->addFloatVariable(prefix+"_sumpttrk",0.0);
//    configMgr->treeMaker->addIntVariable  (prefix+"_nTrk",-1);
}
void VBF2020Selector::setJetVariables(const ConfigMgr* configMgr, TString prefix, const JetVariable* jet) {
    configMgr->treeMaker->setFloatVariable(prefix+"_Pt",jet->Pt());
    configMgr->treeMaker->setFloatVariable(prefix+"_Eta",jet->Eta());
    configMgr->treeMaker->setFloatVariable(prefix+"_Phi",jet->Phi());
    configMgr->treeMaker->setFloatVariable(prefix+"_M",jet->M());
    configMgr->treeMaker->setFloatVariable(prefix+"_tileEnergy",jet->tileEnergy);
    configMgr->treeMaker->setFloatVariable(prefix+"_jvt",jet->jvt);
//    configMgr->treeMaker->setFloatVariable(prefix+"_EMFrac",jet->EMFrac);
//    configMgr->treeMaker->setFloatVariable(prefix+"_HECFrac",jet->HECFrac);
//    configMgr->treeMaker->setFloatVariable(prefix+"_sumpttrk",jet->sumpttrk);
//    configMgr->treeMaker->setIntVariable  (prefix+"_nTrk",jet->nTrk);
}
*/




