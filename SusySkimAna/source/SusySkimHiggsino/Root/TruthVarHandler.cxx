#include "SusySkimHiggsino/Common.h"

#include "TruthDecayContainer/ProcClassifier.h"

#include <iostream>
#include <fstream>
#include <algorithm>

// Helper funciton for calculating helicity angle (forward declaration)
float costh(const TLorentzVector &a, const TLorentzVector &b);
TLorentzVector BoostBack_parent_RF(const TLorentzVector &child_lab, const TLorentzVector &parent_lab);
float costhStar(const TLorentzVector &child_lab, const TLorentzVector &parent_lab);

// ------------------------------------------------------------------------------------------ //
void Common::addTruthVariables(ConfigMgr* configMgr){

  if(configMgr->isData()) return;

  const int DSID = configMgr->getDSID();

  m_writeTruth_Gammajets = ProcClassifier::IsGammaJets_Sherpa(DSID);
  m_writeTruth_Vjets = ProcClassifier::IsWjets_Sherpa(DSID) || ProcClassifier::IsZjets_Sherpa(DSID);
  m_writeTruth_VV    = ProcClassifier::IsVV(DSID);
  m_writeTruth_TT    = ProcClassifier::IsTT(DSID) || ProcClassifier::IsTTPlusX(DSID);
  m_writeTruth_XX    = ProcClassifier::IsXX(DSID) || ProcClassifier::IsXXGGMZh(DSID);

  std::cout
    << "  isData: " << configMgr->isData()
    << "  DSID: "   << DSID << std::endl
    << "  writeTruth_Vjets: " << m_writeTruth_Vjets
    << "  writeTruth_VV: "    << m_writeTruth_VV
    << "  writeTruth_TT: "    << m_writeTruth_TT
    << "  writeTruth_XX: "    << m_writeTruth_XX
    << std::endl;

  // ---------------------------------------------------------------

  //
  // Gamma+jets
  //
  if(m_writeTruth_Gammajets){
    std::cout << "SusySkimHiggsino::Common  INFO  Add truth branches for Gamma+jets." << std::endl;
    configMgr->treeMaker->addVecFloatVariable("TruthJetPt");
    configMgr->treeMaker->addVecFloatVariable("TruthJetEta");
    configMgr->treeMaker->addVecFloatVariable("TruthJetPhi");
    configMgr->treeMaker->addVecFloatVariable("TruthJetM");
    configMgr->treeMaker->addVecIntVariable("TruthJetLabel");
  }

  //
  // W/Z+jets
  //
  if(m_writeTruth_Vjets){
    std::cout << "SusySkimHiggsino::Common  INFO  Add truth branches for W/Z+jets." << std::endl;
    configMgr->treeMaker->addIntVariable("truthDecayLabel", -1);
    configMgr->treeMaker->addTLVariable("tr_pB");
    configMgr->treeMaker->addTLVariable("tr_pq1");
    configMgr->treeMaker->addTLVariable("tr_pq2");
    configMgr->treeMaker->addIntVariable("tr_B_pdg" , -1);
    configMgr->treeMaker->addIntVariable("tr_q1_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q2_pdg", -1);
    configMgr->treeMaker->addTLVariable("tr_ptau1child");
    configMgr->treeMaker->addIntVariable("tr_tau1DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau1SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau1DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau1RhoDecayAngle", -1);

    configMgr->treeMaker->addVecFloatVariable("TruthJetPt");
    configMgr->treeMaker->addVecFloatVariable("TruthJetEta");
    configMgr->treeMaker->addVecFloatVariable("TruthJetPhi");
    configMgr->treeMaker->addVecFloatVariable("TruthJetM");
    configMgr->treeMaker->addVecIntVariable("TruthJetLabel");
  
    configMgr->treeMaker->addVecFloatVariable("TruthZBosonPt");
    configMgr->treeMaker->addVecFloatVariable("TruthZBosonEta");
    configMgr->treeMaker->addVecFloatVariable("TruthZBosonPhi");
    configMgr->treeMaker->addVecFloatVariable("TruthZBosonM");
    configMgr->histMaker->addHist("TruthZBosonPt",100,0,3000);
  }

  //
  // TT
  //
  else if(m_writeTruth_TT){
    std::cout << "SusySkimHiggsino::Common  INFO  Add truth branches for TT." << std::endl;
    configMgr->treeMaker->addIntVariable("truthDecayLabel",    -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_B1", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_B2", -1);
    configMgr->treeMaker->addTLVariable("tr_pt1");
    configMgr->treeMaker->addTLVariable("tr_pt2");
    configMgr->treeMaker->addTLVariable("tr_pb1");
    configMgr->treeMaker->addTLVariable("tr_pb2");
    configMgr->treeMaker->addTLVariable("tr_pW1");
    configMgr->treeMaker->addTLVariable("tr_pW2");
    configMgr->treeMaker->addTLVariable("tr_pq11");
    configMgr->treeMaker->addTLVariable("tr_ptau11child");
    configMgr->treeMaker->addIntVariable("tr_tau11DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau11SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau11DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau11RhoDecayAngle", -1);
    configMgr->treeMaker->addTLVariable("tr_pq12");
    configMgr->treeMaker->addTLVariable("tr_pq21");
    configMgr->treeMaker->addTLVariable("tr_ptau21child");
    configMgr->treeMaker->addIntVariable("tr_tau21DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau21SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau21DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau21RhoDecayAngle", -1);
    configMgr->treeMaker->addTLVariable("tr_pq22");
    configMgr->treeMaker->addIntVariable("tr_q11_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q12_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q21_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q22_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_nTau", -1);
    configMgr->treeMaker->addIntVariable("tr_nEMu", -1);
    configMgr->treeMaker->addTLVariable("tr_pX");
    configMgr->treeMaker->addIntVariable("tr_X_pdg" , 0);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_B1_t1",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_B2_t2",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_q11_B1",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_q21_B2",-9);

    configMgr->treeMaker->addVecFloatVariable("TruthJetPt");
    configMgr->treeMaker->addVecFloatVariable("TruthJetEta");
    configMgr->treeMaker->addVecFloatVariable("TruthJetPhi");
    configMgr->treeMaker->addVecFloatVariable("TruthJetM");
    configMgr->treeMaker->addVecIntVariable("TruthJetLabel");
  }

  //
  // VV
  //
  else if(m_writeTruth_VV){
    std::cout << "SusySkimHiggsino::Common  INFO  Add truth branches for VV." << std::endl;
    configMgr->treeMaker->addIntVariable("truthDecayLabel", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_B1", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_B2", -1);
    configMgr->treeMaker->addTLVariable("tr_pB1");
    configMgr->treeMaker->addTLVariable("tr_pB2");
    configMgr->treeMaker->addIntVariable("tr_B1_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_B2_pdg", -1);
    configMgr->treeMaker->addTLVariable("tr_pq11");
    configMgr->treeMaker->addTLVariable("tr_ptau11child");
    configMgr->treeMaker->addIntVariable("tr_tau11DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau11SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau11DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau11RhoDecayAngle", -1);
    configMgr->treeMaker->addTLVariable("tr_pq12");
    configMgr->treeMaker->addTLVariable("tr_pq21");
    configMgr->treeMaker->addTLVariable("tr_ptau21child");
    configMgr->treeMaker->addIntVariable("tr_tau21DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau21SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau21DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau21RhoDecayAngle", -1);
    configMgr->treeMaker->addTLVariable("tr_pq22");
    configMgr->treeMaker->addIntVariable("tr_q11_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q12_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q21_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q22_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_nTau", -1);
    configMgr->treeMaker->addIntVariable("tr_nEMu", -1);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_B1_BB",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_q11_B1",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_q21_B2",-9);
  }

  //
  // XX
  //
  else if(m_writeTruth_XX){
    std::cout << "SusySkimHiggsino::Common  INFO  Add truth branches for XX." << std::endl;
    configMgr->treeMaker->addTLVariable("tr_pXX");
    configMgr->treeMaker->addTLVariable("tr_pchi1");
    configMgr->treeMaker->addTLVariable("tr_pchi2");
    configMgr->treeMaker->addIntVariable("tr_chi1_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_chi2_pdg", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_chi1", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_chi2", -1);

    configMgr->treeMaker->addTLVariable("tr_pB1");
    configMgr->treeMaker->addTLVariable("tr_pB2");
    configMgr->treeMaker->addIntVariable("tr_B1_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_B2_pdg", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_B1", -1);
    configMgr->treeMaker->addIntVariable("truthDecayLabel_B2", -1);

    configMgr->treeMaker->addTLVariable("tr_pN1A");
    configMgr->treeMaker->addTLVariable("tr_pN1B");
    configMgr->treeMaker->addTLVariable("tr_pq11");
    configMgr->treeMaker->addTLVariable("tr_ptau11child");
    configMgr->treeMaker->addIntVariable("tr_tau11DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau11SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau11DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau11RhoDecayAngle", -1);
    configMgr->treeMaker->addTLVariable("tr_pq12");
    configMgr->treeMaker->addTLVariable("tr_pq21");
    configMgr->treeMaker->addTLVariable("tr_ptau21child");
    configMgr->treeMaker->addIntVariable("tr_tau21DecayLabel", -1);
    configMgr->treeMaker->addIntVariable("tr_tau21SubDecayLabel", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau21DecayAngle", -1);
    configMgr->treeMaker->addFloatVariable("tr_tau21RhoDecayAngle", -1);
    configMgr->treeMaker->addTLVariable("tr_pq22");
    configMgr->treeMaker->addIntVariable("tr_q11_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q12_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q21_pdg", -1);
    configMgr->treeMaker->addIntVariable("tr_q22_pdg", -1);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_X1_XX",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_B1_X1",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_B2_X2",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_q11_B1",-9);
    configMgr->treeMaker->addFloatVariable("tr_costhStar_q21_B2",-9);

    configMgr->treeMaker->addIntVariable("prodLabel",-1);
    configMgr->treeMaker->addBoolVariable("isC1C1",false);
    configMgr->treeMaker->addBoolVariable("isC1N1",false);
    configMgr->treeMaker->addBoolVariable("isC1N2",false);
    configMgr->treeMaker->addBoolVariable("isC1N3",false);
    configMgr->treeMaker->addBoolVariable("isN2N1",false);
    configMgr->treeMaker->addBoolVariable("isN2N3",false);
    configMgr->treeMaker->addIntVariable("isWW", -1);
    configMgr->treeMaker->addIntVariable("isZZ", -1);
    configMgr->treeMaker->addIntVariable("ishh", -1);
    configMgr->treeMaker->addIntVariable("isWZ", -1);
    configMgr->treeMaker->addIntVariable("isWh", -1);
    configMgr->treeMaker->addIntVariable("isZh", -1);

  }

  // Define the branch for all the processes
  configMgr->treeMaker->addFloatVariable("polWeight",1.);
}

// ------------------------------------------------------------------------------------------ //
void Common::setTruthVariables(ConfigMgr* configMgr){

  float polWeight = 1.;
  if(!configMgr->isData()){

    //
    // Gamma+jets
    //
    if(m_writeTruth_Gammajets){

      const TruthEvent evt = configMgr->obj->truthEvent;
      for(int jj=0, n=(int)evt.truthJets.size(); jj<n; ++jj){
        configMgr->treeMaker->appendVecFloatVariable("TruthJetPt", evt.truthJets[jj].Pt());
        configMgr->treeMaker->appendVecFloatVariable("TruthJetEta", evt.truthJets[jj].Eta());
        configMgr->treeMaker->appendVecFloatVariable("TruthJetPhi", evt.truthJets[jj].Phi());
        configMgr->treeMaker->appendVecFloatVariable("TruthJetM", evt.truthJets[jj].M());
        configMgr->treeMaker->appendVecIntVariable("TruthJetLabel", evt.truthJets_label[jj]);
      }
    
    }

    //
    // V+jets
    //
    if(m_writeTruth_Vjets){
      const TruthEvent_Vjets evt = configMgr->obj->truthEvent_Vjets;
      configMgr->treeMaker->setIntVariable("truthDecayLabel", evt.V.decayLabel);
      configMgr->treeMaker->setTLVariable("tr_pB", evt.V.P4);
      configMgr->treeMaker->setTLVariable("tr_pq1", evt.V.pq1);
      configMgr->treeMaker->setTLVariable("tr_pq2", evt.V.pq2);
      configMgr->treeMaker->setIntVariable("tr_B_pdg", evt.V.pdg);
      configMgr->treeMaker->setIntVariable("tr_q1_pdg", evt.V.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q2_pdg", evt.V.q2_pdg);
      configMgr->treeMaker->setTLVariable("tr_ptau1child", evt.V.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau1DecayLabel", evt.V.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau1SubDecayLabel", evt.V.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau1DecayAngle", evt.V.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau1RhoDecayAngle", evt.V.tau1.coschild_rho);

      for(int jj=0, n=(int)evt.truthJets.size(); jj<n; ++jj){
	configMgr->treeMaker->appendVecFloatVariable("TruthJetPt", evt.truthJets[jj].Pt());
	configMgr->treeMaker->appendVecFloatVariable("TruthJetEta", evt.truthJets[jj].Eta());
	configMgr->treeMaker->appendVecFloatVariable("TruthJetPhi", evt.truthJets[jj].Phi());
	configMgr->treeMaker->appendVecFloatVariable("TruthJetM", evt.truthJets[jj].M());
	configMgr->treeMaker->appendVecIntVariable("TruthJetLabel", evt.truthJets_label[jj]);
      }

      const TruthEvent truth_evt = configMgr->obj->truthEvent;
      for(int i=0, n=(int)truth_evt.truthZbosons.size(); i<n; ++i){
          configMgr->treeMaker->appendVecFloatVariable("TruthZBosonPt", truth_evt.truthZbosons[i].Pt());
          configMgr->treeMaker->appendVecFloatVariable("TruthZBosonEta", truth_evt.truthZbosons[i].Eta());
          configMgr->treeMaker->appendVecFloatVariable("TruthZBosonPhi", truth_evt.truthZbosons[i].Phi());
          configMgr->treeMaker->appendVecFloatVariable("TruthZBosonM", truth_evt.truthZbosons[i].M());
          configMgr->histMaker->fill("TruthZBosonPt",truth_evt.truthZbosons[i].Pt(),1);
      }
  
    }

    //
    // TT
    //
    if(m_writeTruth_TT){
      const TruthEvent_TT evt = configMgr->obj->truthEvent_TT;
      configMgr->treeMaker->setIntVariable("truthDecayLabel",       evt.decayLabel);
      configMgr->treeMaker->setIntVariable("truthDecayLabel_B1", evt.B1.decayLabel);
      configMgr->treeMaker->setIntVariable("truthDecayLabel_B2", evt.B2.decayLabel);
      configMgr->treeMaker->setTLVariable("tr_pt1", evt.pt1);
      configMgr->treeMaker->setTLVariable("tr_pt2", evt.pt2);
      configMgr->treeMaker->setTLVariable("tr_pb1", evt.pb1);
      configMgr->treeMaker->setTLVariable("tr_pb2", evt.pb2);
      configMgr->treeMaker->setTLVariable("tr_pW1", evt.B1.P4);
      configMgr->treeMaker->setTLVariable("tr_pW2", evt.B2.P4);
      configMgr->treeMaker->setTLVariable("tr_pq11", evt.B1.pq1);
      configMgr->treeMaker->setTLVariable("tr_ptau11child", evt.B1.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau11DecayLabel", evt.B1.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau11SubDecayLabel", evt.B1.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau11DecayAngle", evt.B1.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau11RhoDecayAngle", evt.B1.tau1.coschild_rho);
      configMgr->treeMaker->setTLVariable("tr_pq12", evt.B1.pq2);
      configMgr->treeMaker->setTLVariable("tr_pq21", evt.B2.pq1);
      configMgr->treeMaker->setTLVariable("tr_ptau21child", evt.B2.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau21DecayLabel", evt.B2.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau21SubDecayLabel", evt.B2.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau21DecayAngle", evt.B2.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau21RhoDecayAngle", evt.B2.tau1.coschild_rho);
      configMgr->treeMaker->setTLVariable("tr_pq22", evt.B2.pq2);
      configMgr->treeMaker->setIntVariable("tr_q11_pdg", evt.B1.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q12_pdg", evt.B1.q2_pdg);
      configMgr->treeMaker->setIntVariable("tr_q21_pdg", evt.B2.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q22_pdg", evt.B2.q2_pdg);
      configMgr->treeMaker->setIntVariable("tr_nTau", evt.nTau);
      configMgr->treeMaker->setIntVariable("tr_nEMu", evt.nEMu);
      configMgr->treeMaker->setTLVariable("tr_pX", evt.pX);
      configMgr->treeMaker->setIntVariable("tr_X_pdg" , evt.X_pdg);
      configMgr->treeMaker->setFloatVariable("tr_costhStar_B1_t1", costhStar(evt.B1.P4,evt.pt1));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_B2_t2", costhStar(evt.B2.P4,evt.pt2));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_q11_B1", costhStar(evt.B1.pq1,evt.B1.P4));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_q21_B2", costhStar(evt.B2.pq1,evt.B2.P4));

      const TruthEvent evt2 = configMgr->obj->truthEvent;
      for(int jj=0, n=(int)evt2.truthJets.size(); jj<n; ++jj){
        configMgr->treeMaker->appendVecFloatVariable("TruthJetPt", evt2.truthJets[jj].Pt());
        configMgr->treeMaker->appendVecFloatVariable("TruthJetEta", evt2.truthJets[jj].Eta());
        configMgr->treeMaker->appendVecFloatVariable("TruthJetPhi", evt2.truthJets[jj].Phi());
        configMgr->treeMaker->appendVecFloatVariable("TruthJetM", evt2.truthJets[jj].M());
        configMgr->treeMaker->appendVecIntVariable("TruthJetLabel", evt2.truthJets_label[jj]);
      }
    }

    //
    // VV
    //
    else if(m_writeTruth_VV){
      const TruthEvent_VV evt = configMgr->obj->truthEvent_VV;
      configMgr->treeMaker->setIntVariable("truthDecayLabel",       evt.decayLabel);
      configMgr->treeMaker->setIntVariable("truthDecayLabel_B1", evt.B1.decayLabel);
      configMgr->treeMaker->setIntVariable("truthDecayLabel_B2", evt.B2.decayLabel);
      configMgr->treeMaker->setTLVariable("tr_pB1", evt.B1.P4);
      configMgr->treeMaker->setTLVariable("tr_pB2", evt.B2.P4);
      configMgr->treeMaker->setIntVariable("tr_B1_pdg", evt.B1.pdg);
      configMgr->treeMaker->setIntVariable("tr_B2_pdg", evt.B2.pdg);
      configMgr->treeMaker->setTLVariable("tr_pq11", evt.B1.pq1);
      configMgr->treeMaker->setTLVariable("tr_ptau11child", evt.B1.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau11DecayLabel", evt.B1.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau11SubDecayLabel", evt.B1.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau11DecayAngle", evt.B1.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau11RhoDecayAngle", evt.B1.tau1.coschild_rho);
      configMgr->treeMaker->setTLVariable("tr_pq12", evt.B1.pq2);
      configMgr->treeMaker->setTLVariable("tr_pq21", evt.B2.pq1);
      configMgr->treeMaker->setTLVariable("tr_ptau21child", evt.B2.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau21DecayLabel", evt.B2.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau21SubDecayLabel", evt.B2.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau21DecayAngle", evt.B2.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau21RhoDecayAngle", evt.B2.tau1.coschild_rho);
      configMgr->treeMaker->setTLVariable("tr_pq22", evt.B2.pq2);
      configMgr->treeMaker->setIntVariable("tr_q11_pdg", evt.B1.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q12_pdg", evt.B1.q2_pdg);
      configMgr->treeMaker->setIntVariable("tr_q21_pdg", evt.B2.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q22_pdg", evt.B2.q2_pdg);
      configMgr->treeMaker->setIntVariable("tr_nTau", evt.nTau);
      configMgr->treeMaker->setIntVariable("tr_nEMu", evt.nEMu);
      configMgr->treeMaker->setFloatVariable("tr_costhStar_B1_BB", costhStar(evt.B1.P4,evt.B1.P4+evt.B2.P4));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_q11_B1", costhStar(evt.B1.pq1,evt.B1.P4));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_q21_B2", costhStar(evt.B2.pq1,evt.B2.P4));
    }

    //
    // XX
    //
    if(m_writeTruth_XX){
      const TruthEvent_XX evt = configMgr->obj->truthEvent_XX;
      configMgr->treeMaker->setTLVariable("tr_pXX", evt.pchi1+evt.pchi2 );
      configMgr->treeMaker->setTLVariable("tr_pchi1", evt.pchi1 );
      configMgr->treeMaker->setTLVariable("tr_pchi2", evt.pchi2 );
      configMgr->treeMaker->setIntVariable("tr_chi1_pdg", evt.chi1_pdg );
      configMgr->treeMaker->setIntVariable("tr_chi2_pdg", evt.chi2_pdg );
      configMgr->treeMaker->setIntVariable("truthDecayLabel_chi1", evt.decayLabel_chi1);
      configMgr->treeMaker->setIntVariable("truthDecayLabel_chi2", evt.decayLabel_chi2);

      configMgr->treeMaker->setTLVariable("tr_pB1", evt.B1.P4 );
      configMgr->treeMaker->setTLVariable("tr_pB2", evt.B2.P4 );
      configMgr->treeMaker->setIntVariable("tr_B1_pdg", evt.B1.pdg );
      configMgr->treeMaker->setIntVariable("tr_B2_pdg", evt.B2.pdg );
      configMgr->treeMaker->setIntVariable("truthDecayLabel_B1", evt.B1.decayLabel);
      configMgr->treeMaker->setIntVariable("truthDecayLabel_B2", evt.B2.decayLabel);

      configMgr->treeMaker->setTLVariable("tr_pN1A", evt.pN1A );
      configMgr->treeMaker->setTLVariable("tr_pN1B", evt.pN1B );
      configMgr->treeMaker->setTLVariable("tr_pq11", evt.B1.pq1);
      configMgr->treeMaker->setTLVariable("tr_ptau11child", evt.B1.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau11DecayLabel", evt.B1.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau11SubDecayLabel", evt.B1.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau11DecayAngle", evt.B1.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau11RhoDecayAngle", evt.B1.tau1.coschild_rho);
      configMgr->treeMaker->setTLVariable("tr_pq12", evt.B1.pq2);
      configMgr->treeMaker->setTLVariable("tr_pq21", evt.B2.pq1);
      configMgr->treeMaker->setTLVariable("tr_ptau21child", evt.B2.tau1.pchild);
      configMgr->treeMaker->setIntVariable("tr_tau21DecayLabel", evt.B2.tau1.decayLabel);
      configMgr->treeMaker->setIntVariable("tr_tau21SubDecayLabel", evt.B2.tau1.subdecayLabel);
      configMgr->treeMaker->setFloatVariable("tr_tau21DecayAngle", evt.B2.tau1.coschild_tau);
      configMgr->treeMaker->setFloatVariable("tr_tau21RhoDecayAngle", evt.B2.tau1.coschild_rho);
      configMgr->treeMaker->setTLVariable("tr_pq22", evt.B2.pq2);
      configMgr->treeMaker->setIntVariable("tr_q11_pdg", evt.B1.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q12_pdg", evt.B1.q2_pdg);
      configMgr->treeMaker->setIntVariable("tr_q21_pdg", evt.B2.q1_pdg);
      configMgr->treeMaker->setIntVariable("tr_q22_pdg", evt.B2.q2_pdg);
      configMgr->treeMaker->setFloatVariable("tr_costhStar_X1_XX", costhStar(evt.pchi1,evt.pchi1+evt.pchi2));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_B1_X1", costhStar(evt.B1.P4,evt.pchi1));
      configMgr->treeMaker->setFloatVariable("tr_costhStar_B2_X2", costhStar(evt.B2.P4,evt.pchi2));

      float costhStar_q11_B1 = costhStar(evt.B1.pq1,evt.B1.P4);
      float costhStar_q21_B2 = costhStar(evt.B2.pq1,evt.B2.P4);
      configMgr->treeMaker->setFloatVariable("tr_costhStar_q11_B1", costhStar_q11_B1);
      configMgr->treeMaker->setFloatVariable("tr_costhStar_q21_B2", costhStar_q21_B2);

      float polWeight1 = 1.;
      if( (std::abs(evt.B1.pdg)==24 || evt.B1.pdg==23) && fabs(costhStar_q11_B1)<1.)
	polWeight1 = (3./2.)*(1-pow(costhStar_q11_B1,2));
      float polWeight2 = 1.;
      if( (std::abs(evt.B2.pdg)==24 || evt.B2.pdg==23) && fabs(costhStar_q21_B2)<1.)
	polWeight2 = (3./2.)*(1-pow(costhStar_q21_B2,2));
      polWeight = polWeight1*polWeight2;

      configMgr->treeMaker->setIntVariable("prodLabel",evt.prodLabel);
      configMgr->treeMaker->setBoolVariable("isC1C1",evt.prodLabel==22);
      configMgr->treeMaker->setBoolVariable("isC1N1",evt.prodLabel==20);
      configMgr->treeMaker->setBoolVariable("isC1N2",evt.prodLabel==21);
      configMgr->treeMaker->setBoolVariable("isC1N3",evt.prodLabel==32);
      configMgr->treeMaker->setBoolVariable("isN2N1",evt.prodLabel==10);
      configMgr->treeMaker->setBoolVariable("isN2N3",evt.prodLabel==31);

      int p1=evt.B1.pdg, p2=evt.B2.pdg;
      configMgr->treeMaker->setIntVariable("isWW", std::abs(p1)==24 && std::abs(p2)==24 );
      configMgr->treeMaker->setIntVariable("isZZ", p1==23 && p2==23);
      configMgr->treeMaker->setIntVariable("ishh", p1==25 && p2==25 );
      configMgr->treeMaker->setIntVariable("isWZ", (std::abs(p1)==24 && p2==23) || (p1==23 && std::abs(p2)==24) );
      configMgr->treeMaker->setIntVariable("isWh", (std::abs(p1)==24 && p2==25) || (p1==25 && std::abs(p2)==24) );
      configMgr->treeMaker->setIntVariable("isZh", (p1==23 && p2==25) || (p1==25 && p2==23) );
    }

  }

  if(!configMgr->isData())
    configMgr->treeMaker->setFloatVariable("polWeight", polWeight);

}
// ------------------------------------------------------------------------------------------ //

// ------------------------------------------------------------------------------------------ //
int Common::getHiggsinoDM(int DSID){
  int deltaM = 0;
  if (DSID==393596 || DSID==394173 || DSID==394177 || DSID==394181 || DSID==394185 || DSID==394189 || DSID==394193 || DSID==393597 || DSID==394174 || DSID==394178 || DSID==394182 || DSID==394186 || DSID==394190 || DSID==394194 || DSID==393598 || DSID==394175 || DSID==394179 || DSID==394183 || DSID==394187 || DSID==394191 || DSID==394195 || DSID==393599 || DSID==394176 || DSID==394180 || DSID==394184 || DSID==394188 || DSID==394192 || DSID==394196){
    deltaM = 2;
  } else if (DSID==393400 || DSID==393407 || DSID==393414 || DSID==393421 || DSID==393428 || DSID==393435 || DSID==393442 || DSID==393449 || DSID==393456 || DSID==393463 || DSID==393470 || DSID==393477 || DSID==393484 || DSID==393491 || DSID==393498 || DSID==393505 || DSID==393512 || DSID==393519 || DSID==393526 || DSID==393533 || DSID==393540 || DSID==393547 || DSID==393554 || DSID==393561 || DSID==393568 || DSID==393575 || DSID==393582 || DSID==393589){
    deltaM = 3;
  } else if (DSID==393401 || DSID==393408 || DSID==393415 || DSID==393422 || DSID==393429 || DSID==393436 || DSID==393443 || DSID==393450 || DSID==393457 || DSID==393464 || DSID==393471 || DSID==393478 || DSID==393485 || DSID==393492 || DSID==393499 || DSID==393506 || DSID==393513 || DSID==393520 || DSID==393527 || DSID==393534 || DSID==393541 || DSID==393548 || DSID==393555 || DSID==393562 || DSID==393569 || DSID==393576 || DSID==393583 || DSID==393590){
    deltaM = 5;
  } else if (DSID==393402 || DSID==393409 || DSID==393416 || DSID==393423 || DSID==393430 || DSID==393437 || DSID==393444 || DSID==393451 || DSID==393458 || DSID==393465 || DSID==393472 || DSID==393479 || DSID==393486 || DSID==393493 || DSID==393500 || DSID==393507 || DSID==393514 || DSID==393521 || DSID==393528 || DSID==393535 || DSID==393542 || DSID==393549 || DSID==393556 || DSID==393563 || DSID==393570 || DSID==393577 || DSID==393584 || DSID==393591){
    deltaM = 10;
  } else if (DSID==393403 || DSID==393410 || DSID==393417 || DSID==393424 || DSID==393431 || DSID==393438 || DSID==393445 || DSID==393452 || DSID==393459 || DSID==393466 || DSID==393473 || DSID==393480 || DSID==393487 || DSID==393494 || DSID==393501 || DSID==393508 || DSID==393515 || DSID==393522 || DSID==393529 || DSID==393536 || DSID==393543 || DSID==393550 || DSID==393557 || DSID==393564 || DSID==393571 || DSID==393578 || DSID==393585 || DSID==393592){
    deltaM = 20;
  } else if (DSID==393404 || DSID==393411 || DSID==393418 || DSID==393425 || DSID==393432 || DSID==393439 || DSID==393446 || DSID==393453 || DSID==393460 || DSID==393467 || DSID==393474 || DSID==393481 || DSID==393488 || DSID==393495 || DSID==393502 || DSID==393509 || DSID==393516 || DSID==393523 || DSID==393530 || DSID==393537 || DSID==393544 || DSID==393551 || DSID==393558 || DSID==393565 || DSID==393572 || DSID==393579 || DSID==393586 || DSID==393593){
    deltaM = 40;
  } else if (DSID==393405 || DSID==393412 || DSID==393419 || DSID==393426 || DSID==393433 || DSID==393440 || DSID==393447 || DSID==393454 || DSID==393461 || DSID==393468 || DSID==393475 || DSID==393482 || DSID==393489 || DSID==393496 || DSID==393503 || DSID==393510 || DSID==393517 || DSID==393524 || DSID==393531 || DSID==393538 || DSID==393545 || DSID==393552 || DSID==393559 || DSID==393566 || DSID==393573 || DSID==393580 || DSID==393587 || DSID==393594){
    deltaM = 60;
  } else if (DSID==393406 || DSID==393413 || DSID==393420 || DSID==393427 || DSID==393434 || DSID==393441 || DSID==393448 || DSID==393455 || DSID==393462 || DSID==393469 || DSID==393476 || DSID==393483 || DSID==393490 || DSID==393497 || DSID==393504 || DSID==393511 || DSID==393518 || DSID==393525 || DSID==393532 || DSID==393539 || DSID==393546 || DSID==393553 || DSID==393560 || DSID==393567 || DSID==393574 || DSID==393581 || DSID==393588 || DSID==393595){
    deltaM = 100;
  } else {
    deltaM = 0;
  }
  return deltaM;
}
// ------------------------------------------------------------------------------------------ //
bool Common::isHiggsinoN2C1p(int DSID){
  bool isN2C1p = false;
  if (DSID==393596 || DSID==393400 || DSID==393401 || DSID==393402 || DSID==393403 || DSID==393404 || DSID==393405 || DSID==393406 || DSID==394173 || DSID==393407 || DSID==393408 || DSID==393409 || DSID==393410 || DSID==393411 || DSID==393412 || DSID==393413 || DSID==394177 || DSID==393414 || DSID==393415 || DSID==393416 || DSID==393417 || DSID==393418 || DSID==393419 || DSID==393420 || DSID==394181 || DSID==393421 || DSID==393422 || DSID==393423 || DSID==393424 || DSID==393425 || DSID==393426 || DSID==393427 || DSID==394185 || DSID==393428 || DSID==393429 || DSID==393430 || DSID==393431 || DSID==393432 || DSID==393433 || DSID==393434 || DSID==394189 || DSID==393435 || DSID==393436 || DSID==393437 || DSID==393438 || DSID==393439 || DSID==393440 || DSID==393441 || DSID==394193 || DSID==393442 || DSID==393443 || DSID==393444 || DSID==393445 || DSID==393446 || DSID==393447 || DSID==393448){
    isN2C1p = true;
  } else {
    isN2C1p = false;
  }
  return isN2C1p;
}
// ------------------------------------------------------------------------------------------ //
bool Common::isHiggsinoN2C1m(int DSID){
  bool isN2C1m = false;
  if (DSID==393597 || DSID==393449 || DSID==393450 || DSID==393451 || DSID==393452 || DSID==393453 || DSID==393454 || DSID==393455 || DSID==394174 || DSID==393456 || DSID==393457 || DSID==393458 || DSID==393459 || DSID==393460 || DSID==393461 || DSID==393462 || DSID==394178 || DSID==393463 || DSID==393464 || DSID==393465 || DSID==393466 || DSID==393467 || DSID==393468 || DSID==393469 || DSID==394182 || DSID==393470 || DSID==393471 || DSID==393472 || DSID==393473 || DSID==393474 || DSID==393475 || DSID==393476 || DSID==394186 || DSID==393477 || DSID==393478 || DSID==393479 || DSID==393480 || DSID==393481 || DSID==393482 || DSID==393483 || DSID==394190 || DSID==393484 || DSID==393485 || DSID==393486 || DSID==393487 || DSID==393488 || DSID==393489 || DSID==393490 || DSID==394194 || DSID==393491 || DSID==393492 || DSID==393493 || DSID==393494 || DSID==393495 || DSID==393496 || DSID==393497){
    isN2C1m = true;
  } else{
    isN2C1m = false;
  }
  return isN2C1m;
}
// ------------------------------------------------------------------------------------------ //
bool Common::isHiggsinoC1C1(int DSID){
  bool isC1C1 = false;
  if (DSID==393599 || DSID==393547 || DSID==393548 || DSID==393549 || DSID==393550 || DSID==393551 || DSID==393552 || DSID==393553 || DSID==394176 || DSID==393554 || DSID==393555 || DSID==393556 || DSID==393557 || DSID==393558 || DSID==393559 || DSID==393560 || DSID==394180 || DSID==393561 || DSID==393562 || DSID==393563 || DSID==393564 || DSID==393565 || DSID==393566 || DSID==393567 || DSID==394184 || DSID==393568 || DSID==393569 || DSID==393570 || DSID==393571 || DSID==393572 || DSID==393573 || DSID==393574 || DSID==394188 || DSID==393575 || DSID==393576 || DSID==393577 || DSID==393578 || DSID==393579 || DSID==393580 || DSID==393581 || DSID==394192 || DSID==393582 || DSID==393583 || DSID==393584 || DSID==393585 || DSID==393586 || DSID==393587 || DSID==393588 || DSID==394196 || DSID==393589 || DSID==393590 || DSID==393591 || DSID==393592 || DSID==393593 || DSID==393594 || DSID==393595){
    isC1C1 = true;
  } else {
    isC1C1 = false;
  }
  return isC1C1;
}
// ------------------------------------------------------------------------------------------ //
bool Common::isHiggsinoN2N1(int DSID){
  bool isN2N1 = false;
  if (DSID==393598 || DSID==393498 || DSID==393499 || DSID==393500 || DSID==393501 || DSID==393502 || DSID==393503 || DSID==393504 || DSID==394175 || DSID==393505 || DSID==393506 || DSID==393507 || DSID==393508 || DSID==393509 || DSID==393510 || DSID==393511 || DSID==394179 || DSID==393512 || DSID==393513 || DSID==393514 || DSID==393515 || DSID==393516 || DSID==393517 || DSID==393518 || DSID==394183 || DSID==393519 || DSID==393520 || DSID==393521 || DSID==393522 || DSID==393523 || DSID==393524 || DSID==393525 || DSID==394187 || DSID==393526 || DSID==393527 || DSID==393528 || DSID==393529 || DSID==393530 || DSID==393531 || DSID==393532 || DSID==394191 || DSID==393533 || DSID==393534 || DSID==393535 || DSID==393536 || DSID==393537 || DSID==393538 || DSID==393539 || DSID==394195 || DSID==393540 || DSID==393541 || DSID==393542 || DSID==393543 || DSID==393544 || DSID==393545 || DSID==393546){
    isN2N1 = true;
  } else{
    isN2N1 = false;
  }
  return isN2N1;
}
// ------------------------------------------------------------------------------------------ //
double Common::getWinoBinoFuncMllDistr(double x, double *par){
  // See https://its.cern.ch/jira/browse/HIGGSINO-27:
  // taken from https://arxiv.org/abs/0704.2515
  // equation 4, page 9
  // NB there is a typo in the formula (just in writing it in the the paper, not in the work)
  // the correct formula is in this code

  double m1=par[1];
  double m2=par[2];
  double mu=m2-m1;
  double m=x;
  double M=m1+m2;
  double m_Z=91;
  double norm=par[0]*m;

  double radice=sqrt( pow(m,4) -pow(m,2)*(pow(mu,2) + pow(M,2) ) + pow(mu*M,2) );

  double normalizzazione = pow(  pow(m,2) -pow(m_Z,2),2);
  double var = (norm* radice)/ normalizzazione  ;
  var = var* (-2*pow(m,4)+ pow(m,2)*(2*pow(M,2) - pow(mu,2)) +pow((mu*M),2));
  if(x > fabs(m2) - fabs(m1)) var=1;
  return var;
}
// ------------------------------------------------------------------------------------------ //
double Common::getWinoBinoMllWeight(int DSID, double mass){
  // See https://its.cern.ch/jira/browse/HIGGSINO-27:

  // Don't calculate a weight unless it is N2C1 Higgsino
  // if( !isHiggsinoN2C1p(DSID) && !isHiggsinoN2C1m(DSID) ) {
  //   return 1.;
  // }

  // if (getHiggsinoDM(DSID) == 100){ // no reweighting for on-shell Z
  //   return 1.;
  // }

  double winoBinoMllWeight = 1.0;
  std::vector<double> parHvec;
  parHvec = {0.0, 0.0, 0.0};

  if (DSID == 377400) parHvec = {3.00007, 88.5, 90};
  else if (DSID == 377401) parHvec = {3.00012, 88, 90};
  else if (DSID == 377402) parHvec = {3.00026, 87, 90};
  else if (DSID == 377403) parHvec = {3.00066, 85, 90};
  else if (DSID == 377404) parHvec = {3.00011, 98.5, 100};
  else if (DSID == 377405) parHvec = {3.0002, 98, 100};
  else if (DSID == 377406) parHvec = {3.00045, 97, 100};
  else if (DSID == 377407) parHvec = {3.0012, 95, 100};
  else if (DSID == 377408) parHvec = {3.00437, 90, 100};
  else if (DSID == 377409) parHvec = {3.00884, 85, 100};
  else if (DSID == 377410) parHvec = {3.01873, 75, 100};
  else if (DSID == 377411) parHvec = {3.02204, 60, 100};
  else if (DSID == 377412) parHvec = {2.94836, 40, 100};
  else if (DSID == 377413) parHvec = {3.00019, 123.5, 125};
  else if (DSID == 377414) parHvec = {3.00033, 123, 125};
  else if (DSID == 377415) parHvec = {3.00074, 122, 125};
  else if (DSID == 377416) parHvec = {3.00203, 120, 125};
  else if (DSID == 377417) parHvec = {3.00793, 115, 125};
  else if (DSID == 377418) parHvec = {3.01751, 110, 125};
  else if (DSID == 377419) parHvec = {3.04726, 100, 125};
  else if (DSID == 377420) parHvec = {3.11928, 85, 125};
  else if (DSID == 377421) parHvec = {3.29273, 65, 125};
  else if (DSID == 377422) parHvec = {3.00022, 148.5, 150};
  else if (DSID == 377423) parHvec = {3.0004, 148, 150};
  else if (DSID == 377424) parHvec = {3.00089, 147, 150};
  else if (DSID == 377425) parHvec = {3.00247, 145, 150};
  else if (DSID == 377426) parHvec = {3.00982, 140, 150};
  else if (DSID == 377427) parHvec = {3.02205, 135, 150};
  else if (DSID == 377428) parHvec = {3.06182, 125, 150};
  else if (DSID == 377429) parHvec = {3.1673, 110, 150};
  else if (DSID == 377430) parHvec = {3.45895, 90, 150};
  else if (DSID == 377431) parHvec = {3.00044, 173, 175};
  else if (DSID == 377432) parHvec = {3.00099, 172, 175};
  else if (DSID == 377433) parHvec = {3.00274, 170, 175};
  else if (DSID == 377434) parHvec = {3.01094, 165, 175};
  else if (DSID == 377435) parHvec = {3.02471, 160, 175};
  else if (DSID == 377436) parHvec = {3.07023, 150, 175};
  else if (DSID == 377437) parHvec = {3.19438, 135, 175};
  else if (DSID == 377438) parHvec = {3.55058, 115, 175};
  else if (DSID == 377439) parHvec = {3.00047, 198, 200};
  else if (DSID == 377440) parHvec = {3.00105, 197, 200};
  else if (DSID == 377441) parHvec = {3.00291, 195, 200};
  else if (DSID == 377442) parHvec = {3.01166, 190, 200};
  else if (DSID == 377443) parHvec = {3.02641, 185, 200};
  else if (DSID == 377444) parHvec = {3.07552, 175, 200};
  else if (DSID == 377445) parHvec = {3.21111, 160, 200};
  else if (DSID == 377446) parHvec = {3.6061, 140, 200};
  else if (DSID == 377447) parHvec = {3.00048, 223, 225};
  else if (DSID == 377448) parHvec = {3.00109, 222, 225};
  else if (DSID == 377449) parHvec = {3.00302, 220, 225};
  else if (DSID == 377450) parHvec = {3.01214, 215, 225};
  else if (DSID == 377451) parHvec = {3.02756, 210, 225};
  else if (DSID == 377452) parHvec = {3.07907, 200, 225};
  else if (DSID == 377453) parHvec = {3.22216, 185, 225};
  else if (DSID == 377454) parHvec = {3.64219, 165, 225};
  else if (DSID == 377455) parHvec = {3.00112, 247, 250};
  else if (DSID == 377456) parHvec = {3.00311, 245, 250};
  else if (DSID == 377457) parHvec = {3.01249, 240, 250};
  else if (DSID == 377458) parHvec = {3.02837, 235, 250};
  else if (DSID == 377459) parHvec = {3.08155, 225, 250};
  else if (DSID == 377460) parHvec = {3.22983, 210, 250};
  else if (DSID == 377461) parHvec = {3.00114, 272, 275};
  else if (DSID == 377462) parHvec = {3.00317, 270, 275};
  else if (DSID == 377463) parHvec = {3.01274, 265, 275};
  else if (DSID == 377464) parHvec = {3.02897, 260, 275};
  else if (DSID == 377465) parHvec = {3.08336, 250, 275};
  else if (DSID == 377466) parHvec = {3.23537, 235, 275};
  else if (DSID == 377467) parHvec = {3.00322, 295, 300};
  else if (DSID == 377468) parHvec = {3.01294, 290, 300};
  else if (DSID == 377469) parHvec = {3.02941, 285, 300};
  else if (DSID == 377470) parHvec = {3.08472, 275, 300};
  else if (DSID == 377471) parHvec = {3.00325, 320, 325};
  else if (DSID == 377472) parHvec = {3.01309, 315, 325};
  else if (DSID == 377473) parHvec = {3.02976, 310, 325};
  else if (DSID == 377474) parHvec = {3.08577, 300, 325};
  else if (DSID == 377475) parHvec = {3.00007, 88.5, 90};
  else if (DSID == 377476) parHvec = {3.00012, 88, 90};
  else if (DSID == 377477) parHvec = {3.00026, 87, 90};
  else if (DSID == 377478) parHvec = {3.00066, 85, 90};
  else if (DSID == 377479) parHvec = {3.00011, 98.5, 100};
  else if (DSID == 377480) parHvec = {3.0002, 98, 100};
  else if (DSID == 377481) parHvec = {3.00045, 97, 100};
  else if (DSID == 377482) parHvec = {3.0012, 95, 100};
  else if (DSID == 377483) parHvec = {3.00437, 90, 100};
  else if (DSID == 377484) parHvec = {3.00884, 85, 100};
  else if (DSID == 377485) parHvec = {3.01873, 75, 100};
  else if (DSID == 377486) parHvec = {3.02204, 60, 100};
  else if (DSID == 377487) parHvec = {2.94836, 40, 100};
  else if (DSID == 377488) parHvec = {3.00019, 123.5, 125};
  else if (DSID == 377489) parHvec = {3.00033, 123, 125};
  else if (DSID == 377490) parHvec = {3.00074, 122, 125};
  else if (DSID == 377491) parHvec = {3.00203, 120, 125};
  else if (DSID == 377492) parHvec = {3.00793, 115, 125};
  else if (DSID == 377493) parHvec = {3.01751, 110, 125};
  else if (DSID == 377494) parHvec = {3.04726, 100, 125};
  else if (DSID == 377495) parHvec = {3.11928, 85, 125};
  else if (DSID == 377496) parHvec = {3.29273, 65, 125};
  else if (DSID == 377497) parHvec = {3.00022, 148.5, 150};
  else if (DSID == 377498) parHvec = {3.0004, 148, 150};
  else if (DSID == 377499) parHvec = {3.00089, 147, 150};
  else if (DSID == 377500) parHvec = {3.00247, 145, 150};
  else if (DSID == 377501) parHvec = {3.00982, 140, 150};
  else if (DSID == 377502) parHvec = {3.02205, 135, 150};
  else if (DSID == 377503) parHvec = {3.06182, 125, 150};
  else if (DSID == 377504) parHvec = {3.1673, 110, 150};
  else if (DSID == 377505) parHvec = {3.45895, 90, 150};
  else if (DSID == 377506) parHvec = {3.00044, 173, 175};
  else if (DSID == 377507) parHvec = {3.00099, 172, 175};
  else if (DSID == 377508) parHvec = {3.00274, 170, 175};
  else if (DSID == 377509) parHvec = {3.01094, 165, 175};
  else if (DSID == 377510) parHvec = {3.02471, 160, 175};
  else if (DSID == 377511) parHvec = {3.07023, 150, 175};
  else if (DSID == 377512) parHvec = {3.19438, 135, 175};
  else if (DSID == 377513) parHvec = {3.55058, 115, 175};
  else if (DSID == 377514) parHvec = {3.00047, 198, 200};
  else if (DSID == 377515) parHvec = {3.00105, 197, 200};
  else if (DSID == 377516) parHvec = {3.00291, 195, 200};
  else if (DSID == 377517) parHvec = {3.01166, 190, 200};
  else if (DSID == 377518) parHvec = {3.02641, 185, 200};
  else if (DSID == 377519) parHvec = {3.07552, 175, 200};
  else if (DSID == 377520) parHvec = {3.21111, 160, 200};
  else if (DSID == 377521) parHvec = {3.6061, 140, 200};
  else if (DSID == 377522) parHvec = {3.00048, 223, 225};
  else if (DSID == 377523) parHvec = {3.00109, 222, 225};
  else if (DSID == 377524) parHvec = {3.00302, 220, 225};
  else if (DSID == 377525) parHvec = {3.01214, 215, 225};
  else if (DSID == 377526) parHvec = {3.02756, 210, 225};
  else if (DSID == 377527) parHvec = {3.07907, 200, 225};
  else if (DSID == 377528) parHvec = {3.22216, 185, 225};
  else if (DSID == 377529) parHvec = {3.64219, 165, 225};
  else if (DSID == 377530) parHvec = {3.00112, 247, 250};
  else if (DSID == 377531) parHvec = {3.00311, 245, 250};
  else if (DSID == 377532) parHvec = {3.01249, 240, 250};
  else if (DSID == 377533) parHvec = {3.02837, 235, 250};
  else if (DSID == 377534) parHvec = {3.08155, 225, 250};
  else if (DSID == 377535) parHvec = {3.22983, 210, 250};
  else if (DSID == 377536) parHvec = {3.00114, 272, 275};
  else if (DSID == 377537) parHvec = {3.00317, 270, 275};
  else if (DSID == 377538) parHvec = {3.01274, 265, 275};
  else if (DSID == 377539) parHvec = {3.02897, 260, 275};
  else if (DSID == 377540) parHvec = {3.08336, 250, 275};
  else if (DSID == 377541) parHvec = {3.23537, 235, 275};
  else if (DSID == 377542) parHvec = {3.00322, 295, 300};
  else if (DSID == 377543) parHvec = {3.01294, 290, 300};
  else if (DSID == 377544) parHvec = {3.02941, 285, 300};
  else if (DSID == 377545) parHvec = {3.08472, 275, 300};
  else if (DSID == 377546) parHvec = {3.00325, 320, 325};
  else if (DSID == 377547) parHvec = {3.01309, 315, 325};
  else if (DSID == 377548) parHvec = {3.02976, 310, 325};
  else if (DSID == 377549) parHvec = {3.08577, 300, 325};
  else if (DSID == 377550) parHvec = {3.00007, 88.5, 90};
  else if (DSID == 377551) parHvec = {3.00012, 88, 90};
  else if (DSID == 377552) parHvec = {3.00026, 87, 90};
  else if (DSID == 377553) parHvec = {3.00066, 85, 90};
  else if (DSID == 377554) parHvec = {3.00011, 98.5, 100};
  else if (DSID == 377555) parHvec = {3.0002, 98, 100};
  else if (DSID == 377556) parHvec = {3.00045, 97, 100};
  else if (DSID == 377557) parHvec = {3.0012, 95, 100};
  else if (DSID == 377558) parHvec = {3.00437, 90, 100};
  else if (DSID == 377559) parHvec = {3.00884, 85, 100};
  else if (DSID == 377560) parHvec = {3.01873, 75, 100};
  else if (DSID == 377561) parHvec = {3.02204, 60, 100};
  else if (DSID == 377562) parHvec = {3.00019, 123.5, 125};
  else if (DSID == 377563) parHvec = {3.00033, 123, 125};
  else if (DSID == 377564) parHvec = {3.00074, 122, 125};
  else if (DSID == 377565) parHvec = {3.00203, 120, 125};
  else if (DSID == 377566) parHvec = {3.00793, 115, 125};
  else if (DSID == 377567) parHvec = {3.01751, 110, 125};
  else if (DSID == 377568) parHvec = {3.04726, 100, 125};
  else if (DSID == 377569) parHvec = {3.11928, 85, 125};
  else if (DSID == 377570) parHvec = {3.00022, 148.5, 150};
  else if (DSID == 377571) parHvec = {3.0004, 148, 150};
  else if (DSID == 377572) parHvec = {3.00089, 147, 150};
  else if (DSID == 377573) parHvec = {3.00247, 145, 150};
  else if (DSID == 377574) parHvec = {3.00982, 140, 150};
  else if (DSID == 377575) parHvec = {3.02205, 135, 150};
  else if (DSID == 377576) parHvec = {3.06182, 125, 150};
  else if (DSID == 377577) parHvec = {3.1673, 110, 150};
  else if (DSID == 377578) parHvec = {3.06743, 95, 125};
  else if (DSID == 377579) parHvec = {3.09132, 90, 125};
  else if (DSID == 377580) parHvec = {3.09013, 120, 150};
  else if (DSID == 377581) parHvec = {3.12493, 115, 150};
  else if (DSID == 377582) parHvec = {3.10313, 145, 175};
  else if (DSID == 377583) parHvec = {3.14403, 140, 175};
  else if (DSID == 377584) parHvec = {3.11126, 170, 200};
  else if (DSID == 377585) parHvec = {3.1559, 165, 200};
  else if (DSID == 377586) parHvec = {3.06743, 95, 125};
  else if (DSID == 377587) parHvec = {3.09132, 90, 125};
  else if (DSID == 377588) parHvec = {3.09013, 120, 150};
  else if (DSID == 377589) parHvec = {3.12493, 115, 150};
  else if (DSID == 377590) parHvec = {3.10313, 145, 175};
  else if (DSID == 377591) parHvec = {3.14403, 140, 175};
  else if (DSID == 377592) parHvec = {3.11126, 170, 200};
  else if (DSID == 377593) parHvec = {3.1559, 165, 200};
  else return winoBinoMllWeight; //return 1 for other cases

  double parH[3] = {           1., parHvec.at(1),    parHvec.at(2)};
  double parS[3] = {parHvec.at(0), parHvec.at(1), -1*parHvec.at(2)};

  double hig_w = getWinoBinoFuncMllDistr(mass,parH);
  double stop_w = getWinoBinoFuncMllDistr(mass,parS);
  winoBinoMllWeight = stop_w/hig_w;
  // std::cout << "(mll, hig_w, stop_w, winoBinoMllWeight) = ("
  // 	    << mass << ", "
  // 	    << hig_w << ", "
  // 	    << stop_w << ", "
  // 	    << winoBinoMllWeight
  // 	    << ") " << std::endl;

  return winoBinoMllWeight;
}

// ------------------------------------------------------------------------------------------ //
double Common::getWinoBinoXsecWeight(int DSID){
  // See https://its.cern.ch/jira/browse/HIGGSINO-27: simply ratio of Higgsino and WinoBino xsec

  double winoBinoXsecWeight = 1.0;

  if (isHiggsinoN2C1p(DSID) || isHiggsinoN2C1m(DSID)){
    if (DSID == 393596) { //N2C1p_82_80
      winoBinoXsecWeight *= 3.89;
    } else if (DSID == 393400) { //N2C1p_83_80
      winoBinoXsecWeight *= 3.85;
    } else if (DSID == 393401) { //N2C1p_85_80
      winoBinoXsecWeight *= 3.79;
    } else if (DSID == 393402) { //N2C1p_90_80
      winoBinoXsecWeight *= 3.64;
    } else if (DSID == 393403) { //N2C1p_100_80
      winoBinoXsecWeight *= 3.36;
    } else if (DSID == 393404) { //N2C1p_120_80
      winoBinoXsecWeight *= 2.98;
    } else if (DSID == 393405) { //N2C1p_140_80
      winoBinoXsecWeight *= 2.73;
    } else if (DSID == 393406) { //N2C1p_180_80
      winoBinoXsecWeight *= 2.42;
    } else if (DSID == 394173) { //N2C1p_102_100
      winoBinoXsecWeight *= 3.97;
    } else if (DSID == 393407) { //N2C1p_103_100
      winoBinoXsecWeight *= 3.93;
    } else if (DSID == 393408) { //N2C1p_105_100
      winoBinoXsecWeight *= 3.87;
    } else if (DSID == 393409) { //N2C1p_110_100
      winoBinoXsecWeight *= 3.72;
    } else if (DSID == 393410) { //N2C1p_120_100
      winoBinoXsecWeight *= 3.48;
    } else if (DSID == 393411) { //N2C1p_140_100
      winoBinoXsecWeight *= 3.12;
    } else if (DSID == 393412) { //N2C1p_160_100
      winoBinoXsecWeight *= 2.87;
    } else if (DSID == 393413) { //N2C1p_200_100
      winoBinoXsecWeight *= 2.54;
    } else if (DSID == 394177) { //N2C1p_152_150
      winoBinoXsecWeight *= 3.99;
    } else if (DSID == 393414) { //N2C1p_153_150
      winoBinoXsecWeight *= 3.96;
    } else if (DSID == 393415) { //N2C1p_155_150
      winoBinoXsecWeight *= 3.92;
    } else if (DSID == 393416) { //N2C1p_160_150
      winoBinoXsecWeight *= 3.81;
    } else if (DSID == 393417) { //N2C1p_170_150
      winoBinoXsecWeight *= 3.63;
    } else if (DSID == 393418) { //N2C1p_190_150
      winoBinoXsecWeight *= 3.33;
    } else if (DSID == 393419) { //N2C1p_210_150
      winoBinoXsecWeight *= 3.10;
    } else if (DSID == 393420) { //N2C1p_250_150
      winoBinoXsecWeight *= 2.76;
    } else if (DSID == 394181) { //N2C1p_202_200
      winoBinoXsecWeight *= 3.99;
    } else if (DSID == 393421) { //N2C1p_203_200
      winoBinoXsecWeight *= 3.97;
    } else if (DSID == 393422) { //N2C1p_205_200
      winoBinoXsecWeight *= 3.94;
    } else if (DSID == 393423) { //N2C1p_210_200
      winoBinoXsecWeight *= 3.86;
    } else if (DSID == 393424) { //N2C1p_220_200
      winoBinoXsecWeight *= 3.71;
    } else if (DSID == 393425) { //N2C1p_240_200
      winoBinoXsecWeight *= 3.45;
    } else if (DSID == 393426) { //N2C1p_260_200
      winoBinoXsecWeight *= 3.24;
    } else if (DSID == 393427) { //N2C1p_300_200
      winoBinoXsecWeight *= 2.91;
    } else if (DSID == 394185) { //N2C1p_252_250
      winoBinoXsecWeight *= 4.00;
    } else if (DSID == 393428) { //N2C1p_253_250
      winoBinoXsecWeight *= 3.98;
    } else if (DSID == 393429) { //N2C1p_255_250
      winoBinoXsecWeight *= 3.95;
    } else if (DSID == 393430) { //N2C1p_260_250
      winoBinoXsecWeight *= 3.88;
    } else if (DSID == 393431) { //N2C1p_270_250
      winoBinoXsecWeight *= 3.75;
    } else if (DSID == 393432) { //N2C1p_290_250
      winoBinoXsecWeight *= 3.53;
    } else if (DSID == 393433) { //N2C1p_310_250
      winoBinoXsecWeight *= 3.33;
    } else if (DSID == 393434) { //N2C1p_350_250
      winoBinoXsecWeight *= 3.03;
    } else if (DSID == 394189) { //N2C1p_302_300
      winoBinoXsecWeight *= 4.00;
    } else if (DSID == 393435) { //N2C1p_303_300
      winoBinoXsecWeight *= 3.98;
    } else if (DSID == 393436) { //N2C1p_305_300
      winoBinoXsecWeight *= 3.96;
    } else if (DSID == 393437) { //N2C1p_310_300
      winoBinoXsecWeight *= 3.90;
    } else if (DSID == 393438) { //N2C1p_320_300
      winoBinoXsecWeight *= 3.79;
    } else if (DSID == 393439) { //N2C1p_340_300
      winoBinoXsecWeight *= 3.58;
    } else if (DSID == 393440) { //N2C1p_360_300
      winoBinoXsecWeight *= 3.41;
    } else if (DSID == 393441) { //N2C1p_400_300
      winoBinoXsecWeight *= 3.11;
    } else if (DSID == 394193) { //N2C1p_402_400
      winoBinoXsecWeight *= 4.00;
    } else if (DSID == 393442) { //N2C1p_403_400
      winoBinoXsecWeight *= 3.99;
    } else if (DSID == 393443) { //N2C1p_405_400
      winoBinoXsecWeight *= 3.97;
    } else if (DSID == 393444) { //N2C1p_410_400
      winoBinoXsecWeight *= 3.92;
    } else if (DSID == 393445) { //N2C1p_420_400
      winoBinoXsecWeight *= 3.83;
    } else if (DSID == 393446) { //N2C1p_440_400
      winoBinoXsecWeight *= 3.66;
    } else if (DSID == 393447) { //N2C1p_460_400
      winoBinoXsecWeight *= 3.50;
    } else if (DSID == 393448) { //N2C1p_500_400
      winoBinoXsecWeight *= 3.23;
    } else if (DSID == 393597) { //N2C1m_82_80
      winoBinoXsecWeight *= 3.98;
    } else if (DSID == 393449) { //N2C1m_83_80
      winoBinoXsecWeight *= 3.94;
    } else if (DSID == 393450) { //N2C1m_85_80
      winoBinoXsecWeight *= 3.86;
    } else if (DSID == 393451) { //N2C1m_90_80
      winoBinoXsecWeight *= 3.69;
    } else if (DSID == 393452) { //N2C1m_100_80
      winoBinoXsecWeight *= 3.34;
    } else if (DSID == 393453) { //N2C1m_120_80
      winoBinoXsecWeight *= 2.94;
    } else if (DSID == 393454) { //N2C1m_140_80
      winoBinoXsecWeight *= 2.68;
    } else if (DSID == 393455) { //N2C1m_180_80
      winoBinoXsecWeight *= 2.35;
    } else if (DSID == 394174) { //N2C1m_102_100
      winoBinoXsecWeight *= 3.98;
    } else if (DSID == 393456) { //N2C1m_103_100
      winoBinoXsecWeight *= 3.94;
    } else if (DSID == 393457) { //N2C1m_105_100
      winoBinoXsecWeight *= 3.88;
    } else if (DSID == 393458) { //N2C1m_110_100
      winoBinoXsecWeight *= 3.72;
    } else if (DSID == 393459) { //N2C1m_120_100
      winoBinoXsecWeight *= 3.46;
    } else if (DSID == 393460) { //N2C1m_140_100
      winoBinoXsecWeight *= 3.08;
    } else if (DSID == 393461) { //N2C1m_160_100
      winoBinoXsecWeight *= 2.82;
    } else if (DSID == 393462) { //N2C1m_200_100
      winoBinoXsecWeight *= 2.47;
    } else if (DSID == 394178) { //N2C1m_152_150
      winoBinoXsecWeight *= 4.00;
    } else if (DSID == 393463) { //N2C1m_153_150
      winoBinoXsecWeight *= 3.97;
    } else if (DSID == 393464) { //N2C1m_155_150
      winoBinoXsecWeight *= 3.93;
    } else if (DSID == 393465) { //N2C1m_160_150
      winoBinoXsecWeight *= 3.81;
    } else if (DSID == 393466) { //N2C1m_170_150
      winoBinoXsecWeight *= 3.62;
    } else if (DSID == 393467) { //N2C1m_190_150
      winoBinoXsecWeight *= 3.30;
    } else if (DSID == 393468) { //N2C1m_210_150
      winoBinoXsecWeight *= 3.05;
    } else if (DSID == 393469) { //N2C1m_250_150
      winoBinoXsecWeight *= 2.69;
    } else if (DSID == 394182) { //N2C1m_202_200
      winoBinoXsecWeight *= 4.01;
    } else if (DSID == 393470) { //N2C1m_203_200
      winoBinoXsecWeight *= 3.99;
    } else if (DSID == 393471) { //N2C1m_205_200
      winoBinoXsecWeight *= 3.95;
    } else if (DSID == 393472) { //N2C1m_210_200
      winoBinoXsecWeight *= 3.86;
    } else if (DSID == 393473) { //N2C1m_220_200
      winoBinoXsecWeight *= 3.70;
    } else if (DSID == 393474) { //N2C1m_240_200
      winoBinoXsecWeight *= 3.42;
    } else if (DSID == 393475) { //N2C1m_260_200
      winoBinoXsecWeight *= 3.20;
    } else if (DSID == 393476) { //N2C1m_300_200
      winoBinoXsecWeight *= 2.85;
    } else if (DSID == 394186) { //N2C1m_252_250
      winoBinoXsecWeight *= 4.01;
    } else if (DSID == 393477) { //N2C1m_253_250
      winoBinoXsecWeight *= 3.99;
    } else if (DSID == 393478) { //N2C1m_255_250
      winoBinoXsecWeight *= 3.96;
    } else if (DSID == 393479) { //N2C1m_260_250
      winoBinoXsecWeight *= 3.89;
    } else if (DSID == 393480) { //N2C1m_270_250
      winoBinoXsecWeight *= 3.75;
    } else if (DSID == 393481) { //N2C1m_290_250
      winoBinoXsecWeight *= 3.50;
    } else if (DSID == 393482) { //N2C1m_310_250
      winoBinoXsecWeight *= 3.30;
    } else if (DSID == 393483) { //N2C1m_350_250
      winoBinoXsecWeight *= 2.97;
    } else if (DSID == 394190) { //N2C1m_302_300
      winoBinoXsecWeight *= 4.01;
    } else if (DSID == 393484) { //N2C1m_303_300
      winoBinoXsecWeight *= 4.00;
    } else if (DSID == 393485) { //N2C1m_305_300
      winoBinoXsecWeight *= 3.97;
    } else if (DSID == 393486) { //N2C1m_310_300
      winoBinoXsecWeight *= 3.90;
    } else if (DSID == 393487) { //N2C1m_320_300
      winoBinoXsecWeight *= 3.78;
    } else if (DSID == 393488) { //N2C1m_340_300
      winoBinoXsecWeight *= 3.56;
    } else if (DSID == 393489) { //N2C1m_360_300
      winoBinoXsecWeight *= 3.37;
    } else if (DSID == 393490) { //N2C1m_400_300
      winoBinoXsecWeight *= 3.05;
    } else if (DSID == 394194) { //N2C1m_402_400
      winoBinoXsecWeight *= 4.01;
    } else if (DSID == 393491) { //N2C1m_403_400
      winoBinoXsecWeight *= 4.00;
    } else if (DSID == 393492) { //N2C1m_405_400
      winoBinoXsecWeight *= 3.98;
    } else if (DSID == 393493) { //N2C1m_410_400
      winoBinoXsecWeight *= 3.92;
    } else if (DSID == 393494) { //N2C1m_420_400
      winoBinoXsecWeight *= 3.82;
    } else if (DSID == 393495) { //N2C1m_440_400
      winoBinoXsecWeight *= 3.63;
    } else if (DSID == 393496) { //N2C1m_460_400
      winoBinoXsecWeight *= 3.47;
    } else if (DSID == 393497) { //N2C1m_500_400
      winoBinoXsecWeight *= 3.18;
    }
  } else if (isHiggsinoC1C1(DSID) || isHiggsinoN2N1(DSID)) {
    winoBinoXsecWeight *= 0.0;
  } else {
    winoBinoXsecWeight *= 1.0;
  }

  return winoBinoXsecWeight;
}
// ------------------------------------------------------------------------------------------ //
double Common::getWinoBinoBrFracWeight(int DSID){
  // See https://its.cern.ch/jira/browse/HIGGSINO-27:
  // unclear if there is a difference here or not

  double winoBinoBrFracWeight = 1.0;

  if (getHiggsinoDM(DSID) == 3 || getHiggsinoDM(DSID) == 2){ //fixme
    winoBinoBrFracWeight *= (0.065005/0.099413);
  } else if (getHiggsinoDM(DSID) == 5){
    winoBinoBrFracWeight *= (0.064803/0.098670);
  } else if (getHiggsinoDM(DSID) == 10){
    winoBinoBrFracWeight *= (0.075414/0.112346);
  } else if (getHiggsinoDM(DSID) == 20){
    winoBinoBrFracWeight *= (0.073124/0.109839);
  } else if (getHiggsinoDM(DSID) == 40){
    winoBinoBrFracWeight *= (0.072993/0.105571);
  } else if (getHiggsinoDM(DSID) == 60){
    winoBinoBrFracWeight *= (0.076728/0.104532);
  } else if (getHiggsinoDM(DSID) == 100){
    winoBinoBrFracWeight *= (0.100974/0.100974);
  } else {
    winoBinoBrFracWeight *= 1.0;
  }

  return winoBinoBrFracWeight;
}

/////////// Helper funciton for calculating helicity angle /////////////
float costh(const TLorentzVector &a, const TLorentzVector &b){
  return a.Vect().Unit() * b.Vect().Unit();
}
TLorentzVector BoostBack_parent_RF(const TLorentzVector &child_lab, const TLorentzVector &parent_lab){
  // output TLorentzVector for child in the parent rest frame
  TLorentzVector out = child_lab;    out.Boost(-parent_lab.BoostVector());
  return out;
}
float costhStar(const TLorentzVector &child_lab, const TLorentzVector &parent_lab){
  if(!(child_lab.E()>0 && parent_lab.E()>0)) return -9.;

  TLorentzVector child_parent = BoostBack_parent_RF(child_lab, parent_lab);
  return costh(child_parent, parent_lab);
}
////////////////////////////////////////////////////////////////////////
