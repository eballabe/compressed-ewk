#include "SusySkimHiggsino/Common.h"
#include "SusySkimMaker/Objects.h"
#include "PathResolver/PathResolver.h"

#include <iostream>
#include <fstream>
#include <algorithm>


//////////////////////////////////////////////////////////////////
void Common::addLeptonVariables(const ConfigMgr* configMgr, TString prefix){
  /*
  configMgr->treeMaker->addFloatVariable(prefix+"pt_ID",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"eta_ID",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"phi_ID",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"pt_ME",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"eta_ME",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"phi_ME",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"pt_MS",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"eta_MS",-99);
  configMgr->treeMaker->addFloatVariable(prefix+"phi_MS",-99);
  configMgr->treeMaker->addIntVariable(prefix+"isStaco",-99);
  configMgr->treeMaker->addIntVariable(prefix+"innerSmallHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"innerLargeHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"middleSmallHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"middleLargeHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"outerSmallHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"outerLargeHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"extendedSmallHits",-99);
  configMgr->treeMaker->addIntVariable(prefix+"extendedLargeHits",-99);
  */
  configMgr->treeMaker->addIntVariable(prefix+"Flavor",0);
  configMgr->treeMaker->addIntVariable(prefix+"Charge",0);
  configMgr->treeMaker->addIntVariable(prefix+"Author",-1);
  configMgr->treeMaker->addFloatVariable(prefix+"Pt",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Eta",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Phi",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"M",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"MT_Met",-1);
  configMgr->treeMaker->addFloatVariable(prefix+"MT_Met_LepInvis",-1);
  configMgr->treeMaker->addFloatVariable(prefix+"_DPhiMet",-1);
  configMgr->treeMaker->addFloatVariable(prefix+"_DPhiMet_LepInvis",-1);
  configMgr->treeMaker->addFloatVariable(prefix+"D0",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"D0Sig",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Z0",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Z0SinTheta",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Topoetcone20",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Topoetcone30",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Topoetcone40",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"SUSY20_Topoetcone20",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"SUSY20_Topoetcone30",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"SUSY20_Topoetcone40",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"SUSY20_Topoetclus20",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"SUSY20_Topoetclus30",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"SUSY20_Topoetclus40",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Ptvarcone20",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Ptvarcone30",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"Ptvarcone40",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrTopoetcone20",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrTopoetcone30",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrTopoetcone40",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPtvarcone20",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPtvarcone30",-999.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPtvarcone40",-999.0);
  configMgr->treeMaker->addBoolVariable(prefix+"PassOR",false);
  configMgr->treeMaker->addIntVariable(prefix+"IFFType",-1);
  configMgr->treeMaker->addIntVariable(prefix+"Type",-1);
  configMgr->treeMaker->addIntVariable(prefix+"Origin",-1);
  configMgr->treeMaker->addIntVariable(prefix+"EgMotherType",-1);
  configMgr->treeMaker->addIntVariable(prefix+"EgMotherOrigin",-1);
  configMgr->treeMaker->addIntVariable(prefix+"NPix",-1);
  configMgr->treeMaker->addBoolVariable(prefix+"PassBL",false);
  configMgr->treeMaker->addBoolVariable(prefix+"baseline",false);
  configMgr->treeMaker->addBoolVariable(prefix+"VeryLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"Loose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"Medium",false);
  configMgr->treeMaker->addBoolVariable(prefix+"Tight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"LowPtWP",false);
  configMgr->treeMaker->addBoolVariable(prefix+"LooseBLllh",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoHighPtCaloOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoHighPtTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"corr_IsoHighPtCaloOnly",false);

  //Old WPs
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCHighPtCaloOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCTightTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCLoose_FixedRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCTight_FixedRad",false);

  //New WPs
  configMgr->treeMaker->addBoolVariable(prefix+"IsoLoose_VarRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoLoose_FixedRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoTight_VarRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoTight_FixedRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoTightTrackOnly_VarRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoTightTrackOnly_FixedRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPflowLoose_VarRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPflowLoose_FixedRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPflowTight_VarRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPflowTight_FixedRad",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPLVLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPLVTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPLImprovedTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoPLImprovedVeryTight",false);

  /*
  configMgr->treeMaker->addBoolVariable(prefix+"IsoLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoGradient",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoGradientLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoLooseTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFixedCutLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFixedCutTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFixedCutTightTrackOnly",false);
  // TTMVA Isolation WPs
  configMgr->treeMaker->addFloatVariable(prefix+"Ptvarcone30_TightTTVA_pt1000",false);
  configMgr->treeMaker->addFloatVariable(prefix+"Ptvarcone30_TightTTVA_pt500",false);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPtvarcone30_TightTTVA_pt1000",false);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPtvarcone30_TightTTVA_pt500",false);
  configMgr->treeMaker->addBoolVariable(prefix+"neflowisol20",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFixedCutHighPtTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoFCHighPtCaloOnly",false);
  // Corr
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrGradient",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrGradientLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrLooseTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFixedCutLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFixedCutTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFixedCutTightTrackOnly",false);
  // TTMVA Isolation WPs (Corr)
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFixedCutHighPtTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFCTightTrackOnly",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFCLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFCTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"IsoCorrFCHighPtCaloOnly",false);
  */
  configMgr->treeMaker->addBoolVariable(prefix+"Signal",false);
  configMgr->treeMaker->addBoolVariable(prefix+"SignalPLTLoose",false);
  configMgr->treeMaker->addBoolVariable(prefix+"SignalPLTTight",false);
  configMgr->treeMaker->addBoolVariable(prefix+"TruthMatched",false);
  configMgr->treeMaker->addIntVariable(prefix+"TruthCharge",0);
  configMgr->treeMaker->addFloatVariable(prefix+"TruthPt",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"TruthEta",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"TruthPhi",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"TruthM",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"TrackPt",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"TrackEta",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"TrackPhi",0.0);

  // Individual lepton weights
  configMgr->treeMaker->addDoubleVariable(prefix+"Weight",0.0);

  // Rel. 21 PromptLeptonTagger output
  configMgr->treeMaker->addFloatVariable(prefix+"PromptLepVeto_score",0.0);

  // Rel. 21 PromptLeptonTagger inputs (need to toggle on "WriteDetailedSkim" in the deep config in SusySkimMaker)
  configMgr->treeMaker->addFloatVariable(prefix+"PLTInput_TrackJetNTrack",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"PLTInput_DRlj",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"PLTInput_rnnip",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"PLTInput_DL1mu",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"PLTInput_PtRel",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"PLTInput_PtFrac",0.0);

  // Near-by-lepton corrected PromptLeptonTagger inputs
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPLTInput_TrackJetNTrack",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPLTInput_DRlj",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPLTInput_rnnip",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPLTInput_DL1mu",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPLTInput_PtRel",0.0);
  configMgr->treeMaker->addFloatVariable(prefix+"CorrPLTInput_PtFrac",0.0);

  // Nominal PromptLepVeto score and new Low-Pt PLT score with corrected input
  for(TString algName : {"CorrPromptLepVeto", "LowPtPLT", "LowPtPLT_Cone40"} ){
    configMgr->treeMaker->addFloatVariable(prefix+algName, 0.0);
  }

  // IsoPLT working points
  for(TString algName : {"LowPtPLT", "LowPtPLT_MediumLH"}){
    configMgr->treeMaker->addBoolVariable(prefix+"Iso"+algName+"_Loose",0.0);
    configMgr->treeMaker->addBoolVariable(prefix+"Iso"+algName+"_Tight",0.0);
  }

  configMgr->treeMaker->addBoolVariable(prefix+"EWKCombiBaseline",false);
  configMgr->treeMaker->addBoolVariable(prefix+"PassZCR",false);
  configMgr->treeMaker->addBoolVariable(prefix+"PassWCR",false);
}

//////////////////////////////////////////////////////////////////
void Common::setLeptonVariables(const ConfigMgr* configMgr, TString prefix, LeptonVariable* lep){

   int lepFlavor = 0;
   int lepEgMotherType = -1;
   int lepEgMotherOrigin = -1;
   int lepNPix = -1;
   bool lepPassBL = false;
   bool lepTruthMatched = false;
   bool passesLowPtWP = false;
   bool passLooseBLllh = false;

  if(lep->isEle()){
    const ElectronVariable* el = dynamic_cast<const ElectronVariable*>(lep);
    lepFlavor         = 1;
    lepPassBL         = el->passBL;
    lepNPix           = el->nPixHitsPlusDeadSensors;
    // use now type from IFFTruthClassifier for TruthMatched flag
    // lepTruthMatched   = el->type == 2 || (el->type == 4 && el->egMotherType == 2);
    lepTruthMatched   = el->iff_type == 2;
    lepEgMotherType   = el->egMotherType;
    lepEgMotherOrigin = el->egMotherOrigin;
    passLooseBLllh         = el->looseBLllh;
  }
  else if(lep->isMu()){
    const MuonVariable* mu = dynamic_cast<const MuonVariable*>(lep);
    lepFlavor = 2;
    // use now type from IFFTruthClassifier for TruthMatched flag
    // lepTruthMatched = mu->type == 6;
    lepTruthMatched = mu->iff_type == 4;
    passesLowPtWP = mu->passesLowPtWP();
  }

  // For data, set these to true so that data can be processed
  // with the same cuts as the MC
  if( !configMgr->obj->evt.isMC ){
    lepTruthMatched = true;
  }

  //Transverse Mass with Met
  TLorentzVector vec_met;
  TLorentzVector vec_lep;
  vec_met.SetPtEtaPhiM(configMgr->obj->met.Et,0.0, configMgr->obj->met.phi,0.0);
  vec_lep.SetPtEtaPhiM(lep->Pt(), lep->Eta(), lep->Phi(), lep->M());
  //const MetVariable* met_LepInvis = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP); //Sicong: TODO: need to understand what they do and get these back...
  //TLorentzVector vec_met_LepInvis;
  //vec_met_LepInvis.SetPtEtaPhiM(met_LepInvis->Et, 0, met_LepInvis->phi, 0);
  double MT_Met = getM_T(vec_met, vec_lep);
  //double MT_Met_LepInvis = getM_T(vec_met_LepInvis, vec_lep);

  /*
  // STEFANO
  configMgr->treeMaker->setFloatVariable(prefix+"pt_ID",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->pt_ID : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"eta_ID",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->eta_ID : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"phi_ID",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->phi_ID : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"pt_ME",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->pt_ME : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"eta_ME",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->eta_ME : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"phi_ME",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->phi_ME : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"pt_MS",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->pt_MS : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"eta_MS",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->eta_MS : -99));
  configMgr->treeMaker->setFloatVariable(prefix+"phi_MS",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->phi_MS : -99));
  configMgr->treeMaker->setIntVariable(prefix+"isStaco",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->isStaco : -99));
  configMgr->treeMaker->setIntVariable(prefix+"innerSmallHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->innerSmallHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"innerLargeHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->innerLargeHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"middleSmallHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->middleSmallHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"middleLargeHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->middleLargeHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"outerSmallHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->outerSmallHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"outerLargeHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->outerLargeHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"extendedSmallHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->extendedSmallHits : -99));
  configMgr->treeMaker->setIntVariable(prefix+"extendedLargeHits",(lep->isMu()? (dynamic_cast<const MuonVariable*>(lep))->extendedLargeHits : -99));
  */
  configMgr->treeMaker->setIntVariable(prefix+"Flavor",lepFlavor);
  configMgr->treeMaker->setIntVariable(prefix+"Charge",lep->q);
  configMgr->treeMaker->setIntVariable(prefix+"Author",lep->author);
  configMgr->treeMaker->setFloatVariable(prefix+"Pt",lep->Pt());
  configMgr->treeMaker->setFloatVariable(prefix+"Eta",lep->Eta());;
  configMgr->treeMaker->setFloatVariable(prefix+"Phi",lep->Phi());
  configMgr->treeMaker->setFloatVariable(prefix+"M",lep->M());
  configMgr->treeMaker->setFloatVariable(prefix+"MT_Met",MT_Met);
  //configMgr->treeMaker->setFloatVariable(prefix+"MT_Met_LepInvis",MT_Met_LepInvis);
  configMgr->treeMaker->setFloatVariable(prefix+"_DPhiMet",fabs(lep->DeltaPhi(configMgr->obj->met)));
  //configMgr->treeMaker->setFloatVariable(prefix+"_DPhiMet_LepInvis",fabs(lep->DeltaPhi(*configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP))));
  configMgr->treeMaker->setFloatVariable(prefix+"D0",lep->d0);
  configMgr->treeMaker->setFloatVariable(prefix+"D0Sig",fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0);
  configMgr->treeMaker->setFloatVariable(prefix+"Z0",lep->z0);
  configMgr->treeMaker->setFloatVariable(prefix+"Z0SinTheta",fabs(lep->z0 * TMath::Sin(lep->Theta())));
  configMgr->treeMaker->setFloatVariable(prefix+"Topoetcone20",lep->topoetcone20);
  configMgr->treeMaker->setFloatVariable(prefix+"Topoetcone30",lep->topoetcone30);
  configMgr->treeMaker->setFloatVariable(prefix+"Topoetcone40",lep->topoetcone40);
  configMgr->treeMaker->setFloatVariable(prefix+"SUSY20_Topoetcone20",lep->SUSY20_topoetcone20);
  configMgr->treeMaker->setFloatVariable(prefix+"SUSY20_Topoetcone30",lep->SUSY20_topoetcone30);
  configMgr->treeMaker->setFloatVariable(prefix+"SUSY20_Topoetcone40",lep->SUSY20_topoetcone40);
  configMgr->treeMaker->setFloatVariable(prefix+"SUSY20_Topoetclus20",lep->SUSY20_topoetcone20NonCoreCone);
  configMgr->treeMaker->setFloatVariable(prefix+"SUSY20_Topoetclus30",lep->SUSY20_topoetcone30NonCoreCone);
  configMgr->treeMaker->setFloatVariable(prefix+"SUSY20_Topoetclus40",lep->SUSY20_topoetcone40NonCoreCone);
  configMgr->treeMaker->setFloatVariable(prefix+"Ptvarcone20",lep->ptvarcone20);
  configMgr->treeMaker->setFloatVariable(prefix+"Ptvarcone30",lep->ptvarcone30);
  configMgr->treeMaker->setFloatVariable(prefix+"Ptvarcone40",lep->ptvarcone40);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrTopoetcone20",lep->corr_topoetcone20);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrTopoetcone30",lep->corr_topoetcone30);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrTopoetcone40",lep->corr_topoetcone40);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrPtvarcone20",lep->corr_ptvarcone20);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrPtvarcone30",lep->corr_ptvarcone30);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrPtvarcone40",lep->corr_ptvarcone40);
  configMgr->treeMaker->setBoolVariable(prefix+"PassOR",lep->passOR);
  configMgr->treeMaker->setIntVariable(prefix+"IFFType",lep->iff_type);
  configMgr->treeMaker->setIntVariable(prefix+"Type",lep->type);
  configMgr->treeMaker->setIntVariable(prefix+"Origin",lep->origin);
  configMgr->treeMaker->setIntVariable(prefix+"EgMotherType",lepEgMotherType);
  configMgr->treeMaker->setIntVariable(prefix+"EgMotherOrigin",lepEgMotherOrigin);
  configMgr->treeMaker->setIntVariable(prefix+"NPix",lepNPix);
  configMgr->treeMaker->setBoolVariable(prefix+"PassBL",lepPassBL);
  configMgr->treeMaker->setBoolVariable(prefix+"baseline",lep->baseline);
  configMgr->treeMaker->setBoolVariable(prefix+"VeryLoose",lep->passesVeryLoose());
  configMgr->treeMaker->setBoolVariable(prefix+"Loose",lep->passesLoose());
  configMgr->treeMaker->setBoolVariable(prefix+"Medium",lep->passesMedium());
  configMgr->treeMaker->setBoolVariable(prefix+"Tight",lep->passesTight());
  configMgr->treeMaker->setBoolVariable(prefix+"LowPtWP",passesLowPtWP);
  configMgr->treeMaker->setBoolVariable(prefix+"LooseBLllh", passLooseBLllh);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoHighPtCaloOnly",lep->IsoHighPtCaloOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoHighPtTrackOnly",lep->IsoHighPtCaloOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"corr_IsoHighPtCaloOnly",lep->corr_IsoHighPtCaloOnly);

  //Old WPs
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFCHighPtCaloOnly",lep->IsoFCHighPtCaloOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFCTightTrackOnly",lep->IsoFCTightTrackOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFCLoose",lep->IsoFCLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFCTight",lep->IsoFCTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFCLoose_FixedRad",lep->IsoFCLoose_FixedRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFCTight_FixedRad",lep->IsoFCTight_FixedRad);

  //New WPs
  configMgr->treeMaker->setBoolVariable(prefix+"IsoLoose_VarRad",lep->IsoLoose_VarRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoLoose_FixedRad",lep->IsoLoose_FixedRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoTight_VarRad",lep->IsoTight_VarRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoTight_FixedRad",lep->IsoTight_FixedRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoTightTrackOnly_VarRad",lep->IsoTightTrackOnly_VarRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoTightTrackOnly_FixedRad",lep->IsoTightTrackOnly_FixedRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPflowLoose_VarRad",lep->IsoPflowLoose_VarRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPflowLoose_FixedRad",lep->IsoPflowLoose_FixedRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPflowTight_VarRad",lep->IsoPflowTight_VarRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPflowTight_FixedRad",lep->IsoPflowTight_FixedRad);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPLVLoose",lep->IsoPLVLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPLVTight",lep->IsoPLVTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPLImprovedTight",lep->IsoPLImprovedTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoPLImprovedVeryTight",lep->IsoPLImprovedVeryTight);


  /*
  configMgr->treeMaker->setBoolVariable(prefix+"IsoLoose",lep->IsoLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoTight",lep->IsoTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoGradient",lep->IsoGradient);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoGradientLoose",lep->IsoGradientLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoLooseTrackOnly",lep->IsoLooseTrackOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFixedCutLoose",lep->IsoFixedCutLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFixedCutTight",lep->IsoFixedCutTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFixedCutTightTrackOnly",lep->IsoFixedCutTightTrackOnly);
  // TTMVA Isolation WPs
  configMgr->treeMaker->setFloatVariable(prefix+"Ptvarcone30_TightTTVA_pt1000",lep->ptvarcone30_TightTTVA_pt1000);
  configMgr->treeMaker->setFloatVariable(prefix+"Ptvarcone30_TightTTVA_pt500",lep->ptvarcone30_TightTTVA_pt500);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrPtvarcone30_TightTTVA_pt1000",lep->corr_ptvarcone30_TightTTVA_pt1000);
  configMgr->treeMaker->setFloatVariable(prefix+"CorrPtvarcone30_TightTTVA_pt500",lep->corr_ptvarcone30_TightTTVA_pt500);
  configMgr->treeMaker->setBoolVariable(prefix+"neflowisol20",lep->neflowisol20);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoFixedCutHighPtTrackOnly",lep->IsoFixedCutHighPtTrackOnly);
  // Corr
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrLoose",lep->corr_IsoLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrTight",lep->corr_IsoTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrGradient",lep->corr_IsoGradient);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrGradientLoose",lep->corr_IsoGradientLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrLooseTrackOnly",lep->corr_IsoLooseTrackOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFixedCutLoose",lep->corr_IsoFixedCutLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFixedCutTight",lep->corr_IsoFixedCutTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFixedCutTightTrackOnly",lep->corr_IsoFixedCutTightTrackOnly);
  // TTMVA Isolation WPs (Corr)
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFixedCutHighPtTrackOnly",lep->corr_IsoFixedCutHighPtTrackOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFCTightTrackOnly",lep->corr_IsoFCTightTrackOnly);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFCLoose",lep->corr_IsoFCLoose);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFCTight",lep->corr_IsoFCTight);
  configMgr->treeMaker->setBoolVariable(prefix+"IsoCorrFCHighPtCaloOnly",lep->corr_IsoFCHighPtCaloOnly);
  */
  configMgr->treeMaker->setBoolVariable(prefix+"Signal",lep->signal);
  configMgr->treeMaker->setBoolVariable(prefix+"TruthMatched",lepTruthMatched);
  configMgr->treeMaker->setIntVariable(prefix+"TruthCharge",lep->truthQ);
  configMgr->treeMaker->setFloatVariable(prefix+"TruthPt",  lep->truthTLV.Pt());
  configMgr->treeMaker->setFloatVariable(prefix+"TruthEta", lep->truthTLV.Eta());;
  configMgr->treeMaker->setFloatVariable(prefix+"TruthPhi", lep->truthTLV.Phi());
  configMgr->treeMaker->setFloatVariable(prefix+"TruthM",   lep->truthTLV.M());
  configMgr->treeMaker->setFloatVariable(prefix+"TrackPt",  lep->trackTLV.Pt());
  configMgr->treeMaker->setFloatVariable(prefix+"TrackEta", lep->trackTLV.Eta());;
  configMgr->treeMaker->setFloatVariable(prefix+"TrackPhi", lep->trackTLV.Phi());

  // Individual lepton weights
 //configMgr->treeMaker->setDoubleVariable(prefix+"Weight",lep->getRecoSF(""));

  // Rel. 21 PromptLeptonTagger output
  configMgr->treeMaker->setFloatVariable(prefix+"PromptLepVeto_score",     lep->promptLepVeto_score);

  // Rel. 21 PromptLeptonTagger inputs (need to toggle on "WriteDetailedSkim" in the deep config in SusySkimMaker)
  configMgr->treeMaker->setFloatVariable(prefix+"PLTInput_TrackJetNTrack", lep->plt_input_TrackJetNTrack);
  configMgr->treeMaker->setFloatVariable(prefix+"PLTInput_DRlj",           lep->plt_input_DRlj);
  configMgr->treeMaker->setFloatVariable(prefix+"PLTInput_rnnip",          lep->plt_input_rnnip);
  configMgr->treeMaker->setFloatVariable(prefix+"PLTInput_DL1mu",          lep->plt_input_DL1mu);
  configMgr->treeMaker->setFloatVariable(prefix+"PLTInput_PtRel",          lep->plt_input_PtRel);
  configMgr->treeMaker->setFloatVariable(prefix+"PLTInput_PtFrac",         lep->plt_input_PtFrac);

  // Set PLT scores and WPs
  setPLTscore(configMgr, prefix);
  setPLTWP(configMgr, prefix);

  // Set SignalPLT flag manually
  if(lep->isEle()){
    bool Passd0Sig = (fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0) < 5;
    bool PassZ0SinTheta = fabs(lep->z0 * TMath::Sin(lep->Theta())) < 0.5 ;
    bool PassID = lep->passesMedium();
    bool PassPLTLoose = configMgr->treeMaker->getBoolVariable(prefix+"IsoLowPtPLT_MediumLH_Loose");
    bool PassPLTTight = configMgr->treeMaker->getBoolVariable(prefix+"IsoLowPtPLT_MediumLH_Tight");
    configMgr->treeMaker->setBoolVariable(prefix+"SignalPLTLoose", Passd0Sig && PassZ0SinTheta && PassID && PassPLTLoose);
    configMgr->treeMaker->setBoolVariable(prefix+"SignalPLTTight", Passd0Sig && PassZ0SinTheta && PassID && PassPLTTight);
  }
  else if(lep->isMu()){
    const MuonVariable* mu = dynamic_cast<const MuonVariable*>(lep);
    bool Passd0Sig = (fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0) < 3;
    bool PassZ0SinTheta = fabs(lep->z0 * TMath::Sin(lep->Theta())) < 0.5 ;
    bool PassID = configMgr->treeMaker->getBoolVariable(prefix+"LowPtWP");
    bool PassPLTLoose = configMgr->treeMaker->getBoolVariable(prefix+"IsoLowPtPLT_Loose");
    bool PassPLTTight = configMgr->treeMaker->getBoolVariable(prefix+"IsoLowPtPLT_Tight");
    configMgr->treeMaker->setBoolVariable(prefix+"SignalPLTLoose", Passd0Sig && PassZ0SinTheta && PassID && PassPLTLoose);
    configMgr->treeMaker->setBoolVariable(prefix+"SignalPLTTight", Passd0Sig && PassZ0SinTheta && PassID && PassPLTTight);
  }

  configMgr->treeMaker->setBoolVariable(prefix+"EWKCombiBaseline",IsCombiBaselineLepton(lep));
  configMgr->treeMaker->setBoolVariable(prefix+"PassZCR",LepPassVBF_ZCR(lep));
  configMgr->treeMaker->setBoolVariable(prefix+"PassWCR",LepPassVBF_WCR(lep));

  return;
}
//////////////////////////////////////////////////////////////////
bool Common::setCorrPLTInputVariables(const ConfigMgr* configMgr, TString prefix1, TString prefix2, const LeptonVariable* lep1, const LeptonVariable* lep2){
  /// Step[0] Define corrected variables, initialiize with original value ///
  int    lep1_CorrTrackJetNTrack = lep1->plt_input_TrackJetNTrack;
  double lep1_CorrDRlj           = lep1->plt_input_DRlj;
  double lep1_Corrrnnip          = lep1->plt_input_rnnip;
  double lep1_CorrDL1mu          = lep1->plt_input_DL1mu;
  double lep1_CorrPtRel          = lep1->plt_input_PtRel;
  double lep1_CorrPtFrac         = lep1->plt_input_PtFrac;
  int    lep2_CorrTrackJetNTrack = lep2->plt_input_TrackJetNTrack;
  double lep2_CorrDRlj           = lep2->plt_input_DRlj;
  double lep2_Corrrnnip          = lep2->plt_input_rnnip;
  double lep2_CorrDL1mu          = lep2->plt_input_DL1mu;
  double lep2_CorrPtRel          = lep2->plt_input_PtRel;
  double lep2_CorrPtFrac         = lep2->plt_input_PtFrac;

  /// Step[1] Check if correction is needed ///
  TLorentzVector lep1_vec;
  TLorentzVector lep2_vec;
  lep1_vec.SetPtEtaPhiM(lep1->Pt(), lep1->Eta(), lep1->Phi(), 0);
  lep2_vec.SetPtEtaPhiM(lep2->Pt(), lep2->Eta(), lep2->Phi(), 0);
  double dR_lep1_lep2 = lep1_vec.DeltaR(lep2_vec);
  double dR_lep1_J    = lep1->plt_input_DRlj;
  double dR_lep2_J    = lep2->plt_input_DRlj;
  bool need_correction = true;
  if(dR_lep1_lep2 <=0 || dR_lep1_lep2 > 0.4) need_correction = false; // Must be close enough
  // Ensure both leptons included in the same track-jet
  if(dR_lep1_lep2 > dR_lep1_J + dR_lep2_J) need_correction = false;
  if(dR_lep1_J > dR_lep1_lep2 + dR_lep2_J) need_correction = false;
  if(dR_lep2_J > dR_lep1_lep2 + dR_lep1_J) need_correction = false;
  if(lep1->plt_input_TrackJetNTrack != lep2->plt_input_TrackJetNTrack) need_correction = false;
  if(lep1->plt_input_TrackJetNTrack == 1) need_correction = false;


  if(need_correction){
  /// Step[3] Get track-jet center coordinates from lep1, lep2 information ///
    double dPhi_with_sign = lep2_vec.DeltaPhi(lep1_vec);
    double dEta_with_sign = lep2->Eta() - lep1->Eta();
    // angle 1 and 2 are "virtual" angle on eta-phi coordinate system
    double angle1 = 0;
    if (dEta_with_sign/dR_lep1_lep2 >  1) angle1 = asin(1);
    else if (dEta_with_sign/dR_lep1_lep2 < -1) angle1 = asin(-1);
    else angle1 = asin(dEta_with_sign/dR_lep1_lep2);
    if(dPhi_with_sign < 0) angle1 = M_PI - angle1;
    double angle2 = 0;
    angle2 = acos( (dR_lep1_J*dR_lep1_J + dR_lep1_lep2*dR_lep1_lep2 - dR_lep2_J*dR_lep2_J) / (2*dR_lep1_J*dR_lep1_lep2) ); // Law of cosines
    // two track-jet center candidates  (cand1, cand2)
    double eta_cand1 = lep1->Eta() + dR_lep1_J*sin(angle1 + angle2);
    double eta_cand2 = lep1->Eta() + dR_lep1_J*sin(angle1 - angle2);
    double phi_cand1 = lep1->Phi() + dR_lep1_J*cos(angle1 + angle2);
    double phi_cand2 = lep1->Phi() + dR_lep1_J*cos(angle1 - angle2);
    if (phi_cand1 >  M_PI) phi_cand1 = phi_cand1 - 2*M_PI;
    if (phi_cand1 < -M_PI) phi_cand1 = phi_cand1 + 2*M_PI;
    if (phi_cand2 >  M_PI) phi_cand2 = phi_cand2 - 2*M_PI;
    if (phi_cand2 < -M_PI) phi_cand2 = phi_cand2 + 2*M_PI;

  /// Step[4] Choose the finale track-jet center from 2 candidates ///
    double Pt_J = lep1->Pt()/lep1->plt_input_PtFrac;
    TLorentzVector cand1_vec, cand2_vec;
    cand1_vec.SetPtEtaPhiM(Pt_J, eta_cand1, phi_cand1, 0);
    cand2_vec.SetPtEtaPhiM(Pt_J, eta_cand2, phi_cand2, 0);
    double angle_lep1_cand1 = lep1_vec.Angle(cand1_vec.Vect());
    double angle_lep1_cand2 = lep1_vec.Angle(cand2_vec.Vect());
    double PtRel_lep1_cand1 = lep1->Pt()*sin(angle_lep1_cand1)*1000; // in MeV
    double PtRel_lep1_cand2 = lep1->Pt()*sin(angle_lep1_cand2)*1000; // in MeV
    double dPtRel_lep1_cand1 = fabs(PtRel_lep1_cand1 - lep1->plt_input_PtRel);
    double dPtRel_lep1_cand2 = fabs(PtRel_lep1_cand2 - lep1->plt_input_PtRel);
    // choose the candidates which describes PtRel of lep1 better
    TLorentzVector J;
    if (dPtRel_lep1_cand1 < dPtRel_lep1_cand2) J = cand1_vec;
    else J = cand2_vec;

  /// Step[5] Correct ///
    TLorentzVector J_minus_lep1, J_minus_lep2;
    J_minus_lep1 = J - lep1_vec; // subtract lep1, for lep2
    J_minus_lep2 = J - lep2_vec; // subtract lep2, for lep1
    double angle_lep1_newJ = lep1_vec.Angle(J_minus_lep2.Vect());
    double angle_lep2_newJ = lep2_vec.Angle(J_minus_lep1.Vect());

    lep1_CorrTrackJetNTrack = lep1_CorrTrackJetNTrack - 1;
    lep1_CorrDRlj           = lep1_vec.DeltaR(J_minus_lep2);
    lep1_CorrPtRel          = lep1->Pt()*sin(angle_lep1_newJ)*1000;
    lep1_CorrPtFrac         = lep1->Pt()/J_minus_lep2.Pt();

    lep2_CorrTrackJetNTrack = lep2_CorrTrackJetNTrack - 1;
    lep2_CorrDRlj           = lep2_vec.DeltaR(J_minus_lep1);
    lep2_CorrPtRel          = lep2->Pt()*sin(angle_lep2_newJ)*1000;
    lep2_CorrPtFrac         = lep2->Pt()/J_minus_lep1.Pt();

  }

  configMgr->treeMaker->setFloatVariable(prefix1+"CorrPLTInput_TrackJetNTrack", lep1_CorrTrackJetNTrack);
  configMgr->treeMaker->setFloatVariable(prefix1+"CorrPLTInput_DRlj",           lep1_CorrDRlj);
  configMgr->treeMaker->setFloatVariable(prefix1+"CorrPLTInput_rnnip",          lep1_Corrrnnip);
  configMgr->treeMaker->setFloatVariable(prefix1+"CorrPLTInput_DL1mu",          lep1_CorrDL1mu);
  configMgr->treeMaker->setFloatVariable(prefix1+"CorrPLTInput_PtRel",          lep1_CorrPtRel);
  configMgr->treeMaker->setFloatVariable(prefix1+"CorrPLTInput_PtFrac",         lep1_CorrPtFrac);

  configMgr->treeMaker->setFloatVariable(prefix2+"CorrPLTInput_TrackJetNTrack", lep2_CorrTrackJetNTrack);
  configMgr->treeMaker->setFloatVariable(prefix2+"CorrPLTInput_DRlj",           lep2_CorrDRlj);
  configMgr->treeMaker->setFloatVariable(prefix2+"CorrPLTInput_rnnip",          lep2_Corrrnnip);
  configMgr->treeMaker->setFloatVariable(prefix2+"CorrPLTInput_DL1mu",          lep2_CorrDL1mu);
  configMgr->treeMaker->setFloatVariable(prefix2+"CorrPLTInput_PtRel",          lep2_CorrPtRel);
  configMgr->treeMaker->setFloatVariable(prefix2+"CorrPLTInput_PtFrac",         lep2_CorrPtFrac);

  return need_correction;
}
//////////////////////////////////////////////////////////////////
void Common::setPLTscore(const ConfigMgr* configMgr, TString prefix){
  double score = 0.1;
  Int_t flavor = configMgr->treeMaker->getIntVariable(prefix+"Flavor");
  if(flavor!=1 && flavor!=2) return;

  for(TString algName : {"CorrPromptLepVeto", "LowPtPLT", "LowPtPLT_Cone40"} ){
    if(algName == "CorrPromptLepVeto"){
      BDT_Vars[0]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_TrackJetNTrack");
      BDT_Vars[1]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_rnnip");
      BDT_Vars[2]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_DL1mu");
      BDT_Vars[3]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_PtRel");
      BDT_Vars[4]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_PtFrac");
      BDT_Vars[5]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_DRlj");
      BDT_Vars[6]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrTopoetcone20")/configMgr->treeMaker->getFloatVariable(prefix+"Pt"); // update when topoetcone30 is accessible
      if(flavor == 1) BDT_Vars[7]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPtvarcone20")/configMgr->treeMaker->getFloatVariable(prefix+"Pt"); // update when ptvarcone30 is accessible
      else BDT_Vars[7]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPtvarcone30")/configMgr->treeMaker->getFloatVariable(prefix+"Pt");
      score = CorrPromptLepVeto_reader[flavor-1]->EvaluateMVA( algName );
    }

    if(algName == "LowPtPLT"){
      if(flavor == 1) BDT_Vars[0]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPtvarcone20")/configMgr->treeMaker->getFloatVariable(prefix+"Pt"); // update when ptvarcone30 is accessible
      else BDT_Vars[0]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPtvarcone30")/configMgr->treeMaker->getFloatVariable(prefix+"Pt");
      BDT_Vars[1]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrTopoetcone20")/configMgr->treeMaker->getFloatVariable(prefix+"Pt");
      BDT_Vars[2]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_PtRel");
      BDT_Vars[3]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_PtFrac");
      BDT_Vars[4]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_DRlj");
      BDT_Vars[5]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_TrackJetNTrack");
      score = LowPtPLT_reader[flavor-1]->EvaluateMVA( algName );
    }

    if(algName == "LowPtPLT_Cone40"){
      BDT_Vars[0]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPtvarcone40")/configMgr->treeMaker->getFloatVariable(prefix+"Pt");
      BDT_Vars[1]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrTopoetcone40")/configMgr->treeMaker->getFloatVariable(prefix+"Pt");
      BDT_Vars[2]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_PtRel");
      BDT_Vars[3]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_PtFrac");
      BDT_Vars[4]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_DRlj");
      BDT_Vars[5]  = configMgr->treeMaker->getFloatVariable(prefix+"CorrPLTInput_TrackJetNTrack");
      score = LowPtPLT_Cone40_reader[flavor-1]->EvaluateMVA( algName );
    }
    configMgr->treeMaker->setFloatVariable(prefix+algName, score);
  }
}
//////////////////////////////////////////////////////////////////
void Common::setPLTWP(const ConfigMgr* configMgr, TString prefix ){
  // Get Pt bins
  Float_t Pt = configMgr->treeMaker->getFloatVariable(prefix+"Pt");
  Int_t Ptbin = 0;
  if     (Pt > 3.0 && Pt < 3.5) Ptbin = 0;
  else if(Pt > 3.5 && Pt < 4.0) Ptbin = 1;
  else if(Pt > 4.0 && Pt < 4.5) Ptbin = 2;
  else if(Pt > 4.5 && Pt < 5.0) Ptbin = 3;
  else if(Pt > 5.0 && Pt < 6.0) Ptbin = 4;
  else if(Pt > 6.0 && Pt < 8.0) Ptbin = 5;
  else if(Pt > 8.0 && Pt < 10 ) Ptbin = 6;
  else if(Pt > 10  && Pt < 12 ) Ptbin = 7;
  else if(Pt > 12  && Pt < 14 ) Ptbin = 8;
  else if(Pt > 14  && Pt < 17 ) Ptbin = 9;
  else if(Pt > 17             ) Ptbin = 10;

  // declare parameters
  Int_t flavor = configMgr->treeMaker->getIntVariable(prefix+"Flavor");

  // Upper cut on "Corrected" nominal PLT score
  bool passLoose  = false;
  bool passTight  = false;
  bool passMediumLHLoose  = false;
  bool passMediumLHTight  = false;

  /*
  bool passLooseTrackOnly = configMgr->treeMaker->getBoolVariable(prefix+"IsoCorrLooseTrackOnly");
  bool passGradient       = configMgr->treeMaker->getBoolVariable(prefix+"IsoCorrGradient");
  bool passFCLoose        = configMgr->treeMaker->getBoolVariable(prefix+"IsoCorrFixedCutLoose");
 */

  for(TString algName : {"LowPtPLT"} ){
    if(algName == "LowPtPLT"){
      Float_t score = configMgr->treeMaker->getFloatVariable(prefix+algName);
      if((flavor==1 && Ptbin <10) || (flavor==2 && Ptbin<8)){
	// apply PLT cut
	if(score > LowPtPLT_WP[flavor-1][0][Ptbin] ) passLoose = true;
	if(score > LowPtPLT_WP[flavor-1][1][Ptbin] ) passTight = true;
	if(score > LowPtPLT_MediumLH_WP[0][Ptbin] ) passMediumLHLoose = true;
	if(score > LowPtPLT_MediumLH_WP[1][Ptbin] ) passMediumLHTight = true;

	// if low pt lepton, apply LooseTrackOnly on top of PLT
	//passLoose = passLoose && passLooseTrackOnly;
	//passTight = passTight && passLooseTrackOnly;
    //passMediumLHLoose = passMediumLHLoose && passLooseTrackOnly;
	//passMediumLHTight = passMediumLHTight && passLooseTrackOnly;
      }
      else{
	// if high pt, then overwrite with FCLoose or Gradient
	//passLoose = passFCLoose;
	//passTight = passFCLoose;
	//passMediumLHLoose = passGradient;
	//passMediumLHTight = passGradient;
      }
    }
    configMgr->treeMaker->setBoolVariable(prefix+"Iso"+algName+"_Loose",  passLoose);
    configMgr->treeMaker->setBoolVariable(prefix+"Iso"+algName+"_Tight",  passTight);
    configMgr->treeMaker->setBoolVariable(prefix+"Iso"+algName+"_MediumLH"+"_Loose",  passMediumLHLoose);
    configMgr->treeMaker->setBoolVariable(prefix+"Iso"+algName+"_MediumLH"+"_Tight",  passMediumLHTight);
  }

}
//////////////////////////////////////////////////////////////////
void Common::PreparePLTWP(){
  // Initialize cut values
  Float_t LowPtPLT_muon_Loose[11]     = { -0.18 , -0.12 , -0.04 ,  0.00 ,  0.06 ,  0.16 ,  0.24 ,  0.28 ,  0.30 ,  0.36 ,  0.34};
  Float_t LowPtPLT_muon_Tight[11]     = { -0.02 ,  0.02 ,  0.08 ,  0.14 ,  0.20 ,  0.28,   0.32 ,  0.38 ,  0.40 ,  0.42 ,  0.42};
  Float_t LowPtPLT_elec_Loose[11]     = { -1.00 , -1.00 , -1.00 , -0.52 , -0.54 , -0.56 , -0.56 , -0.50 , -0.48 , -0.32 , -0.24};
  Float_t LowPtPLT_elec_Tight[11]     = { -1.00 , -1.00 , -1.00 , -0.22 , -0.26 , -0.34 , -0.44 , -0.46 , -0.44 , -0.26 , -0.22};
  Float_t LowPtPLT_MediumLH_Loose[11] = { -1.00 , -1.00 , -1.00 , -0.04 , -0.10 , -0.16 , -0.20 , -0.12 , -0.12 , -0.04 , 0.00};
  Float_t LowPtPLT_MediumLH_Tight[11] = { -1.00 , -1.00 , -1.00 ,  0.22 ,  0.20 ,  0.16 ,  0.14 ,  0.14  , 0.14  , 0.14 , 0.14};

  // Set values to global variables
  for (int i=0; i<11; i++){
    LowPtPLT_WP[0][0][i]          = LowPtPLT_elec_Loose[i];
    LowPtPLT_WP[0][1][i]          = LowPtPLT_elec_Tight[i];
    LowPtPLT_WP[1][0][i]          = LowPtPLT_muon_Loose[i];
    LowPtPLT_WP[1][1][i]          = LowPtPLT_muon_Tight[i];
    LowPtPLT_MediumLH_WP[0][i]    = LowPtPLT_MediumLH_Loose[i];
    LowPtPLT_MediumLH_WP[1][i]    = LowPtPLT_MediumLH_Tight[i];
  }
}
//////////////////////////////////////////////////////////////////
void Common::PrepareBDTConfig(){
  // Retrieve All the BDT configurations here

  struct BDT_Config myConfig;
  TString VarName;
  float dummy = 0; // for spec var

  for(int flav=0; flav<2; flav++){
    CorrPromptLepVeto_reader[flav] = new TMVA::Reader( "!Color:!Silent" );
    myConfig = GetPLT_Config(flav+1, "CorrPromptLepVeto");
    for (int i=0; i<myConfig.varnum; i++){
      VarName = myConfig.varnames.at(i);
      CorrPromptLepVeto_reader[flav]->AddVariable( VarName, &BDT_Vars[i] );
    }
    for (int i=0; i<myConfig.specvarnum; i++){
      VarName = myConfig.specvarnames.at(i);
      CorrPromptLepVeto_reader[flav]->AddSpectator( VarName, &dummy );
    }
    CorrPromptLepVeto_reader[flav]->BookMVA( myConfig.algName, myConfig.weightFile );

    // LowPtPLT
    LowPtPLT_reader[flav] = new TMVA::Reader( "!Color:!Silent" );
    myConfig = GetPLT_Config(flav+1, "LowPtPLT");
    for (int i=0; i<myConfig.varnum; i++){
      VarName = myConfig.varnames.at(i);
      LowPtPLT_reader[flav]->AddVariable( VarName, &BDT_Vars[i] );
    }
    for (int i=0; i<myConfig.specvarnum; i++){
      VarName = myConfig.specvarnames.at(i);
      LowPtPLT_reader[flav]->AddSpectator( VarName, &dummy );
    }
    LowPtPLT_reader[flav]->BookMVA( myConfig.algName, myConfig.weightFile );

    // LowPtPLT_Cone40
    LowPtPLT_Cone40_reader[flav] = new TMVA::Reader( "!Color:!Silent" );
    myConfig = GetPLT_Config(flav+1, "LowPtPLT_Cone40");
    for (int i=0; i<myConfig.varnum; i++){
      VarName = myConfig.varnames.at(i);
      LowPtPLT_Cone40_reader[flav]->AddVariable( VarName, &BDT_Vars[i] );
    }
    for (int i=0; i<myConfig.specvarnum; i++){
      VarName = myConfig.specvarnames.at(i);
      LowPtPLT_Cone40_reader[flav]->AddSpectator( VarName, &dummy );
    }
    LowPtPLT_Cone40_reader[flav]->BookMVA( myConfig.algName, myConfig.weightFile );
  }
}
//////////////////////////////////////////////////////////////////
struct Common::BDT_Config Common::GetPLT_Config(int flavor, TString algName){
  struct BDT_Config myConfig;
  if(algName == "CorrPromptLepVeto"){
    if(flavor == 1) myConfig.weightFile = PathResolverFindCalibFile("SusySkimHiggsino/TMVAClassification_BDT_Electron_PromptLeptonVeto.weights.xml");
    else            myConfig.weightFile = PathResolverFindCalibFile("SusySkimHiggsino/TMVAClassification_BDT_Muon_PromptLeptonVeto.weights.xml");
    myConfig.algName = algName;
    myConfig.varnum = 8;
    myConfig.varnames.push_back("TrackJetNTrack");
    myConfig.varnames.push_back("rnnip");
    myConfig.varnames.push_back("DL1mu");
    myConfig.varnames.push_back("PtRel");
    myConfig.varnames.push_back("PtFrac");
    myConfig.varnames.push_back("DRlj");
    myConfig.varnames.push_back("TopoEtCone30Rel");
    myConfig.varnames.push_back("PtVarCone30Rel");
    myConfig.specvarnum = 0;
  }
  else if (algName == "LowPtPLT"){
    if(flavor == 1) myConfig.weightFile = PathResolverFindCalibFile("SusySkimHiggsino/TMVAClassification_BDT_Electron_LowPtPromptLeptonTagger_20181001.weights.xml");
    else            myConfig.weightFile = PathResolverFindCalibFile("SusySkimHiggsino/TMVAClassification_BDT_Muon_LowPtPromptLeptonTagger_20181001.weights.xml");
    myConfig.algName = algName;
    myConfig.varnum = 6;
    if(flavor == 1) myConfig.varnames.push_back("CorrPtvarcone20/Pt/1000"); // training sample defines Ptvarcone in MeV
    else            myConfig.varnames.push_back("CorrPtvarcone30/Pt/1000"); // training sample defines Ptvarcone in MeV
    myConfig.varnames.push_back("CorrTopoetcone20/Pt");
    myConfig.varnames.push_back("PLTInput_PtRel");
    myConfig.varnames.push_back("PLTInput_PtFrac");
    myConfig.varnames.push_back("PLTInput_DRlj");
    myConfig.varnames.push_back("PLTInput_TrackJetNTrack");
    myConfig.specvarnum = 0;
  }
  else if (algName == "LowPtPLT_Cone40"){
    if(flavor == 1) myConfig.weightFile = PathResolverFindCalibFile("SusySkimHiggsino/TMVAClassification_BDT_Electron_LowPtPromptLeptonTagger_Cone40_20181001.weights.xml");
    else            myConfig.weightFile = PathResolverFindCalibFile("SusySkimHiggsino/TMVAClassification_BDT_Muon_LowPtPromptLeptonTagger_Cone40_20181001.weights.xml");
    myConfig.algName = algName;
    myConfig.varnum = 6;
    myConfig.varnames.push_back("CorrPtvarcone40/Pt/1000"); // training sample defines Ptvarcone in MeV
    myConfig.varnames.push_back("CorrTopoetcone40/Pt");
    myConfig.varnames.push_back("PLTInput_PtRel");
    myConfig.varnames.push_back("PLTInput_PtFrac");
    myConfig.varnames.push_back("PLTInput_DRlj");
    myConfig.varnames.push_back("PLTInput_TrackJetNTrack");
    myConfig.specvarnum = 0;
  }

  return myConfig;
}
//////////////////////////////////////////////////////////////////
void Common::setPLT_nLep_signal(const ConfigMgr* configMgr){
  Int_t nLep_signal_PLTLooseLoose = 0;
  Int_t nLep_signal_PLTLooseTight = 0;
  Int_t nLep_signal_PLTTightLoose = 0;
  Int_t nLep_signal_PLTTightTight = 0;

  for(TString prefix : {"lep1", "lep2", "lep3"} ){
    Int_t flavor     = configMgr->treeMaker->getIntVariable(prefix+"Flavor");
    if (flavor==1 ){
      if(configMgr->treeMaker->getBoolVariable(prefix+"SignalPLTLoose")) {
	nLep_signal_PLTLooseLoose++;
	nLep_signal_PLTLooseTight++;
      }
      if(configMgr->treeMaker->getBoolVariable(prefix+"SignalPLTTight")) {
	nLep_signal_PLTTightLoose++;
	nLep_signal_PLTTightTight++;
      }
    }
    if (flavor==2 ){
      if(configMgr->treeMaker->getBoolVariable(prefix+"SignalPLTLoose")) {
	nLep_signal_PLTLooseLoose++;
	nLep_signal_PLTTightLoose++;
      }
      if(configMgr->treeMaker->getBoolVariable(prefix+"SignalPLTTight")) {
	nLep_signal_PLTLooseTight++;
	nLep_signal_PLTTightTight++;
      }
    }
  }

  configMgr->treeMaker->setIntVariable("nLep_signal_PLTLooseLoose", nLep_signal_PLTLooseLoose);
  configMgr->treeMaker->setIntVariable("nLep_signal_PLTLooseTight", nLep_signal_PLTLooseTight);
  configMgr->treeMaker->setIntVariable("nLep_signal_PLTTightLoose", nLep_signal_PLTTightLoose);
  configMgr->treeMaker->setIntVariable("nLep_signal_PLTTightTight", nLep_signal_PLTTightTight);

}
//////////////////////////////////////////////////////////////////
void Common::addTauVariables(const ConfigMgr* configMgr, TString prefix){

  configMgr->treeMaker->addBoolVariable(prefix+"Loose", false);
  configMgr->treeMaker->addBoolVariable(prefix+"Medium", false);
  configMgr->treeMaker->addBoolVariable(prefix+"Tight", false);
  configMgr->treeMaker->addFloatVariable(prefix+"Pt", 0.);
  configMgr->treeMaker->addFloatVariable(prefix+"Eta", 0.);
  configMgr->treeMaker->addFloatVariable(prefix+"Phi", 0.);
  configMgr->treeMaker->addFloatVariable(prefix+"M", 0.);
  configMgr->treeMaker->addIntVariable(prefix+"NTracks", 0);
}
//////////////////////////////////////////////////////////////////
void Common::setTauVariables(const ConfigMgr* configMgr, TString prefix, const TauVariable* tau){

  configMgr->treeMaker->setBoolVariable(prefix+"Loose", tau->loose);
  configMgr->treeMaker->setBoolVariable(prefix+"Medium", tau->medium);
  configMgr->treeMaker->setBoolVariable(prefix+"Tight", tau->tight);
  configMgr->treeMaker->setFloatVariable(prefix+"Pt", tau->Pt());
  configMgr->treeMaker->setFloatVariable(prefix+"Eta", tau->Eta());
  configMgr->treeMaker->setFloatVariable(prefix+"Phi", tau->Phi());
  configMgr->treeMaker->setFloatVariable(prefix+"M", tau->M());
  configMgr->treeMaker->setIntVariable(prefix+"NTracks", tau->nTrack);
}
//////////////////////////////////////////////////////////////////
