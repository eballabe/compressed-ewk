#include "SusySkimHiggsino/Common.h"
#include "SusySkimMaker/MT2_ROOT.h"
#include <algorithm>
#include "PathResolver/PathResolver.h"

// includes needed for emulation of L1Topo based triggers
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODJet/JetContainer.h"

// includes needed for RJR variable computation
#include "RestFrames/RestFrames.hh"
#include <TLorentzVector.h>
#include <TVector3.h>
#include <vector>

#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/Observables.h"
#include "PathResolver/PathResolver.h"

#include "TruthDecayContainer/ProcClassifier.h"
Observables obs;


// ------------------------------------------------------------------------------------------ //
void Common::setup(ConfigMgr*& configMgr, bool add_jets)
{
  // Set internal flag(s)
  m_addJets = add_jets;

  // Make a cutflow stream
  configMgr->cutflow->defineCutFlow("cutFlow", configMgr->treeMaker->getFile("tree"));

  // Use object def from SUSYTools
  configMgr->obj->useSUSYToolsSignalDef(true);

  // Bad muon veto
  configMgr->obj->applyBadMuonVeto(false);

  // Muon cuts
  configMgr->obj->setBaselineMuonPt(3.0);
  configMgr->obj->setBaselineMuonEta(2.70);
  configMgr->obj->setSignalMuonPt(3.0);
  configMgr->obj->setSignalMuonEta(2.50);

  // Electrons
  configMgr->obj->setBaselineElectronEt(4.5);
  configMgr->obj->setBaselineElectronEta(2.47);
  configMgr->obj->setSignalElectronEt(4.5);
  configMgr->obj->setSignalElectronEta(2.47);

  // Taus
  configMgr->obj->setSignalTauPt(20.0);
  configMgr->obj->setSignalTauEta(2.5);

  // Jets
  /*
  configMgr->obj->setCJetPt(20.0);
  configMgr->obj->setCJetEta(2.80);
  configMgr->obj->setFJetPt(30.0);
  configMgr->obj->setFJetEtaMin(2.80);
  configMgr->obj->setFJetEtaMax(4.50);
  */
  
  //Jet cleaning for Pflow jets 
  configMgr->obj->applyBadJetVeto(false); 
  configMgr->obj->applyEvtLooseBadJetVeto(true);  // or applyEvtTightBadJetVeto

  // Turn off OR for baseline muons for fakes selection
  //configMgr->obj->disableORBaselineMuons(true);
  //configMgr->obj->disableOR(true);

  // MET Triggers
  // Defined by run number <start,end>; -1 means ignore that bound
  // configMgr->addTriggerAna("HLT_xe70",276262,284484,"metTrig"); //2015
  configMgr->addTriggerAna("HLT_xe70_mht",276262,284484,"metTrig"); //2015
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",296939,302872,"metTrig"); //this is 2016 A-D3
  configMgr->addTriggerAna("HLT_xe100_mht_L1XE50",302919,303892,"metTrig"); //this is 2016 D4-F1
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",303943,320000,"metTrig"); //this is rest of 2016
  configMgr->addTriggerAna("HLT_xe110_pufit_L1XE55",320000, 348884,"metTrig"); // this is all of 2017
  configMgr->addTriggerAna("HLT_xe110_pufit_xe70_L1XE50", 348885, 350013,"metTrig"); //this is 2018 B-C5
  configMgr->addTriggerAna("HLT_xe110_pufit_xe65_L1XE50", 350067, -1,"metTrig"); //this is the rest of 2018  configMgr->addTriggerAna("HLT_xe70",276262,284484,"metTrig");

  //Good runs by year
  //TODO: find a better home for/way to get these? 
  std::vector<int> GR_2015 = {276262, 284484};
  std::vector<int> GR_2016 = {297730, 311481};
  std::vector<int> GR_2017 = {325713, 340453};
  std::vector<int> GR_2018 = {348885, 364292};
  
  // Single lepton triggers
  
  //Single Muon
  // Stores the trigger decision and corresponding SFs ( and their systematics )
  //configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu40",276262,284484,"singleMuonTrig"); //All 2015
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu50",GR_2015[0],GR_2015[1],"singleMuonTrig"); //2015
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",GR_2016[0], GR_2016[1],"singleMuonTrig");//2016
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50_OR_HLT_mu60_0eta105_msonly",GR_2017[0],-1,"singleMuonTrig");//2017 & 2018 

  //Single Electron
  //configMgr->addTriggerAna("SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",276262,-1, "singleElectronTrig");
  configMgr->addTriggerAna("HLT_e24_lhmedium_L1EM20VH_OR_HLT_e60_lhmedium_OR_HLT_e120_lhloose", GR_2015[0], GR_2015[1], "singleElectronTrig"); //2015
  configMgr->addTriggerAna("HLT_e24_lhtight_nod0_ivarloose_OR_HLT_e60_lhmedium_nod0_OR_HLT_e140_lhloose_nod0", GR_2016[0], 304008, "singleElectronTrig"); //2016 up to 304008
  configMgr->addTriggerAna("HLT_e26_lhtight_nod0_ivarloose_OR_HLT_e60_lhmedium_nod0_OR_HLT_e140_lhloose_nod0", 304009, GR_2016[1], "singleElectronTrig"); //rest of 2016
  configMgr->addTriggerAna("HLT_e26_lhtight_nod0_ivarloose_OR_HLT_e60_lhmedium_nod0_OR_HLT_e140_lhloose_nod0_OR_HLT_e300_etcut", GR_2017[0], GR_2017[1], "singleElectronTrig"); //2017
  configMgr->addTriggerAna("HLT_e26_lhtight_nod0_ivarloose_OR_HLT_e26_lhtight_nod0_OR_HLT_e60_lhmedium_nod0_OR_HLT_e140_lhloose_nod0_OR_HLT_e300_etcut", GR_2018[0], -1, "singleElectronTrig"); //2018


  //DiLepton triggers
  //DiMuon 
  configMgr->addTriggerAna("HLT_mu18_mu8noL1", GR_2015[0], GR_2015[1], "diMuonTrig"); //2015
  configMgr->addTriggerAna("HLT_mu22_mu8noL1", GR_2016[0], -1, "diMuonTrig"); //2015

  //DiElectron
  configMgr->addTriggerAna("HLT_2e12_lhloose_L12EM10VH", GR_2015[0], GR_2015[1], "diElectronTrig"); //2015
  configMgr->addTriggerAna("HLT_2e17_lhvloose_nod0", GR_2016[0], GR_2016[1], "diElectronTrig"); //2016
  configMgr->addTriggerAna("HLT_2e24_lhvloose_nod0", GR_2017[0], GR_2017[1], "diElectronTrig"); //2017
  configMgr->addTriggerAna("LT_2e17_lhvloose_nod0_L12EM15VHI", GR_2017[0], GR_2017[1], "diElectronTrig"); //2017


  // Read in TH1s that contain SFs for ETmiss-Triggers
  load_sfs();

  // Read in TH1 that is contains the fit of data and MGP8 Zjets in pT(mu, mu)
  load_ISR_fit();

  //
  // ---- Output trees ---- //
  //

  //Jet Cleaning Variables
  configMgr->treeMaker->addBoolVariable("passEmulTightJetCleaning1Jet", 0);
  configMgr->treeMaker->addBoolVariable("passEmulTightJetCleaning2Jet", 0);
  configMgr->treeMaker->addBoolVariable("passBadTileJetVeto", 0);

  // Global vars
  configMgr->treeMaker->addFloatVariable("mu",0.0);
  configMgr->treeMaker->addFloatVariable("avg_mu",    0.0);
  configMgr->treeMaker->addFloatVariable("actual_mu", 0.0);
  configMgr->treeMaker->addIntVariable("nVtx",        0.0);

  // Lepton vars
  configMgr->treeMaker->addIntVariable("nLep_base",   0);
  configMgr->treeMaker->addIntVariable("nLep_signal", 0);
  configMgr->treeMaker->addIntVariable("nEle_base", 0);
  configMgr->treeMaker->addIntVariable("nEle_signal", 0);
  configMgr->treeMaker->addIntVariable("nMu_base",  0);
  configMgr->treeMaker->addIntVariable("nMu_signal",  0);
  configMgr->treeMaker->addIntVariable("nLep_signal_PLTLooseLoose", 0);
  
  // Tau vars
  configMgr->treeMaker->addIntVariable("nTau_base",   0);
  configMgr->treeMaker->addIntVariable("nTau_signal", 0);

  addTauVariables(configMgr,"tau1");
  addTauVariables(configMgr,"tau2");
  
  configMgr->treeMaker->addIntVariable("nLep_signal_PLTLooseTight", 0);
  configMgr->treeMaker->addIntVariable("nLep_signal_PLTTightLoose", 0);
  configMgr->treeMaker->addIntVariable("nLep_signal_PLTTightTight", 0);

  addLeptonVariables(configMgr,"lep1");  // add lep1Pt, lep1Eta etc... here
  addLeptonVariables(configMgr,"lep2");
  addLeptonVariables(configMgr,"lep3");
  //addLeptonVariables(configMgr,"lep4");


  // Jets
  configMgr->treeMaker->addIntVariable("nJet30",0.0);
  configMgr->treeMaker->addIntVariable("nJet25",0.0);
  configMgr->treeMaker->addIntVariable("nJet20",0.0);

  configMgr->treeMaker->addIntVariable("nBJet30",0.0);
  configMgr->treeMaker->addIntVariable("nBJet25",0.0);
  configMgr->treeMaker->addIntVariable("nBJet20",0.0);

  configMgr->treeMaker->addIntVariable("nTotalJet",      0.0);
  configMgr->treeMaker->addIntVariable("nTotalJet20",    0.0);

  configMgr->treeMaker->addIntVariable("nBJet20_DL1r_FixedCutBEff_60",0);
  configMgr->treeMaker->addIntVariable("nBJet20_DL1r_FixedCutBEff_70",0);
  configMgr->treeMaker->addIntVariable("nBJet20_DL1r_FixedCutBEff_77",0);
  configMgr->treeMaker->addIntVariable("nBJet20_DL1r_FixedCutBEff_85",0);
  configMgr->treeMaker->addIntVariable("nBJet25_DL1r_FixedCutBEff_60",0);
  configMgr->treeMaker->addIntVariable("nBJet25_DL1r_FixedCutBEff_70",0);
  configMgr->treeMaker->addIntVariable("nBJet25_DL1r_FixedCutBEff_77",0);
  configMgr->treeMaker->addIntVariable("nBJet25_DL1r_FixedCutBEff_85",0);

  configMgr->treeMaker->addIntVariable("DecayModeTTbar",-1);

  // Truth info for tau decay from W
  configMgr->treeMaker->addIntVariable ("truthTauFromW_n", 0);
  configMgr->treeMaker->addVecIntVariable("truthTauFromW_DMV");

  if (m_addJets) {
    configMgr->treeMaker->addVecFloatVariable("jetPt");
    configMgr->treeMaker->addVecFloatVariable("jetEta");
    configMgr->treeMaker->addVecFloatVariable("jetPhi");
    configMgr->treeMaker->addVecFloatVariable("jetM");
    configMgr->treeMaker->addVecFloatVariable("jetBtagged");
    configMgr->treeMaker->addVecFloatVariable("jetDL1r");    
  // For L1Calo timing issue check:
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SusyBgForumRun2CheckList#Standard_diagnostic_plots_checks
  // Note that this var is only saved in the detailed skims, which we're only producing for data currently!
    configMgr->treeMaker->addVecFloatVariable("jetTileEnergy");
  }

  // MET
  configMgr->treeMaker->addFloatVariable("met_Et",               0.0);
  configMgr->treeMaker->addFloatVariable("met_Phi",              0.0);
  configMgr->treeMaker->addFloatVariable("met_Signif",           0.0);
  configMgr->treeMaker->addFloatVariable("TST_Et",               0.0);
  configMgr->treeMaker->addFloatVariable("TST_Phi",              0.0);
  configMgr->treeMaker->addFloatVariable("met_track_Et",         0.0);
  configMgr->treeMaker->addFloatVariable("met_track_Phi",        0.0);
  configMgr->treeMaker->addFloatVariable("deltaPhi_MET_TST_Phi", 0.0);
  configMgr->treeMaker->addFloatVariable("met_muons_invis_Et",   0.0);
  configMgr->treeMaker->addFloatVariable("met_muons_invis_Phi",  0.0);
  configMgr->treeMaker->addFloatVariable("met_Et_LepInvis",   -1.0);
  configMgr->treeMaker->addFloatVariable("met_Phi_LepInvis",   -1.0);
  configMgr->treeMaker->addFloatVariable("met_Signif_LepInvis",   -1.0);

  // Analysis variables
  configMgr->treeMaker->addFloatVariable("meffInc30",     0.0);
  configMgr->treeMaker->addFloatVariable("Ht30",          0.0);
  configMgr->treeMaker->addFloatVariable("LepAplanarity", 0.0);
  configMgr->treeMaker->addFloatVariable("JetAplanarity", 0.0);

  // Dijet variables
  configMgr->treeMaker->addFloatVariable("mjj",    -1.);
  configMgr->treeMaker->addFloatVariable("dEtajj", -1.);
  configMgr->treeMaker->addFloatVariable("dPhijj", -1.);

  // other analysis variables
  configMgr->treeMaker->addFloatVariable("mt_lep1",                0.0);
  configMgr->treeMaker->addFloatVariable("mt_lep2",                0.0);
  configMgr->treeMaker->addFloatVariable("mt_lep3",                0.0);
  configMgr->treeMaker->addFloatVariable("mt_lep1_metTrack",       0.0);
  configMgr->treeMaker->addFloatVariable("mt_lep2_metTrack",       0.0);
  configMgr->treeMaker->addFloatVariable("mt_lep3_metTrack",       0.0);
  configMgr->treeMaker->addFloatVariable("METOverHT",              -1.);
  configMgr->treeMaker->addFloatVariable("METOverJ1pT",            -1.);
  configMgr->treeMaker->addFloatVariable("METTrackOverHT",         -1.);
  configMgr->treeMaker->addFloatVariable("METTrackOverJ1pT",       -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ1Met",              -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ2Met",              -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ3Met",              -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ4Met",              -1.);
  configMgr->treeMaker->addFloatVariable("minDPhiCenJetsMet",      -1.);
  configMgr->treeMaker->addFloatVariable("minDPhiAllJetsMet",      -1.);
  configMgr->treeMaker->addFloatVariable("minDPhiAllJetsMet_LepInvis", -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ1MetTrack",         -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ2MetTrack",         -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ3MetTrack",         -1.);
  configMgr->treeMaker->addFloatVariable("DPhiJ4MetTrack",         -1.);
  configMgr->treeMaker->addFloatVariable("minDPhiCenJetsMetTrack", -1.);

  // 2L Analysis Variables
  configMgr->treeMaker->addFloatVariable("METOverHTLep",      -1.);
  configMgr->treeMaker->addFloatVariable("METTrackOverHTLep", -1.);
  configMgr->treeMaker->addFloatVariable("mll",               -1.);
  configMgr->treeMaker->addFloatVariable("Rll",               -1.);
  configMgr->treeMaker->addFloatVariable("Ptll",              -1.);
  configMgr->treeMaker->addFloatVariable("dPhiPllMet",        -1.);
  configMgr->treeMaker->addFloatVariable("dPhiPllMetTrack",   -1.);
  configMgr->treeMaker->addFloatVariable("hasZCand",   0.0);

  configMgr->treeMaker->addFloatVariable("METRel",            -1.);
  configMgr->treeMaker->addFloatVariable("METTrackRel",       -1.);
  configMgr->treeMaker->addFloatVariable("dPhiNearMet",       -1.);
  configMgr->treeMaker->addFloatVariable("dPhiNearMetTrack",  -1.);
  configMgr->treeMaker->addFloatVariable("dPhiMetAndMetTrack",-1.);

  configMgr->treeMaker->addFloatVariable("MTauTau",          -99999.);
  configMgr->treeMaker->addFloatVariable("MTauTau_metTrack", -99999.);
  configMgr->treeMaker->addFloatVariable("RjlOverEl",     -1.);
  configMgr->treeMaker->addFloatVariable("LepCosThetaLab",-1.);
  configMgr->treeMaker->addFloatVariable("LepCosThetaCoM",-1.);

  // mt2(L1,L2,MET) with various trial invisible particle masses
  configMgr->treeMaker->addFloatVariable("mt2leplsp_0",   -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_50",  -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_100", -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_150", -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_200", -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_300", -1.);

  configMgr->treeMaker->addFloatVariable("mt2leplsp_0_metTrack",   -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_50_metTrack",  -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_100_metTrack", -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_150_metTrack", -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_200_metTrack", -1.);
  configMgr->treeMaker->addFloatVariable("mt2leplsp_300_metTrack", -1.);

  // 3L specific variables
  configMgr->treeMaker->addIntVariable(  "nSFOS",              0);
  configMgr->treeMaker->addFloatVariable("mt_minMll",          -1.);
  configMgr->treeMaker->addFloatVariable("minMll",             -1.);
  configMgr->treeMaker->addFloatVariable("minRll",             -1.);
  configMgr->treeMaker->addFloatVariable("m3l",                -1.);
  configMgr->treeMaker->addFloatVariable("pT3l",               -1.);
  configMgr->treeMaker->addFloatVariable("dPhi3lMet",          -1.);
  configMgr->treeMaker->addFloatVariable("ptZ_minMll",         -1.);
  configMgr->treeMaker->addFloatVariable("ptW_minMll",         -1.);
  configMgr->treeMaker->addFloatVariable("ptWlep_minMll",      -1.);
  configMgr->treeMaker->addFloatVariable("RZlep_minMll",       -1.);
  configMgr->treeMaker->addFloatVariable("RZWlep_minMll",      -1.);
  configMgr->treeMaker->addFloatVariable("dPhiZMet_minMll",    -1.);
  configMgr->treeMaker->addFloatVariable("dPhiWLepMet_minMll", -1.);
  configMgr->treeMaker->addFloatVariable("mt2WZlsp_0",         -1.);
  configMgr->treeMaker->addFloatVariable("mt2WZlsp_100",       -1.);

  // Weights
  configMgr->treeMaker->addDoubleVariable("pileupWeight",  1.0);
  configMgr->treeMaker->addDoubleVariable("leptonWeight",  1.0);
  configMgr->treeMaker->addDoubleVariable("eventWeight",   1.0);
  configMgr->treeMaker->addDoubleVariable("genWeight",     1.0);
  configMgr->treeMaker->addDoubleVariable("bTagWeight",    1.0);
  configMgr->treeMaker->addDoubleVariable("jvtWeight",     1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightUp",   1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightDown", 1.0);
  configMgr->treeMaker->addDoubleVariable("triggerWeight", 1.0);
  configMgr->treeMaker->addDoubleVariable("totalWeight", 1.0);
  

  // For Wino-Bino reweighting
  configMgr->treeMaker->addDoubleVariable("truthMll",-999);
  configMgr->treeMaker->addDoubleVariable("winoBinoMllWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("winoBinoXsecWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("winoBinoBrFracWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("winoBinoWeight",1.0);

  //NUHM2
  configMgr->treeMaker->addDoubleVariable("NUHM2weight_350m12",1.0);
  configMgr->treeMaker->addDoubleVariable("NUHM2weight_400m12",1.0);
  configMgr->treeMaker->addDoubleVariable("NUHM2weight_500m12",1.0);
  configMgr->treeMaker->addDoubleVariable("NUHM2weight_600m12",1.0);
  configMgr->treeMaker->addDoubleVariable("NUHM2weight_700m12",1.0);
  configMgr->treeMaker->addDoubleVariable("NUHM2weight_800m12",1.0);

  // For use in FF estimate (which is a post-processing step, so we
  // only create these branches here and give them dummy values)
  configMgr->treeMaker->addDoubleVariable("FFWeight",1.0);
  configMgr->treeMaker->addIntVariable("nLep_antiID",0);
  configMgr->treeMaker->addBoolVariable("lep1AntiID",false);
  configMgr->treeMaker->addBoolVariable("lep2AntiID",false);
  configMgr->treeMaker->addBoolVariable("lep3AntiID",false);
  configMgr->treeMaker->addIntVariable("nLep_signalActual",0);
  configMgr->treeMaker->addBoolVariable("lep1SignalActual",false);
  configMgr->treeMaker->addBoolVariable("lep2SignalActual",false);
  configMgr->treeMaker->addBoolVariable("lep3SignalActual",false);

  // SF for ETMiss triggers
  configMgr->treeMaker->addDoubleVariable("ETMissTrigSF",1.0);

  // MET trigger decision from SUSYTools
  configMgr->treeMaker->addBoolVariable("IsMETTrigPassed",false);

  // Write individual trigger decisions for supporting studies
  this->triggerChains =  {
    // ETmiss (contained in addTriggerAna)
    "HLT_xe70",
    "HLT_xe70_mht",
    "HLT_xe90_mht_L1XE50",
    "HLT_xe100_mht_L1XE50",
    "HLT_xe110_mht_L1XE50",
    "HLT_xe110_pufit_L1XE55",
    "HLT_xe110_pufit_xe70_L1XE50",
    "HLT_xe110_pufit_xe65_L1XE50",
    // single muon (contained in addTriggerAna)
    "HLT_mu20_iloose_L1MU15",
    "HLT_mu26_ivarmedium",
    "HLT_mu50",
    "HLT_mu60_0eta105_msonly",
    // di-muon (contained in addTriggerAna)
    "HLT_mu18_mu8noL1",
    "HLT_mu22_mu8noL1",
    // single electron (contained in addTriggerAna)
    "HLT_e24_lhmedium_L1EM20VH",
    "HLT_e24_lhtight_nod0_ivarloose",
    "HLT_e26_lhtight_nod0",
    "HLT_e60_lhmedium",
    "HLT_e60_lhmedium_nod0",
    "HLT_e120_lhloose",
    "HLT_e140_lhloose_nod0",
    "HLT_e300_etcut",
    // di-electron (contained in addTriggerAna)
    "HLT_2e12_lhloose_L12EM10VH",
    "HLT_2e17_lhvloose_nod0",
    "HLT_2e24_lhvloose_nod0",
    "LT_2e17_lhvloose_nod0_L12EM15VHI",
    // prescaled single-lepton, e.g. for fake factor measurements
    "HLT_mu4",
    "HLT_mu10",
    "HLT_mu10_idperf",
    "HLT_mu14",
    "HLT_mu18",
    "HLT_e5_lhvloose",
    "HLT_e5_lhvloose_nod0",
    "HLT_e10_lhvloose_L1EM7",
    "HLT_e12_lhvloose_nod0_L1EM10VH",
    "HLT_e17_lhvloose_nod0"
    "HLT_e15_lhvloose_nod0_L1EM7",
    "HLT_e15_lhvloose_L1EM13VH",
    "HLT_e20_lhvloose",
    "HLT_e20_lhvloose_nod0"
  };  
  for (const char* chainName : this->triggerChains) {
    configMgr->treeMaker->addBoolVariable(chainName, false);
  }

  // Do the same for L1 triggers
  // currently not really needed, restrict to L1 seeds of ETmiss triggers
  this->L1Chains = {
    "L1_XE30",
    "L1_XE40",
    "L1_XE50",
    "L1_XE55",
  };

  for (const char* chainName : this->L1Chains) {
    configMgr->treeMaker->addBoolVariable(chainName, false);
  }

  // PDF variables
  configMgr->treeMaker->addFloatVariable("x1",       -1.0);
  configMgr->treeMaker->addFloatVariable("x2",       -1.0);
  configMgr->treeMaker->addFloatVariable("pdf1",     -1.0);
  configMgr->treeMaker->addFloatVariable("pdf2",     -1.0);
  configMgr->treeMaker->addFloatVariable("scalePDF", -1.0);
  configMgr->treeMaker->addIntVariable("id1", 0);
  configMgr->treeMaker->addIntVariable("id2", 0);

  // Generator filtering variables (decorations calculated at derivation level)
  configMgr->treeMaker->addFloatVariable("GenFiltMET", -1.0);
  configMgr->treeMaker->addFloatVariable("GenFiltHT", -1.0);

  // RJR variables !
  configMgr->treeMaker->addDoubleVariable("RJR_PTISR",    -1.0);
  configMgr->treeMaker->addDoubleVariable("RJR_RISR",     -1.0);
  configMgr->treeMaker->addDoubleVariable("RJR_MS",       -1.0);
  configMgr->treeMaker->addDoubleVariable("RJR_MISR",       -1.0);
  configMgr->treeMaker->addDoubleVariable("RJR_dphiISRI", -1.0);
  configMgr->treeMaker->addIntVariable("RJR_NjV",         -1);
  configMgr->treeMaker->addIntVariable("RJR_NjISR",       -1);

  // All of the VBF varialbes
  //vbfSetup(configMgr);

  // Pt of SUSY-system and weight for ISR reweighting
  configMgr->treeMaker->addDoubleVariable("SUSYPt",-1.0);
  configMgr->treeMaker->addDoubleVariable("ISRWeight", 1.0);

  // Set lumi:
  // We want to set the lumis proportionally
  // dependent on year. i.e. set 2015+2016 to 36100/80000, and similar
  // for 2017. Then when we add the samples together, the total lumi weight
  // will be 1 pb-1, which we can then scale up
  //
  // 2015 in R21: 3219.56
  // 2016 in R21: 32988.1
  // 2017 in R21: 43813.7
  //
  
  /*
  if(configMgr->obj->evt.isMC){

    // Note 320000 comes from SUSYTools' treatAsYear function
    if(configMgr->obj->evt.randomRunNumber < 320000){
      // 2015+2016 lumi / total lumi
      configMgr->objectTools->setLumi(0.452475);
    }
    else{
      // 2017 lumi / total lumi
      configMgr->objectTools->setLumi(0.547525);
    }

  }
  */

  // Object class contains the definitions of all physics objects, eg muons, electrons, jets
  // See xAODNtupleMaker::Objects for available methods; configMgr->obj

  PrepareBDTConfig();
  PreparePLTWP();

}
// ------------------------------------------------------------------------------------------ //
bool Common::doAnalysis(ConfigMgr*& configMgr)
{
  /*
    This is the main method, which is called for each event
  */
  
  //Jet Cleaning Variables
  configMgr->treeMaker->setBoolVariable("passEmulTightJetCleaning1Jet", configMgr->obj->evt.passEvtCleaning(EventVariable::EventCleaning::EmulEvtTightBad1Jet));
  configMgr->treeMaker->setBoolVariable("passEmulTightJetCleaning2Jet", configMgr->obj->evt.passEvtCleaning(EventVariable::EventCleaning::EmulEvtTightBad2Jet));
  
  bool passBadTileJetVeto=true;
  for(auto jet : configMgr->obj->baselineJets){
    if(jet->badTile) {  passBadTileJetVeto=false; break; }
  }
  configMgr->treeMaker->setBoolVariable("passBadTileJetVeto", passBadTileJetVeto);

  bool useBaseline = true; // to calculate our observables using baseline leptons rather than signal leptons
  bool useTrackMET = true; // for calculating only SOME variables using track-based MET
  int nLep_base = configMgr->obj->baseLeptons.size();

  // We requested at least two baseline leptons in the skim
  //const LeptonVariable* lep1 = configMgr->obj->baseLeptons[0];
  //const LeptonVariable* lep1 = nLep_base < 1 ? new LeptonVariable() : configMgr->obj->baseLeptons[0];
  //const LeptonVariable* lep2 = nLep_base < 2 ? new LeptonVariable() : configMgr->obj->baseLeptons[1];
  //const LeptonVariable* lep3 = nLep_base < 3 ? new LeptonVariable() : configMgr->obj->baseLeptons[2];
  LeptonVariable* lep1 = nLep_base < 1 ? new LeptonVariable() : configMgr->obj->baseLeptons[0];
  LeptonVariable* lep2 = nLep_base < 2 ? new LeptonVariable() : configMgr->obj->baseLeptons[1];
  LeptonVariable* lep3 = nLep_base < 3 ? new LeptonVariable() : configMgr->obj->baseLeptons[2];

  // Tau
  int nTau_base = configMgr->obj->baseTaus.size();
  TauVariable* tau1 = nTau_base < 1 ? new TauVariable() : configMgr->obj->baseTaus[0];
  TauVariable* tau2 = nTau_base < 2 ? new TauVariable() : configMgr->obj->baseTaus[1];

  // Truth information for ttbar
  int intTTbarDecayMode = -1;
  if( configMgr->obj->truthEvent.TTbarTLVs.size()>0 ){

    // Decay mode
    TruthEvent::TTbarDecayMode decayMode = configMgr->obj->truthEvent.decayMode;
    intTTbarDecayMode = (int)decayMode;

  }

  // Truth info for tau decay from W
  int nTruthTauFromW = configMgr->obj->truthEvent.nTruthTauFromW;
  std::vector<int> TruthTauFromW_DMV;
  if (nTruthTauFromW > 0)
  {
    TruthTauFromW_DMV = configMgr->obj->truthEvent.TruthTauFromW_DMV;
  }

  float mjj   = -1.0;
  float dEtajj = -1.0;
  float dPhijj = -1.0;

  // Consider all (central and forward) jets for dijet calculations
  if (configMgr->obj->aJets.size() >= 2 ) {
    const auto *jet1 = configMgr->obj->aJets[0];
    const auto *jet2 = configMgr->obj->aJets[1];
    //(*(configMgr->obj->aJets[0])+*(configMgr->obj->aJets[1])).M();
    mjj   = ( *jet1 + *jet2 ).M();
    dEtajj = fabs( jet1->Eta() - jet2->Eta() );
    dPhijj = fabs( jet1->DeltaPhi( *jet2 ) );
  }

  // Inclusive Meff, all jets with pT>30 GeV
  float meffInc30 = obs.getMeff( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Ht: Scalar sum of jets
  float Ht30 = obs.getHt( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Aplanarity
  float JetAplanarity = obs.getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),0.0, 30.0, useBaseline );
  float LepAplanarity = obs.getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),configMgr->obj->signalLeptons.size(), 30.0, useBaseline );

  // Jet multiplicity and kinematics
  int nJet30  = obs.getNJets( configMgr->obj, 30.0 );

  std::vector<float> jetPtVec;
  std::vector<float> jetEtaVec;
  std::vector<float> jetPhiVec;
  std::vector<float> jetMVec;
  std::vector<float> jetBtaggedVec;
  std::vector<float> jetDL1rVec;
  std::vector<float> jetTileEnergy;

  for(JetVariable* jet : configMgr->obj->cJets){

    jetPtVec.push_back(jet->Pt());
    jetEtaVec.push_back(jet->Eta());
    jetPhiVec.push_back(jet->Phi());
    jetMVec.push_back(jet->M());
    jetBtaggedVec.push_back(jet->bjet);
    jetDL1rVec.push_back(jet->dl1r);

    // For background forum checklist
    jetTileEnergy.push_back(jet->tileEnergy);

  }

  // B-jets
  int nBJet30 = obs.getNBJets( configMgr->obj, 30.0 );
  int nBJet25 = obs.getNBJets( configMgr->obj, 25.0 );
  int nBJet20 = obs.getNBJets( configMgr->obj, 20.0 );

  // Let's store a lot of b-jet WPs
  int nBJet20_DL1r_FixedCutBEff_60=0;
  int nBJet20_DL1r_FixedCutBEff_70=0;
  int nBJet20_DL1r_FixedCutBEff_77=0;
  int nBJet20_DL1r_FixedCutBEff_85=0;

  int nBJet25_DL1r_FixedCutBEff_60=0;
  int nBJet25_DL1r_FixedCutBEff_70=0;
  int nBJet25_DL1r_FixedCutBEff_77=0;
  int nBJet25_DL1r_FixedCutBEff_85=0;

  for( auto& jet : configMgr->obj->cJets ){

    // The pT cut is redundant, and possibly the eta cut is built into the
    // DL1r definition, but better to keep them both
    if( fabs(jet->Eta()) > 2.5 || jet->Pt()<20.0 ) continue;

    // Different WPs, based on:
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21#DL1rnn_tagger_AN3
    if( jet->dl1r >= 4.31 ) nBJet20_DL1r_FixedCutBEff_60++;
    if( jet->dl1r >= 2.96 ) nBJet20_DL1r_FixedCutBEff_70++;
    if( jet->dl1r >= 2.20 ) nBJet20_DL1r_FixedCutBEff_77++;
    if( jet->dl1r >= 1.27 ) nBJet20_DL1r_FixedCutBEff_85++;

    if(jet->Pt() < 25.0) continue;
    if( jet->dl1r >= 4.31 ) nBJet25_DL1r_FixedCutBEff_60++;
    if( jet->dl1r >= 2.96 ) nBJet25_DL1r_FixedCutBEff_70++;
    if( jet->dl1r >= 2.20 ) nBJet25_DL1r_FixedCutBEff_77++;
    if( jet->dl1r >= 1.27 ) nBJet25_DL1r_FixedCutBEff_85++;

  }

  // MET
  float met   = configMgr->obj->met.Et;

  // Track-based MET
  float metTrack   = configMgr->obj->trackMet.Et;

  // Other analysis variables
  float mt_lep1  = obs.getMt(configMgr->obj, useBaseline, 0); // leading lepton
  float mt_lep2  = nLep_base < 2 ? -999 : obs.getMt(configMgr->obj, useBaseline, 1); // subleading lepton
  float mt_lep3  = nLep_base < 3 ? -999 : obs.getMt(configMgr->obj, useBaseline, 2); // 3rd lepton

  float mt_lep1_metTrack  = obs.getMt(configMgr->obj, useBaseline, 0, useTrackMET); // leading lepton
  float mt_lep2_metTrack  = nLep_base < 2 ? -999 : obs.getMt(configMgr->obj, useBaseline, 1, useTrackMET); // subleading lepton
  float mt_lep3_metTrack  = nLep_base < 3 ? -999 : obs.getMt(configMgr->obj, useBaseline, 2, useTrackMET); // 3rd lepton

  float METOverHT      = met / Ht30;
  float METTrackOverHT = metTrack / Ht30;

  float METOverJ1pT = configMgr->obj->cJets.size() < 1 ? -999 : met / configMgr->obj->cJets[0]->Pt();
  float METTrackOverJ1pT = configMgr->obj->cJets.size() < 1 ? -999 : metTrack / configMgr->obj->cJets[0]->Pt();

  float HTLep = 0;
  for (LeptonVariable* lep: configMgr->obj->baseLeptons){
    HTLep += lep->Pt();
  }
  float METOverHTLep  = HTLep > 0 ? met / HTLep : -999;
  float METTrackOverHTLep  = HTLep > 0 ? metTrack / HTLep : -999;

  float DPhiJ1Met       = obs.getDPhiJNMet(configMgr->obj, 0, 30.0);
  float DPhiJ2Met       = obs.getDPhiJNMet(configMgr->obj, 1, 30.0);
  float DPhiJ3Met       = obs.getDPhiJNMet(configMgr->obj, 2, 30.0);
  float DPhiJ4Met       = obs.getDPhiJNMet(configMgr->obj, 3, 30.0);

  float DPhiJ1MetTrack       = obs.getDPhiJNMet(configMgr->obj, 0, 30.0, useTrackMET);
  float DPhiJ2MetTrack       = obs.getDPhiJNMet(configMgr->obj, 1, 30.0, useTrackMET);
  float DPhiJ3MetTrack       = obs.getDPhiJNMet(configMgr->obj, 2, 30.0, useTrackMET);
  float DPhiJ4MetTrack       = obs.getDPhiJNMet(configMgr->obj, 3, 30.0, useTrackMET);

  float minDPhiCenJetsMet = 99999;
  float minDPhiCenJetsMetTrack = 99999;
  for(unsigned int jetIndex = 0; jetIndex < configMgr->obj->cJets.size(); ++jetIndex){
    minDPhiCenJetsMet = std::min(minDPhiCenJetsMet, obs.getDPhiJNMet(configMgr->obj, jetIndex, 30.0));
    minDPhiCenJetsMetTrack = std::min(minDPhiCenJetsMetTrack, obs.getDPhiJNMet(configMgr->obj, jetIndex, 30.0, useTrackMET));
  }

  float minDPhiAllJetsMet = 99999;
  float minDPhiAllJetsMet_LepInvis = 99999;
  for(unsigned int jetIndex = 0; jetIndex < configMgr->obj->aJets.size(); ++jetIndex){
    minDPhiAllJetsMet = std::min(minDPhiAllJetsMet, getDPhiAllJNMet(configMgr->obj, jetIndex, 30.0));
    minDPhiAllJetsMet_LepInvis = std::min(minDPhiAllJetsMet_LepInvis, getDPhiAllJNMet(configMgr->obj, jetIndex, 30.0, false, true));
  }
  

  // 2L Analysis variables
  float mll           = obs.getMll(configMgr->obj, useBaseline);
  float Rll           = obs.getRll(configMgr->obj);
  float Ptll          = (*lep1 + *lep2).Pt();

  float dPhiPllMet      = (*lep1 + *lep2).DeltaPhi(configMgr->obj->met);
  float dPhiPllMetTrack = (*lep1 + *lep2).DeltaPhi(configMgr->obj->trackMet);

  bool hasZCand = hasZCandidate(configMgr->obj, false, 20.0);

  // METRel
  float dPhiNearMet = 99999;
  float dPhiNearMetTrack = 99999;

  // FIXME Ideally just rely on the definition inside of the Observables class,
  // but right now our cJet pT definition differs from what we use for our
  // signal jets, so it won't quite work yet
  for(LeptonVariable* lep : configMgr->obj->baseLeptons){
    float tmp1 = fabs(lep->DeltaPhi(configMgr->obj->met));
    if( tmp1 < dPhiNearMet ) dPhiNearMet = tmp1;

    float tmp2 = fabs(lep->DeltaPhi(configMgr->obj->trackMet));
    if( tmp2 < dPhiNearMetTrack ) dPhiNearMetTrack = tmp2;
  }
  for(JetVariable* jet : configMgr->obj->cJets){

    // Only use signal jets
    if( fabs(jet->Eta()) > 2.8 || jet->Pt() < 30.0 ) continue;

    float tmp1 = fabs(jet->DeltaPhi(configMgr->obj->met));
    if( tmp1 < dPhiNearMet ) dPhiNearMet = tmp1;

    float tmp2 = fabs(jet->DeltaPhi(configMgr->obj->trackMet));
    if( tmp2 < dPhiNearMetTrack ) dPhiNearMetTrack = tmp2;
  }

  float METRel = met;
  if(dPhiNearMet < M_PI/2) METRel = met * TMath::Sin(dPhiNearMet);

  float METTrackRel = metTrack;
  if(dPhiNearMetTrack < M_PI/2) METTrackRel = metTrack * TMath::Sin(dPhiNearMetTrack);


  // dPhi between our two MET definitions
  float dPhiMetAndMetTrack = configMgr->obj->met.DeltaPhi(configMgr->obj->trackMet);

  float RjlOverEl     = obs.getRjlOverEl(configMgr->obj);

  // MTauTau definitions
  float MSqTauTau_2   = obs.getMSqTauTau(configMgr->obj, 2); // version 2 : more natural definition
  float MSqTauTau_2_metTrack   = obs.getMSqTauTau(configMgr->obj, 2, useTrackMET); // version 2 : more natural definition

  // Take signed square root of MSqTauTau_2
  float MTauTau       = -99999.;
  if (MSqTauTau_2 >= 0.) MTauTau =  sqrt( MSqTauTau_2 );
  if (MSqTauTau_2 < 0.)  MTauTau = -sqrt( fabs( MSqTauTau_2 ) );

  // Take signed square root of MSqTauTau_2
  float MTauTau_metTrack       = -99999.;
  if (MSqTauTau_2_metTrack >= 0.) MTauTau_metTrack =  sqrt( MSqTauTau_2_metTrack );
  if (MSqTauTau_2_metTrack < 0.)  MTauTau_metTrack = -sqrt( fabs( MSqTauTau_2_metTrack ) );

  // lepton-beam polar angle
  float LepCosThetaLab  = obs.getLepCosThetaLab(configMgr->obj);
  float LepCosThetaCoM  = obs.getLepCosThetaCoM(configMgr->obj);


  // mt2(L1,L2,MET) with various trial invisible particle masses
  float mt2leplsp_0   = obs.getMt2(configMgr->obj, obs.MT2LL, 0.,   useBaseline);
  float mt2leplsp_50  = obs.getMt2(configMgr->obj, obs.MT2LL, 50.,  useBaseline);
  float mt2leplsp_100 = obs.getMt2(configMgr->obj, obs.MT2LL, 100., useBaseline);
  float mt2leplsp_150 = obs.getMt2(configMgr->obj, obs.MT2LL, 150., useBaseline);
  float mt2leplsp_200 = obs.getMt2(configMgr->obj, obs.MT2LL, 200., useBaseline);
  float mt2leplsp_300 = obs.getMt2(configMgr->obj, obs.MT2LL, 300., useBaseline);

  float mt2leplsp_0_metTrack   = obs.getMt2(configMgr->obj, obs.MT2LL, 0.,   useBaseline, useTrackMET);
  float mt2leplsp_50_metTrack  = obs.getMt2(configMgr->obj, obs.MT2LL, 50.,  useBaseline, useTrackMET);
  float mt2leplsp_100_metTrack = obs.getMt2(configMgr->obj, obs.MT2LL, 100., useBaseline, useTrackMET);
  float mt2leplsp_150_metTrack = obs.getMt2(configMgr->obj, obs.MT2LL, 150., useBaseline, useTrackMET);
  float mt2leplsp_200_metTrack = obs.getMt2(configMgr->obj, obs.MT2LL, 200., useBaseline, useTrackMET);
  float mt2leplsp_300_metTrack = obs.getMt2(configMgr->obj, obs.MT2LL, 300., useBaseline, useTrackMET);

  // 3L variables targeting WZ > lvll
  // Using minMll assignment (treat lepton pair with minimum invariant mass as that from Z(*) boson)
  // From https://gitlab.cern.ch/atlas-phys-susy-higgsino/SusySkimHiggsino/blob/v2.1/Root/CompressedSelector.cxx

  int nSFOS = 0;
  int leadZindex_minMll = -1;
  int subZindex_minMll  = -1;
  int Windex_minMll     = -1;
  float minMll          = 999. , minRll        = 999. , mt_minMll          = -999. ;
  float m3l = -1., pT3l = -1.  , dPhi3lMet     = -1.  , ptZ_minMll         = -1.   , ptW_minMll = -1.  , ptWlep_minMll = -1.;
  float dPhiZMet_minMll = -1.  , RZWlep_minMll = -1.  , dPhiWLepMet_minMll = -1.   , RZlep_minMll = -1.;
  float mt2WZlsp_0      = -1.  , mt2WZlsp_100  = -1.  ;

  // Consider 2 or 3 leptons
  if (nLep_base >= 2 && nLep_base <= 3) {

    // Loop through all pairwise combinations of leptons
    for(int ilep=0; ilep<nLep_base-1; ilep++){
      for(int jlep=ilep+1; jlep<nLep_base; jlep++){
        int klep = nLep_base - ilep - jlep;
        LeptonVariable* tmp_lep1 = configMgr->obj->baseLeptons[ilep];
        LeptonVariable* tmp_lep2 = configMgr->obj->baseLeptons[jlep];

        // Find minimum interlepton distance (in case we need to clean very nearby fakes)
        float tmp_Rll = ( *tmp_lep1 ).DeltaR( *tmp_lep2 );
        if (tmp_Rll < minRll){
          minRll = tmp_Rll;
        }

        // For nLep_base = 2, tmp_mll equals mll
        float tmp_mll = ( *tmp_lep1 + *tmp_lep2 ).M();
        // For nLep_base = 2, set mt_minMll = mt_lep1
        // For nLep_base > 2, mt_minMll is the remainng lepton not used to calculate mll
        float tmp_mt = mt_lep1;
        if ( nLep_base >= 3 ) { tmp_mt = obs.getMt(configMgr->obj, useBaseline, klep) ; }

        // If there is a SFOS pair, advance the nSFOS counter
        bool SF = (tmp_lep1->isEle() && tmp_lep2->isEle()) || (tmp_lep1->isMu() && tmp_lep2->isMu());
        bool OS = (tmp_lep1->q == -tmp_lep2->q);
        if(SF && OS){ nSFOS++; }

        // This logic also has non-SFOS pairs into the caluclations, so signal region requires "nSFOS >= 1"
        // This means we can also select the orthogonal "nSFOS == 0" as a VR (VRDF or VRSS in limit nLep_base = 2)
        // minMll assignment
        if (tmp_mll < minMll){
            mt_minMll = tmp_mt;
            minMll    = tmp_mll;
            leadZindex_minMll = ilep;
            subZindex_minMll  = jlep;
            Windex_minMll     = klep;
          }
      }
    }

    // Need 3 leptons to meaningfully assign leptons to Z or W
    //if (nLep_base >= 3) {
    if (nLep_base >= 3 && leadZindex_minMll < 3 && subZindex_minMll < 3 && Windex_minMll < 3 && 
    leadZindex_minMll >= 0 && subZindex_minMll >=0  && Windex_minMll >= 0) {
      LeptonVariable* z1Lep_minMll = configMgr->obj->baseLeptons[leadZindex_minMll];
      LeptonVariable* z2Lep_minMll = configMgr->obj->baseLeptons[subZindex_minMll];
      LeptonVariable* wLep_minMll  = configMgr->obj->baseLeptons[Windex_minMll];

      m3l                = (*lep1 + *lep2 + *lep3).M();
      pT3l               = (*lep1 + *lep2 + *lep3).Pt();
      dPhi3lMet          = fabs( (*lep1 + *lep2 + *lep3).DeltaPhi( configMgr->obj->met ) );
      ptZ_minMll         = (*z1Lep_minMll + *z2Lep_minMll).Pt();
      ptW_minMll         = (*wLep_minMll  + configMgr->obj->met ).Pt();
      ptWlep_minMll      = (*wLep_minMll ).Pt();
      dPhiZMet_minMll    = fabs( (*z1Lep_minMll + *z2Lep_minMll).DeltaPhi( configMgr->obj->met ) );
      RZWlep_minMll      = (*z1Lep_minMll + *z2Lep_minMll).DeltaR( *wLep_minMll );
      dPhiWLepMet_minMll = fabs( wLep_minMll->DeltaPhi( configMgr->obj->met) );
      RZlep_minMll       = z1Lep_minMll->DeltaR( *z2Lep_minMll );

      // Calculate mT2 using leptons identified with W and Z
      ComputeMT2 mymt2_0 = ComputeMT2(*z1Lep_minMll + *z2Lep_minMll, *wLep_minMll, configMgr->obj->met, 0.);
      mt2WZlsp_0 = mymt2_0.Compute();
      ComputeMT2 mymt2_100 = ComputeMT2(*z1Lep_minMll + *z2Lep_minMll, *wLep_minMll, configMgr->obj->met, 100.);
      mt2WZlsp_100 = mymt2_100.Compute();

    } // end nLep_base >= 3
  } // end nLep_base >= 2 && nLep_base <= 3


  // calculate pT of SUSY system
  double SUSYPt = -1.0;
  // temporary access Truth Record directly for sleptons
  // DSIDs based on this file https://gitlab.cern.ch/atlas-phys-susy-higgsino/SusySkimHiggsino/blob/ae7c054fd4ce27ab9b09d9904dcac3bb28596b19/data/samples/R21_SUSY16_Signal_mc16cd/MGPy8EG_A14N23LO_SlepSlep_direct_2L2MET75_mc16cd.txt
  if (configMgr->obj->evt.dsid >= 395890 && configMgr->obj->evt.dsid <= 396041){
    TruthVector truth = configMgr->obj->truth;
    TruthVariable* slepton1 = 0;
    TruthVariable* slepton2 = 0;
    int n_sleptons = 0;
    for(TruthVariable* &p : truth){
      if (abs(p->pdgId) == 1000011 || abs(p->pdgId) == 1000013 || abs(p->pdgId) == 1000015
          || abs(p->pdgId) == 2000011 || abs(p->pdgId) == 2000013 || abs(p->pdgId) == 2000015){
        // std::cout << p << std::endl;
        // std::cout << "ID: " << p->pdgId << " Status: " << p->status << " Pt: " << p->Pt() << " Eta: " << p->Eta() << std::endl;
        if (!p) continue;
        if (!slepton1)
          slepton1 = p;
        else
          slepton2 = p;
        n_sleptons++;
      }
    }
    if (slepton1 && slepton2 && n_sleptons == 2) {
      SUSYPt = (*slepton1 + *slepton2).Pt();
    }
  }
  // otherwise use TruthDecayContainer
  else{
    TruthEvent_XX te = configMgr->obj->truthEvent_XX;
    SUSYPt = (te.pchi1 + te.pchi2).Pt();
  }

  // retrieve ISR weight based on SUSYPt
  double ISRWeight = retrieve_ISRWeight(SUSYPt);

  //
  // -------- Fill the output tree variables ---------- //
  //

  // Global Vars
  configMgr->treeMaker->setFloatVariable("mu",configMgr->obj->evt.mu); // this is avg_mu or actual_mu dependent on the period
  configMgr->treeMaker->setFloatVariable("actual_mu",configMgr->obj->evt.actual_mu);
  configMgr->treeMaker->setFloatVariable("avg_mu",configMgr->obj->evt.avg_mu);
  configMgr->treeMaker->setIntVariable("nVtx",configMgr->obj->evt.nVtx);

  //RJR variables
  //computeJigsawVariables(configMgr);

  // Leptons
  configMgr->treeMaker->setIntVariable("nLep_base",   nLep_base);
  configMgr->treeMaker->setIntVariable("nLep_signal", configMgr->obj->signalLeptons.size());
  configMgr->treeMaker->setIntVariable("nEle_base", configMgr->obj->baseElectrons.size());
  configMgr->treeMaker->setIntVariable("nEle_signal", configMgr->obj->signalElectrons.size());
  configMgr->treeMaker->setIntVariable("nMu_base",  configMgr->obj->baseMuons.size());
  configMgr->treeMaker->setIntVariable("nMu_signal",  configMgr->obj->signalMuons.size());
  bool corrected =  setCorrPLTInputVariables(configMgr, "lep1", "lep2", lep1, lep2);  // Near-by-lepton corrected PromptLeptonTagger inputs
  if(!corrected) corrected = setCorrPLTInputVariables(configMgr, "lep1", "lep3", lep1, lep3); // if lep1-lep2 are not corrected, correct lep1-lep3
  if(!corrected) corrected = setCorrPLTInputVariables(configMgr, "lep2", "lep3", lep2, lep3); // if lep1-lep3 are not corrected, correct lep2-lep3
  setLeptonVariables(configMgr, "lep1", lep1);   // set lep1Pt, lep1Eta etc... here
  setLeptonVariables(configMgr, "lep2", lep2);
  setLeptonVariables(configMgr, "lep3", lep3);
  setPLT_nLep_signal(configMgr);

  // Taus
  configMgr->treeMaker->setIntVariable("nTau_base",   nTau_base);
  configMgr->treeMaker->setIntVariable("nTau_signal", configMgr->obj->signalTaus.size());
  setTauVariables(configMgr, "tau1", tau1);
  setTauVariables(configMgr, "tau2", tau2);

  // Jets
  configMgr->treeMaker->setIntVariable("nJet30", nJet30);
  configMgr->treeMaker->setIntVariable("nJet25", obs.getNJets( configMgr->obj, 25.0 ));
  configMgr->treeMaker->setIntVariable("nJet20", obs.getNJets( configMgr->obj, 20.0 ));
  configMgr->treeMaker->setIntVariable("nBJet30", nBJet30);
  configMgr->treeMaker->setIntVariable("nBJet25", nBJet25);
  configMgr->treeMaker->setIntVariable("nBJet20", nBJet20);
  configMgr->treeMaker->setIntVariable("nTotalJet", obs.getNJets( configMgr->obj, 25.0 ) + configMgr->obj->fJets.size() );
  configMgr->treeMaker->setIntVariable("nTotalJet20", obs.getNJets( configMgr->obj, 20.0 ) + configMgr->obj->fJets.size() );
  
  configMgr->treeMaker->setIntVariable("nBJet20_DL1r_FixedCutBEff_60",nBJet20_DL1r_FixedCutBEff_60);
  configMgr->treeMaker->setIntVariable("nBJet20_DL1r_FixedCutBEff_70",nBJet20_DL1r_FixedCutBEff_70);
  configMgr->treeMaker->setIntVariable("nBJet20_DL1r_FixedCutBEff_77",nBJet20_DL1r_FixedCutBEff_77);
  configMgr->treeMaker->setIntVariable("nBJet20_DL1r_FixedCutBEff_85",nBJet20_DL1r_FixedCutBEff_85);
  configMgr->treeMaker->setIntVariable("nBJet25_DL1r_FixedCutBEff_60",nBJet25_DL1r_FixedCutBEff_60);
  configMgr->treeMaker->setIntVariable("nBJet25_DL1r_FixedCutBEff_70",nBJet25_DL1r_FixedCutBEff_70);
  configMgr->treeMaker->setIntVariable("nBJet25_DL1r_FixedCutBEff_77",nBJet25_DL1r_FixedCutBEff_77);
  configMgr->treeMaker->setIntVariable("nBJet25_DL1r_FixedCutBEff_85",nBJet25_DL1r_FixedCutBEff_85);
  
  configMgr->treeMaker->setIntVariable("DecayModeTTbar",intTTbarDecayMode);

  configMgr->treeMaker->setIntVariable ("truthTauFromW_n",nTruthTauFromW);
  configMgr->treeMaker->setVecIntVariable("truthTauFromW_DMV",TruthTauFromW_DMV);

  if (m_addJets) {
    configMgr->treeMaker->setVecFloatVariable("jetPt",jetPtVec);
    configMgr->treeMaker->setVecFloatVariable("jetEta",jetEtaVec);
    configMgr->treeMaker->setVecFloatVariable("jetPhi",jetPhiVec);
    configMgr->treeMaker->setVecFloatVariable("jetM",jetMVec);
    configMgr->treeMaker->setVecFloatVariable("jetBtagged",jetBtaggedVec);
    configMgr->treeMaker->setVecFloatVariable("jetDL1r",jetDL1rVec);    
    configMgr->treeMaker->setVecFloatVariable("jetTileEnergy",jetTileEnergy); 
  }

  // MET
  configMgr->treeMaker->setFloatVariable("met_Et",               configMgr->obj->met.Et);
  configMgr->treeMaker->setFloatVariable("met_Phi",              configMgr->obj->met.phi );
  configMgr->treeMaker->setFloatVariable("met_Signif",           configMgr->obj->met.metSignif);
  configMgr->treeMaker->setFloatVariable("TST_Et",               configMgr->obj->met.Et_soft );
  configMgr->treeMaker->setFloatVariable("TST_Phi",              configMgr->obj->met.phi_soft );
  configMgr->treeMaker->setFloatVariable("met_track_Et",         configMgr->obj->trackMet.Et);
  configMgr->treeMaker->setFloatVariable("met_track_Phi",        configMgr->obj->trackMet.phi );
  configMgr->treeMaker->setFloatVariable("deltaPhi_MET_TST_Phi", TVector2::Phi_mpi_pi(configMgr->obj->met.phi_soft-configMgr->obj->met.phi) );
  configMgr->treeMaker->setFloatVariable("met_muons_invis_Et",   configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->Et );
  configMgr->treeMaker->setFloatVariable("met_muons_invis_Phi",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->phi );
  configMgr->treeMaker->setFloatVariable("met_Et_LepInvis",   configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et );
  configMgr->treeMaker->setFloatVariable("met_Phi_LepInvis",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi );
  configMgr->treeMaker->setFloatVariable("met_Signif_LepInvis",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->metSignif );

  // Analysis variables
  configMgr->treeMaker->setFloatVariable("meffInc30", meffInc30);
  configMgr->treeMaker->setFloatVariable("Ht30",      Ht30);

  configMgr->treeMaker->setFloatVariable("LepAplanarity",LepAplanarity);
  configMgr->treeMaker->setFloatVariable("JetAplanarity",JetAplanarity);

  // Dijet variables
  configMgr->treeMaker->setFloatVariable("mjj",    mjj);
  configMgr->treeMaker->setFloatVariable("dEtajj", dEtajj);
  configMgr->treeMaker->setFloatVariable("dPhijj", dPhijj);

  // other analysis variables
  configMgr->treeMaker->setFloatVariable("mt_lep1",                mt_lep1); // mT leading lepton
  configMgr->treeMaker->setFloatVariable("mt_lep2",                mt_lep2); // mT subleading lepton
  configMgr->treeMaker->setFloatVariable("mt_lep3",                mt_lep3); // mT 3rd lepton
  configMgr->treeMaker->setFloatVariable("mt_lep1_metTrack",       mt_lep1_metTrack);
  configMgr->treeMaker->setFloatVariable("mt_lep2_metTrack",       mt_lep2_metTrack);
  configMgr->treeMaker->setFloatVariable("mt_lep3_metTrack",       mt_lep3_metTrack);
  configMgr->treeMaker->setFloatVariable("METOverHT",              METOverHT);
  configMgr->treeMaker->setFloatVariable("METOverJ1pT",            METOverJ1pT);
  configMgr->treeMaker->setFloatVariable("METTrackOverHT",         METTrackOverHT);
  configMgr->treeMaker->setFloatVariable("METTrackOverJ1pT",       METTrackOverJ1pT);
  configMgr->treeMaker->setFloatVariable("DPhiJ1Met",              DPhiJ1Met);
  configMgr->treeMaker->setFloatVariable("DPhiJ2Met",              DPhiJ2Met);
  configMgr->treeMaker->setFloatVariable("DPhiJ3Met",              DPhiJ3Met);
  configMgr->treeMaker->setFloatVariable("DPhiJ4Met",              DPhiJ4Met);
  configMgr->treeMaker->setFloatVariable("minDPhiCenJetsMet",      minDPhiCenJetsMet);
  configMgr->treeMaker->setFloatVariable("minDPhiAllJetsMet",      minDPhiAllJetsMet);
  configMgr->treeMaker->setFloatVariable("minDPhiAllJetsMet_LepInvis",      minDPhiAllJetsMet_LepInvis);
  configMgr->treeMaker->setFloatVariable("DPhiJ1MetTrack",         DPhiJ1MetTrack);
  configMgr->treeMaker->setFloatVariable("DPhiJ2MetTrack",         DPhiJ2MetTrack);
  configMgr->treeMaker->setFloatVariable("DPhiJ3MetTrack",         DPhiJ3MetTrack);
  configMgr->treeMaker->setFloatVariable("DPhiJ4MetTrack",         DPhiJ4MetTrack);
  configMgr->treeMaker->setFloatVariable("minDPhiCenJetsMetTrack", minDPhiCenJetsMetTrack);
  configMgr->treeMaker->setFloatVariable("METOverHTLep",           METOverHTLep);
  configMgr->treeMaker->setFloatVariable("METTrackOverHTLep",      METTrackOverHTLep);
  // 2L analysis variables
  configMgr->treeMaker->setFloatVariable("mll",             mll);
  configMgr->treeMaker->setFloatVariable("Rll",             Rll);
  configMgr->treeMaker->setFloatVariable("Ptll",            Ptll);
  configMgr->treeMaker->setFloatVariable("dPhiPllMet",      dPhiPllMet);
  configMgr->treeMaker->setFloatVariable("dPhiPllMetTrack", dPhiPllMetTrack);
  configMgr->treeMaker->setFloatVariable("hasZCand", hasZCand);

  configMgr->treeMaker->setFloatVariable("METRel",             METRel);
  configMgr->treeMaker->setFloatVariable("METTrackRel",        METTrackRel);
  configMgr->treeMaker->setFloatVariable("dPhiNearMet",        dPhiNearMet);
  configMgr->treeMaker->setFloatVariable("dPhiNearMetTrack",   dPhiNearMetTrack);
  configMgr->treeMaker->setFloatVariable("dPhiMetAndMetTrack", dPhiMetAndMetTrack);

  //MSqTauTau_1 and MSqTauTau_2 are obsolete (we decided to take the signed square root to give MTauTau)
  configMgr->treeMaker->setFloatVariable("MTauTau",          MTauTau);
  configMgr->treeMaker->setFloatVariable("MTauTau_metTrack", MTauTau_metTrack);
  configMgr->treeMaker->setFloatVariable("RjlOverEl",        RjlOverEl);
  configMgr->treeMaker->setFloatVariable("LepCosThetaLab",   LepCosThetaLab);
  configMgr->treeMaker->setFloatVariable("LepCosThetaCoM",   LepCosThetaCoM);

  // mt2(L1,L2,MET) with various trial invisible particle masses
  configMgr->treeMaker->setFloatVariable("mt2leplsp_0",   mt2leplsp_0);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_50",  mt2leplsp_50);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_100", mt2leplsp_100);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_150", mt2leplsp_150);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_200", mt2leplsp_200);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_300", mt2leplsp_300);

  configMgr->treeMaker->setFloatVariable("mt2leplsp_0_metTrack",   mt2leplsp_0_metTrack);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_50_metTrack",  mt2leplsp_50_metTrack);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_100_metTrack", mt2leplsp_100_metTrack);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_150_metTrack", mt2leplsp_150_metTrack);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_200_metTrack", mt2leplsp_200_metTrack);
  configMgr->treeMaker->setFloatVariable("mt2leplsp_300_metTrack", mt2leplsp_300_metTrack);

  // 3L specific
  configMgr->treeMaker->setIntVariable(  "nSFOS",              nSFOS);
  configMgr->treeMaker->setFloatVariable("mt_minMll",          mt_minMll);
  configMgr->treeMaker->setFloatVariable("minMll",             minMll);
  configMgr->treeMaker->setFloatVariable("minRll",             minRll);
  configMgr->treeMaker->setFloatVariable("m3l",                m3l);
  configMgr->treeMaker->setFloatVariable("pT3l",               pT3l);
  configMgr->treeMaker->setFloatVariable("dPhi3lMet",          dPhi3lMet);
  configMgr->treeMaker->setFloatVariable("ptZ_minMll",         ptZ_minMll);
  configMgr->treeMaker->setFloatVariable("ptW_minMll",         ptW_minMll);
  configMgr->treeMaker->setFloatVariable("ptWlep_minMll",      ptWlep_minMll);
  configMgr->treeMaker->setFloatVariable("RZlep_minMll",       RZlep_minMll);
  configMgr->treeMaker->setFloatVariable("RZWlep_minMll",      RZWlep_minMll);
  configMgr->treeMaker->setFloatVariable("dPhiZMet_minMll",    dPhiZMet_minMll);
  configMgr->treeMaker->setFloatVariable("dPhiWLepMet_minMll", dPhiWLepMet_minMll);
  configMgr->treeMaker->setFloatVariable("mt2WZlsp_0",         mt2WZlsp_0);
  configMgr->treeMaker->setFloatVariable("mt2WZlsp_100",       mt2WZlsp_100);

  // Triggers
  for (const char* chainName : this->triggerChains) {
    configMgr->treeMaker->setBoolVariable(chainName, configMgr->obj->evt.getHLTEvtTrigDec(chainName));
  }

  for (const char* chainName : this->L1Chains) {
    configMgr->treeMaker->setBoolVariable(chainName, configMgr->obj->evt.getL1EvtTrigDec(chainName));
  }

  configMgr->treeMaker->setBoolVariable("IsMETTrigPassed", configMgr->susyObj.IsMETTrigPassed(configMgr->obj->evt.randomRunNumber, true));

  // Weights
  configMgr->treeMaker->setDoubleVariable("pileupWeight",  configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP));
  configMgr->treeMaker->setDoubleVariable("leptonWeight",  configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP));
  configMgr->treeMaker->setDoubleVariable("eventWeight",   configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT));
  configMgr->treeMaker->setDoubleVariable("genWeight",     configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("bTagWeight",    configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG));
  configMgr->treeMaker->setDoubleVariable("jvtWeight",     configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT));
  configMgr->treeMaker->setDoubleVariable("genWeightUp",   configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("genWeightDown", configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("triggerWeight", configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::TRIG));  
  
  //Total weight calculation
  double pileupWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP);
  double leptonWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP);
  double eventWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT);
  double genWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN);
  double bTagWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG);
  double jvtWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT);
  double triggerWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::TRIG);
  
  double totalWeight = pileupWeight * leptonWeight * eventWeight * genWeight * bTagWeight * jvtWeight * triggerWeight; //not FFWeight -> currently we're just setting this to 1
  configMgr->treeMaker->setDoubleVariable("totalWeight", totalWeight);  

  //Corrections to weights
  VBF_CorrectSherpaWeights(configMgr);

  // Wino-Bino weight, for signal samples
  double truthMll = configMgr->obj->truthEvent.truthMll;
  double winoBinoMllWeight = getWinoBinoMllWeight(configMgr->obj->evt.dsid, truthMll);
  double winoBinoXsecWeight = getWinoBinoXsecWeight(configMgr->obj->evt.dsid);
  double winoBinoBrFracWeight = getWinoBinoBrFracWeight(configMgr->obj->evt.dsid);
  double winoBinoWeight = winoBinoMllWeight*winoBinoXsecWeight*winoBinoBrFracWeight;
  configMgr->treeMaker->setDoubleVariable("truthMll",truthMll);
  configMgr->treeMaker->setDoubleVariable("winoBinoMllWeight",winoBinoMllWeight);
  configMgr->treeMaker->setDoubleVariable("winoBinoXsecWeight",winoBinoXsecWeight);
  configMgr->treeMaker->setDoubleVariable("winoBinoBrFracWeight",winoBinoBrFracWeight);
  configMgr->treeMaker->setDoubleVariable("winoBinoWeight",winoBinoWeight);

  // PDF variables (only for nominal trees)
  if (configMgr->treeMaker->getTreeState()=="") {
    configMgr->treeMaker->setFloatVariable("x1",       configMgr->obj->evt.x1);
    configMgr->treeMaker->setFloatVariable("x2",       configMgr->obj->evt.x2);
    configMgr->treeMaker->setFloatVariable("pdf1",     configMgr->obj->evt.pdf1);
    configMgr->treeMaker->setFloatVariable("pdf2",     configMgr->obj->evt.pdf2);
    configMgr->treeMaker->setFloatVariable("scalePDF", configMgr->obj->evt.scalePDF);
    configMgr->treeMaker->setIntVariable("id1", configMgr->obj->evt.id1);
    configMgr->treeMaker->setIntVariable("id2", configMgr->obj->evt.id2);
    //configMgr->treeMaker->setVecFloatVariable("LHE3Weights",configMgr->obj->evt.LHE3Weights);
  }

// Generator 
configMgr->treeMaker->setFloatVariable("GenFiltMET", configMgr->obj->evt.GenMET);
configMgr->treeMaker->setFloatVariable("GenFiltHT", configMgr->obj->evt.GenHt);

  //computeVBFVariables(configMgr);

  // Pt of SUSY-system and ISR weight
  configMgr->treeMaker->setDoubleVariable("SUSYPt", SUSYPt);
  configMgr->treeMaker->setDoubleVariable("ISRWeight", ISRWeight);

  // SFs for ETMiss Triggers
  if (!configMgr->obj->evt.isData())
    configMgr->treeMaker->setDoubleVariable("ETMissTrigSF", retrieve_sf(configMgr->obj->evt.randomRunNumber, configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->Et, nBJet20));
  else
    configMgr->treeMaker->setDoubleVariable("ETMissTrigSF", 1.0);

  //
  configMgr->doWeightSystematics();

  // Fill the output tree
  //configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  // We may have new-ed these, so better delete them
  if(nLep_base < 1) delete lep1;
  if(nLep_base < 2) delete lep2;
  if(nLep_base < 3) delete lep3;

  if(nTau_base < 1) delete tau1;
  if(nTau_base < 2) delete tau2;

  return true;

}

// ------------------------------------------------------------------------------------------ //
void Common::GetJets(ConfigMgr*& configMgr, std::vector<Jet>& JETs, double pt_cut, double eta_cut){
  JETs.clear();
  for(JetVariable* jet : configMgr->obj->cJets){
    Jet JET;
    JET.P.SetPtEtaPhiM( jet->Pt(), jet->Eta(), jet->Phi(), jet->M() );
    if((JET.P.Pt() >= pt_cut) && (fabs(JET.P.Eta()) < eta_cut || eta_cut < 0)){
       JETs.push_back(JET);
       }
    }
}
TVector3 Common::GetMET(ConfigMgr*& configMgr){
  TVector3 met;
  double exmiss = configMgr->obj->met.Et * cos(configMgr->obj->met.phi);   //2tipi di etmiss, questa da tracce e calorimetro, l'altra->solo dal calorimetro
  double eymiss = configMgr->obj->met.Et * sin(configMgr->obj->met.phi);
  met.SetXYZ(exmiss, eymiss, 0.0);
  //met.SetPtEtaPhi(EtMiss_truth, 0.0, EtMiss_truthPhi/1000.);
  return met;
}
void Common::GetLeptons(ConfigMgr*& configMgr, std::vector<TLorentzVector>& LEPs, std::vector<int>& IDs, double pt_cut, double eta_cut) {
  LEPs.clear();
  IDs.clear();


  //for(int i = 0; i < nLep_base; i++){
  for(LeptonVariable* lep : configMgr->obj->baseLeptons){
    //LeptonVariable* lep= configMgr->obj->baseLeptons[i];
    TLorentzVector LEP;

    if(lep->isEle()){
      LEP.SetPtEtaPhiM(lep->Pt() , lep->Eta(),
                            lep->Phi(), 0.00051 );
      if((LEP.Pt() >= pt_cut) && (fabs(LEP.Eta()) < eta_cut || eta_cut < 0)){
              LEPs.push_back(LEP);
              IDs.push_back(lep->q*1);  //charge*flavor
      }
    }


    else if(lep->isMu()){
      LEP.SetPtEtaPhiM(lep->Pt() , lep->Eta(),
                            lep->Phi(), 0.105658 );
      if((LEP.Pt() >= pt_cut) && (fabs(LEP.Eta()) < eta_cut || eta_cut < 0)){
              LEPs.push_back(LEP);
              IDs.push_back(lep->q*2);  //charge*flavor
      }
    }
  }

}
// ------------------------------------------------------------------------------------------ //
void Common::computeJigsawVariables(ConfigMgr*& configMgr){

  using namespace RestFrames;
  LabRecoFrame        LAB("LAB","lab");
  DecayRecoFrame      CM("CM","CM");
  DecayRecoFrame      S("S","S");
  VisibleRecoFrame    ISR("ISR","ISR");
  VisibleRecoFrame    V("V","Vis");
  VisibleRecoFrame    L("L","Lep");
  InvisibleRecoFrame  I("I","Inv");
  InvisibleGroup      INV("INV","Invisible System");
  SetMassInvJigsaw    InvMass("InvMass", "Invisible system mass Jigsaw");
  CombinatoricGroup   VIS("VIS","Visible Objects");
  MinMassesCombJigsaw SplitVis("SplitVis","Minimize M_{ISR} and M_{S} Jigsaw");

  ////////////// Tree set-up /////////////////
  LAB.SetChildFrame(CM);
  CM.AddChildFrame(ISR);
  CM.AddChildFrame(S);
  S.AddChildFrame(V);
  S.AddChildFrame(I);
  S.AddChildFrame(L);

  LAB.InitializeTree();

  ////////////// Tree set-up /////////////////

  ////////////// Jigsaw rules set-up /////////////////
  INV.AddFrame(I);

  VIS.AddFrame(ISR);
  VIS.SetNElementsForFrame(ISR,1,false);
  VIS.AddFrame(V);
  VIS.SetNElementsForFrame(V,0,false);

  // set the invisible system mass to zero
  INV.AddJigsaw(InvMass);

  // define the rule for partitioning objects between "ISR" and "V"
  VIS.AddJigsaw(SplitVis);
  // "0" group (ISR)
  SplitVis.AddFrame(ISR, 0);
  // "1" group (V + I + L)
  SplitVis.AddFrame(V,1);
  SplitVis.AddFrame(I,1);
  SplitVis.AddFrame(L,1);


  LAB.InitializeAnalysis();
  ////////////// Jigsaw rules set-up /////////////////

  //std::cout << "MET: " << configMgr->obj->met.Et << std::endl;
  //no met cut for higgsino
  //if(configMgr->obj->met.Et < 100) return;

  std::vector<Jet> Jets;
  GetJets(configMgr, Jets, 30, 2.8);
  if (Jets.size() < 1) //REQUIRE THIS IF WE ARE FORCING LEPTONS
    return;

  std::vector<TLorentzVector> Leptons;
  std::vector<int> LepIDs;
  GetLeptons(configMgr,Leptons,LepIDs);
  //allow for single lepton events for dijet FFs
  //if(Leptons.size() < 2)
  //  return;

  TVector3 ETMiss = GetMET(configMgr);

  // analyze event in RestFrames tree
  LAB.ClearEvent();

  double m_HT = 0.;
  std::vector<RFKey> jetID;
  for(int i = 0; i < int(Jets.size()); i++){
    TLorentzVector jet = Jets[i].P;
    // transverse view of jet 4-vectors
    jet.SetPtEtaPhiM(jet.Pt(),0.0,jet.Phi(),jet.M());
    jetID.push_back(VIS.AddLabFrameFourVector(jet));
    m_HT += jet.Pt();
  }

  TLorentzVector lepSys;
  lepSys.SetPtEtaPhiM(0.0,0.0,0.0,0.0);
  std::vector<RFKey> lepID;
  for(int i = 0; i < int(Leptons.size()); i++){
    TLorentzVector lep1;
    // transverse view of lepton 4-vectors
    //std::cout << "Lpton pt: " << Leptons[i].Pt() << ", eta: " << Leptons[i].Eta() << ", m: " << Leptons[i].M() << std::endl;
    lep1.SetPtEtaPhiM(Leptons[i].Pt(),0.0,Leptons[i].Phi(),Leptons[i].M());
    //lepID.push_back(VIS->AddLabFrameFourVector(lep1));  // uncheck this to add leptons to VIS combinatoric group (mass minimization)
    lepSys = lepSys + lep1;
  }
  L.SetLabFrameFourVector(lepSys);

  INV.SetLabFrameThreeVector(ETMiss);
  if(!LAB.AnalyzeEvent()) std::cout << "Something went wrong..." << std::endl;

  // Compressed variables from tree
  int m_NjV = 0;
  int m_NjISR = 0;
  // assuming pT ordered jets
  for(int i = 0; i < int(Jets.size()); i++){
    if(VIS.GetFrame(jetID[i]) == V){ // sparticle group
      m_NjV++;
    } else {
      m_NjISR++;
    }
  }

  //bool foundOS = false;
  //// assuming pT ordered leptons
  //for(int i = 0; i < int(Leptons.size()); i++){
  //              for(int j = 0; j < int(Leptons.size()); j++){
  //                      if (!foundOS) {
  //                              if (LepIDs[i] == -LepIDs[j]) {
  //                                    foundOS = true;
  //                                    TLorentzVector lep1;
  //                                    TLorentzVector lep2;
  //                                    lep1.SetPtEtaPhiM(Leptons[i].Pt(), Leptons[i].Eta(),
  //                                                      Leptons[i].Phi(), Leptons[i].M());
  //                                    lep2.SetPtEtaPhiM(Leptons[j].Pt(), Leptons[j].Eta(),
  //                                                      Leptons[j].Phi(), Leptons[j].M());
  //                                    //m_mllOS = (lep1 + lep2).M();
  //                                    lep1.Clear();
  //                                    lep2.Clear();
  //                                    lep1.SetPtEtaPhiM(Leptons[i].Pt(), 0.0,
  //                                                      Leptons[i].Phi(), std::max(0.,Leptons[i].M()));
  //                                    lep2.SetPtEtaPhiM(Leptons[j].Pt(), 0.0,
  //                                                      Leptons[j].Phi(), std::max(0.,Leptons[j].M()));
  //                                    TLorentzVector vL1_CM   = CM.GetFourVector(lep1);
  //                                    TLorentzVector vL2_CM   = CM.GetFourVector(lep2);
  //                                    TLorentzVector vL1_S    = S.GetFourVector(lep1);
  //                                    TLorentzVector vL2_S    = S.GetFourVector(lep2);
  //                                    TLorentzVector vL1_L    = L.GetFourVector(lep1);
  //                                    TLorentzVector vL2_L    = L.GetFourVector(lep2);
  //                                    TLorentzVector vCM_lab = CM.GetFourVector();
  //                                    TLorentzVector vS_CM   = S.GetFourVector(CM);
  //                                    TLorentzVector vI_S    = I.GetFourVector(S);
  //                                    TLorentzVector vL_S    = L.GetFourVector(S);
  //                                    //m_MZ = (vL1_S + vL2_S).M();
  //                                    //if (fabs(vL1_S.Phi()) < TMath::Pi())
  //                                    //  m_dphiLLOS = fabs(vL1_L.DeltaPhi(vL2_L));
  //                                    //m_cosLLOS = -vL_S.Vect().Unit().Dot(vL1_L.Vect().Unit());
  //                                    //}
  //                            }
  //                    }
  //            }
  //}


  //RJR variables
  double m_PTISR = 0.0;
  double m_RISR = 0.0;
  double m_MS = 0.0;
  double m_MISR = 0.0;
  double m_dphiISRI = 0.0;

  TVector3 vP_ISR = ISR.GetFourVector(CM).Vect();
  TVector3 vP_I   = I.GetFourVector(CM).Vect();
  m_PTISR = vP_ISR.Mag();
  m_RISR = fabs(vP_I.Dot(vP_ISR.Unit())) / m_PTISR;
  m_MS = S.GetMass();
  m_MISR = ISR.GetMass();
  m_dphiISRI = fabs(vP_ISR.DeltaPhi(vP_I));

  //std::cout << "RSR: " << m_RISR << ", MS: " << m_MS << std::endl;

  configMgr->treeMaker->setDoubleVariable("RJR_PTISR",m_PTISR);
  configMgr->treeMaker->setDoubleVariable("RJR_RISR",m_RISR);
  configMgr->treeMaker->setDoubleVariable("RJR_MS",m_MS);
  configMgr->treeMaker->setDoubleVariable("RJR_MISR",m_MISR);
  configMgr->treeMaker->setDoubleVariable("RJR_dphiISRI",m_dphiISRI);
  configMgr->treeMaker->setIntVariable("RJR_NjV",m_NjV);
  configMgr->treeMaker->setIntVariable("RJR_NjISR",m_NjISR);

}
// ------------------------------------------------------------------------------------------ //
void Common::load_sfs(){
  std::string file_sfs = PathResolverFindCalibFile("SusySkimHiggsino/binaries/ETMiss_SFs.root");
  TFile t(file_sfs.c_str(), "READ");

  // known triggers
  std::vector<std::string> triggers = {
           "HLT_xe70_mht",
           "HLT_xe90_mht_L1XE50",
           "HLT_xe100_mht_L1XE50",
           "HLT_xe110_mht_L1XE50",
           "HLT_xe110_pufit_L1XE55"
           };

  // currently SFs are binned in bveto and btag
  std::vector<std::string> regions = {
    "btag",
    "bveto"
  };

  // get TF1s for all triggers and regions
  for(const std::string &trig : triggers){
    for (const std::string &reg : regions){
     std::string name = trig + "_" + reg;
     // retrieve function from file and copy to local TF1 so we can close file
     TF1* f_sf = (TF1*) t.Get(name.c_str());
     TF1 sf_curve = TF1(*f_sf);
     this->sf_curves[name] = sf_curve;
    }
  }
  t.Close();
}
// ------------------------------------------------------------------------------------------ //
double Common::retrieve_sf(int runnumber, double met, int nBjet){
    if (met < 100){
      return 1.0;
    }
    std::string trigger;
    if (runnumber <= 284484)
      trigger = "HLT_xe70_mht";
    else if(runnumber >= 296939 && runnumber <= 302872)
      trigger = "HLT_xe90_mht_L1XE50";
    else if(runnumber >= 302919 && runnumber <= 303892)
      trigger = "HLT_xe100_mht_L1XE50";
    else if(runnumber >= 303943 && runnumber <= 320000)
      trigger = "HLT_xe110_mht_L1XE50";
    else if(runnumber > 320000)
      trigger = "HLT_xe110_pufit_L1XE55";
    else{
      std::cout << "<Common::retrieve_sf> cannot assign trigger for RunNumber " << runnumber << std::endl;
      return 1.0;
    }
    std::string region = (nBjet == 0) ? "bveto" : "btag";
    return this->sf_curves[trigger+"_"+region].Eval(met);
}
// ------------------------------------------------------------------------------------------ //
void Common::load_ISR_fit(){
  std::string file_sfs = PathResolverFindCalibFile("SusySkimHiggsino/binaries/ISR_reweighting.root");
  TFile t(file_sfs.c_str(), "READ");
  TF1* f_sf = (TF1*) t.Get("f");
  this->ISR_fit = TF1(*f_sf);
  t.Close();
}
// ------------------------------------------------------------------------------------------ //
double Common::retrieve_ISRWeight(double SUSYPt){
  // no reweighting fur pT < 100 GeV, weights for pT > 700 are contained in 700
  // but this still need to be discussed if its the proper way to do it
 if (SUSYPt < 100)
    return 1.0;
 else if (SUSYPt > 700)
    return this->ISR_fit.Eval(700);
 else
    return this->ISR_fit.Eval(SUSYPt);
}

// Setup the VBF analysis Variables
void Common::vbfSetup(const ConfigMgr* configMgr) {

    //VBFVariables

    configMgr->treeMaker->addIntVariable("nAjets", 0.0 ); //all jets
    configMgr->treeMaker->addIntVariable("nBasejets", 0.0 ); //all jets
    configMgr->treeMaker->addIntVariable("nFjets", 0.0 ); //fwd jets
    configMgr->treeMaker->addIntVariable("nCjets", 0.0 ); //Central jets
    configMgr->treeMaker->addIntVariable("nVBFjets", 0.0 ); //jets passing VBF selection (pt > 30, non btagged)

    //Lepton mulitplicities 
    configMgr->treeMaker->addIntVariable("nLep_EWKCombiBaseline", 0.0);//Number of EWK Combination Baseline leptons (as using different ID for working point)
    configMgr->treeMaker->addIntVariable("nLep_ZCR", 0.0); //number of leptons passing ZCR selection
    configMgr->treeMaker->addIntVariable("nLep_WCR", 0.0); //number of leptons passing WCR selection
    
    //Vector variables for all jets
    configMgr->treeMaker->addVecFloatVariable("aJets_Pt");
    configMgr->treeMaker->addVecFloatVariable("aJets_Eta");
    configMgr->treeMaker->addVecFloatVariable("aJets_Phi");
    configMgr->treeMaker->addVecFloatVariable("aJets_M");
    configMgr->treeMaker->addVecFloatVariable("aJets_Btagged");
    configMgr->treeMaker->addVecFloatVariable("aJets_dl1r");
    

    //Pt sum of all jets with pt > 30 and eta < 5
    configMgr->treeMaker->addFloatVariable("HtVec30_allJets", 0.0); //Vector sum
    configMgr->treeMaker->addFloatVariable("Ht30_allJets", 0.0); //scalar sum
    configMgr->treeMaker->addFloatVariable("HtVec20_allJets", 0.0); //Vector sum
    configMgr->treeMaker->addFloatVariable("Ht20_allJets", 0.0); //scalar sum

    //Bools
    configMgr->treeMaker->addBoolVariable("validVBFtags", false);
    configMgr->treeMaker->addBoolVariable("validSpecJet", false);

    //Jets
    addJetVariables(configMgr,"vbfTagJet1");
    addJetVariables(configMgr,"vbfTagJet2");
    addJetVariables(configMgr,"vbfSpecJet");
    addJetVariables(configMgr,"jetC");
    addJetVariables(configMgr,"jetD");
    
    //Jet Indices
    configMgr->treeMaker->addIntVariable("vbfTagJet1_Index", -1);
    configMgr->treeMaker->addIntVariable("vbfTagJet2_Index", -1);
    configMgr->treeMaker->addIntVariable("SpecJet_Index", -1);
    configMgr->treeMaker->addIntVariable("jetC_Index", -1);
    configMgr->treeMaker->addIntVariable("jetD_Index", -1);

    configMgr->treeMaker->addFloatVariable("vbfjjPt",   -1.0);
    configMgr->treeMaker->addFloatVariable("vbfjjEta",   0.0);
    configMgr->treeMaker->addFloatVariable("vbfjjPhi",   0.0);
    configMgr->treeMaker->addFloatVariable("vbfjjM",    -1.0);
    configMgr->treeMaker->addFloatVariable("vbfjjDEta", -1.0);
    configMgr->treeMaker->addFloatVariable("vbfjjDPhi", -1.0);
    configMgr->treeMaker->addFloatVariable("vbfjjDR",   -1.0);
    configMgr->treeMaker->addFloatVariable("vbfEtaEta",  false);
   
    //Centralities
    //Cen = linear Cen, ExpCen = Exponential Centrality
    configMgr->treeMaker->addFloatVariable("SpecJet_Cen", -1.0);
    configMgr->treeMaker->addFloatVariable("SpecJet_ExpCen", -1.0);
    configMgr->treeMaker->addFloatVariable("jetC_Cen", -1.0);
    configMgr->treeMaker->addFloatVariable("jetC_ExpCen", -1.0);
    configMgr->treeMaker->addFloatVariable("jetD_Cen", -1.0);
    configMgr->treeMaker->addFloatVariable("jetD_ExpCen", -1.0);

    //Relative invariant masses (compared to vbfmjj)
    configMgr->treeMaker->addFloatVariable("jetC_mjjrel", -1.0);
    configMgr->treeMaker->addFloatVariable("jetD_mjjrel", -1.0);

    // Ratio HTs
    configMgr->treeMaker->addFloatVariable("HTVBF", -1.0);
    configMgr->treeMaker->addFloatVariable("HTVBFOverMET",   -1.0);
    configMgr->treeMaker->addFloatVariable("HTVBFOverMETTrack",   -1.0);
    configMgr->treeMaker->addFloatVariable("vbfjj_dphi_met",-1.0);
    configMgr->treeMaker->addFloatVariable("vbfjj_dphi_mettrack",-1.0);
    
    //Transverse Mass of VBF + MET system
    configMgr->treeMaker->addFloatVariable("MT_VBFMET", -1.0);


    //Variables for 1 and 2 lepton CRs
    configMgr->treeMaker->addFloatVariable("HTVBFOverHTLep", -1.0);
    configMgr->treeMaker->addFloatVariable("SMET", -1.0);
    configMgr->treeMaker->addFloatVariable("SMET_LepInvis", -1.0);
    configMgr->treeMaker->addIntVariable("WCR_LepIndex", -1);
    addVBFLeptonVariables(configMgr, "lep1"); 
    addVBFLeptonVariables(configMgr, "lep2"); 
    addVBFLeptonVariables(configMgr, "lep3"); 

    //Z CR variables 
    addllVars(configMgr, "ZCR_ll"); 
    configMgr->treeMaker->addIntVariable("ZCR_mll_Lep1Index", -1);
    configMgr->treeMaker->addIntVariable("ZCR_mll_Lep2Index", -1);
    configMgr->treeMaker->addIntVariable("ZCR_mll_flavour", -1); //1 = electron, 2 = muon

    //MET subtracting leading baseline leptons at end 
    //Note these are just for fun, should use the "_LepInvis" variables instead
    configMgr->treeMaker->addFloatVariable("met_Et_MinusBaseL1" , -1.0);
    configMgr->treeMaker->addFloatVariable("met_Et_MinusBaseL1L2" , -1.0);

    //MET variables using MET with invisible leptons
    configMgr->treeMaker->addFloatVariable("vbfjj_dphi_met_LepInvis" , -1.0);
    configMgr->treeMaker->addFloatVariable("MT_VBFMET_LepInvis" , -1.0);
    configMgr->treeMaker->addFloatVariable("HTVBFOverMET_LepInvis",   -1.0);
    configMgr->treeMaker->addFloatVariable("TST_Et_LepInvis",   -1.0); // DMJ: Might be the same as normal TST...
    configMgr->treeMaker->addFloatVariable("deltaPhi_MET_TST_Phi_LepInvis",   -1.0);


    // RJR variables !
    configMgr->treeMaker->addFloatVariable ("vbfRJR_PTVBF",-1.0);
    configMgr->treeMaker->addFloatVariable ("vbfRJR_PTS",-1.0);
    configMgr->treeMaker->addFloatVariable ("vbfRJR_RVBF",-1.0);
    configMgr->treeMaker->addFloatVariable ("vbfRJR_MS",-1.0);
    configMgr->treeMaker->addFloatVariable ("vbfRJR_MVBF",-1.0);
    configMgr->treeMaker->addFloatVariable ("vbfRJR_dphiVBFI",-1.0);

    //V + jets background Truth Variables (to use in Merging)
    configMgr->treeMaker->addIntVariable ("NtruthVBFJets", -1.0);
    configMgr->treeMaker->addIntVariable ("NtruthStatus3Leptons", -1.0);
    //configMgr->treeMaker->addIntVariable ("NtruthStatus20Leptons", -1.0);
    //configMgr->treeMaker->addIntVariable ("NtruthStatus3Jets", -1.0);
    //configMgr->treeMaker->addIntVariable ("NtruthStatus20Jets", -1.0);
    
    configMgr->treeMaker->addFloatVariable ("Truth_vbfPtV", -1.0);
    configMgr->treeMaker->addFloatVariable ("Truth_vbfjjM", -1.0);
    configMgr->treeMaker->addFloatVariable ("Truth_vbfjjDPhi", -1.0);
    configMgr->treeMaker->addFloatVariable ("Truth_j0_pt", -1.0);
    configMgr->treeMaker->addFloatVariable ("Truth_j1_pt", -1.0);
    configMgr->treeMaker->addFloatVariable ("Truth_MET", -1.0);

    configMgr->treeMaker->addIntVariable ("truthStatus3Lepton1_pdgId", 0.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton1_Pt", -1.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton1_Eta", 0.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton1_Phi", 0.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton1_E", -1.0);

    configMgr->treeMaker->addIntVariable ("truthStatus3Lepton2_pdgId", 0.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton2_Pt", -1.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton2_Eta", 0.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton2_Phi", 0.0);
    configMgr->treeMaker->addFloatVariable ("truthStatus3Lepton2_E", -1.0);

    configMgr->treeMaker->addIntVariable ("truthTauFromW_n", 0);
    configMgr->treeMaker->addVecIntVariable("truthTauFromW_DMV");

    // jetY = minimum DeltaPhiMET; jetZ = minimum (MT - 80)
    configMgr->treeMaker->addIntVariable("jetY_Index", -1);
    configMgr->treeMaker->addIntVariable("jetZ_Index", -1);
    configMgr->treeMaker->addIntVariable("jetY_LepInvis_Index", -1);
    configMgr->treeMaker->addIntVariable("jetZ_LepInvis_Index", -1);
    configMgr->treeMaker->addFloatVariable("jetY_DPhiMet", -1.0);
    configMgr->treeMaker->addFloatVariable("jetZ_DPhiMet", -1.0);
    configMgr->treeMaker->addFloatVariable("jetY_LepInvis_DPhiMet", -1.0);
    configMgr->treeMaker->addFloatVariable("jetZ_LepInvis_DPhiMet", -1.0);
    configMgr->treeMaker->addFloatVariable("jetY_MT", -1.0);
    configMgr->treeMaker->addFloatVariable("jetZ_MT", -1.0);
    configMgr->treeMaker->addFloatVariable("jetY_LepInvis_MT", -1.0);
    configMgr->treeMaker->addFloatVariable("jetZ_LepInvis_MT", -1.0);

}



void Common::writeVBF(ConfigMgr*& configMgr) 
{
    //Variables that need vbf jets to be calculated 
    if (validVBFtags == false) return;

    TLorentzVector jj_sys = vbftag_J1 + vbftag_J2;

    configMgr->treeMaker->setFloatVariable("vbfjjPt",   jj_sys.Pt());
    configMgr->treeMaker->setFloatVariable("vbfjjEta",  jj_sys.Eta());
    configMgr->treeMaker->setFloatVariable("vbfjjPhi",  jj_sys.Phi());
    configMgr->treeMaker->setFloatVariable("vbfjjM",    jj_sys.M());
    configMgr->treeMaker->setFloatVariable("vbfjjDEta", TMath::Abs(vbftag_J1.Eta() - vbftag_J2.Eta()));
    configMgr->treeMaker->setFloatVariable("vbfjjDPhi", TMath::Abs(vbftag_J1.DeltaPhi(vbftag_J2)));
    configMgr->treeMaker->setFloatVariable("vbfjjDR",   vbftag_J1.DeltaR(vbftag_J2));
    configMgr->treeMaker->setFloatVariable("vbfEtaEta",   vbftag_J1.Eta()*vbftag_J2.Eta());


    // MET
    TLorentzVector vec_met;
    TLorentzVector vec_metTrack;
    vec_met.SetPtEtaPhiM(configMgr->obj->met.Et,0.0, configMgr->obj->met.phi,0.0);
    vec_metTrack.SetPtEtaPhiM(configMgr->obj->trackMet.Et, 0.0, configMgr->obj->trackMet.phi, 0.0);
    TLorentzVector vec_met_LepInvis; 
    vec_met_LepInvis.SetPtEtaPhiM(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et, 0, configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi, 0);

    // HT of vbf
    float HTVBF = 0;
    HTVBF = vbftag_J1.Pt() + vbftag_J2.Pt();
    configMgr->treeMaker->setFloatVariable("HTVBF", HTVBF);

    // Ratio HTs
    float HTVBFOverMET = vec_met.Pt() > 0 ? HTVBF / vec_met.Pt() : -1;
    float HTVBFOverMETTrack = vec_metTrack.Pt() > 0 ? HTVBF / vec_metTrack.Pt() : -1;
    configMgr->treeMaker->setFloatVariable("HTVBFOverMET",      HTVBFOverMET);
    configMgr->treeMaker->setFloatVariable("HTVBFOverMETTrack", HTVBFOverMETTrack);
    
    float HTVBFOverMET_LepInvis = vec_met_LepInvis.Pt() > 0 ? HTVBF / vec_met_LepInvis.Pt() : -1;
    configMgr->treeMaker->setFloatVariable("HTVBFOverMET_LepInvis",      HTVBFOverMET_LepInvis);

    //MetDeltaPhis
    float vbfjj_dphi_met = fabs(jj_sys.DeltaPhi(vec_met));
    float vbfjj_dphi_mettrack = fabs(jj_sys.DeltaPhi(vec_metTrack));
    float vbfjj_dphi_met_LepInvis = fabs(jj_sys.DeltaPhi(vec_met_LepInvis));
    configMgr->treeMaker->setFloatVariable("vbfjj_dphi_met",      TMath::Abs(vbfjj_dphi_met));
    configMgr->treeMaker->setFloatVariable("vbfjj_dphi_mettrack", TMath::Abs(vbfjj_dphi_mettrack));
    configMgr->treeMaker->setFloatVariable("vbfjj_dphi_met_LepInvis", vbfjj_dphi_met_LepInvis  );
    configMgr->treeMaker->setFloatVariable("TST_Et_LepInvis", configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et_soft);
    configMgr->treeMaker->setFloatVariable("deltaPhi_MET_TST_Phi_LepInvis", TVector2::Phi_mpi_pi(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi_soft-configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi));
    
    //Transverse Mass
    float MT_VBFMET = getM_T(vec_met, jj_sys);
    configMgr->treeMaker->setFloatVariable("MT_VBFMET", MT_VBFMET);
    float MT_VBFMET_LepInvis = getM_T(vec_met_LepInvis, jj_sys);
    configMgr->treeMaker->setFloatVariable("MT_VBFMET_LepInvis", MT_VBFMET_LepInvis);

    //VBF Lepton Variables
    int nLep_base = configMgr->obj->baseLeptons.size();
    LeptonVariable* lep1 = nLep_base < 1 ? new LeptonVariable() : configMgr->obj->baseLeptons[0];
    LeptonVariable* lep2 = nLep_base < 2 ? new LeptonVariable() : configMgr->obj->baseLeptons[1];
    LeptonVariable* lep3 = nLep_base < 3 ? new LeptonVariable() : configMgr->obj->baseLeptons[2];
    
    float HTLep = 0;
    for (LeptonVariable* lep: configMgr->obj->baseLeptons) HTLep += lep->Pt();
    float HTVBFOverHTLep = HTLep > 0 ? HTVBF / HTLep : -1;

    setVBFLeptonVariables(configMgr, "lep1", lep1,  jj_sys);
    setVBFLeptonVariables(configMgr, "lep2", lep2,  jj_sys);
    setVBFLeptonVariables(configMgr, "lep3", lep3,  jj_sys);

    //Delete new lepton variables
    if(nLep_base < 1) delete lep1;
    if(nLep_base < 2) delete lep2;
    if(nLep_base < 3) delete lep3;

    //CR Variables
    
    //W CR
    float SMET = -1;
    float SMET_LepInvis = -1;
    UInt_t WCR_LepIndex = -1;    
    for (UInt_t i=0; i<configMgr->obj->baseLeptons.size(); i++)
    {
        LeptonVariable* lep = configMgr->obj->baseLeptons[i];
        if (!LepPassVBF_WCR(lep)) continue;
        SMET = configMgr->obj->met.Et / sqrt(vbftag_J1.Pt() + vbftag_J2.Pt() + lep->Pt() );
        SMET_LepInvis = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et / sqrt(vbftag_J1.Pt() + vbftag_J2.Pt() + lep->Pt() );
        WCR_LepIndex = i; 
        break;
    }
    configMgr->treeMaker->setFloatVariable("HTVBFOverHTLep",    HTVBFOverHTLep);
    configMgr->treeMaker->setFloatVariable("SMET", SMET);
    configMgr->treeMaker->setFloatVariable("SMET_LepInvis", SMET_LepInvis);
    configMgr->treeMaker->setIntVariable("WCR_LepIndex", WCR_LepIndex);
    
    //ZCR
    computeVBF_ZCR_Vars(configMgr);

}



void Common::addJetVariables(const ConfigMgr* configMgr, TString prefix) {
    configMgr->treeMaker->addFloatVariable(prefix+"_Pt",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_Eta",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_Phi",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_M",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_tileEnergy",0.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_DPhiMet",-1.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_DPhiMet_LepInvis",-1.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_minDRaJet20",-1.0);
    configMgr->treeMaker->addIntVariable(prefix+"_Btagged",-1.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_dl1r",-1.0);
    configMgr->treeMaker->addFloatVariable(prefix+"_JVT",-1.0);
    configMgr->treeMaker->addBoolVariable(prefix+"_passOR",-1.0);
    configMgr->treeMaker->addBoolVariable(prefix+"_passDRcut",-1.0);
//    configMgr->treeMaker->addFloatVariable(prefix+"_EMFrac",0.0);
//    configMgr->treeMaker->addFloatVariable(prefix+"_HECFrac",0.0);
//    configMgr->treeMaker->addFloatVariable(prefix+"_sumpttrk",0.0);
//    configMgr->treeMaker->addIntVariable  (prefix+"_nTrk",-1);
}

void Common::setJetVariables(const ConfigMgr* configMgr, TString prefix, const JetVariable* jet) {
    configMgr->treeMaker->setFloatVariable(prefix+"_Pt",jet->Pt());
    configMgr->treeMaker->setFloatVariable(prefix+"_Eta",jet->Eta());
    configMgr->treeMaker->setFloatVariable(prefix+"_Phi",jet->Phi());
    configMgr->treeMaker->setFloatVariable(prefix+"_M",jet->M());
    configMgr->treeMaker->setFloatVariable(prefix+"_tileEnergy",jet->tileEnergy);
    configMgr->treeMaker->setFloatVariable(prefix+"_DPhiMet",fabs(jet->DeltaPhi(configMgr->obj->met)));
    configMgr->treeMaker->setFloatVariable(prefix+"_DPhiMet_LepInvis",fabs(jet->DeltaPhi(*configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP))));
    configMgr->treeMaker->setFloatVariable(prefix+"_minDRaJet20", getMinDRaJets(configMgr->obj, jet, 20));
    configMgr->treeMaker->setIntVariable(prefix+"_Btagged", jet->bjet);
    configMgr->treeMaker->setFloatVariable(prefix+"_dl1r",jet->dl1r);
    configMgr->treeMaker->setFloatVariable(prefix+"_JVT",jet->jvt);
    configMgr->treeMaker->setBoolVariable(prefix+"_passOR",jet->passOR);
    configMgr->treeMaker->setBoolVariable(prefix+"_passDRcut",jet->passDRcut);
//    configMgr->treeMaker->setFloatVariable(prefix+"_EMFrac",jet->EMFrac);
//    configMgr->treeMaker->setFloatVariable(prefix+"_HECFrac",jet->HECFrac);
//    configMgr->treeMaker->setFloatVariable(prefix+"_sumpttrk",jet->sumpttrk);
//    configMgr->treeMaker->setIntVariable  (prefix+"_nTrk",jet->nTrk);
}



void Common::addVBFLeptonVariables(const ConfigMgr* configMgr, TString prefix) {
    configMgr->treeMaker->addFloatVariable(prefix+"_DPhi_vbfjj",-1);
    configMgr->treeMaker->addFloatVariable(prefix+"_DR_vbfjj",-1);
    configMgr->treeMaker->addFloatVariable(prefix+"_CenVBF",-1);
}

void Common::setVBFLeptonVariables(const ConfigMgr* configMgr, TString prefix, LeptonVariable* lep, const TLorentzVector& jj_sys) 
{
    configMgr->treeMaker->setFloatVariable(prefix+"_DPhi_vbfjj", lep->DeltaPhi(jj_sys)); 
    // configMgr->treeMaker->setFloatVariable(prefix+"_DR_vbfjj", lep->DeltaR(jj_sys)); 
    double_t tmp = lep->DeltaR(jj_sys);
    configMgr->treeMaker->setFloatVariable(prefix+"_DR_vbfjj", tmp); 
    float avg_eta = (vbftag_J1.Eta() + vbftag_J2.Eta()) / 2.0;
    float deta = fabs(vbftag_J1.Eta() - vbftag_J2.Eta());
    configMgr->treeMaker->setFloatVariable(prefix+"_CenVBF", TMath::Abs((lep->Eta() - avg_eta) / deta));
}


void Common::addllVars(const ConfigMgr* configMgr, TString prefix) {
    configMgr->treeMaker->addFloatVariable(prefix + "_M" , -1);
    configMgr->treeMaker->addFloatVariable(prefix + "_Pt" , -1);
    configMgr->treeMaker->addFloatVariable(prefix + "_DPhiMet" , -1);
    configMgr->treeMaker->addFloatVariable(prefix + "_DPhiMet_LepInvis" , -1);
    configMgr->treeMaker->addFloatVariable(prefix + "_DPhi_l1l2" , -1);
    configMgr->treeMaker->addFloatVariable(prefix + "_DR_l1l2" , -1);
}

void Common::setllVars(const ConfigMgr* configMgr, TString prefix, LeptonVariable* lep1, LeptonVariable* lep2 )
{
    const TLorentzVector* lep1_TLV = dynamic_cast<const TLorentzVector*>(lep1);
    const TLorentzVector* lep2_TLV = dynamic_cast<const TLorentzVector*>(lep2);
    TLorentzVector ll_sys = *lep1_TLV + *lep2_TLV; 
    configMgr->treeMaker->setFloatVariable(prefix + "_M", ll_sys.M());
    configMgr->treeMaker->setFloatVariable(prefix + "_Pt", ll_sys.Pt());
    configMgr->treeMaker->setFloatVariable(prefix+"_DPhiMet",fabs(ll_sys.DeltaPhi(configMgr->obj->met)));
    configMgr->treeMaker->setFloatVariable(prefix+"_DPhiMet_LepInvis",fabs(ll_sys.DeltaPhi(*configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP))));
    configMgr->treeMaker->setFloatVariable(prefix + "_DPhi_l1l2" , fabs(lep1_TLV->DeltaPhi(*lep2_TLV)));
    configMgr->treeMaker->setFloatVariable(prefix + "_DR_l1l2" , fabs(lep1_TLV->DeltaR(*lep2_TLV)));
}

void Common::computeVBFVariables(ConfigMgr*& configMgr)
{

    int nAjets = configMgr->obj->aJets.size();
    configMgr->treeMaker->setIntVariable("nAjets", nAjets );
    configMgr->treeMaker->setIntVariable("nBasejets", configMgr->obj->baselineJets.size() );
    configMgr->treeMaker->setIntVariable("nFjets", configMgr->obj->fJets.size() );
    configMgr->treeMaker->setIntVariable("nCjets", configMgr->obj->cJets.size() );

    //Lepton Multiplicities
    int nLep_EWKCombiBaseline = 0;
    int nLep_ZCR = 0;
    int nLep_WCR = 0;
    for ( auto& lep: configMgr->obj->baseLeptons) {
        if (IsCombiBaselineLepton(lep)) ++ nLep_EWKCombiBaseline;
        if (LepPassVBF_ZCR(lep)) ++ nLep_ZCR;
        if (LepPassVBF_WCR(lep)) ++ nLep_WCR;
    }
    configMgr->treeMaker->setIntVariable("nLep_EWKCombiBaseline", nLep_EWKCombiBaseline );
    configMgr->treeMaker->setIntVariable("nLep_ZCR", nLep_ZCR);
    configMgr->treeMaker->setIntVariable("nLep_WCR", nLep_WCR); 

    validVBFtags = false;
    validSpecJet = false;
    
    //Keep track of indices
    int vbfTagJet1_Index = -1;
    int vbfTagJet2_Index = -1;
    int SpecJet_Index = -1;
    int JetC_Index = -1;
    int JetD_Index = -1;
    
    float jetC_Cen = -1.0;
    double jetC_ExpCen = -1;
    float jetD_Cen = -1.0;
    double jetD_ExpCen = -1;
    float SpecJet_Cen = -1.0;
    float SpecJet_ExpCen = -1.0;

    vbfjjM = -1.0;
    double jetC_mjjrel = -1;
    double jetD_mjjrel = -1;


    //VBF selection
    eventnokey = -1;
    vbftag_J1.SetPtEtaPhiM(0.,0.,0.,0.);
    vbftag_J2.SetPtEtaPhiM(0.,0.,0.,0.);
    vbftag_J3.SetPtEtaPhiM(0., 0., 0., 0.);
    jetC.SetPtEtaPhiM(0.,0.,0.,0.);
    jetD.SetPtEtaPhiM(0.,0.,0.,0.);

    int njets = 0;

    //Get jets
    //auto jets = configMgr->obj->aJets;
    //FilterJets(configMgr, jets, 30, 5.0);
    //auto jets = FilterJets(configMgr->obj->aJets, 30, 5.0);

    auto jets = configMgr->obj->aJets;
    njets = jets.size();

    std::vector<float> aJets_PtVec;
    std::vector<float> aJets_EtaVec;
    std::vector<float> aJets_PhiVec;
    std::vector<float> aJets_MVec;
    std::vector<float> aJets_dl1rVec;
    std::vector<float> aJets_BtaggedVec;
    std::vector<float> aJets_DPhiMET;
    std::vector<float> aJets_MT;
    std::vector<float> aJets_MTdiff80;
    std::vector<float> aJets_DPhiMET_LepInvis;
    std::vector<float> aJets_MT_LepInvis;
    std::vector<float> aJets_MTdiff80_LepInvis;
    TLorentzVector aJets_TLV;
    TLorentzVector vec_met;
    vec_met.SetPtEtaPhiM(configMgr->obj->met.Et,0.0, configMgr->obj->met.phi,0.0);
    TLorentzVector vec_met_LepInvis;
    vec_met_LepInvis.SetPtEtaPhiM(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et, 0, configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi, 0);
    for(JetVariable* jet : jets){
        aJets_PtVec.push_back(jet->Pt());
        aJets_EtaVec.push_back(jet->Eta());
        aJets_PhiVec.push_back(jet->Phi());
        aJets_MVec.push_back(jet->M());
        aJets_dl1rVec.push_back(jet->dl1r);
        aJets_BtaggedVec.push_back(jet->bjet);
        aJets_TLV.SetPtEtaPhiM( jet->Pt(),jet->Eta(),jet->Phi(),jet->M() );
        aJets_DPhiMET.push_back( fabs( jet->DeltaPhi(configMgr->obj->met) ) );
        aJets_MT.push_back( getM_T(vec_met,aJets_TLV) );
        aJets_MTdiff80.push_back( fabs(80. - getM_T(vec_met,aJets_TLV) ) );
        aJets_DPhiMET_LepInvis.push_back( fabs( jet->DeltaPhi(vec_met_LepInvis) ) );
        aJets_MT_LepInvis.push_back( getM_T(vec_met_LepInvis,aJets_TLV) );
        aJets_MTdiff80_LepInvis.push_back( fabs(80. - getM_T(vec_met_LepInvis,aJets_TLV) ) );
    }

    if (njets > 0) {
      int jetY_Index = std::distance( aJets_DPhiMET.begin(),std::min_element( aJets_DPhiMET.begin(),aJets_DPhiMET.end() ) );
      int jetZ_Index = std::distance( aJets_MTdiff80.begin(),std::min_element( aJets_MTdiff80.begin(),aJets_MTdiff80.end() ) );
      int jetY_LepInvis_Index = std::distance( aJets_DPhiMET_LepInvis.begin(),std::min_element( aJets_DPhiMET_LepInvis.begin(),aJets_DPhiMET_LepInvis.end() ) );
      int jetZ_LepInvis_Index = std::distance( aJets_MTdiff80_LepInvis.begin(),std::min_element( aJets_MTdiff80_LepInvis.begin(),aJets_MTdiff80_LepInvis.end() ) );
      // jetY = minimum DeltaPhiMET; jetZ = minimum (MT - 80)
      configMgr->treeMaker->setIntVariable("jetY_Index", jetY_Index);
      configMgr->treeMaker->setIntVariable("jetZ_Index", jetZ_Index);
      configMgr->treeMaker->setIntVariable("jetY_LepInvis_Index", jetY_LepInvis_Index);
      configMgr->treeMaker->setIntVariable("jetZ_LepInvis_Index", jetZ_LepInvis_Index);
      configMgr->treeMaker->setFloatVariable("jetY_DPhiMet", aJets_DPhiMET[jetY_Index]);
      configMgr->treeMaker->setFloatVariable("jetZ_DPhiMet", aJets_DPhiMET[jetZ_Index]);
      configMgr->treeMaker->setFloatVariable("jetY_LepInvis_DPhiMet", aJets_DPhiMET_LepInvis[jetY_LepInvis_Index]);
      configMgr->treeMaker->setFloatVariable("jetZ_LepInvis_DPhiMet", aJets_DPhiMET_LepInvis[jetZ_LepInvis_Index]);
      configMgr->treeMaker->setFloatVariable("jetY_MT", aJets_MT[jetY_Index]);
      configMgr->treeMaker->setFloatVariable("jetZ_MT", aJets_MT[jetZ_Index]);
      configMgr->treeMaker->setFloatVariable("jetY_LepInvis_MT", aJets_MT_LepInvis[jetY_LepInvis_Index]);
      configMgr->treeMaker->setFloatVariable("jetZ_LepInvis_MT", aJets_MT_LepInvis[jetZ_LepInvis_Index]);
    }

    double Ht30_allJets = getHtJets(jets, 30, 4.5);
    double HtVec30_allJets = getVecHtJets ( jets, 30.0, 4.5);
    configMgr->treeMaker->setFloatVariable("Ht30_allJets", Ht30_allJets );
    configMgr->treeMaker->setFloatVariable("HtVec30_allJets", HtVec30_allJets );

    double Ht20_allJets = getHtJets(jets, 20, 4.5);
    double HtVec20_allJets = getVecHtJets ( jets, 20.0, 4.5);
    configMgr->treeMaker->setFloatVariable("Ht20_allJets", Ht20_allJets );
    configMgr->treeMaker->setFloatVariable("HtVec20_allJets", HtVec20_allJets );
    
    //int tag_index1 = -1, tag_index2 = -1;
    //njets = configMgr->obj->aJets.size();
    //njets = configMgr->obj->jets.size();
    if (njets < 2){ 
        configMgr->treeMaker->setBoolVariable("validVBFtags", validVBFtags);
        return; 
    }
    // New m(jj) calculation is defined as:
    // Iterate through all jets, eta<5 and pT > 30, that are not loose b-jets
    // Only consider pairs of jets which are in opposite hemispheres (eta1 *eta2 < 0)
    // Only consider non b tagged jets
    // Take max m(jj) of all combinations
    // Set the spectator jet tag index to highest pT jet that is not one of the current tags
    float tmp_mjj = -1;
    int nVBFjets = 0;
    for (int i = 0; i < njets; i++) {
        if (jets[i]->bjet) continue;
        if (jets[i]->Pt() < 30.0) continue;
        ++nVBFjets;
        for (int j = i + 1; j < njets; j++) {
            if (jets[j]->bjet) continue;
            if (jets[j]->Pt() < 30.0) continue;
            if (jets[i]->Eta() * jets[j]->Eta() >= 0) continue; 
            if ((*jets[i] + *jets[j]).M() > tmp_mjj)  {
                tmp_mjj = (*jets[i] + *jets[j]).M();
                vbfTagJet1_Index = i;
                vbfTagJet2_Index = j;
            }
        }
    }
    vbfjjM = tmp_mjj;
    configMgr->treeMaker->setIntVariable("nVBFjets", nVBFjets );
    
    // Keep track of this event's information
    if ( (vbfTagJet1_Index >= 0) && (vbfTagJet2_Index >= 0) && (vbfTagJet1_Index != vbfTagJet2_Index) ) validVBFtags = true;
    
    configMgr->treeMaker->setBoolVariable("validVBFtags", validVBFtags);
   
    if (!validVBFtags) return;
    
    JetVariable * j1; //highest pt vbf tagged jet
    JetVariable * j2; //second highest pt vbf tagged jet
    JetVariable * j3; //possible spectator jet

    // Make sure that j1 is returned as the jet with the highest pT!!
    if (jets[vbfTagJet1_Index]->Pt() > jets[vbfTagJet2_Index]->Pt()) {
        j1 = jets[vbfTagJet1_Index];
        j2 = jets[vbfTagJet2_Index];
    } else {
        j1 = jets[vbfTagJet2_Index];
        j2 = jets[vbfTagJet1_Index];
        
        //swap indices
        int tempIndex = vbfTagJet1_Index;
        vbfTagJet1_Index = vbfTagJet2_Index;
        vbfTagJet2_Index = tempIndex;
    }

    vbftag_J1.SetPtEtaPhiM(j1->Pt(), j1->Eta(), j1->Phi(), j1->M());
    vbftag_J2.SetPtEtaPhiM(j2->Pt(), j2->Eta(), j2->Phi(), j2->M());


    float avg_eta = (vbftag_J1.Eta() + vbftag_J2.Eta()) / 2.0;
    float deta = fabs(vbftag_J1.Eta() - vbftag_J2.Eta());
    eventnokey = configMgr->obj->evt.evtNumber;
    
    //Get indices of Non VBF tagged non btagged Jets
    std::vector<int> NonVBFtaggedJets;
    for (int i = 0; i < njets; i++ ) {
        //if (jets[i]->bjet) continue;
        if ( (i != vbfTagJet1_Index) & (i != vbfTagJet2_Index) ) {
            NonVBFtaggedJets.push_back(i);
        }
    }

    //Spectator Jet
    for (int i = 0; i < njets; i++) {
        if (jets[i]->bjet) continue;
        if (i == vbfTagJet1_Index || i == vbfTagJet2_Index) continue;
        if (jets[i]->DeltaR(vbftag_J1) <= 2.0) continue;
        if (jets[i]->DeltaR(vbftag_J2) <= 1.0) continue;
        j3 = jets[i];
        validSpecJet=true;
        SpecJet_Index = i;
        vbftag_J3.SetPtEtaPhiM(j3->Pt(), j3->Eta(), j3->Phi(), j3->M());
        setJetVariables(configMgr, "vbfSpecJet", j3);
        SpecJet_Cen = TMath::Abs((vbftag_J3.Eta() - avg_eta) / deta);
        SpecJet_ExpCen = getJetCentrality(j1, j2, j3); 
        break;
    }

    //Get Centralities and Mrels for 2 leading non VBF tagged Jets
    //These can be btagged! 
    //One of these could also be the Spectator jet!
    if (NonVBFtaggedJets.size() > 0) {
        JetVariable * jC = jets[NonVBFtaggedJets[0]]; 
        jetC.SetPtEtaPhiM(jC->Pt(), jC->Eta(), jC->Phi(), jC->M());
        jetC_Cen = TMath::Abs((jetC.Eta() - avg_eta) / deta);
        jetC_ExpCen  = getJetCentrality (j1, j2, jC);
        jetC_mjjrel = getMjjRel (j1, j2, jC);
        JetC_Index = NonVBFtaggedJets[0];
        setJetVariables(configMgr, "jetC", jC);
    }

    if (NonVBFtaggedJets.size() > 1) {
        JetVariable * jD = jets[NonVBFtaggedJets[1]]; 
        jetD.SetPtEtaPhiM(jD->Pt(), jD->Eta(), jD->Phi(), jD->M());
        jetD_Cen = TMath::Abs((jetD.Eta() - avg_eta) / deta);
        jetD_ExpCen  = getJetCentrality (j1, j2, jD);
        jetD_mjjrel = getMjjRel (j1, j2, jD);
        JetD_Index = NonVBFtaggedJets[1];
        setJetVariables(configMgr, "jetD", jD);
    }

    configMgr->treeMaker->setVecFloatVariable("aJets_Pt",aJets_PtVec);
    configMgr->treeMaker->setVecFloatVariable("aJets_Eta",aJets_EtaVec);
    configMgr->treeMaker->setVecFloatVariable("aJets_Phi",aJets_PhiVec);
    configMgr->treeMaker->setVecFloatVariable("aJets_M",aJets_MVec);
    configMgr->treeMaker->setVecFloatVariable("aJets_Btagged",aJets_BtaggedVec);
    configMgr->treeMaker->setVecFloatVariable("aJets_dl1r",aJets_dl1rVec);

    configMgr->treeMaker->setBoolVariable("validSpecJet",validSpecJet);
    setJetVariables(configMgr, "vbfTagJet1", j1);
    setJetVariables(configMgr, "vbfTagJet2", j2);
    configMgr->treeMaker->setFloatVariable("SpecJet_Cen", SpecJet_Cen);
    configMgr->treeMaker->setFloatVariable("SpecJet_ExpCen", SpecJet_ExpCen);
    configMgr->treeMaker->setFloatVariable("jetC_ExpCen", jetC_ExpCen );
    configMgr->treeMaker->setFloatVariable("jetC_Cen", jetC_Cen );
    configMgr->treeMaker->setFloatVariable("jetC_mjjrel", jetC_mjjrel );
    configMgr->treeMaker->setFloatVariable("jetD_ExpCen", jetD_ExpCen );
    configMgr->treeMaker->setFloatVariable("jetD_Cen", jetD_Cen );
    configMgr->treeMaker->setFloatVariable("jetD_mjjrel", jetD_mjjrel );

    configMgr->treeMaker->setIntVariable("vbfTagJet1_Index", vbfTagJet1_Index);
    configMgr->treeMaker->setIntVariable("vbfTagJet2_Index", vbfTagJet2_Index);
    configMgr->treeMaker->setIntVariable("SpecJet_Index", SpecJet_Index);
    configMgr->treeMaker->setIntVariable("jetC_Index", JetC_Index);
    configMgr->treeMaker->setIntVariable("jetD_Index", JetD_Index);

    
    double met_Et_MinusBaseL1 = getMetMinusBaseL1(configMgr);
    configMgr->treeMaker->setFloatVariable("met_Et_MinusBaseL1" , met_Et_MinusBaseL1);
    double met_Et_MinusBaseL1L2 = getMetMinusBaseL1L2(configMgr);
    configMgr->treeMaker->setFloatVariable("met_Et_MinusBaseL1L2" , met_Et_MinusBaseL1L2);


}



void Common::setSpecTag(ConfigMgr*& configMgr) {
    
    auto jets = configMgr->obj->aJets;
    JetVariable * j3;
    
    int njets = 0;
    njets = configMgr->obj->cJets.size();
    for (int i = 0; i < njets; i++) {
        if (jets[i]->bjet) continue;
    if (jets[i]->DeltaR(vbftag_J1) <= 2.0) continue;
    if (jets[i]->DeltaR(vbftag_J2) <= 1.0) continue;
    j3 = jets[i];
    Common::setJetVariables(configMgr, "vbfSpecJet", j3);
    configMgr->treeMaker->setBoolVariable("validSpecJet",true);
    validSpecJet=true;
    vbftag_J3.SetPtEtaPhiM(j3->Pt(), j3->Eta(), j3->Phi(), j3->M());
    break;
    }

}

void Common::VBF_computeTruthVariables(ConfigMgr* & configMgr) {

    double ptV = -1;
    double Truth_mjj = -1; 
    double Truth_vbfjjDPhi = -1;
    double Truth_j0_pt = -1;
    double Truth_j1_pt = -1;
    double Truth_MET = -1;

    int NtruthJets = -1;
    int NtruthStatus3Leptons = -1;

    int truthStatus3Lepton1_pdgId = 0;
    double truthStatus3Lepton1_Pt = -1;
    double truthStatus3Lepton1_Eta = 0;
    double truthStatus3Lepton1_Phi = 0;
    double truthStatus3Lepton1_E = -1;

    int truthStatus3Lepton2_pdgId = 0;
    double truthStatus3Lepton2_Pt = -1;
    double truthStatus3Lepton2_Eta = 0;
    double truthStatus3Lepton2_Phi = 0;
    double truthStatus3Lepton2_E = -1;

    auto TruthStatus3Leptons =  configMgr->obj->truthStatus3Leptons;
    NtruthStatus3Leptons = TruthStatus3Leptons.size();

    if (NtruthStatus3Leptons > 0) {
      truthStatus3Lepton1_pdgId = TruthStatus3Leptons[0]->pdgId;
      truthStatus3Lepton1_Pt = TruthStatus3Leptons[0]->Pt();
      truthStatus3Lepton1_Eta = TruthStatus3Leptons[0]->Eta();
      truthStatus3Lepton1_Phi = TruthStatus3Leptons[0]->Phi();
      truthStatus3Lepton1_E = TruthStatus3Leptons[0]->E();
      if (NtruthStatus3Leptons > 1) {
        truthStatus3Lepton2_pdgId = TruthStatus3Leptons[1]->pdgId;
        truthStatus3Lepton2_Pt = TruthStatus3Leptons[1]->Pt();
        truthStatus3Lepton2_Eta = TruthStatus3Leptons[1]->Eta();
        truthStatus3Lepton2_Phi = TruthStatus3Leptons[1]->Phi();
        truthStatus3Lepton2_E = TruthStatus3Leptons[1]->E();
      }
    }


    /*
    int NtruthStatus20Leptons = -1; 
    int NtruthStatus3Jets = -1; 
    int NtruthStatus20Jets = -1; 
    */

    const int DSID = configMgr->getDSID();

    if (Common::IsZjets_Sherpa(DSID) | Common::IsWjets_Sherpa(DSID) | Common::IsEW_Vjets_Sherpa(DSID) | Common::IsZjets_MadGraph(DSID) | Common::IsWjets_MadGraph(DSID)) {

        //Both Sherpa and MadGraph  
        //auto TruthParticles = configMgr->obj->truthParticles;
        // auto TruthStatus3Leptons =  configMgr->obj->truthStatus3Leptons;
        
        /*
        auto TruthStatus20Leptons =  configMgr->obj->truthStatus20Leptons;
        auto TruthStatus3Jets =  configMgr->obj->truthStatus3Jets;
        auto TruthStatus20Jets =  configMgr->obj->truthStatus20Jets;
        */

        auto TruthLeptons =  configMgr->obj->truthLeptons;
        auto TruthVBFJets = VBF_getVBFTruthJets(configMgr, 20.0, 5.0); //does OR with truth electrons and taus (in addition to what is done in SusySkimMaker/Objects::getTruth())

        NtruthJets = TruthVBFJets.size();
        // NtruthStatus3Leptons = TruthStatus3Leptons.size();
        //NtruthStatus20Leptons = TruthStatus20Leptons.size();
        //NtruthStatus3Jets = TruthStatus3Jets.size();
        //NtruthStatus20Jets = TruthStatus20Jets.size();
        
        if (TruthVBFJets.size() >= 2) {
            Truth_mjj = (TruthVBFJets.at(0)->truthTLV + TruthVBFJets.at(1)->truthTLV).M();
        }
        
        //Sherpa Only
        if (Common::IsZjets_Sherpa(DSID) | Common::IsWjets_Sherpa(DSID)) {
            ptV = VBF_computeTruthPtV(configMgr);
        }

        //MadGraph Only 
        if ( Common::IsZjets_MadGraph(DSID) | Common::IsWjets_MadGraph(DSID) ){

            if (TruthVBFJets.size() >= 2) {
                Truth_vbfjjDPhi = TMath::Abs (TruthVBFJets[0]->truthTLV.DeltaPhi(TruthVBFJets[1]->truthTLV) );
            }
            if (TruthVBFJets.size() >= 1) Truth_j0_pt = TruthVBFJets[0]->Pt();
            if (TruthVBFJets.size() >= 2) Truth_j1_pt = TruthVBFJets[1]->Pt();
            Truth_MET = configMgr->obj->met.getTruthTLV().Et();
        } 

    }

    //Write
    configMgr->treeMaker->setFloatVariable ("Truth_vbfPtV", ptV);
    configMgr->treeMaker->setFloatVariable ("Truth_vbfjjM", Truth_mjj);
    configMgr->treeMaker->setFloatVariable ("Truth_vbfjjDPhi", Truth_vbfjjDPhi);
    configMgr->treeMaker->setFloatVariable ("Truth_j0_pt", Truth_j0_pt);
    configMgr->treeMaker->setFloatVariable ("Truth_j1_pt", Truth_j1_pt);
    configMgr->treeMaker->setFloatVariable ("Truth_MET", Truth_MET);

    configMgr->treeMaker->setIntVariable ("NtruthVBFJets", NtruthJets);
    configMgr->treeMaker->setIntVariable ("NtruthStatus3Leptons", NtruthStatus3Leptons);
    //configMgr->treeMaker->setIntVariable ("NtruthStatus20Leptons", NtruthStatus20Leptons);
    //configMgr->treeMaker->setIntVariable ("NtruthStatus3Jets", NtruthStatus3Jets);
    //configMgr->treeMaker->setIntVariable ("NtruthStatus20Jets", NtruthStatus20Jets);

    configMgr->treeMaker->setIntVariable ("truthStatus3Lepton1_pdgId", truthStatus3Lepton1_pdgId);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton1_Pt", truthStatus3Lepton1_Pt);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton1_Eta", truthStatus3Lepton1_Eta);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton1_Phi", truthStatus3Lepton1_Phi);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton1_E", truthStatus3Lepton1_E);

    configMgr->treeMaker->setIntVariable ("truthStatus3Lepton2_pdgId", truthStatus3Lepton2_pdgId);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton2_Pt", truthStatus3Lepton2_Pt);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton2_Eta", truthStatus3Lepton2_Eta);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton2_Phi", truthStatus3Lepton2_Phi);
    configMgr->treeMaker->setFloatVariable ("truthStatus3Lepton2_E", truthStatus3Lepton2_E);


}


TruthVector Common::VBF_getVBFTruthJets(ConfigMgr*& configMgr, float TruthJetPtMin, float TruthJetEtaMax)
{

    TruthVector TruthVBFJets;
    TruthVector TruthElectrons; 
    
    auto TruthAllJets = configMgr->obj->truthAllJets;
    auto TruthTaus = configMgr->obj->truthTaus;
    auto TruthLeptons = configMgr->obj->truthLeptons;

    /*

    //Test
	int nLep_base = configMgr->obj->baseLeptons.size();
    for (int i=0; i < nLep_base; i++) { 
        std::cout << "origin " << configMgr->obj->baseLeptons[i]->origin << std::endl;  
        }

    std::cout << "base Leptons size : "  << nLep_base << ", truth Leptons size : " << TruthLeptons.size() << std::endl; 
    
    */
    
    // DMJ TODO: This might need altering (for the MG samples in particular)
    for (const auto &lepton : TruthLeptons) {

        if (abs(lepton->pdgId) == 11) TruthElectrons.push_back(lepton);
    }

    for ( const auto &jet : TruthAllJets) {
        if ( (jet->Pt() < TruthJetPtMin) || (fabs(jet->Eta()) > TruthJetEtaMax) ) continue;   
        bool passOR = true;

        if (TruthElectrons.size() >= 1){
            for (const auto &part : TruthElectrons) {
                if (part->Pt() < 20.0) continue; 
                if (fabs(part->Eta()) > 5.0) continue; 
                if (part->status != 1) continue; 
                //std:: cout << "Electron parent pdgId : " << part->parentPdgId << std::endl;
                if (  !( (abs(part->parentPdgId)==12) || (abs(part->parentPdgId)==13) ) ) continue;  //require coming from W or Z - needs checking!!
                if ( jet->truthTLV.DeltaR(part->bareTLV) < 0.3 ) passOR = false;
            }
        }

        if (TruthTaus.size() >= 1){
            for (const auto &part : TruthTaus) {
                if (part->Pt() < 20.0) continue; 
                if (fabs(part->Eta()) > 5.0) continue; 
                if (part->status == 3) continue; 
                if (  !( (abs(part->parentPdgId)==12) || (abs(part->parentPdgId)==13) ) ) continue; //require coming from W or Z - needs checking !! 
                if ( jet->truthTLV.DeltaR(part->bareTLV) < 0.3 ) passOR = false;
            }
        }
        
        if (passOR) TruthVBFJets.push_back(jet);
    }

    //Sort 
    std::sort(TruthVBFJets.begin(), TruthVBFJets.end(), GreaterPt);

    return TruthVBFJets;
}


double Common::VBF_computeTruthPtV (ConfigMgr*& configMgr) 
{
    double TruthPtV = -1;
    auto TruthStatus3Leptons =  configMgr->obj->truthStatus3Leptons;
    if (TruthStatus3Leptons.size() >= 2) {
        TruthPtV = (TruthStatus3Leptons.at(0)->bareTLV + TruthStatus3Leptons.at(1)->bareTLV).Pt();
    }
    return TruthPtV; 
}

void Common::VBF_CorrectSherpaWeights(ConfigMgr* & configMgr)
{
    //Set events with high weights in Sh221 samples to 1
    const int DSID = configMgr->getDSID();
    double eventWeight = configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT);
    if (( (364156 <= DSID && DSID <= 364215) | // Strong_Wjets_Sh221_maxHTpTV
          (364224 <= DSID && DSID <= 364229) | // Strong_Wjets_Sh221_pTV
          (364100 <= DSID && DSID <= 364141) | // Strong_Zll_jets_Sh221_maxHTpTV
          (364216 <= DSID && DSID <= 364221) | // Strong_Zll_jets_Sh221_pTV
          (364142 <= DSID && DSID <= 364155) | // Strong_Znunu_jets_Sh221_maxHTpTV
          (364222 <= DSID && DSID <= 364223) | // Strong_Znunu_jets_Sh221_pTV
          (366010 <= DSID && DSID <= 366017) | (366019 <= DSID && DSID <= 366026) | (366028 <= DSID && DSID <= 366035) | //Strong_Znunu_jets_Sh221_pTV_Mjj
          (308092 <= DSID && DSID <= 308098) ) // EW_Vjets_Sh221_TChan
          && abs(eventWeight) > 100 ) 
        {
            eventWeight = 1;
            configMgr->treeMaker->setDoubleVariable("eventWeight", eventWeight);
        }
}



void Common::computeMT2lephad(ConfigMgr*& configMgr) {
    if (!validSpecJet) return;
    int nLep_base = configMgr->obj->baseLeptons.size();
    const LeptonVariable* lep1 = nLep_base < 1 ? new LeptonVariable() : configMgr->obj->baseLeptons[0];
    const LeptonVariable* lep2 = nLep_base < 2 ? new LeptonVariable() : configMgr->obj->baseLeptons[1];
    ComputeMT2 mt2_lephadsys = ComputeMT2(*lep1 + *lep2, vbftag_J3, configMgr->obj->met, 100.0, 100.0);
    float mt2_lephadval = mt2_lephadsys.Compute();

    float mT = 0.0;
    mT = TMath::Sqrt(2.0 * vbftag_J3.Pt() * configMgr->obj->met.Et * (1.0 - TMath::Cos(vbftag_J3.DeltaPhi(configMgr->obj->met) ) ));

    configMgr->treeMaker->setFloatVariable("mt_jet3",mT);
    configMgr->treeMaker->setFloatVariable("mt2_WZ_HadLep",mt2_lephadval);

    if (nLep_base < 1) delete lep1; 
    if (nLep_base < 2) delete lep2; 
}

TLorentzVector Common::getVBFTagJet1(ConfigMgr*& configMgr) {
    if (validVBFtags == false) {
        MsgLog::WARNING("Common::getVBFTagJet1", "No tagjets found!");
        return TLorentzVector();
    }
    if (configMgr->obj->evt.evtNumber != eventnokey) {
        MsgLog::WARNING("Common::getVBFTagJet1", "You attempted to grap another event's tag jets!!");
        return TLorentzVector();
    }
    return vbftag_J1;
}
TLorentzVector Common::getVBFTagJet2(ConfigMgr*& configMgr) {
    if (validVBFtags == false) {
        MsgLog::WARNING("Common::getVBFTagJet2", "No tagjets found!");
        return TLorentzVector();
    }
    if (configMgr->obj->evt.evtNumber != eventnokey) {
        MsgLog::WARNING("Common::getVBFTagJet2", "You attempted to grap another event's tag jets!!");
        return TLorentzVector();
    }
    return vbftag_J2;
}


void Common::computeVBFJigSawVariables(ConfigMgr*& configMgr) {
    
    //Only compute for valid VBF events
    if (validVBFtags == false) return;
    int nLep_base = configMgr->obj->baseLeptons.size();

    //Return if nothing useful to add, to suppress warnings of empty rest frames
    //if (validSpecJet == false && nLep_base == 0) return;
    //if (nLep_base < 2) return; 

    using namespace RestFrames;

    LabRecoFrame        LAB("LAB","LAB");
    DecayRecoFrame      CM("CM","CM");
    // ISR/FSR frame
    VisibleRecoFrame    VBF("VBF","VBF");
    // SUSY frame
    DecayRecoFrame      S("S","S");
    VisibleRecoFrame    specJ("specJ","j_{3}");
    VisibleRecoFrame    LL("L","#it{ll}");


    InvisibleRecoFrame  I("I","Inv");
    // Groups
    InvisibleGroup      INV("INV","Invisible System");

    SetRapidityInvJigsaw    RapidityJigsaw    ("RapidityJigsaw",    "#eta_{I} = #eta_{VBF} Jigsaw");
    SetMassInvJigsaw        MassInvJigsaw     ("InvMass",           "m_{I} = m_{S} Jigsaw");

    //-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//

    LAB.SetChildFrame(CM);
    CM.AddChildFrame(VBF);
    CM.AddChildFrame(S);
    if (validSpecJet) S.AddChildFrame(specJ);
    if (nLep_base > 0) S.AddChildFrame(LL);
    S.AddChildFrame(I);

    if(!LAB.InitializeTree())   MsgLog::WARNING("Common::computeVBFJigSawVariables","..Failed initializing reconstruction trees");

    //-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//-//
    // Invisible Jigsaw Rules (resolve mass and set rapidity based on VBF system)
    INV.AddFrame(I);
    INV.AddJigsaw(MassInvJigsaw);
    INV.AddJigsaw(RapidityJigsaw);
    MassInvJigsaw.AddVisibleFrames(S.GetListVisibleFrames());
    RapidityJigsaw.AddVisibleFrames(S.GetListVisibleFrames());

    if(!LAB.InitializeAnalysis()) MsgLog::WARNING("Common::computeVBFJigSawVariables","...Failed initialized analyses");

    /////////////////////////////////////////////////////////////////////////////////////////

    LAB.ClearEvent();

    TVector3 ETMiss;
    ETMiss.SetPtEtaPhi(configMgr->obj->met.Et,0.0,configMgr->obj->met.phi);
    
    VBF.SetLabFrameFourVector(vbftag_J1 + vbftag_J2);
  
    /*
    TLorentzVector Lep1;
    TLorentzVector Lep2;
    const LeptonVariable* lep1 = nLep_base < 1 ? new LeptonVariable() : configMgr->obj->baseLeptons[0];
    const LeptonVariable* lep2 = nLep_base < 2 ? new LeptonVariable() : configMgr->obj->baseLeptons[1];
    if (nLep_base >= 2) LL.SetLabFrameFourVector(*lep1 + *lep2);
    */
    
    //Add all leptons to visible frame
    TLorentzVector TotalLep;
    for (int i = 0; i < nLep_base; i ++){
        TLorentzVector LepVec;
        LeptonVariable *lep = configMgr->obj->baseLeptons[i];
        LepVec.SetPtEtaPhiM(lep->Pt(), lep->Eta(), lep->Phi(), lep->M());
        TotalLep += LepVec;
    }
    LL.SetLabFrameFourVector(TotalLep);
    
    INV.SetLabFrameThreeVector(ETMiss);

    if (validSpecJet) specJ.SetLabFrameFourVector(vbftag_J3);

    if (!LAB.AnalyzeEvent()) MsgLog::WARNING("Common::computeVBFJigSawVariables","..Failed analyzing event");

    //RJR variables
    float m_PTVBF = 0.0;
    float m_PTS = 0.0;
    float m_RVBF = 0.0;
    float m_MS = 0.0;
    float m_MVBF = 0.0;
    float m_dphiVBFI = 0.0;

    TVector3 vP_VBF = VBF.GetFourVector(CM).Vect();
    TVector3 vP_I   = I.GetFourVector(CM).Vect();
    TVector3 vP_S   = S.GetFourVector(CM).Vect();
    m_PTVBF = vP_VBF.Mag();
    m_PTS = vP_S.Mag();
    m_RVBF = fabs(vP_I.Dot(vP_VBF.Unit())) / m_PTVBF;
    m_MS = S.GetMass();
    m_MVBF = VBF.GetMass();
    m_dphiVBFI = fabs(vP_VBF.DeltaPhi(vP_I));

    configMgr->treeMaker->setFloatVariable("vbfRJR_PTVBF",m_PTVBF);
    configMgr->treeMaker->setFloatVariable("vbfRJR_PTS",m_PTS);
    configMgr->treeMaker->setFloatVariable("vbfRJR_RVBF",m_RVBF);
    configMgr->treeMaker->setFloatVariable("vbfRJR_MS",m_MS);
    configMgr->treeMaker->setFloatVariable("vbfRJR_MVBF",m_MVBF);
    configMgr->treeMaker->setFloatVariable("vbfRJR_dphiVBFI",m_dphiVBFI);
}




double Common::getJetCentrality (JetVariable * jet1, JetVariable * jet2, JetVariable * jetI)
{
    double eta1 = jet1->Eta();
    double eta2 = jet2->Eta();
    double etaI = jetI->Eta();

    double centrality = TMath::Exp(-4*1*TMath::Power(eta1 - eta2, -2) * TMath::Power((etaI - 0.5*(eta1 +eta2)), 2) );
    return centrality;
}

double Common::getMjjRel (JetVariable * jet1, JetVariable * jet2, JetVariable * jetI) 
{
    double mjj_12 = (*jet1 + *jet2).M();
    double mjj_1I = (*jet1 + *jetI).M();
    double mjj_2I = (*jet2 + *jetI).M();

    double m_compare = (mjj_1I < mjj_2I) ? mjj_1I : mjj_2I;
    return m_compare/mjj_12;
}

//Transverse mass of two particle system
double Common::getM_T(TLorentzVector vec1, TLorentzVector vec2) {
     double mt = (vec1.Mt() + vec2.Mt())*(vec1.Mt() + vec2.Mt()) -
     (vec1+vec2).Perp2();
      mt = (mt >= 0.) ? sqrt(mt) : sqrt(-mt);
       return mt;
}

double Common::getHtJets (std::vector<JetVariable*> Jets, float minPt, float maxEta) {

    double Ht = 0; 
    for (JetVariable* jet: Jets){
        if (jet->Pt() < minPt) continue; 
        if (fabs(jet->Eta()) > maxEta) continue;
        Ht += jet->Pt();
    }
    return Ht;
}

double Common::getVecHtJets (std::vector<JetVariable*> Jets, float minPt, float maxEta) {

    TLorentzVector SumJetVec;

    for (JetVariable* jet: Jets){
        if (jet->Pt() < minPt) continue; 
        if (fabs(jet->Eta()) > maxEta) continue;
        TLorentzVector TempJetVec;    
        TempJetVec.SetPtEtaPhiM(jet->Pt(), jet->Eta(), jet->Phi(), jet->M());
        SumJetVec += TempJetVec;
    }
    return SumJetVec.Pt();
}



bool Common::IsCombiBaselineLepton(LeptonVariable* lep){
  //additional requirements compared to our SUSYTools baseline
  if(lep->isEle()){
    const ElectronVariable* el = dynamic_cast<const ElectronVariable*>(lep);
    if (!(el->looseBLllh)) return false;
    return true;
  }
  else if(lep->isMu()){
    const MuonVariable* mu = dynamic_cast<const MuonVariable*>(lep);
    if (!mu->passesMedium()) return false;
    return true;
  }
  return false;
}


float Common::getDPhiAllJNMet(Objects *obj, unsigned int jetIndex, float jet_threshold, bool useTrackMET, bool useLepInvis)
{
  // Azimuthal angle between leading jet and MET
  float DPhiJetMet = 999.;
  
  if(obj->aJets.size() > jetIndex && obj->aJets[jetIndex]->Pt() > jet_threshold) { 

    if(useTrackMET){
      DPhiJetMet = fabs(obj->aJets[jetIndex]->DeltaPhi( obj->trackMet ));
    }
    else if(useLepInvis){
      DPhiJetMet = fabs(obj->aJets[jetIndex]->DeltaPhi(*obj->getMETFlavor(MetVariable::INVISIBLE_LEP) ));
    }
    else{
      DPhiJetMet = fabs(obj->aJets[jetIndex]->DeltaPhi( obj->met ));
    }
  }
  return DPhiJetMet;
}


float Common::getMinDRaJets(const Objects* obj, const TLorentzVector* ref, float minjetpt)
{
  float mindr = 99;
  for( auto& j : obj->aJets ){
    if(  j->Pt() < minjetpt ) continue;
    float dr =  j->DeltaR( *ref );
    if (dr < mindr && dr > 0.0001)
      mindr = dr;
  }
  return mindr;
}

double Common::getMetMinusBaseL1(ConfigMgr*& configMgr){
    if (configMgr->obj->baseLeptons.size() > 0) {
        //Put met in a tlorentz vector
        TLorentzVector TLV_MET;
        TLV_MET.SetPtEtaPhiM(configMgr->obj->met.Et, 0, configMgr->obj->met.phi, 0);
        //Get leading baseline lepton
        LeptonVariable* BaseLep1 = configMgr->obj->baseLeptons[0]; 
        TLorentzVector TLV_BaseLep1;
        TLV_BaseLep1.SetPtEtaPhiM(BaseLep1->Pt(), BaseLep1->Eta(), BaseLep1->Phi(), BaseLep1->M());
        return (TLV_MET + TLV_BaseLep1).Pt();
    }
    else return -1; 
}

double Common::getMetMinusBaseL1L2(ConfigMgr*& configMgr){
    if (configMgr->obj->baseLeptons.size() > 1) {
        //Put met in a tlorentz vector
        TLorentzVector TLV_MET;
        TLV_MET.SetPtEtaPhiM(configMgr->obj->met.Et, 0, configMgr->obj->met.phi, 0);
        //Get leading baseline lepton
        LeptonVariable* BaseLep1 = configMgr->obj->baseLeptons[0]; 
        LeptonVariable* BaseLep2 = configMgr->obj->baseLeptons[1]; 
        TLorentzVector TLV_BaseLep1;
        TLorentzVector TLV_BaseLep2;
        TLV_BaseLep1.SetPtEtaPhiM(BaseLep1->Pt(), BaseLep1->Eta(), BaseLep1->Phi(), BaseLep1->M());
        TLV_BaseLep2.SetPtEtaPhiM(BaseLep2->Pt(), BaseLep2->Eta(), BaseLep2->Phi(), BaseLep2->M());
        return (TLV_MET + TLV_BaseLep1 + TLV_BaseLep2).Pt();
    }
    else return -1; 
}

bool Common::hasZCandidate(const Objects* obj, bool useBaseline, float massWindow){
    bool hasZCandidate = false;
    LeptonVector lep = obj->signalLeptons;
    if(useBaseline) lep = obj->baseLeptons;  
  
    if( lep.size() >= 2 ){
        // Loop through all pairwise combinations of leptons
        for(unsigned int ilep=0; ilep<lep.size()-1; ilep++){
            for(unsigned int jlep=ilep+1; jlep<lep.size(); jlep++){
                LeptonVariable* tmp_lep1 = lep[ilep];
                LeptonVariable* tmp_lep2 = lep[jlep];

                float tmp_mll = ( *tmp_lep1 + *tmp_lep2 ).M();

                bool SF = (tmp_lep1->isEle() && tmp_lep2->isEle()) || (tmp_lep1->isMu() && tmp_lep2->isMu());
                bool OS = (tmp_lep1->q == -tmp_lep2->q);
                bool withinWindow = abs(91.2 - tmp_mll) < massWindow;
                if(SF && OS && withinWindow){ return true;}
            }
        }
    }
  return hasZCandidate;
}

void Common::computeVBF_ZCR_Vars(ConfigMgr*& configMgr)
{
    // lorentz vector of ll system for Z CR: SFOS leptons passing specific requirements with invariant mass closest to Z boson mass
    const float ZBOSONMASS = 91.1876;
    double mll = -1;
    TLorentzVector ll; 
    int ZCR_mll_Lep1Index = -1; 
    int ZCR_mll_Lep2Index = -1; 
    int ZCR_mll_flavour = -1;
    LeptonVector leptons = configMgr->obj->baseLeptons;
    int nLep_base = leptons.size();
    LeptonVariable* lep1 = nLep_base < 1 ? new LeptonVariable() : leptons[0];
    LeptonVariable* lep2 = nLep_base < 2 ? new LeptonVariable() : leptons[1];

    if (leptons.size() > 1){
        for (UInt_t i = 0; i < nLep_base; i++){
            if (!LepPassVBF_ZCR(leptons[i])) continue; 
            lep1 = leptons[i];
            for (UInt_t j = i+1; j < leptons.size(); j++){
                if (!obs.isSFOS(leptons[i],leptons[j])) continue; 
                lep2 = leptons[j];
                double mass = (*lep1+*lep2).M(); 
                if( mll<0.0 || fabs(mass-ZBOSONMASS)<fabs(mll-ZBOSONMASS)){
                    mll = mass; 
                    ll.SetPtEtaPhiM((*lep1 + *lep2).Pt(), (*lep1 + *lep2).Eta(), (*lep1+*lep2).Phi(), (*lep1+*lep2).M()); 
                    ZCR_mll_Lep1Index = i; 
                    ZCR_mll_Lep2Index = j; 
                    if (lep1->isEle() && lep2-> isEle()) ZCR_mll_flavour = 1;
                    else if (lep1->isMu() && lep2-> isMu()) ZCR_mll_flavour = 2;
                }
            }
        }
    }
    
    setllVars(configMgr, "ZCR_ll", lep1, lep2 );
    configMgr->treeMaker->setIntVariable("ZCR_mll_Lep1Index", ZCR_mll_Lep1Index);
    configMgr->treeMaker->setIntVariable("ZCR_mll_Lep2Index", ZCR_mll_Lep2Index);
    configMgr->treeMaker->setIntVariable("ZCR_mll_flavour", ZCR_mll_flavour); //1 = electron, 2 = muon

    if (nLep_base < 1) delete lep1; 
    if (nLep_base < 2) delete lep2; 
}


bool Common::LepPassVBF_ZCR(const LeptonVariable* lep)
{
  if(lep->isEle()){
    const ElectronVariable* el = dynamic_cast<const ElectronVariable*>(lep);
    return ElePassVBF_ZCR(el);
  }
  else if(lep->isMu()){
    const MuonVariable* mu = dynamic_cast<const MuonVariable*>(lep);
    return MuPassVBF_ZCR(mu);
  }
  return false;
}

bool Common::ElePassVBF_ZCR(const ElectronVariable* lep)
{ 
    if (!lep->looseBLllh) return false; 
    if (!lep->IsoFCLoose) return false; 
    bool Passd0Sig = (fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0) < 5;
    bool PassZ0SinTheta = fabs(lep->z0 * TMath::Sin(lep->Theta())) < 0.5 ;
    if (!Passd0Sig) return false;
    if (!PassZ0SinTheta) return false;
    return true;
}


bool Common::MuPassVBF_ZCR(const MuonVariable* lep)
{ 
    if (!lep->passesLoose()) return false;
    if (!lep->IsoFCLoose) return false; 
    bool Passd0Sig = (fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0) < 3;
    bool PassZ0SinTheta = fabs(lep->z0 * TMath::Sin(lep->Theta())) < 0.5 ;
    if (!Passd0Sig) return false;
    if (!PassZ0SinTheta) return false;
    return true;
}


bool Common::LepPassVBF_WCR(const LeptonVariable* lep){
    if (lep->isEle()){
        const ElectronVariable* el = dynamic_cast<const ElectronVariable*>(lep);
        return ElePassVBF_WCR(el);
    }
    else if (lep->isMu()){
        const MuonVariable* mu = dynamic_cast<const MuonVariable*>(lep);
        return (!MuPassVBF_WCR(mu));
    }
    return false;
}

bool Common::ElePassVBF_WCR(const ElectronVariable* lep)
{ 
    if (!(lep->Pt() > 30)) return false; 
    if (!lep->tightllh) return false; 
    if (!lep->IsoFCLoose) return false; 
    if (lep->Pt() > 200 && !(lep->IsoFCHighPtCaloOnly) ) return false; 
    bool Passd0Sig = (fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0) < 5;
    bool PassZ0SinTheta = fabs(lep->z0 * TMath::Sin(lep->Theta())) < 0.5 ;
    if (!Passd0Sig) return false;
    if (!PassZ0SinTheta) return false;
    return true;
}

bool Common::MuPassVBF_WCR(const MuonVariable* lep)
{ 
    if (!(lep->Pt() > 30)) return false; 
    if (!lep->passesMedium()) return false;
    if (!lep->IsoFCLoose) return false; 
    bool Passd0Sig = (fabs(lep->d0Err) > 0 ? fabs(lep->d0 / lep->d0Err) : 999.0) < 3;
    bool PassZ0SinTheta = fabs(lep->z0 * TMath::Sin(lep->Theta())) < 0.5 ;
    if (!Passd0Sig) return false;
    if (!PassZ0SinTheta) return false;
    return true;
}
