# Software used for v2.7b ntuples
<pre>
AnalysisBase 21.2.55
SUSYTools from here: /usatlas/u/jgonski/SusySkim_v27/source/SUSYTools
SusySkimMaker-compressed-ewk-v2.7b
SusySkimDriver-compressed-ewk-v2.7b
new: Ext_RestFrames
</pre>


# Setup the code
<pre>
mkdir myArea
cd myArea
mkdir source build
cd source
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-higgsino/SusySkimHiggsino.git
git clone ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimMaker.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-wg/Common/Ext_RestFrames.git
source SusySkimMaker/scripts/setupRelease.sh --release21
build
</pre>

(In the past we've used customized SUSYTools is currently customized with some extra signal xsec files, and some protections I've added for edge cases that come up and cause seg faults in the framework. These protections are currently being added to the SUSYTools master, and xsecs are being handled elsewhere in PMG, so from this point it should be fine to take whatever SUSYTools comes with the AnalysisBase.)

I also recommend going into  set_groupset_env.sh and setting GROUP_SET_TAG to v2.7b (or whatever tag) since it in principle means we don't need to specify the tag in later steps and makes some of the merging simpler. In principle we can do the same with the deep config file, but I had some issues recently because the deep config was being set in set_groupset_env.sh, in the groupSet, and on the command line, and it wasn't obvious which took precedence! So now I have been setting it explicitly when running any steps which don't involve the selector.

In subsequent setups, you can just do

<pre>
cd myArea/source
source setup.sh
build
</pre>

and of course run "build" again every time you need to recompile.

The addition of the Ext_RestFrames directory means there are some environment variables that need to be set after compilation:
<pre>
export CMAKE_PREFIX_PATH=$WorkDir_DIR/../x86_64-slc6-gcc62-opt/lib:$CMAKE_PREFIX_PATH
export DYLD_LIBRARY_PATH=$WorkDir_DIR/../x86_64-slc6-gcc62-opt/lib:$DYLD_LIBRARY_PATH
export PATH=$WorkDir_DIR/../x86_64-slc6-gcc62-opt/bin:$PATH
export ROOT_INCLUDE_PATH=$WorkDir_DIR/../x86_64-slc6-gcc62-opt/include:$ROOT_INCLUDE_PATH
export LD_LIBRARY_PATH=$WorkDir_DIR/../x86_64-slc6-gcc62-opt/lib:$LD_LIBRARY_PATH
</pre>

# Making the ntuples

The actual run commands use input groupSets, which are all stored [here](https://gitlab.cern.ch/atlas-phys-susy-higgsino/SusySkimHiggsino/tree/master/data/samples). The groupset.config file in each groupSet allows you to specify the systematics file you want to run over. I use the following commands to test with a singe input file in place of a full groupset, both locally and with condor. If you want to run over data, you need to add the "--isData" flag.

**0. Useful Tests**
<pre>
run_xAODNtMaker --skipPRWCheck -selector HiggsinoSelector -F mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_SUSY16.e6348_s3126_r10724_p3652/DAOD_SUSY16.15633639._000003.pool.root.1 -d "test_local" -tag test -writeTrees 1 -writeSkims 1
run_xAODNtMaker --skipPRWCheck -selector HiggsinoSelector -F mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_SUSY16.e6348_s3126_r10724_p3652/DAOD_SUSY16.15633639._000003.pool.root.1 -d "test_condor" -tag test --condor |& tee log_test_condor.txt;
</pre>

The order of operations for a full production is as follows. Note that the example given is just for mc16a backgrounds; a full production for the 2019 paper has 7 commands: mc16a, mc16d, and mc16e, separated into signal and background (because signal is AFII and has a different config specification), and data. 

**1. Run**: make the trees and skims. 
<pre>
run_xAODNtMaker --skipPRWCheck -selector HiggsinoSelector -groupSet R21_SUSY16_repro_Bkgs_mc16a_allSys -d "/usatlas/groups/bnl_local/jgonski/December15_2018_Higgsino_R21_v2.7b_Production/R21_SUSY16_repro_Bkgs_mc16a_allSys_Skims/skims" -tag v2.7b --condor |& tee log_bkgs_mc16a_v2.7b.txt
</pre>
An output directory structure will be created at the path you give as the `-d` parameter; the full directory must already exist, but in the example given here, the `skims` part of the path will be added as a prefix to the sample name. The `fetch` subdirectory contains the output trees and skims. `status` contains a simple fail/completed tag for each subjob, and `submit` contains the subjob log/err files.

In 2019, this step could take anywhere from 24-48 hours without systematics, and up to 3-4 weeks for full systematics.

**2. Make/move**: make the final directory structure necessary for merging, which includes a directory that shares the groupset name and the tag (example: `R21_SUSY16_repro_Bkgs_mc16a_NoSys/v2.7b`). Within that directory is a `trees` directory, into which all production output trees in `fetch/data-tree/` are moved via the moveCondorOutputsToGroupsetStructure Command.   No real computing is done at this level, no jobs are submitted to condor, and this step proceeds quickly. 
<pre>
run_makeDirectory -groupSet R21_SUSY16_repro_Bkgs_mc16a_allSys -d "/usatlas/groups/bnl_local/jgonski/December15_2018_Higgsino_R21_v2.7b_Production/R21_SUSY16_repro_Bkgs_mc16a_allSys_Skims" -tag v2.7b -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21.config |& tee log_fix_bkgs_v2.7b_makeDir.txt;
moveCondorOutputsToGroupsetStructure -subdirPrefix skims -groupSet R21_SUSY16_repro_Bkgs_mc16a_allSys -d "/usatlas/groups/bnl_local/jgonski/December15_2018_Higgsino_R21_v2.7b_Production/R21_SUSY16_repro_Bkgs_mc16a_allSys_Skims" -tag v2.7b -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21.config |& tee log_bkgs_mc16a_v2.7b_move.txt;
</pre>

When running via the grid, the `moveCondorOutputsToGroupsetStructure` step is not necessary, and should instead be replaced by the `run_download` step described [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework#Downloading_and_merging_samples).

**3. Merge**: combine subjobs of a particular sample, generating weights that yield the specific luminosity. This step creates a new directory parallel to your `skims_` output directories. It has the name of the sample being merged with the prefix `skims_v2.7b_` (or whatever tag you're using.) Merge logs can be found in `merge_submit` in these new merge directories. Final merged files will appear in the `merged` directory which is adjacent to `R21_SUSY16_repro_Bkgs_mc16a_NoSys/v2.7b/trees`, mentioned in the previous step.  
<pre>
run_merge -groupSet R21_SUSY16_repro_Bkgs_mc16a_allSys -d "/usatlas/groups/bnl_local/jgonski/December15_2018_Higgsino_R21_v2.7b_Production/R21_SUSY16_repro_Bkgs_mc16a_allSys_Skims/" -tag v2.7b --condor --skipSampleInfoDump --disableMetaDataCheck -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21.config -lumi "0.258" |& tee log_bkgs_mc16a_v2.7b_merge.txt
</pre>

Note that the -lumi parameter in the merge command has to be specified as follows: 
* If you have all three mc16 campaigns: mc16a = 0.258, mc16d = 0.315, mc16e = 0.427
* If you only have mc16a+d: mc16a = 0.452475, mc16d = 0.547525
* For data, just set -lumi "1.0"

Also note that this example merge command includes the flag --disableMetaDataCheck, which turns off checks for duplicated events / missing events. Ultimately this needs to be on (and may need to implement adjustments to these checks, which currently might falsely report issues due to our combination of samples with multiple e-tags!), but I was seeing AMI issues at the time, so I chose to turn them off for now.

Each sample merging is submitted to condor, so while this can create a large number of jobs when launched, it generally doesn't take a huge amount of time (order ~hours depending on BNL status).

# Post-Production Checks

The job status is reported as "fail" or "completed" in /status directory. This makes it easy to see if any subjobs failed after everything is finished running:
`find . -name "fail-*"`

At any point, you can also open the logs of each individual job (.log and .err), in /submit directory, to look for errors and/or if the job completed as expected.

After merge, I run the following checks and pipe the output into a text file:
<pre>
grep -r "ERROR" skims_v*/merge_submit
grep -r "rror" skims_v*/merge_submit
grep -r "arning" skims_v*/merge_submit
grep -r "WARNING" skims_v*/merge_submit
grep -r "areful" skims_v*/merge_submit
grep -r "!!!" skims_v*/merge_submit 
</pre>

This is designed to find any/all general issues with the production, including "invisible" seg faults. Some of these are not problematic, like: 
`Warning in <StatusCode>: Number of unchecked successes: 2` or `Warning in <TObject::MergeTool::SetBranchAddress>: Could not find branch GenMET, will not be used in the merge!`

And some of them do point to issues you have to address, like this error for a missing cross section: `Error in <TObject::MergeTool::calculateNormalization>: No xsec found for DSID 345323, FS 0. Be careful with this DSID and FS!`

# Common Problems

**1. Zombies**: In the R21 analysis we struggled a lot with "zombies" at BNL, which occur somewhat mysteriously and result in output files that are missing trees or improperly closed. You can find them in the above grep checks, by way of the message: 

<pre>
SampleSet::getTChainList    WARNING     Can't add /usatlas/groups/bnl_local2/jgonski/December5_2018_Higgsino_R21_v2.7_Production/R21_SUSY16_repro_Bkgs_mc16e_Skims_3//R21_SUSY16_repro_Bkgs_mc16e/v2.7/trees//user.unused.PhPy8EG_A14_ttbar_hdamp258p75_dil.410472.e6348_s3126_r10724_p3652_v2.7/mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_SUSY16.e6348_s3126_r10724_p3652-134.root to the TChain - check if the root file ends with ".root" !!!
</pre>
and subsequently 
<pre>
Warning in <TFile::Init>: file /usatlas/groups/bnl_local2/jgonski/December5_2018_Higgsino_R21_v2.7_Production/R21_SUSY16_repro_Bkgs_mc16e_Skims_3//R21_SUSY16_repro_Bkgs_mc16e/v2.7/trees//user.unused.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.410645.e6527_s3126_r10724_p3652_v2.7/mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_SUSY16.e6527_s3126_r10724_p3652-9.root probably not closed, trying to recover
Warning in <TFile::Init>: no keys recovered, file has been made a Zombie
</pre>

Though this is unfortunate, it's not usually a huge problem for the MC production: the normalization is set at the merge step anyway, so a few jobs missing just means fewer stats, but the yields will still be correct. There is one crucial exception, which is that sometimes Root is able to recover some, but not all of the trees of a zombie file. You can tell this happened if you find any statements like this in your log file: 

<pre>
Warning in <TFile::Init>: successfully recovered 5 keys
</pre> 

We had some instances where the CutBookkeepers tree was recovered, but the actual NoSys tree was not, meaning that there were events reported which didn't actually exist. Such a scenario means the final normalization done at the merge step would be incorrect. **If you find any files that are partially recovered, they need to be removed before merging!** Of course, if any data file breaks or is missing, that needs to be rerun by hand for completeness. 

**2. BNL Grid Issues**: these tend to show up in your log files as authorization issues when trying to open DAODs. 

<pre>
TNetXNGFile::Open         ERROR   [ERROR] Server responded with an error: [3012] Failed to open file (Could not initialize class java.net.PlainDatagramSocketImpl [10011])
TNetXNGFile::Open         ERROR   [FATAL] Auth failed
open failed, waiting 299.961 seconds:
</pre>

Over time we've built up a series of solutions to try here, some of which can work at certain times and not at others. If you have an authorization issue, here are some changes to try out (with no guarantee that they will work but worth a shot anyway.)

* Change grid proxy: after setting up your proxy with `voms-proxy-init -voms atlas`, try: `cp $X509_USER_PROXY $HOME/`, then `export X509_USER_PROXY="/usatlas/u/jgonski/x509up_u11960"` (replacing jgonski with your username).
* Change submission node in run_xAODNtMaker as given [here](https://gitlab.cern.ch/SusySkimAna/SusySkimDriver/blob/master/util/run_xAODNtMaker.cxx#L353). Possibilities include `root://dcdoor11.usatlas.bnl.gov:1094//pnfs/...`, `root://dcdoor16.usatlas.bnl.gov:1094//pnfs/...`, or `root://dcgftp.usatlas.bnl.gov:1096//pnfs/...`. 

If none of these work, you'll have to contact the BNL Tier3 Help Desk at RT-RACF-USAtlasSharedT3@bnl.gov.

# Finalizing and Uploading
Detailed in full at https://gitlab.cern.ch/atlas-phys-susy-higgsino/SusySkimHiggsino/blob/master/scripts/README

# Helpful Rucio / BNL Commands

* `rucio list-account-limits jgonski`: check out quota on different disks
* `rucio add-rule mc16_13TeV.* (—notify Y)`: replication command; for BNL, use for example: `rucio add-rule mc16_13TeV:mc16_13TeV.394988.MGPy8EG_A14N23LO_SM_N2C1p_85_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6587_e5984_a875_r10201_r10210_p3531 1 BNL-OSG2_LOCALGROUPDISK`
* `rucio list-file-replicas data17_13TeV.periodE.physics_Main.PhysCont.DAOD_SUSY16.grp17_v01_p3372 --rse BNL-OSG2_LOCALGROUPDISK —-missing`: tells you what files are not replicated for the given dataset
* `rucio list-rules --account jgonski`: lists current replicas associated to the given account
* BNL Quota: check online at https://network.racf.bnl.gov/Facility/GCE/GPFS/. You may also run the below commands to look at quota/usage: 
`df -h /atlasgpfs01/usatlas/bnl_local    (Quota is 32TB not 33TB).`
`df -h /atlasgpfs01/usatlas/bnl_local2  (Quota is 48TB not 49TB).`
* Request more space at BNL: https://atlas-lgdm.cern.ch/LocalDisk_Usage/USER/RequestFormUsage/


