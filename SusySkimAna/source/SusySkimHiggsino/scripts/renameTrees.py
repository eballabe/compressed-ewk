import ROOT,glob,os

# organizational details
tag = os.getenv("GROUP_SET_TAG")
if not tag :
    exit("Need to set GROUP_SET_TAG! Exiting.")
print "tag is "+tag

groupsetID = "SUSY16"

# List of data files, which we'll need to rename
# and hadd if they are present
listOfDataFilesNoSys = glob.glob("data*tree*NoSys*.root")
listOfDataFilesLoose = glob.glob("data*tree*LOOSE_NOMINAL*.root")

if listOfDataFilesNoSys or listOfDataFilesLoose :
    if os.path.exists("unhadded_data") :
        exit("unhadded_data directory already exists!")

    os.mkdir("unhadded_data")

    # Rename the trees to just "data"
    print "Renaming data trees"

    # FIXME include listOfDataFilesLoose once we have systs again
    for listOfDataFiles in [listOfDataFilesNoSys, listOfDataFilesLoose] :
    #for listOfDataFiles in [listOfDataFilesNoSys] :

        postfix = ""

        dataStr = "data"
        dataWildcard = "data*tree*NoSys*.root"
        appendToTreeName = ""
        if listOfDataFiles == listOfDataFilesLoose :
            dataStr += "_LOOSE_NOMINAL"
            dataWildcard = "data*tree*LOOSE_NOMINAL*.root"
            appendToTreeName += "_LOOSE_NOMINAL"

        for fName in listOfDataFiles :

            # make a copy before we do anything
            os.system("cp "+fName+" unhadded_data/orig_"+fName)
            f = ROOT.TFile(fName,"UPDATE")

            # split fName into two pieces
            origFileNameSplit = fName.split("_", 1)
            origTreeName = origFileNameSplit[0]

            # postfix on the output file name
            if postfix == "" :
                postfix = origFileNameSplit[1]
                #print "postfix",postfix

            # Now we rename the tree
            t = f.Get(origTreeName+appendToTreeName)

            t.SetName(dataStr)
            t.SetTitle(dataStr)
            t.Write(dataStr)

            # remove the old tree!
            ROOT.gDirectory.Delete(origTreeName+appendToTreeName+";*")
            f.Write()

        print "Hadding data trees"
        os.system("hadd data_"+postfix+" "+dataWildcard)

        # Move the unhadded files
        for fName in listOfDataFiles :
            os.system("mv "+fName+" unhadded_data/")

print "Renaming all trees"
os.system("rename _merged_processed _SusySkimHiggsino_"+tag+"_"+groupsetID+" *root")
