listOfFiles="
300
350
400
500
600
700
800
"

listOfSys="
NoSys.root
LOOSE_NOMINAL.root
EG_RESOLUTION_ALL__1down.root
EG_RESOLUTION_ALL__1up.root
EG_SCALE_ALL__1down.root
EG_SCALE_ALL__1up.root
JET_EtaIntercalibration_NonClosure__1down.root
JET_EtaIntercalibration_NonClosure__1up.root
JET_GroupedNP_1__1down.root
JET_GroupedNP_1__1up.root
JET_GroupedNP_2__1down.root
JET_GroupedNP_2__1up.root
JET_GroupedNP_3__1down.root
JET_GroupedNP_3__1up.root
JET_JER_SINGLE_NP__1up.root
JET_RelativeNonClosure_AFII__1down.root
JET_RelativeNonClosure_AFII__1up.root
MET_SoftTrk_ResoPara.root
MET_SoftTrk_ResoPerp.root
MET_SoftTrk_ScaleDown.root
MET_SoftTrk_ScaleUp.root
MUON_ID__1down.root
MUON_ID__1up.root
MUON_MS__1down.root
MUON_MS__1up.root
MUON_SCALE__1down.root
MUON_SCALE__1up.root
"

mkdir unhadded_signal

for systematic in $listOfSys;
do
  echo "starting $systematic"
  for arg in $listOfFiles;
  do
    echo "starting $arg"
    if ls MGPy8EG_A14N23LO_NUHM2_m12_*${arg}_weak_*2LMET50_MadSpin_*$systematic &> /dev/null; then
      mv MGPy8EG_A14N23LO_NUHM2_m12_*${arg}_weak_*2LMET50_MadSpin_*$systematic unhadded_signal
      hadd MGPy8EG_A14N23LO_NUHM2_m12_${arg}_weak_2LMET50_MadSpin_SusySkimHiggsino_${GROUP_SET_TAG}_SUSY16_tree_$systematic unhadded_signal/MGPy8EG_A14N23LO_NUHM2_m12_*${arg}_weak_*2LMET50_MadSpin_*$systematic
    else
      echo "file MGPy8EG_A14N23LO_NUHM2_m12_*${arg}_weak_*2LMET50_MadSpin_*$systematic does not exist!"
    fi
  done
done
