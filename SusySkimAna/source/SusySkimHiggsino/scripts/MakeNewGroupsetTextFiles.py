#!/usr/bin/python

#Script to make new groupset text files
#These text files should then be put into the groupset directory
#Need to setupATLAS and Rucio to run

import subprocess
import glob
import os
from MakeGroupSetFunctions import *

#New group sets dictionary (name : DSID range)
#Add New group sets here
NewSamplesDict = {}
NewSamplesDict['Strong_Zll_jets_Sh2211'] = inclusiveRange(700320, 700334)
NewSamplesDict['Strong_Znunu_jets_Sh2211'] = inclusiveRange(700335, 700337)
NewSamplesDict['Strong_Wjets_Sh2211'] = inclusiveRange(700338, 700349)

TopDirec = '../data/samples/' #path to group set directories 
GroupsetName = 'R21_EXOT5_VBFBkgs' #group set name text file belongs to (without mc16 period suffix)

for MCPeriod in MCtagsDict:
    containers = GetAllContainers(MCPeriod, "p4060") #get all containers matching MCperiod and ptag, then filter for specific DSIDs
    for sample in NewSamplesDict:
        Direc = "{}/{}_{}".format(TopDirec, GroupsetName, MCPeriod) #direc to save text file into, needs to alredy exist
        MakeGroupSetTextFile(sample, Direc, NewSamplesDict[sample], containers, MCPeriod)
