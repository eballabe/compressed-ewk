#include <iostream>
#include <sstream>
#include <memory>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>

#include <TFile.h>
#include <TChain.h>
#include <TString.h>
#include <TSystem.h>

#include "CxxUtils/make_unique.h"

#ifndef READ_TREE_ADDRESSES
#define READ_TREE_ADDRESSES(typeName, name) \
  typeName name = 0; chain->SetBranchAddress(#name, &name)
#endif

int main(int argc, char* argv[]){

  if(argc < 2){
    std::cerr << "No input file specified! Exiting." << std::endl;
    return EXIT_FAILURE;
  }

  TString fileNameFullPath = argv[1];
  TString fileName = fileNameFullPath;
  TFile *inFile = TFile::Open(fileName, "READ");
  if(!inFile){
    std::cerr << "Invalid root file! Exiting." << std::endl;
    return EXIT_FAILURE;
  }

  TString treeName = argv[2];

  TChain* chain = new TChain(treeName);
  chain->Add(fileNameFullPath+"/"+treeName);

  READ_TREE_ADDRESSES(float, lep1Pt);
  READ_TREE_ADDRESSES(float, lep2Pt);
  READ_TREE_ADDRESSES(float, mll);
  READ_TREE_ADDRESSES(bool, trigMatch_metTrig); // FIXME bool?
  READ_TREE_ADDRESSES(bool, trigMatch_singleMuonTrig); // FIXME bool?
  READ_TREE_ADDRESSES(bool, trigMatch_singleElectronTrig); // FIXME bool?
  READ_TREE_ADDRESSES(bool, HLT_2mu4_j85_xe50_mht_emul);
  READ_TREE_ADDRESSES(bool, HLT_mu4_j125_xe90_mht_emul);
  READ_TREE_ADDRESSES(bool, HLT_mu4);
  READ_TREE_ADDRESSES(bool, HLT_2mu4);
  READ_TREE_ADDRESSES(bool, HLT_2mu10);
  READ_TREE_ADDRESSES(bool, HLT_2mu4_j85_xe50_mht);
  READ_TREE_ADDRESSES(bool, HLT_mu4_j125_xe90_mht);
  READ_TREE_ADDRESSES(bool, HLT_xe70);
  READ_TREE_ADDRESSES(bool, HLT_xe70_mht);
  READ_TREE_ADDRESSES(bool, HLT_xe70_mht_wEFMu);
  READ_TREE_ADDRESSES(bool, HLT_xe70_tc_lcw);
  READ_TREE_ADDRESSES(bool, HLT_xe70_tc_lcw_wEFMu);
  READ_TREE_ADDRESSES(bool, HLT_xe80_tc_lcw_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe90_tc_lcw_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe90_mht_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe90_tc_lcw_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe90_mht_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe100_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe100_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe100_tc_lcw_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe100_mht_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe100_tc_lcw_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe100_mht_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe110_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe110_tc_em_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe110_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe110_tc_em_wEFMu_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe110_mht_L1XE50);
  READ_TREE_ADDRESSES(bool, HLT_xe90_mht_L1XE40);
  READ_TREE_ADDRESSES(bool, HLT_xe50_mht_L1XE20);
  READ_TREE_ADDRESSES(bool, HLT_j85_L1J40);
  READ_TREE_ADDRESSES(bool, HLT_j125_L1J50);
  READ_TREE_ADDRESSES(bool, HLT_e26_lhtight_nod0_ivarloose);
  READ_TREE_ADDRESSES(bool, HLT_e60_lhmedium_nod0);
  READ_TREE_ADDRESSES(bool, HLT_e60_medium);
  READ_TREE_ADDRESSES(bool, HLT_e140_lhloose_nod0);
  READ_TREE_ADDRESSES(bool, HLT_mu26_ivarmedium);
  READ_TREE_ADDRESSES(bool, lep1TruthMatched);
  READ_TREE_ADDRESSES(bool, lep2TruthMatched);
  READ_TREE_ADDRESSES(bool, lep3TruthMatched);
  READ_TREE_ADDRESSES(bool, lep4TruthMatched);

  // Turn on all branches. Our cloned tree will share
  // the references to the variables which we wish to modify!
  chain->SetBranchStatus("*",1);

  // Identical tree, but modify values
  TFile* outFile_fixValues = new TFile("treeCopy_fixValues.root","RECREATE");
  TTree* outTree_fixValues = chain->CloneTree(0);
  outTree_fixValues->SetTitle(treeName);
  outTree_fixValues->SetName(treeName);

  // Modify values and remove overlap
  TFile* outFile_noOverlap = new TFile("treeCopy_fixValues_and_noOverlap.root","RECREATE");
  TTree* outTree_noOverlap = chain->CloneTree(0);
  outTree_noOverlap->SetTitle(treeName);
  outTree_noOverlap->SetName(treeName);

  // Loop over events!
  int nEventsProcessed = 0;
  int nEventsPassed = 0;

  Long64_t nentries = chain->GetEntries();
  for(Long64_t i = 0; i < nentries; ++i){
    ++nEventsProcessed;

    int getEntry = chain->GetEntry(i);
    if(getEntry < 1){
      std::cerr << "GetEntry() error!! Either an I/O issue, or an invalid entry. Aborting." << std::endl;
      abort();
    }

    // correct tree values here
    trigMatch_metTrig = true;
    trigMatch_singleMuonTrig = true;
    trigMatch_singleElectronTrig = true;
    HLT_2mu4_j85_xe50_mht_emul = true;
    HLT_mu4_j125_xe90_mht_emul = true;
    HLT_mu4 = true;
    HLT_2mu4 = true;
    HLT_2mu10 = true;
    HLT_2mu4_j85_xe50_mht = true;
    HLT_mu4_j125_xe90_mht = true;
    HLT_xe70 = true;
    HLT_xe70_mht = true;
    HLT_xe70_mht_wEFMu = true;
    HLT_xe70_tc_lcw = true;
    HLT_xe70_tc_lcw_wEFMu = true;
    HLT_xe80_tc_lcw_L1XE50 = true;
    HLT_xe90_tc_lcw_L1XE50 = true;
    HLT_xe90_mht_L1XE50 = true;
    HLT_xe90_tc_lcw_wEFMu_L1XE50 = true;
    HLT_xe90_mht_wEFMu_L1XE50 = true;
    HLT_xe100_L1XE50 = true;
    HLT_xe100_wEFMu_L1XE50 = true;
    HLT_xe100_tc_lcw_L1XE50 = true;
    HLT_xe100_mht_L1XE50 = true;
    HLT_xe100_tc_lcw_wEFMu_L1XE50 = true;
    HLT_xe100_mht_wEFMu_L1XE50 = true;
    HLT_xe110_L1XE50 = true;
    HLT_xe110_tc_em_L1XE50 = true;
    HLT_xe110_wEFMu_L1XE50 = true;
    HLT_xe110_tc_em_wEFMu_L1XE50 = true;
    HLT_xe110_mht_L1XE50 = true;
    HLT_xe90_mht_L1XE40 = true;
    HLT_xe50_mht_L1XE20 = true;
    HLT_j85_L1J40 = true;
    HLT_j125_L1J50 = true;
    HLT_e26_lhtight_nod0_ivarloose = true;
    HLT_e60_lhmedium_nod0 = true;
    HLT_e60_medium = true;
    HLT_e140_lhloose_nod0 = true;
    HLT_mu26_ivarmedium = true;
    lep1TruthMatched = true;
    lep2TruthMatched = true;
    lep3TruthMatched = true;
    lep4TruthMatched = true;

    outTree_fixValues->Fill();

    // Remove overlap with the nominal sample
    if(mll < 4 || lep1Pt < 5 || lep2Pt < 5){
      outTree_noOverlap->Fill();
      ++nEventsPassed;
    }
  }

  std::cout << "nEventsProcessed is " << nEventsProcessed << " and nEventsPassed is " << nEventsPassed << std::endl;

  outFile_fixValues->Write();
  outFile_noOverlap->Write();

  return EXIT_SUCCESS;
}
