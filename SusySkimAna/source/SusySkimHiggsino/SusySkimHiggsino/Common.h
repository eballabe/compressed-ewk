#ifndef SusySkimHiggsino_Common_H
#define SusySkimHiggsino_Common_H

//RootCore
#include "SusySkimMaker/BaseUser.h"
#include "TF1.h"

//TMVA
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

class Common 
{

 public:

  void setup(ConfigMgr*& configMgr, bool add_jets=true);
  bool doAnalysis(ConfigMgr*& configMgr);

  //
  // Truth info handling
  //

  // Add/Set truth variables
  void addTruthVariables(ConfigMgr* configMgr);
  void setTruthVariables(ConfigMgr* configMgr);

  //ugly helper functions for all the reweighting
  int getHiggsinoDM(int DSID);
  bool isHiggsinoN2C1p(int DSID);
  bool isHiggsinoN2C1m(int DSID);
  bool isHiggsinoC1C1(int DSID);
  bool isHiggsinoN2N1(int DSID);

  // For Wino-Bino reweighting
  double getWinoBinoFuncMllDistr(double x, double *par);
  double getWinoBinoMllWeight(int DSID, double mass);
  double getWinoBinoXsecWeight(int DSID);
  double getWinoBinoBrFracWeight(int DSID);

  //For NUHM2 reweighting
  double get_NUHM2_weight(int DSID, double mass, int m12);
  double get_NUHM2_N2_mass(int m12);
  double get_NUHM2_N1_mass(int m12);

  // Write truth info for the process? (judged at 'setup')
  // NOTE: Toggle also SaveTruthInfo, SaveTruthEvtInfo and SaveTruthEvtInfoFull to 1 in the deep config
  bool m_writeTruth_Gammajets;  
  bool m_writeTruth_Vjets;
  bool m_writeTruth_VV;
  bool m_writeTruth_TT;
  bool m_writeTruth_XX;

  // Flags to steer computation/write-out of variables
  bool m_addJets; // add central jets to output ntuple

  //
  // Trigger info handling
  //
  std::vector<const char*> triggerChains;
  std::vector<const char*> L1Chains;
  std::vector<TString> prescaledTriggerChains;

  //
  // For computing RJR variables via RestFrames
  //
  void computeJigsawVariables(ConfigMgr*& configMgr);
  struct Jet {
    TLorentzVector P;
    bool btag;
  };
  TVector3 GetMET(ConfigMgr*& configMgr);
  void GetJets(ConfigMgr*& configMgr, std::vector<Jet>& JETs, double pt_cut, double eta_cut=-1);
  void GetLeptons(ConfigMgr*& configMgr, std::vector<TLorentzVector>& LEPs, std::vector<int>& IDs,double pt_cut=-1, double eta_cut=-1);

  //
  // Object info handling
  //

  // Refactored Add/Set for lepton variables (lep1Pt, lep1Eta etc...)
  void addLeptonVariables(const ConfigMgr* configMgr, TString prefix);
  void setLeptonVariables(const ConfigMgr* configMgr, TString prefix, LeptonVariable* lep);

  // Refactored Add/Set for tau variables (tau1Pt, tau1Eta etc...)
  void addTauVariables(const ConfigMgr* configMgr, TString prefix);
  void setTauVariables(const ConfigMgr* configMgr, TString prefix, const TauVariable* tau);
  
  // For PLT input variable correction and score augmentation
  bool setCorrPLTInputVariables(const ConfigMgr* configMgr, TString prefix1, TString prefix2, const LeptonVariable* lep1, const LeptonVariable* lep2);
  void setPLTscore(const ConfigMgr* configMgr, TString prefix);
  void setPLTWP(const ConfigMgr* configMgr, TString prefix);
  void setPLT_nLep_signal(const ConfigMgr* configMgr);
  void PrepareBDTConfig();

  TMVA::Reader *CorrPromptLepVeto_reader[2];
  TMVA::Reader *LowPtPLT_reader[2];
  TMVA::Reader *LowPtPLT_Cone40_reader[2];
  Float_t BDT_Vars[8];

  struct BDT_Config {
    TString algName;
    TString weightFile;
    int varnum;
    std::vector<TString> varnames;
    int specvarnum;
    std::vector<TString> specvarnames;    
  };
  struct BDT_Config GetPLT_Config(int flavor, TString algName);

  void PreparePLTWP();
  Float_t LowPtPLT_WP[2][2][11];
  Float_t LowPtPLT_MediumLH_WP[2][11];

  //
  // VBF analysis methods and variables
  //
  bool validVBFtags;
  bool validSpecJet;
  unsigned int eventnokey;
  Float_t vbfjjM;

  TLorentzVector vbftag_J1;
  TLorentzVector vbftag_J2;
  TLorentzVector vbftag_J3;
  TLorentzVector jetC; 
  TLorentzVector jetD; 


  int VBF_ZCR_mll_Lep1Index = -1; //baseline lepton indices of mll leptons in VBF Z CR
  int VBF_ZCR_mll_Lep2Index = -1;
  int VBF_ZCR_mll_flavour = -1; //1 = electron, 2 = muon


  void vbfSetup(const ConfigMgr* configMgr);
  void addJetVariables(const ConfigMgr* configMgr, TString prefix);
  void setJetVariables(const ConfigMgr* configMgr, TString prefix, const JetVariable* jet);
  void computeVBFVariables(ConfigMgr*& configMgr); 
  void computeVBFJigSawVariables(ConfigMgr*& configMgr);
  void writeVBF(ConfigMgr*& configMgr);
  void VBF_computeTruthVariables(ConfigMgr* & configMgr);
  void computeVBF_ZCR_Vars(ConfigMgr*& configMgr);
  void VBF_CorrectSherpaWeights(ConfigMgr* & configMgr);
  void setSpecTag(ConfigMgr*& configMgr);
  void computeMT2lephad(ConfigMgr*& configMgr);
  TLorentzVector getVBFTagJet1(ConfigMgr*& configMgr);	
  TLorentzVector getVBFTagJet2(ConfigMgr*& configMgr);	
  void addVBFLeptonVariables(const ConfigMgr* configMgr, TString prefix);
  void setVBFLeptonVariables(const ConfigMgr* configMgr, TString prefix, LeptonVariable* lep, const TLorentzVector& jj_sys); 
  void addllVars(const ConfigMgr* configMgr, TString prefix); 
  void setllVars(const ConfigMgr* configMgr, TString prefix, LeptonVariable* lep1, LeptonVariable* lep2);
  
  TruthVector VBF_getVBFTruthJets(ConfigMgr*& configMgr, float TruthJetPtMin, float TruthJetEtaMax);
  double VBF_computeTruthPtV (ConfigMgr*& configMgr); 

  //Booleans to identify sample type
  inline bool IsWjets_Sherpa(int DSID) {
      return (  (364156 <= DSID && DSID <= 364215) | // Strong_Wjets_Sh221_maxHTpTV
                (364224 <= DSID && DSID <= 364229) | // Strong_Wjets_Sh221_pTV
                (312496 <= DSID && DSID <= 312531)); // Strong_Wjets_Sh227_KtMerging
  }

  inline bool IsZjets_Sherpa(int DSID) {
      return ( (364100 <= DSID && DSID <= 364141) | // Strong_Zll_jets_Sh221_maxHTpTV
               (364216 <= DSID && DSID <= 364221) | // Strong_Zll_jets_Sh221_pTV
               (312448 <= DSID && DSID <= 312483) | // Strong_Zll_jets_Sh227_KtMerging
               (364142 <= DSID && DSID <= 364155) | // Strong_Znunu_jets_Sh221_maxHTpTV
               (364222 <= DSID && DSID <= 364223) | // Strong_Znunu_jets_Sh221_pTV
               (366010 <= DSID && DSID <= 366017) | (366019 <= DSID && DSID <= 366026) | (366028 <= DSID && DSID <= 366035) | //Strong_Znunu_jets_Sh221_pTV_Mjj
               (312484 <= DSID && DSID <= 312495)); // Strong_Znunu_jets_Sh227_KtMerging
  }

  inline bool IsEW_Vjets_Sherpa(int DSID) {
      return ( (308092 <= DSID && DSID <= 308098) ); // EW_Vjets_Sh221_TChan
  }
  
  inline bool IsZjets_MadGraph(int DSID){
    return ((361510 <= DSID && DSID <= 361514) | (363123 <= DSID && DSID <= 363170) | // Strong_Zll_jets_MG (including AF2 only samples) 
            (361515 <= DSID && DSID <= 361519) | // Strong_Znunu_jets_MG_Np
            (311429 <= DSID && DSID <= 311444)); // VBF_Zjets_MG_Tight 
  }

  inline bool IsWjets_MadGraph(int DSID){
      return (  (363600 <= DSID && DSID <= 363671) | //Strong_Wjets_MG
                (311445 <= DSID && DSID <= 311453)); //Strong_Wjets_MG_VBF
  }

  static bool GreaterPt(const TruthVariable* a, const TruthVariable* b) {
      return a->Pt() > b->Pt();
  }


  //These should be moved to SusySkimMaker Observables
  bool IsCombiBaselineLepton( LeptonVariable* lep);
  double getJetCentrality (JetVariable * jet1, JetVariable * jet2, JetVariable * jetI);
  double getMjjRel (JetVariable * jet1, JetVariable * jet2, JetVariable * jetI);
  double getM_T(TLorentzVector vec1, TLorentzVector vec2);
  double getHtJets (std::vector<JetVariable*> Jets, float minPt, float maxEta); 
  double getVecHtJets (std::vector<JetVariable*> Jets, float minPt, float maxEta);
  float getDPhiAllJNMet(Objects *obj, unsigned int jetIndex, float jet_threshold, bool useTrackMET=false, bool useLepInvis=false);
  float getMinDRaJets(const Objects* obj, const TLorentzVector* ref, float minjetpt);
  double getMetMinusBaseL1(ConfigMgr*& configMgr);
  double getMetMinusBaseL1L2(ConfigMgr*& configMgr);
  bool hasZCandidate(const Objects* obj, bool useBaseline, float massWindow);

  //bool LepPassVBF_ZCR(LeptonVariable* lep);  
  bool LepPassVBF_ZCR(const LeptonVariable* lep);
  bool ElePassVBF_ZCR(const ElectronVariable* lep); 
  bool MuPassVBF_ZCR(const MuonVariable* lep); 
  bool LepPassVBF_WCR(const LeptonVariable* lep);
  bool ElePassVBF_WCR(const ElectronVariable* lep); 
  bool MuPassVBF_WCR(const MuonVariable* lep); 


  //
  // Helpers to load, store and retrieve SFs for MET triggers
  //
  std::map<std::string, TF1> sf_curves;
  void load_sfs(); // called during setup()
  double retrieve_sf(int runnumber, double met, int nBjet);

  //
  // Helpers for applying the weight for ISR
  //
  TF1 ISR_fit;
  void load_ISR_fit();
  double retrieve_ISRWeight(double SUSYPt);

  //
  // Some general helpers
  //
  inline int getYear(int runNumber){
    if(runNumber<276262 || runNumber>370000) return 0;
    if      (runNumber<=284484) return 2015;
    else if (runNumber<=320000) return 2016;
    else if (runNumber<=348884) return 2017;
    return 2018;
  }
  inline float getMtVis(const Objects* obj, const TLorentzVector &pvis){
    return TMath::Sqrt(2*pvis.Pt()*obj->met.Et *(1-TMath::Cos(pvis.Phi()-obj->met.phi)) );
  }

};

#endif
