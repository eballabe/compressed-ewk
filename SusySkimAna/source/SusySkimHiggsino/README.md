# SusySkimHiggsino

This is where the SusySkimHiggsino code lives and includes selectors for the displaced track, VBF search and 2L ISR search of the 2nd wave iteration(s).

See [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework)
for more details on the SusySkimAna code that our package relies on.

Currently based on `AnalysisBase,21.2.222`

### Overview
1. [Setup Instructions](#Setup-Instructions)
2. [Running Locally](#Running-Locally)
3. [Large-Scale Productions on the Grid](#Running-On-the-Grid)
4. [Remarks to Continous Integration](#Remarks-to-Continous-Integration)

## Setup Instructions
Remark: The tool ```acm``` (ATLAS compilation/package management) provides some useful wrappers for setting up the AnalysisRelease, compiling the code, etc. to make life a bit easier and is used in the following instructions. It is documented [here](https://gitlab.cern.ch/atlas-sit/acm/blob/master/README.md) and available after calling ```setupATLAS```.

### First Time Setup
Clone (this branch of) repository recursively with all submodules

    git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-higgsino/SusySkimHiggsino.git;

Setup the Analysis Release

    mkdir build; cd build;
    acmSetup AnalysisBase,21.2.222

To compile just type

    acm compile

### Subsequent Setups
For any further setups using the same Analysis Release it is sufficient to execute
```
acmSetup
```
within the ```build``` directory. In case new packages are added it may be
required to run ```acm find_packages``` before compiling.


## Running Locally
Below some example commands to run the selectors locally can be found.

**Remarks:** in case you are processing
* AFII files (such as signal samples typically), add the `--isAf2` flag
* data files, the add `--isData` flag

the commands below.

### Displaced Track Search
A derivation format for the displaced track search has not yet been established, but some explanatory files  based on SUSY19 are listed in [HIGGSINO-194](https://its.cern.ch/jira/projects/HIGGSINO/issues/HIGGSINO-194?filter=allopenissues). Currently the [MonojetSelector](source/SusySkimHiggsino/Root/MonojetSelector.cxx) implements a preliminary selection for this analysis and can be run with
```
run_xAODNtMaker -deepConfig SusySkimHiggsino_Rel21_DT.config -selector MonojetSelector -F PATH/TO/INPUTFILE -MaxEvents 1000
```

### VBF Search
The derivation currently used is `DAOD_EXOT5` and signal samples have been requested
in this format. The associated selector is [VBF2020Selector](source/SusySkimHiggsino/Root/VBF2020Selector.cxx)
and can be run with
```
run_xAODNtMaker -deepConfig SusySkimHiggsino_Rel21_VBF.config -selector VBF2020Selector -F PATH/TO/INPUTFILE -MaxEvents 1000
```

### Slepton ISR Search
The slepton search used `DAOD_PHYS` as derivation format and utilizes the
[HiggsinoSelector](source/SusySkimHiggsino/Root/HiggsinoSelector.cxx) that targets
2L+ISR events. At the moment it has a dedicated SusySkimAna config and can be run via
```
run_xAODNtMaker -deepConfig SusySkimHiggsino_Rel21_Slep.config -selector HiggsinoSelector -F PATH/TO/INPUTFILE -MaxEvents 1000
```

## Running On the Grid
To govern large-scale production on the grid group sets containing multiple samples can be defined (see [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework#Large_group_productions)). These group sets can then be submitted, and later downloaded and merged in one go. This requires to additionally load the following

```
lsetup panda
lsetup pyami
lsetup "rucio -w"
```

and to set up a voms proxy. Note that mc16a, mc16d and mc16e samples have to be submitted/downloaded/merged individually. For illustration,the set of commands used to submit the slepton signals are given at the end of each sections below.

### Submitting to Grid
To submit a group set

```
run_xAODNtMaker -groupSet MYGROUPSET  -selector MYSELECTOR -tag MYTAG --submitToGrid
```

**Note:** It's recommended to specify a destination of the grid output via the `-destinationSite` flag,
otherwise the output will be stored on some scratch disk where it will deleted after 2 weeks or so.
Ideally, the destination is a local group disk or something similar where there is
no constraint on the lifetime and the ntuple production can be downloaded also at a later
stage again if needed.

Example commands for slepton signals (note that you might not have the permissions to store
output on the MPP local group disk!):

```shell
run_xAODNtMaker -groupSet R21_PHYS_Bkgs_mc16a  -selector HiggsinoSelector -tag Slep_v0.2 --submitToGrid -destinationSite MPPMU_LOCALGROUPDISK
run_xAODNtMaker -groupSet R21_PHYS_Bkgs_mc16b  -selector HiggsinoSelector -tag Slep_v0.2 --submitToGrid -destinationSite MPPMU_LOCALGROUPDISK
run_xAODNtMaker -groupSet R21_PHYS_Bkgs_mc16c  -selector HiggsinoSelector -tag Slep_v0.2 --submitToGrid -destinationSite MPPMU_LOCALGROUPDISK
```

### Download to Local Disc
To download the files that have been produced on the grid do the following (the download of the skims is skipped here)

```
run_download --disableSkims -d DOWNLOADDIR -u USER -tag MYTAG -groupSet MYGROUPSET -deepConfig CONFIG
```
where `USER` refers to the rucio account name of the user who submitted the tasks.

**Hint:** Downloading might take some time, in particular when experimental
systematics have been processed as well. You can "parallelize" this step to some extend
by using `-includeFiles` flag and e.g. one job per file to your local batch system.

Example commands for slepton signals:

```shell
run_download --disableSkims -d ./ -u miholzbo -tag Slep_v0.2 -groupSet R21_PHYS_Signal_mc16a -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21_Slep.config
run_download --disableSkims -d ./ -u miholzbo -tag Slep_v0.2 -groupSet R21_PHYS_Signal_mc16d -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21_Slep.config
run_download --disableSkims -d ./ -u miholzbo -tag Slep_v0.2 -groupSet R21_PHYS_Signal_mc16e -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21_Slep.config

```

### Merge Samples
To merge the downloaded samples type
```
run_merge -d DOWNLOADDIR -u USER -tag MYTAG -groupSet MYGROUPSET -deepConfig CONFIG
```
This step will also calculate the sum of weights and adjust the `genWeight` branch to include it. As default it will normalize the trees in MC to 1/pb, which can be adjusted with the flag `-lumi`.

A common approach for ntuple production is to use the `-lumi` flag to scale the mc16a/d/e samples accordingly to the integrated luminosity they represent (obviously this is needed for MC only). The values for Run 2 are `0.258/0.315/0.427`. This allows then to add the merged samples together (via the `hadd` command from ROOT) so that at the end there is only one final ntuple to deal with that contains events from all mc16a/d/e.

**Hint:** Similar to the download step above, merging ntuples can take quite some
time hence it may be beneficial to also "parallalize" the merging step via a batch
system (again via the `-includeFiles` flag). Merging is also quite i/o intensive
so it's best done on some fast disk (typically the local `\tmp` dir suits that
purpose well).

Example commands for slepton signals:

```shell
run_merge -d ./ -u miholzbo -tag Slep_v0.2 -groupSet R21_PHYS_Signal_mc16a -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21_Slep.config -lumi 0.258
run_merge -d ./ -u miholzbo -tag Slep_v0.2 -groupSet R21_PHYS_Signal_mc16d -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21_Slep.config -lumi 0.315
run_merge -d ./ -u miholzbo -tag Slep_v0.2 -groupSet R21_PHYS_Signal_mc16e -deepConfig SusySkimHiggsino/SusySkimHiggsino_Rel21_Slep.config -lumi 0.427
```

## Monitoring Grid Productions
Unfortunately, we don't submit our tasks and they finish completely automatically,
at least not all of them. For a certain fraction some interventions are required,
mostly re-trying tasks that ended in finished state, i.e. did not process all available
events due to some temporary problems on the grid site (e.g. input files not available, ...).

The (to my knowledge) only official tool to make these interventions is `pbook` which is available
on the command line when you set up panda via `lsetup panda`. But this tool is arguably
not very convenient to use, hence people have built wrappers around it. One useful package
that might come in handy is [pandamonium](https://github.com/dguest/pandamonium).
It's probably best installed (in a virtual python environment) via pip

```shell
pip install pandamonium
```

There are a couple of examples in the package's README, but the most basic call to
list all your grid tasks can be already quite helpful (use your account name):

```shell
pandamon user.<your user name>
```

This gives you a nice brief overview of the progress of all your tasks (by just
sending an http request to the BigPanda web page). It has
quite some functionality, have a look via `pandamon -h`. It becomes really useful
as you can restrict to finished tasks and pipe them into a resubmit command:

```shell
pandamon user.<your user name> -i finished -t | panda-resub-taskid
```

That can safe a lot of work. As of now, two of of these "pipeable" wrappers around `pbook`
are shipped this package: `panda-resub-taskid` and `panda-kill-taskid`. They also come
with some options (see again via `-h`) that are worth knowing.

Final remark: there are probably plenty of other ways and tools to get your ntuple produced
on the grid (which essentially extend the poor functionality of the basis tools), that's just
one of them. Always look out if other people are doing something smarter!

### Tips & Tricks to known Problems during Grid Productions
Please find below a some solutions to problems that can occur when running on the
grid. Don't hesitate to add your own tips & tricks that might help others!

#### Handling exhausted tasks
Exhausted tasks should be resubmitted with more files per job which can be
achieved via a flag of the resubmit executable of pandamonium:

```shell
panda-resub-taskid -n X
```

## Remarks to Continous Integration
As usual, a set of instructions is defined in the [CI file](.gitlab-ci.yml) to build the project, run the selectors and validate their output after changes to the code and pushing them to the repository. To avoid broken pipelines, please find some remarks below to fix common problems.

### Updating the Reference Cutflow of a Selector
Code changes may yield to deviations from the current reference cutflow that are expected. Then it is neccessary to update the reference. This can be done by processing the sample used produced the reference cutflow (defined in the CI file) with the updated code and use the script [readout_cf.py](source/SusySkimHiggsino/scripts/readout_cf.py) to store the new cutflow as json file. If `SusySkimHiggsino` is set up, it should be callable from anywhere via

```
readout_cf.py -i new_sample.root -o new_reference_cf.json
```
Finally, replace the old reference cutflow file associated to the selector under [tests](tests) and commit.

### Updating the Analysis Release
After moving to a more recent Analysis Release it needs to be updated in several places:
* update the associated variable at the top of [.gitlab-ci.yml](.gitlab-ci.yml)
* update the name of the base image in [Dockerfile](Dockerfile) 
* at the top of this README to keep the documentation up to date ;).


***
***

# Setup in R21 [OUTDATED]

Please follow: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework#Release_21

Also see `currentSetup.md` for a working prescription for setting up and running the Higgsino code (at least at the BNL tier3), as of the final ntuples used for the 2019 R21 CONF analysis. Then see `scripts/README` for instructions on how to hadd/combine/finalize the ntuples for sharing and uploading.

# Git commands

**Git global setup**: do this once on each machine you want to use!

<pre>
git config --global user.name "Your Name"
git config --global user.email "your.email@cern.ch"
</pre>

**To checkout**

Do the following (and similar for the other necessary packages)

<pre>
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-susy-higgsino/SusySkimHiggsino.git
</pre>

Note that this assumes you want to use your ssh key for the checkout; other options can be used though.

**To update**

<pre>
git stash # stores all of your local changes, so you can then get all of the updates
git pull # this is the equivalent of svn up
git stash pop # add your local changes back, and automatically flags conflicts
</pre>

**To commit**

<pre>
git add -u # this will take all files that git is already tracking, and schedule them for your commit
git commit -m "your message" # this commits your changes, but only to your local copy of the git repo
git push -u origin master # this pushes your commit to the remote repo, so others can make use of it
</pre>

Note that you can also use "git add FILE" without the "-u" option if you want git to track a new file.

Also note that the "-a" flag of "git commit" will do the "git add -u" step and the commit itself all in one go, so it may be more convenient to use that instead.

**To tag**

<pre>
git tag # list tags
git tag -a v1.0 -m "first tag of SusySkimHiggsino" # create a new tag
git push origin master v1.0 # push the tag to the repo
</pre>

**Other useful commands**

<pre>
git status
git diff # only looks at files which you have not yet used "git add" for
</pre>
