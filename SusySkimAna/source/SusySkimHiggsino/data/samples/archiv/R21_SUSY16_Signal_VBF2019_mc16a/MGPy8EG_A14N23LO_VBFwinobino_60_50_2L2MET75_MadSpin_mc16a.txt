
# NAME     : VBFwinobino_60_50_2L2MET75
# DATA     : 0
# AF2      : 1
# PRIORITY : 0

mc16_13TeV:mc16_13TeV.398092.MGPy8EG_A14N23LO_SM_N2C1pWBVBFQED_60_50_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398122.MGPy8EG_A14N23LO_SM_N2C1mWBVBFQED_60_50_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398152.MGPy8EG_A14N23LO_SM_C1C1WBVBFQED_60_50_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
