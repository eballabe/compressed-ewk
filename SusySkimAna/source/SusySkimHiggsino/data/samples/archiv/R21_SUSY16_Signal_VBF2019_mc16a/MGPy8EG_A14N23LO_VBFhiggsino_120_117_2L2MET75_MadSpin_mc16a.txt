
# NAME     : VBFhiggsino_120_117_2L2MET75
# DATA     : 0
# AF2      : 1
# PRIORITY : 0

mc16_13TeV:mc16_13TeV.397982.MGPy8EG_A14N23LO_SM_N2N1HHVBFQED_120_117_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398012.MGPy8EG_A14N23LO_SM_N2C1pHHVBFQED_120_117_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398042.MGPy8EG_A14N23LO_SM_N2C1mHHVBFQED_120_117_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
mc16_13TeV:mc16_13TeV.398072.MGPy8EG_A14N23LO_SM_C1C1HHVBFQED_120_117_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r9364_r9315_p3652
