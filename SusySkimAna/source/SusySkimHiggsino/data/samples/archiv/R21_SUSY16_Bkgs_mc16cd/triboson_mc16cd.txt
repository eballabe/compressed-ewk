
# NAME 		: triboson
# DATA 		: 0
# AF2 		: 0
# PRIORITY 	: 0

mc16_13TeV.407311.Sherpa_221_NNPDF30NNLO_6l0v_EW6.deriv.DAOD_SUSY16.e5473_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.407312.Sherpa_221_NNPDF30NNLO_5l1v_EW6.deriv.DAOD_SUSY16.e5473_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.407313.Sherpa_221_NNPDF30NNLO_4l2v_EW6.deriv.DAOD_SUSY16.e5473_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.407314.Sherpa_221_NNPDF30NNLO_3l3v_EW6.deriv.DAOD_SUSY16.e5473_e5984_s3126_r10201_r10210_p3387
mc16_13TeV.407315.Sherpa_221_NNPDF30NNLO_2l4v_EW6.deriv.DAOD_SUSY16.e5655_e5984_s3126_r10201_r10210_p3387
