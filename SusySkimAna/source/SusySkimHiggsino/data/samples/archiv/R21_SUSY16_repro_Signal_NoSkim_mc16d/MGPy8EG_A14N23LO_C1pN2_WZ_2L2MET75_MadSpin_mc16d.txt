# NAME 		: MGPy8EG_A14N23LO_C1pN2_WZ_%5_%6_2L2MET75_MadSpin
# DATA 		: 0
# AF2 		: 1
# PRIORITY 	: 0

mc16_13TeV.377404.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_98p5_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377405.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_98p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377406.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_97p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377407.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_95p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377408.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_90p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377409.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_85p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377410.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_75p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377411.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_60p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377412.MGPy8EG_A14N23LO_C1pN2_WZ_100p0_40p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377413.MGPy8EG_A14N23LO_C1pN2_WZ_125p0_123p5_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377416.MGPy8EG_A14N23LO_C1pN2_WZ_125p0_120p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377418.MGPy8EG_A14N23LO_C1pN2_WZ_125p0_110p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377420.MGPy8EG_A14N23LO_C1pN2_WZ_125p0_85p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377422.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_148p5_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377423.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_148p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377424.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_147p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377425.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_145p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377426.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_140p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377427.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_135p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377428.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_125p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377429.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_110p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377430.MGPy8EG_A14N23LO_C1pN2_WZ_150p0_90p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377432.MGPy8EG_A14N23LO_C1pN2_WZ_175p0_172p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377433.MGPy8EG_A14N23LO_C1pN2_WZ_175p0_170p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377434.MGPy8EG_A14N23LO_C1pN2_WZ_175p0_165p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377437.MGPy8EG_A14N23LO_C1pN2_WZ_175p0_135p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377440.MGPy8EG_A14N23LO_C1pN2_WZ_200p0_197p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377441.MGPy8EG_A14N23LO_C1pN2_WZ_200p0_195p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377442.MGPy8EG_A14N23LO_C1pN2_WZ_200p0_190p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377443.MGPy8EG_A14N23LO_C1pN2_WZ_200p0_185p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377445.MGPy8EG_A14N23LO_C1pN2_WZ_200p0_160p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377446.MGPy8EG_A14N23LO_C1pN2_WZ_200p0_140p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377448.MGPy8EG_A14N23LO_C1pN2_WZ_225p0_222p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377450.MGPy8EG_A14N23LO_C1pN2_WZ_225p0_215p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377452.MGPy8EG_A14N23LO_C1pN2_WZ_225p0_200p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377453.MGPy8EG_A14N23LO_C1pN2_WZ_225p0_185p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377455.MGPy8EG_A14N23LO_C1pN2_WZ_250p0_247p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377457.MGPy8EG_A14N23LO_C1pN2_WZ_250p0_240p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377458.MGPy8EG_A14N23LO_C1pN2_WZ_250p0_235p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377459.MGPy8EG_A14N23LO_C1pN2_WZ_250p0_225p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377460.MGPy8EG_A14N23LO_C1pN2_WZ_250p0_210p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377461.MGPy8EG_A14N23LO_C1pN2_WZ_275p0_272p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377463.MGPy8EG_A14N23LO_C1pN2_WZ_275p0_265p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377465.MGPy8EG_A14N23LO_C1pN2_WZ_275p0_250p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377467.MGPy8EG_A14N23LO_C1pN2_WZ_300p0_295p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377468.MGPy8EG_A14N23LO_C1pN2_WZ_300p0_290p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654
mc16_13TeV.377470.MGPy8EG_A14N23LO_C1pN2_WZ_300p0_275p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10201_p3654

