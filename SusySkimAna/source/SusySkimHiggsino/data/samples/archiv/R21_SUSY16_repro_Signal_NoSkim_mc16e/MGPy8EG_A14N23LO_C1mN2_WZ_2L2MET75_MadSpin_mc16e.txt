# NAME 		: MGPy8EG_A14N23LO_C1mN2_WZ_%5_%6_2L2MET75_MadSpin
# DATA 		: 0
# AF2 		: 1
# PRIORITY 	: 0

mc16_13TeV.377479.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_98p5_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377480.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_98p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377481.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_97p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377482.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_95p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377483.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_90p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377484.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_85p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377485.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_75p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377486.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_60p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377487.MGPy8EG_A14N23LO_C1mN2_WZ_100p0_40p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377488.MGPy8EG_A14N23LO_C1mN2_WZ_125p0_123p5_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377491.MGPy8EG_A14N23LO_C1mN2_WZ_125p0_120p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377493.MGPy8EG_A14N23LO_C1mN2_WZ_125p0_110p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377495.MGPy8EG_A14N23LO_C1mN2_WZ_125p0_85p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377497.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_148p5_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377498.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_148p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377499.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_147p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377500.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_145p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377501.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_140p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377502.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_135p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377503.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_125p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377504.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_110p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377505.MGPy8EG_A14N23LO_C1mN2_WZ_150p0_90p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377507.MGPy8EG_A14N23LO_C1mN2_WZ_175p0_172p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377508.MGPy8EG_A14N23LO_C1mN2_WZ_175p0_170p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377509.MGPy8EG_A14N23LO_C1mN2_WZ_175p0_165p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377512.MGPy8EG_A14N23LO_C1mN2_WZ_175p0_135p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377515.MGPy8EG_A14N23LO_C1mN2_WZ_200p0_197p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377516.MGPy8EG_A14N23LO_C1mN2_WZ_200p0_195p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377517.MGPy8EG_A14N23LO_C1mN2_WZ_200p0_190p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377518.MGPy8EG_A14N23LO_C1mN2_WZ_200p0_185p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377520.MGPy8EG_A14N23LO_C1mN2_WZ_200p0_160p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377521.MGPy8EG_A14N23LO_C1mN2_WZ_200p0_140p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377523.MGPy8EG_A14N23LO_C1mN2_WZ_225p0_222p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377525.MGPy8EG_A14N23LO_C1mN2_WZ_225p0_215p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377527.MGPy8EG_A14N23LO_C1mN2_WZ_225p0_200p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377528.MGPy8EG_A14N23LO_C1mN2_WZ_225p0_185p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377530.MGPy8EG_A14N23LO_C1mN2_WZ_250p0_247p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377532.MGPy8EG_A14N23LO_C1mN2_WZ_250p0_240p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377533.MGPy8EG_A14N23LO_C1mN2_WZ_250p0_235p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377534.MGPy8EG_A14N23LO_C1mN2_WZ_250p0_225p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377535.MGPy8EG_A14N23LO_C1mN2_WZ_250p0_210p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377536.MGPy8EG_A14N23LO_C1mN2_WZ_275p0_272p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377538.MGPy8EG_A14N23LO_C1mN2_WZ_275p0_265p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377540.MGPy8EG_A14N23LO_C1mN2_WZ_275p0_250p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377542.MGPy8EG_A14N23LO_C1mN2_WZ_300p0_295p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377543.MGPy8EG_A14N23LO_C1mN2_WZ_300p0_290p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654
mc16_13TeV.377545.MGPy8EG_A14N23LO_C1mN2_WZ_300p0_275p0_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7126_a875_r10724_p3654

