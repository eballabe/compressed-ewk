
# NAME     : VBFhiggsino_40_30_2L2MET75
# DATA     : 0
# AF2      : 1
# PRIORITY : 0

mc16_13TeV:mc16_13TeV.397966.MGPy8EG_A14N23LO_SM_N2N1HHVBFQED_40_30_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10201_r10210_p3652
mc16_13TeV:mc16_13TeV.397996.MGPy8EG_A14N23LO_SM_N2C1pHHVBFQED_40_30_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10201_r10210_p3652
mc16_13TeV:mc16_13TeV.398026.MGPy8EG_A14N23LO_SM_N2C1mHHVBFQED_40_30_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10201_r10210_p3652
mc16_13TeV:mc16_13TeV.398056.MGPy8EG_A14N23LO_SM_C1C1HHVBFQED_40_30_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7550_e5984_a875_r10201_r10210_p3652
