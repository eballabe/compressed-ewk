# NAME          : ttbar_Sherpa2210
# Data          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.700659.Sh_2212_ttbar_AllHadronic_maxHTavrgTopPT.deriv.DAOD_SUSY20.e8461_s3126_r9364_p5241
mc16_13TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_SUSY20.e8461_s3126_r9364_p5241
mc16_13TeV.700661.Sh_2212_ttbar_SingleLeptonM_maxHTavrgTopPT.deriv.DAOD_SUSY20.e8461_s3126_r9364_p5241
mc16_13TeV.700662.Sh_2212_ttbar_SingleLeptonP_maxHTavrgTopPT.deriv.DAOD_SUSY20.e8461_s3126_r9364_p5241
