########################################################
# SUSYTools config for ISR Slepton Search
# Based on EWK Combinations Config
########################################################
#### default baseline electrons #####
EleBaseline.Pt: 4500.
EleBaseline.Eta: 2.47
EleBaseline.Id: LooseAndBLayerLLH
EleBaseline.z0: 0.5
EleBaseline.CrackVeto: false
#
#### signal electrons - analysis dependent, please edit freely #####
Ele.Et: 4500.
Ele.Eta: 2.47
Ele.CrackVeto: false
Ele.Iso: PLVLoose
Ele.IsoHighPt: PLVLoose # tight iso required for electrons pt > 200 GeV
Ele.Id: MediumLLH
Ele.d0sig: 5.
Ele.z0: 0.5
# ChargeIDSelector WP
Ele.CFT: None
# Update efficiency map (not needed for AB 21.2.223 onwards as updated in ST itself)
Ele.EffMapFilePath: ElectronEfficiencyCorrection/2015_2018/rel21.2/Precision_Summer2020_v1/map3.txt
#
#### default baseline muons #####
MuonBaseline.Pt: 3000.
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 1 # Medium
MuonBaseline.z0: 0.5
#
#### signal muons - analysis dependent, please edit freely #####
Muon.Pt: 3000.
Muon.Eta: 2.7
Muon.Id: 1 # Medium
Muon.Iso: PLVLoose
Muon.IsoHighPt: PLVLoose # change WP if you want
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.2
#
#### everything below this is analysis dependent, please edit freely #####
#
#
PhotonBaseline.Pt: 130000.
PhotonBaseline.Eta: 2.37
PhotonBaseline.Id: Tight
#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.5
Tau.Id: Medium
#Tau.DoTruthMatching: false
#
Jet.Pt: 20000.
Jet.Eta: 2.8
Jet.InputType: 9 # EMTopo 1, PFlow: 9. PFlow is new recommended for everything beyond Winter.  
Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_SimpleJER.config # This is the uncertainty for analyses going to perform combinations. Otherwise rel21/Summer2019/R4_SR_Scenario1_SimpleJER.conf can be used if insensitive to JES. If you are wanting to do the fullJER (with PDSmear) please use : rel21/Summer2019/R4_CategoryReduction_FullJER.config and PDSmearing below set to true).
Jet.JvtWP: Default # EMTopo recommended = Default (= Medium), PFlow recommended = Default (= Tight)
Jet.JvtPtMax: 60.0e3
Jet.JMSCalib: false 
#Jet.AnalysisFile: 
Jet.UncertPDsmearing: false # set this to true for pseudo-data smearing for FullJER if using the FullJER or AllJER. This will produce two version of the JET_JER systematics (differing with __1 for the non-PDSmear systematic, and __2 for the PDSmear systematic). This should be false if using SimpleJER (SUSYTools will report an error message) 
#
FwdJet.doJVT: false
FwdJet.JvtEtaMin: 2.5 
FwdJet.JvtWP: Tight # Tight fJVT with Tight MET / Loose fJVT with Tenacious MET
FwdJet.JvtPtMax: 120.0e3
#
Jet.LargeRcollection: None # AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets, set to None to turn this off
Jet.LargeRuncConfig: None # rel21/Spring2019/R10_GlobalReduction.config, set to None to turn this off
#Jet.LargeRuncVars: pT,mass,D2Beta1     # W/Z Taggers
#Jet.LargeRuncVars: pT,Split23,Tau32WTA # Top taggers
#
# The appropriate Large-R Jet Tagger uncertainty tool will be set up provided the config below is set up correctly. More information can be found here for taggers: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BoostedJetTaggingRecommendationFullRun2 and here for the associated uncertainties: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21ConsolidatedLargeRTaggerSF. These taggers/uncertainties are valid only for FullSim. 
Jet.WtaggerConfig: SmoothedInclWTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_SUSYOpt_MC16_20210129.dat # set to None to turn this off
Jet.ZtaggerConfig: SmoothedInclZTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_SUSYOpt_MC16_20210129.dat # set to None to turn this off
Jet.ToptaggerConfig: JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat # set to None to turn this off
#
BadJet.Cut: LooseBad
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
#
Btag.Tagger: DL1r # MV2c10, DL1, DL1mu, DL1r, MV2c10mu, MV2c10rnn, MC2cl100_MV2c100
# ST defaults for calib file and time stamp outdated (status Feb 21) -> should be up-to-date now (Feb 22)
#Btag.CalibPath: xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-12-02_v2.root
Btag.TimeStamp: 201903
Btag.WP: FixedCutBEff_77
#Btag.TimeStamp: # (default) 201810, 201903, or empty (but set & with 2019 CDI) for older derivations
Btag.MinPt: 20000.
#
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets # AntiKt2PV0TrackJets
TrackJet.Pt: 20000.
TrackJet.Eta: 2.8
BtagTrkJet.enable: true
BtagTrkJet.Tagger: DL1
BtagTrkJet.WP: FixedCutBEff_77
#BtagTrkJet.TimeStamp: # (default) 201810, 201903, or empty (but set & with 2019 CDI) for older derivations
BtagTrkJet.MinPt: 10000.
#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: false
OR.ElBjet: false 
OR.MuBjet: false 
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
OR.BJetPtUpperThres: -1
#
OR.DoFatJets: false
OR.EleFatJetDR: -999.
OR.JetFatJetDR: -999.
#
SigLep.RequireIso: true
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight # Loose, Tight, Tighter, Tenacious
MET.RemoveOverlappingCaloTaggedMuons: true 
MET.DoRemoveMuonJets: true
MET.UseGhostMuons: false
MET.DoMuonEloss: false
#
# Trigger matching happend upstream at derivation level for DAOD_PHYS
Trigger.UpstreamMatching: 1
Trigger.MatchingPrefix: TrigMatch_
# Trigger SFs configuration
Ele.TriggerSFStringSingle: SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
#
#Trig.Dilep2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50 || 2e12_lhloose_L12EM10VH || mu18_mu8noL1
# Matching container for HLT_2e12_lhloose_L12EM10VH missing in currend DAOD_PHYS (p4355), leads to crash on mc16a samples ...
Trig.Dilep2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50 || mu18_mu8noL1
Trig.Dilep2016: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e17_lhvloose_nod0 || mu22_mu8noL1
Trig.Dilep2017: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || mu22_mu8noL1
Trig.Dilep2018: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || mu22_mu8noL1
# need to remove it also from the MultiLep expression ...
Trig.Multi2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50 || e17_lhloose_2e9_lhloose || 2e12_lhloose_mu10 || e12_lhloose_2mu10 || e17_lhloose_mu14 || e7_lhmedium_mu24 || mu18_mu8noL1 || 2mu10 || 3mu6
#
# actual Mu files have to be set in SUSYTools
PRW.ActualMu2017File: GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRW.ActualMu2018File: GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
# default to None but do set to BFilter (366010-366017), CFilterBVeto (366019-366026), or CVetoBVeto (366028-366035) to remap MC16e Znunu dsid
PRW.autoconfigPRWHFFilter: None 
#
StrictConfigCheck: true

