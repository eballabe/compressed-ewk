#include "IFFTruthClassifierTestAlg.h"
#include "IFFTruthClassifierTestAlg.h"
#include "GaudiKernel/MsgStream.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"

IFFTruthClassifierTestAlg::IFFTruthClassifierTestAlg(const std::string& name, ISvcLocator* pSvcLocator) :
AthAlgorithm(name, pSvcLocator),
    m_classifier("IFFTruthClassifier/IFFTruthClassifier"),
    m_muon_selector("CP::MuonSelectionTool/MuonSelectionTool"),
    m_tT("truthType"),
    m_tO("truthOrigin"),
    m_mum_type("firstEgMotherTruthType"),
    m_mum_origin("firstEgMotherTruthOrigin")
{

}

StatusCode IFFTruthClassifierTestAlg::initialize(){

    ATH_MSG_INFO ("initialize()");

    ATH_CHECK(m_classifier.retrieve());
    ATH_CHECK(m_muon_selector.retrieve());

    return StatusCode::SUCCESS;
}

StatusCode IFFTruthClassifierTestAlg::execute() {



    std::map<int, int> m_el_counter;
    std::map<int, int> m_mu_counter;
    std::map<int, int> m_unknown_el_origin_counter;
    std::map<int, int> m_unknown_mu_origin_counter;
    std::map<int, std::map<int, int>> m_debug_map;
    std::map<int, std::map<int, std::map<int, std::map<int, int>>>> m_ultra_debug_map;

  
    const xAOD::ElectronContainer* electrons = nullptr;
    ATH_CHECK(evtStore()->retrieve(electrons, "Electrons"));

    for (const auto& el : *electrons) {

      int iff_type = static_cast<int>(m_classifier->classify(*el));
      m_el_counter[iff_type]++;
      if (iff_type == 0) {
        m_unknown_el_origin_counter[m_tT(*el)]++;
        m_ultra_debug_map[m_tT(*el)][m_tO(*el)][m_mum_type(*el)][m_mum_origin(*el)]++;
      }
    }

    // Muons

    const xAOD::EventInfo* event_info = nullptr;
    ATH_CHECK(evtStore()->retrieve(event_info, "EventInfo"));

    const xAOD::MuonContainer* muons = nullptr;
    ATH_CHECK(evtStore()->retrieve(muons, "Muons"));

    for (const auto& mu : *muons) {

      if (mu->pt() < 10.e3) continue;
      if (!m_muon_selector->accept(*mu)) continue;

      int iff_type = static_cast<int>(m_classifier->classify(*mu));

      m_mu_counter[iff_type]++;
      // ATH_MSG_INFO( "Muon type from IFF Classifier = %d", iff_type);
      if (iff_type == 0 || iff_type == 1) {
        m_unknown_mu_origin_counter[m_tO(*mu)]++;
        m_debug_map[m_tT(*mu)][m_tO(*mu)]++;

        const xAOD::TruthParticle* truth_particle = xAOD::TruthHelpers::getTruthParticle(*mu);

        ATH_MSG_INFO("EventInfo. MC Channel = "<<event_info->mcEventNumber()<<", Run Number = "<<event_info->runNumber()<<", Event Number = "<< event_info->eventNumber());

        if (!truth_particle) {
          ATH_MSG_INFO("Unknown muon with NO truth match");
        }
      }
    }


  ATH_MSG_INFO( "Electron summary");
  for (const auto& i: m_el_counter) {
    ATH_MSG_INFO( Form("%d : %d", i.first, i.second));
  }
  ATH_MSG_INFO( "Muon summary");
  for (const auto& i: m_mu_counter) {
    ATH_MSG_INFO( Form("%d : %d", i.first, i.second));
  }
  ATH_MSG_INFO( "Unknown electron origin");
  for (const auto& i: m_unknown_el_origin_counter) {
    ATH_MSG_INFO( Form("%d : %d", i.first, i.second));
  }
  ATH_MSG_INFO( "Unknown muon origin");
  for (const auto& i: m_unknown_mu_origin_counter) {
    ATH_MSG_INFO( Form("%d : %d", i.first, i.second));
  }
  ATH_MSG_INFO( "DEBUG MAP");
  for (const auto& i: m_debug_map) {
    for (const auto& j: i.second) {
      ATH_MSG_INFO( Form("%d | %d = %d", i.first, j.first, j.second));
    }
  }
  ATH_MSG_INFO( "ULTRA DEBUG MAP");
  for (const auto& i: m_ultra_debug_map) {
    for (const auto& j: i.second) {
      for (const auto& k: j.second) {
        for (const auto& m: k.second) {
          ATH_MSG_INFO( Form("%d | %d | %d | %d = %d",
               i.first, j.first, k.first, m.first, m.second));
        }
      }
    }
  }
    return StatusCode::SUCCESS;
}

StatusCode IFFTruthClassifierTestAlg::finalize() {
    
    return StatusCode::SUCCESS;
}
