#ifndef IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIERDEFS_H_
#define IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIERDEFS_H_

#include <iostream>
#include <type_traits>

namespace IFF {
enum class Type {
  Unknown,
  KnownUnknown,
  IsoElectron,
  ChargeFlipIsoElectron,
  PromptMuon,
  PromptPhotonConversion,
  ElectronFromMuon,
  TauDecay,
  BHadronDecay,
  CHadronDecay,
  LightFlavorDecay,
};
}  // namespace IFF

inline std::ostream& operator << (std::ostream& os, const IFF::Type& obj) {
  os << static_cast<std::underlying_type<IFF::Type>::type>(obj);
  return os;
}

#endif  // IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIERDEFS_H_
