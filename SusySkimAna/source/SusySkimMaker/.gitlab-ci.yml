##############################################
#
#   SusySkimAna Continuous Integration
#   Heavy reference from MBJ_Analysis: https://gitlab.cern.ch/MultiBJets/MBJ_Analysis
#
##############################################

stages:
  - build
  - package
  - run

variables:
  AB_FIXED_RELEASE: 21.2.221
  AB_LATEST_RELEASE: latest
  TESTSAMPLE_HOME: "root://eosuser.cern.ch:/eos/user/s/susyskimana/CI/DAOD/"

################################
#  Build
################################
.build_template:
  stage: build
  variables:
    GIT_STRATEGY: fetch
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_SSL_NO_VERIFY: "true"
    SRC_DIR: src
    BUILD_DIR: build
    SRC_DIR_ABS: "${CI_PROJECT_DIR}/${SRC_DIR}"
    BUILD_DIR_ABS: "${CI_PROJECT_DIR}/${BUILD_DIR}"
  before_script:
    - pwd
    - ls
    - echo "Project Directory    ${CI_PROJECT_DIR}"
    - echo "Source Directory     ${SRC_DIR_ABS}"
    - echo "      Directory Name ${SRC_DIR}"
    - echo "Build Directory      ${BUILD_DIR_ABS}"
    - echo "      Directory Name ${BUILD_DIR}"
    - mkdir "${SRC_DIR_ABS}"
    - cp ci/top_CMakeLists.txt "${SRC_DIR_ABS}/CMakeLists.txt"
    - rsync -a "${CI_PROJECT_DIR}" "${SRC_DIR_ABS}"  --exclude "${SRC_DIR}"
  script:
    - cd "${SRC_DIR_ABS}"
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/SusySkimAna/SusySkimValidation.git
    - echo "Cloning SusySkimDriver..."
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/SusySkimAna/SusySkimDriver.git
    - echo "Cloning IFFTruthClassifier tool..."
    - git clone -b v1.0.0 --single-branch https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/ATLAS-IFF/IFFTruthClassifier.git
    - echo "Cloning TruthDecayContainer ..."
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/shion/TruthDecayContainer.git
    - cd ../
    - echo "Copy the ST config from the install area of the AB release & Update the actualMu config for 2018."
    - curl -o "${SRC_DIR_ABS}/SusySkimValidation/data/SUSYTools/SUSYTools_SusySkimValidation_Default.conf" "https://gitlab.cern.ch/atlas/athena/-/raw/release/${AB_FIXED_RELEASE}/PhysicsAnalysis/SUSYPhys/SUSYTools/data/SUSYTools_Default.conf"

    - cat "${SRC_DIR_ABS}/CMakeLists.txt"
    - source /release_setup.sh
    - cmake -S "${SRC_DIR_ABS}" -B "${BUILD_DIR_ABS}"
    - cmake --build "${BUILD_DIR_ABS}"  -- -j 8
    - source "${BUILD_DIR_ABS}/${AnalysisBase_PLATFORM}/setup.sh"
    - run_xAODNtMaker -h
    - if ! (type run_xAODNtMaker > /dev/null 2>&1); then echo "Compile failed. Exit."; exit 1; fi
    - cd "${BUILD_DIR_ABS}"
    - cpack -G RPM
    - ls -lavh
    - mv SusySkimMaker*.rpm ${CI_PROJECT_DIR}/susyskim_ana.rpm

  artifacts:
    name: rpm
    paths:
      - susyskim_ana.rpm
      - Dockerfile
    expire_in: 1 day

################################
# build RPMs of the analysis code to install in docker image

compile:
  extends: .build_template
  image: atlas/analysisbase:$AB_FIXED_RELEASE

compile_latest:
  extends: .build_template
  image: atlas/analysisbase:$AB_LATEST_RELEASE
  allow_failure: true

################################
#  Image building
################################

package_image:
  image:
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder
    entrypoint: [""]
  stage: package
  dependencies: [compile]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR
                       --dockerfile $CI_PROJECT_DIR/Dockerfile
                       --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
                       --build-arg AB_FIXED_RELEASE=$AB_FIXED_RELEASE
                       --build-arg CI_COMMIT_SHA=$CI_COMMIT_SHA
                       --build-arg CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
                       --build-arg CI_COMMIT_TAG=$CI_COMMIT_TAG
                       --build-arg CI_JOB_URL=$CI_JOB_URL
                       --build-arg CI_PROJECT_URL=$CI_PROJECT_URL

################################
# Run
################################
# Anything that wants to use the provided analysis image (with set up) should
# extend this template.  This specifies the built image, as well as the correct
# initialization to get MBJ_run.py and kinit access.
.analysis_image:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - source /release_setup.sh
    - echo $SERVICE_PASS | kinit $CERN_USER

# This template is used by anything that wants to process a provided sample located in /eos/user.
.run_DAOD:
  extends: .analysis_image
  variables:
    NEVENTS: 1000
  stage: run
  script:
    - echo "CI_JOB_NAME=${CI_JOB_NAME}"
    - export submitDir="${CI_JOB_NAME}_vTest"
    - mkdir -p ${submitDir}
    - ls -lavh /eos/
    - run_xAODNtMaker -d ${CI_JOB_NAME} -tag vTest -s ${INPUTDIR} -writeSkims 1 -writeTrees 1 -deepConfig SusySkimValidation_Rel21.config -selector Validation --useXROOTd -DAODKernelName ${DERIVNAME} -MaxEvents ${NEVENTS}
    - ls -lavh
    - ls -lavh ${submitDir}
  artifacts:
    name: submitDir
    paths:
      - ${CI_JOB_NAME}_vTest/
    expire_in: 1 day

run_mc16e_ttbar_SUSY2_p3990:
  extends: .run_DAOD
  variables:
    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY2.e6337_s3126_r10724_p3990/
    DERIVNAME: SUSY2KernelSkim
  stage: run
run_mc16e_ttbar_SUSY5_p3990:
  extends: .run_DAOD
  variables:
    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6337_s3126_r10724_p3990
    DERIVNAME: SUSY5KernelSkim
  stage: run
