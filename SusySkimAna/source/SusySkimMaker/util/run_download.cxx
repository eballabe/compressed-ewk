#include <cstdlib>
#include <string>

// ROOT
#include "TChain.h"

// RootCore
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/ArgParser.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/StatusCodeCheck.h"

using namespace std;

int main(int argc, char** argv)
{

  const char* APP_NAME = "run_dumpSampleInfo";

  TString user              = gSystem->ExpandPathName("$USER");
  TString baseDir           = "";
  TString tag               = "";
  TString deepConfig        = "";
  TString groupSet          = "";
  TString productionGroup   = "";
  TString prefix            = "";
  TString includeFiles      = "";
  TString excludeFiles      = "";
  bool noSubmit             = false;
  bool disableSkims         = false;
  bool disableTrees         = false;
  bool nonofficialproduction = false;
  int priority      = -1;

  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if( arg.parse(argv[i],"-d","Path to base directory. Will create three directories inside: trees, skims, and merged") )
      baseDir = argv[++i];
    else if (arg.parse(argv[i],"-u","Grid user name, default is taken as $USER") )
      user = argv[++i];
    else if (arg.parse(argv[i],"-tag","Tag for production you wish to process" ))
      tag = argv[++i];
    else if (arg.parse(argv[i],"-deepConfig","Deep configuration file" ))
      deepConfig = argv[++i];
    else if (arg.parse(argv[i],"-groupSet","Group set name" ) )
      groupSet = argv[++i];
    else if (arg.parse(argv[i],"-productionGroup","For official group production (e.g. phys-susy), changes prefix of output containers to group.<productionGroup>" ) )
      productionGroup = argv[++i];
    else if (arg.parse(argv[i],"-includeFiles","Specific files to include" ) )
      includeFiles = argv[++i];
    else if (arg.parse(argv[i],"-excludeFiles","Specific files to exclude" ) )
      excludeFiles = argv[++i];
    else if (arg.parse(argv[i], "-priority","Select samples only with this priority, specified as a property in sample text files"))
      priority=atoi(argv[++i]);
    else if (arg.parse(argv[i], "--noSubmit","Don't submit, just print out the input and output samples"))
      noSubmit=true;
    else if (arg.parse(argv[i], "--disableTree","Disable the downloading of the trees"))
      disableTrees=true;
    else if (arg.parse(argv[i], "--disableSkims","Disable the downloading of the skims"))
      disableSkims=true;
    else if (arg.parse(argv[i], "--nonofficialproduction","Non official production"))
      nonofficialproduction=true;
    else
    {
      std::cout << "Unknown argument" << argv[i] << std::endl;
      return 0;
    }
  }

  // Absoutely required for all executables
  CHECK( PkgDiscovery::Init( deepConfig ) );

  SampleHandler* sh = new SampleHandler();

  // Unique to evt counters
  sh->disableSkims(disableSkims);
  sh->disableTrees(disableTrees);
  sh->isBulkSubmission(true);

  // Other useful flags
  sh->noSubmit(noSubmit);
  sh->includeFiles(includeFiles);
  sh->excludeFiles(excludeFiles);
  sh->setSamplePriority(priority);
  sh->nonofficialproduction(nonofficialproduction);

  //Construct the prefix, e.g. user.<username> or group.<groupName>. If productionGroup is given, use group.<productionGroup>, otherwise use user.<username>.
  if ( !productionGroup.IsWhitespace() ) prefix = "group." + productionGroup;
  else prefix = "user." + user;

  sh->initialize(baseDir,tag,groupSet,prefix);

  // Do it
  sh->download();

  return 0;

}
