#!/bin/bash

executable       = $ENV(ROOTCOREBIN)/bin/x86_64-slc6-gcc49-opt/run_SkimNtRun
universe         = vanilla
log              = $(Process).log
output           = $(Process).out
error            = $(Process).err
getenv           = True
queue
