#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/Timer.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/TreeMaker.h"

//
#include <sstream>

// xAOD/RootCore
#include "SusySkimMaker/V0Object.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"
#include "ElectronPhotonSelectorTools/ElectronSelectorHelpers.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "xAODBase/IParticleHelpers.h"

// ROOT
#include "TMath.h"

V0Object::V0Object() : m_convertFromMeV(1.0),
                             m_event(0),
                             m_secondaryTrackContainer(""),
                             m_priSecVtxContainer("")
{
  m_primaryTrackContainer.clear();
  m_v0VectorMap.clear();
  m_caloVectorMap.clear();
}
// ------------------------------------------------------------------------- //
StatusCode V0Object::init(TreeMaker*& treeMaker,xAOD::TEvent* event)
{

  const char* APP_NAME = "V0Object";


  //
  for( auto& sysName : treeMaker->getSysVector() ){
    // Tracks
    V0Vector* tlv  = new V0Vector();
    m_v0VectorMap.insert( std::pair<TString,V0Vector*>(sysName,tlv) );

    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      MsgLog::INFO("TrackObject::init", "Adding a branch tracklets to skims: %s", sysTree->GetName() );
      std::map<TString,V0Vector*>::iterator v0Itr = m_v0VectorMap.find(sysName);
      sysTree->Branch("v0s",&v0Itr->second);
    }      
  }

  
  // JEFF
  CHECK( init_tools() );


  // Set global TEvent object
  m_event = event;
  
  if( !m_event ){
    MsgLog::ERROR("TrackObject::init","TEvent pointer is not valid!");
    return StatusCode::FAILURE;
  }

  // Return gracefully
  return StatusCode::SUCCESS;

}





// ------------------------------------------------------------------------- //
StatusCode V0Object::init_tools()
{

  const char* APP_NAME = "V0Object";
  // JEFF
  m_v0TrkQualityTool = new InDet::InDetTrackSelectionTool("V0TrackQualityTool");
  CHECK(m_v0TrkQualityTool->setProperty("CutLevel", "TightPrimary"));
  CHECK(m_v0TrkQualityTool->initialize());

  return StatusCode::SUCCESS;
}








// -------------------------------------------------------------------------- //
StatusCode V0Object::fillV0Container(const xAOD::VertexContainer* primVertex,
                                           const xAOD::EventInfo* eventInfo,
                                           const xAOD::TruthParticleContainer* truthParticles,
                                           std::string sys_name)
{



  std::map<TString,V0Vector*>::iterator it = m_v0VectorMap.find(sys_name);

  if( it==m_v0VectorMap.end() ){
    MsgLog::WARNING("V0Object::fillV0Container","Request to get track for unknown systematic %s ", sys_name.c_str() );
    return StatusCode::FAILURE;
  }

  // Get V0 vertex container
  const xAOD::VertexContainer* vertices = nullptr;
  //m_event->retrieve( vertices, "SUSY19RecoV0Candidates" );
  m_event->retrieve( vertices, "SUSY20RecoV0Candidates" ); // SICONG
  //std::cout << "JEFF: V0Object::fillV0Container: number of V0s = " <<  vertices->size() << std::endl;
  // Loop over V0 vertices
  for (const xAOD::Vertex *vertex : *vertices) {
    V0Variable* this_v0 = new V0Variable();
    this_v0->Kshort_mass         = vertex->auxdata< float >("Kshort_mass") * 0.001;
    this_v0->Lambda_mass         = vertex->auxdata< float >("Lambda_mass") * 0.001;
    this_v0->Lambdabar_mass      = vertex->auxdata< float >("Lambdabar_mass") * 0.001;
    this_v0->Kshort_massError    = vertex->auxdata< float >("Kshort_massError") * 0.001;
    this_v0->Lambda_massError    = vertex->auxdata< float >("Lambda_massError") * 0.001;
    this_v0->Lambdabar_massError = vertex->auxdata< float >("Lambdabar_massError") * 0.001;
    this_v0->Rxy                 = vertex->auxdata< float >("Rxy");
    this_v0->RxyError            = vertex->auxdata< float >("RxyError");
    this_v0->pT                  = vertex->auxdata< float >("pT") * 0.001;
    this_v0->pTError             = vertex->auxdata< float >("pTError") * 0.001;
    this_v0->px                  = vertex->auxdata< float >("px") * 0.001;
    this_v0->py                  = vertex->auxdata< float >("py") * 0.001;
    this_v0->pz                  = vertex->auxdata< float >("pz") * 0.001;


    //std::cout << "###### New V0 ######" << std::endl;
    int iTrk = 0;
    float trk1_pt  = 0;
    float trk2_pt  = 0;
    float trk1_eta = 0;
    float trk2_eta = 0;
    float trk1_phi = 0;
    float trk2_phi = 0;
    int   trk1_TightPrimary = 0;
    int   trk2_TightPrimary = 0;
    //int   trk1_barcode = -999; // Doesn't work right now
    //int   trk2_barcode = -999; // Doesn't work right now
    /*
    for(auto V0track : vertex->trackParticleLinks()){ //Loop over tracks linked to vertex
      iTrk++;
      if(iTrk == 1){
	trk1_pt           = (*V0track)->pt() * 0.001; 
	trk1_eta          = (*V0track)->eta();
	trk1_phi          = (*V0track)->phi();
	trk1_TightPrimary =  m_v0TrkQualityTool->accept(*V0track);

	// Get barcode
  std::cout << "JEFF: Looping through track 1" << std::endl;
	//const xAOD::TrackParticle* xAODTrk = *V0track;
	//const xAOD::TruthParticle* truthTrack = xAOD::TruthHelpers::getTruthParticle( *xAODTrk );
	//if ( truthTrack ) {
	//  std::cout << "JEFF: V0 Track Barcode = " << truthTrack->barcode() << std::endl;
	//}
	//else{
	//  std::cout << "JEFF: No Truth Track!" << std::endl;
	//}

      }
      if(iTrk == 2){
      std::cout << "JEFF: Looping through track 2" << std::endl;
	trk2_pt           = (*V0track)->pt() * 0.001; 
	trk2_eta          = (*V0track)->eta();
	trk2_phi          = (*V0track)->phi();
	trk2_TightPrimary =  m_v0TrkQualityTool->accept(*V0track);
	//const xAOD::TruthParticle* truthTrack = xAOD::TruthHelpers::getTruthParticle( **V0track );
	//if( truthTrack ){ trk2_barcode = truthTrack->barcode(); }
      }
      //std::cout << "==========> V0 Track pT = " << (*V0track)->pt() * 0.001 << std::endl;


      
    }
    */
    //std::cout << "==========> Track1 pT = " << trk1_pt << "    Track 2 pT = " << trk2_pt << std::endl;
    //std::cout << "==========> Track1 barcode = " << trk1_barcode << "    Track 2 barcode = " << trk2_barcode << std::endl;
    //std::cout << "JEFF: Now Filling..." << std::endl;

    this_v0->track1Pt           = trk1_pt;
    this_v0->track2Pt           = trk2_pt;
    this_v0->track1Eta          = trk1_eta;
    this_v0->track2Eta          = trk2_eta;
    this_v0->track1Phi          = trk1_phi;
    this_v0->track2Phi          = trk2_phi;
    this_v0->track1TightPrimary = trk1_TightPrimary;
    this_v0->track2TightPrimary = trk2_TightPrimary;
    
    

    //this_v0->track1Barcode = trk1_barcode; // Doesn't work right now
    //this_v0->track2Barcode = trk2_barcode; // Doesn't work right now

    // Save
    it->second->push_back(this_v0);
    



  }




  /*
  // Get V0 vertices
  const xAOD::VertexContainer* vertices = nullptr;
  //ANA_CHECK (evtStore()->retrieve( vertices, "SUSY19RecoV0Candidates"));
  m_event->retrieve( vertices, "SUSY19RecoV0Candidates" );
  std::cout << "JEFF: number of V0s = " <<  vertices->size() << std::endl;
  for (const xAOD::Vertex *vertex : *vertices) {
    float Kshort_mass = vertex->auxdata< float >("Kshort_mass") * 0.001;
    std::cout << "JEFF: ========> KShort Mass = " << Kshort_mass << std::endl;
    for(auto V0track : vertex->trackParticleLinks()){
      float V0track_pt  = (*V0track)->pt() * 0.001;
      std::cout << "JEFF: ================> Track pT = " << V0track_pt << std::endl;
    }
  }
  */







  
  /*
  std::map<TString,V0Vector*>::iterator it = m_trackVectorMap.find(sys_name);

  if( it==m_trackVectorMap.end() ){
    MsgLog::WARNING("TrackObject::fillV0Container","Request to get track for unknown systematic %s ", sys_name.c_str() );
    return StatusCode::FAILURE;
  }


  // Check the user actually provided a primary track container name
  // This can happen if SaveTracks is true, but they forgot the name
  if( m_primaryTrackContainer.size()==0 ){
    MsgLog::WARNING("TrackObject::fillV0Container","You've requested to save tracks, but never provided a name for CentralDBFields::PRIMARYTRACKCONTANIER");
    return StatusCode::SUCCESS;
  }

  const xAOD::TrackParticleContainer* secondaryTracks = 0;
  if( !m_secondaryTrackContainer.IsWhitespace() ){ 
    if( m_event->retrieve( secondaryTracks, m_secondaryTrackContainer.Data() ).isFailure() ){
      MsgLog::ERROR("TrackObject::fillV0Container","Could not retrieve secondaery track container %s from TEvent!!!",m_secondaryTrackContainer.Data() );
      return StatusCode::FAILURE;
    }
  }

  const xAOD::VertexContainer* secVertex = 0;
  if( !m_priSecVtxContainer.IsWhitespace() ){
    if( m_event->retrieve( secVertex, m_priSecVtxContainer.Data() ).isFailure() ){
      MsgLog::ERROR("TrackObject::fillV0Container","Could not retrieve vertex container %s from TEvent!!!", m_priSecVtxContainer.Data() );
      return StatusCode::FAILURE;
    }
  }
  
  // Now loop over all requested containers 
  for( auto primaryTrackContainer : m_primaryTrackContainer ){

    const xAOD::TrackParticleContainer* primaryTracks = 0;
    if( m_event->retrieve( primaryTracks, primaryTrackContainer.Data() ).isFailure() ){
      MsgLog::ERROR("TrackObject::fillV0Container","Could not retrieve primary track container %s from TEvent!!!",primaryTrackContainer.Data() );
      continue;
    }


    if( !primaryTracks ){
      MsgLog::ERROR("TrackObject::fillV0Container","Despite reading in %s from TEvent, it's still NULL?!?!?!",primaryTrackContainer.Data() );    
      continue;
    }
    
    V0Variable::TrackType type = V0Variable::TrackType::UNKNOWN;
    if( primaryTrackContainer=="InDetPixelPrdAssociationTrackParticles"  ) type = V0Variable::TrackType::PIXELPRD;
    else if( primaryTrackContainer=="InDetPixelThreeLayerTrackParticles" ) type = V0Variable::TrackType::PIXELTHREELAYER;
    else if( primaryTrackContainer=="InDetTrackParticles"                ) type = V0Variable::TrackType::STD;                
    else if( primaryTrackContainer=="MuonSpectrometerTrackParticles"     ) type = V0Variable::TrackType::MSONLY;


    // Loop over primary tracks
    for( const auto& track : *primaryTracks ){

      // Uncorrected skimming
      if( type==V0Variable::TrackType::PIXELPRD ){
        if( track->pt()<10000.0 ) continue;
      }

      //
      V0Variable* tracklet = new V0Variable();
      tracklet->trackType = type;

      // Fill standard tracking information
      fillTrackInfo(tracklet,track,primVertex,eventInfo,truthParticles);
      
      
      // Save
      it->second->push_back(tracklet);
      
    }
  
  } // Loop over primaryTrackContainers
  */





  // Return gracefully 
  return StatusCode::SUCCESS;

}



// -------------------------------------------------------------------------- //
void V0Object::fillTrackInfo(V0Variable*& trk,const xAOD::TrackParticle* xAODTrk,
                                const xAOD::VertexContainer* primVertex,
                                const xAOD::EventInfo* eventInfo,
				const xAOD::TruthParticleContainer* truthParticles)
{

  // TLV: Use vertex constrained track parameters when available
  if( cacc_KVUqOverP.isAvailable(*xAODTrk) ){
    float trk_theta = cacc_KVUtheta(*xAODTrk);
    float trk_pt    = fabs( sin(trk_theta) / cacc_KVUqOverP(*xAODTrk) ) ;
    float trk_eta   = -log(tan(trk_theta/2.0));
    float trk_phi   = cacc_KVUphi(*xAODTrk);
    // 
    trk->SetPtEtaPhiM( trk_pt * m_convertFromMeV,
                       trk_eta,
                       trk_phi,
                       xAODTrk->m() * m_convertFromMeV );
  }
  else{
    float units = m_convertFromMeV;
     // Units wrong for MS only tracks...?
    //if( trk->trackType == TrackVariable::TrackType::MSONLY ) units *= m_convertFromMeV;
    trk->SetPtEtaPhiM( xAODTrk->pt() * units,
      		       xAODTrk->eta(),
		       xAODTrk->phi(),
		       xAODTrk->m() * units );
  }


  // Charge
  trk->q = xAODTrk->charge();

  // Fit  quality
  trk->fitQuality = TMath::Prob(xAODTrk->chiSquared(),xAODTrk->numberDoF());

  // Track based isolation
  trk->ptcone20 = cacc_ptcone20.isAvailable(*xAODTrk) ? cacc_ptcone20(*xAODTrk) * m_convertFromMeV : getTrackIsolation(xAODTrk,primVertex,0.2); 
  trk->ptcone30 = cacc_ptcone30.isAvailable(*xAODTrk) ? cacc_ptcone30(*xAODTrk) * m_convertFromMeV : getTrackIsolation(xAODTrk,primVertex,0.3);
  trk->ptcone40 = cacc_ptcone40.isAvailable(*xAODTrk) ? cacc_ptcone40(*xAODTrk) * m_convertFromMeV : getTrackIsolation(xAODTrk,primVertex,0.4);

  // Calorimeter isolation
  trk->etcone20Topo = cacc_topoetcone20.isAvailable(*xAODTrk) ? cacc_topoetcone20(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etclus20Topo = cacc_topoetcone40NonCoreCone.isAvailable(*xAODTrk) ? cacc_topoetcone20NonCoreCone(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etclus40Topo = cacc_topoetcone40NonCoreCone.isAvailable(*xAODTrk) ? cacc_topoetcone40NonCoreCone(*xAODTrk) * m_convertFromMeV : -1.0;

  // Impact parameters
  trk->d0     = V0Object::getD0(xAODTrk);
  trk->z0     = V0Object::getZ0(xAODTrk,primVertex);
  trk->d0Err  = V0Object::getD0Err(xAODTrk,eventInfo);
  trk->z0Err  = V0Object::getZ0Err(xAODTrk); 

  // Hit information
  xAODTrk->summaryValue(trk->nIBLHits, xAOD::numberOfInnermostPixelLayerHits);
  xAODTrk->summaryValue(trk->nPixLayers, xAOD::numberOfContribPixelLayers);
  xAODTrk->summaryValue(trk->nExpBLayerHits, xAOD::expectInnermostPixelLayerHit);
  xAODTrk->summaryValue(trk->nPixHits, xAOD::numberOfPixelHits);
  xAODTrk->summaryValue(trk->nPixHoles, xAOD::numberOfPixelHoles);
  xAODTrk->summaryValue(trk->nPixOutliers, xAOD::numberOfPixelOutliers);
  xAODTrk->summaryValue(trk->nSCTHits, xAOD::numberOfSCTHits);
  xAODTrk->summaryValue(trk->nSCTOutliers, xAOD::numberOfSCTOutliers);
  xAODTrk->summaryValue(trk->nTRTHits, xAOD::numberOfTRTHits);
  xAODTrk->summaryValue(trk->nPixSpoiltHits,xAOD::numberOfPixelSpoiltHits);
  xAODTrk->summaryValue(trk->nGangedFlaggedFakes,xAOD::numberOfGangedFlaggedFakes);
  xAODTrk->summaryValue(trk->nSCTHoles,xAOD::numberOfSCTHoles);
  xAODTrk->summaryValue(trk->nSCTSharedHits,xAOD::numberOfSCTSharedHits);

  // Truth information
  trk->type   = xAOD::TruthHelpers::getParticleTruthType(*xAODTrk);
  trk->origin = xAOD::TruthHelpers::getParticleTruthOrigin(*xAODTrk);

  const xAOD::TruthParticle* truthTrack = xAOD::TruthHelpers::getTruthParticle( *xAODTrk );
  if( truthTrack ){

    // Need the barcode to associate truth link
    trk->barcode = truthTrack->barcode();
    
    if( truthParticles ){
      for( const auto& p : *truthParticles ){
        if( p->barcode()==truthTrack->barcode() ){
          for( unsigned int par=0; par<p->nParents(); par++ ){
            if( !p->parent(par) ) continue;

	    // SUSY origin
            if( abs(p->parent(par)->pdgId())>1000001 && abs(p->parent(par)->pdgId())<2000040 ){
              trk->origin = 22;
              break;
            }

	    // Expand as necessary...

          }
          break;
        }
      }
    }

    // TLV information
    trk->truthTLV.SetPtEtaPhiM( truthTrack->pt() * m_convertFromMeV,
				truthTrack->eta(),
				truthTrack->phi(),
				truthTrack->m() * m_convertFromMeV );
  }


}
// -------------------------------------------------------------------------------------------------------------------- //
double V0Object::getTrackIsolation(const xAOD::TrackParticle* track,const xAOD::VertexContainer* vertex,double dR)
{

  const xAOD::Vertex* pV = 0;
  for( const auto& vx : *vertex ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
      pV = vx;
      break;
    }
  }
  if( !pV ){
    MsgLog::ERROR("TrackObject::getTrackIsolation","Cannot get primary vertex. Will not calculate  track based isolation!");
    return -1.0;
  }

  double value = 0;
  for ( size_t i = 0; i < pV->nTrackParticles(); i++ ) {
    const auto* tp = pV->trackParticle(i);
    if ( tp == nullptr ) continue;
    if ( xAOD::P4Helpers::deltaR2(track, tp, false) < dR*dR ) {
      if ( track == tp ) continue;
      if ( xAOD::getOriginalObject(*track) == tp ) continue;
      if ( tp->pt() < 1000.0 ) continue;

      //if ( !m_trkselTool->accept(tp) ) continue;
      value += tp->pt();
    }
  }
  return value*m_convertFromMeV;


}
// -------------------------------------------------------------------------- //
float V0Object::getD0(const xAOD::TrackParticle* track)
{

  return track->d0();

}
// -------------------------------------------------------------------------- //
float V0Object::getD0Err(const xAOD::TrackParticle* track,const xAOD::EventInfo* eventInfo)
{

  /*
    Calculate the d0 uncertainty, taking into account the uncertainty on the beam spot
  */

  double sigma2_d0 = track->definingParametersCovMatrixVec().at(0);
  
  float sigma2_beamSpotd0 = xAOD::TrackingHelpers::d0UncertaintyBeamSpot2(track->phi(),
									  eventInfo->beamPosSigmaX(), 
									  eventInfo->beamPosSigmaY(), 
									  eventInfo->beamPosSigmaXY() );

  float d0Err = TMath::Sqrt(sigma2_d0 + sigma2_beamSpotd0);
  return d0Err;

}
// -------------------------------------------------------------------------- //
float V0Object::getZ0(const xAOD::TrackParticle* track,const xAOD::VertexContainer* vertex)
{

  // Get primary vertex
  const xAOD::Vertex* pV = 0;

  for( const auto& vx : *vertex ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
      pV = vx;
      break;
    }
  }

  if( !pV ){
    std::cout << "<TrackObject::getZ0> WARNING Cannot get primary vertex (PV), z0 will not be extrapolated to the PV" << std::endl;
    return track->z0();
  }
  else{
    // Extrapolate back to PV
    double z0_exPV = track->z0() + track->vz() - pV->z();
    return z0_exPV;
  }
  
}
// -------------------------------------------------------------------------- //
float V0Object::getZ0Err(const xAOD::TrackParticle* track)
{

  /*
    Index
      => d0 uncertainty 0
      => z0 uncertainty 2
      => qOverP uncertainty 14
  */

  std::vector<float> CovMatrixVec = track->definingParametersCovMatrixVec();

  const int idx = 2;

  if(CovMatrixVec.size()==15) return TMath::Sqrt(CovMatrixVec[idx]);
  else                        return (-100.0);

}
// ------------------------------------------------------------------------- //
const V0Vector* V0Object::getObj(TString sysName) 
{

  std::map<TString,V0Vector*>::iterator it = m_v0VectorMap.find(sysName);

  if(it==m_v0VectorMap.end()){
    MsgLog::WARNING("TrackObject::getObj","WARNING Cannot get track vector for systematic %s",sysName.Data() );
    return NULL;
  }

  return it->second;
  
}
// ------------------------------------------------------------------------- //
const ObjectVector* V0Object::getCaloObj(TString sysName)
{

  std::map<TString,ObjectVector*>::iterator it = m_caloVectorMap.find(sysName);

  if(it==m_caloVectorMap.end()){
    MsgLog::WARNING("TrackObject::getObj","WARNING Cannot get calo vector for systematic %s",sysName.Data() );
    return NULL;
  }

  return it->second;

}
// ------------------------------------------------------------------------- //
void V0Object::Reset()
{

  std::map<TString,V0Vector*>::iterator it;
  for(it = m_v0VectorMap.begin(); it != m_v0VectorMap.end(); it++){
    for (V0Vector::iterator muItr = it->second->begin(); muItr != it->second->end(); muItr++) {
      delete *muItr;
    }
    it->second->clear();
  }

  std::map<TString,ObjectVector*>::iterator itC;
  for(itC = m_caloVectorMap.begin(); itC != m_caloVectorMap.end(); itC++){
    for (ObjectVector::iterator objItr = itC->second->begin(); objItr != itC->second->end(); objItr++) {
      delete *objItr;
    }
    itC->second->clear();
  }


}
