
#include "SusySkimMaker/PhotonObject.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/Timer.h"

#include "xAODEgamma/PhotonxAODHelpers.h"

// ------------------------------------------------------------------ //
PhotonObject::PhotonObject() : m_trackObject(0)
{
  m_photonVectorMap.clear();
}
// ------------------------------------------------------------------ //
StatusCode PhotonObject::init(TreeMaker*& treeMaker)
{

  const char* APP_NAME = "PhotonObject";

  for( auto& sysName : treeMaker->getSysVector() ){
    PhotonVector* photon  = new PhotonVector();
    // Save photon vector into map
    m_photonVectorMap.insert( std::pair<TString,PhotonVector*>(sysName,photon) );
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      MsgLog::INFO("PhotonObject::init", "Adding a branch photons to skims: %s", sysTree->GetName() );
      std::map<TString,PhotonVector*>::iterator photonItr = m_photonVectorMap.find(sysName);
      sysTree->Branch("photons",&photonItr->second);
    }
  }

  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);

  // Photon selector tools to get medium and tight
  m_photon_tight = new AsgPhotonIsEMSelector("PhotonObject_TIGHT");
  m_photon_tight->setProperty("isEMMask",egammaPID::PhotonTight);
  // set the file that contains the cuts on the shower shapes (stored in http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/)
  m_photon_tight->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf"); // Rel 21 
  // initialise the tool
  CHECK( m_photon_tight->initialize() );
 
  // Isolation tools
  m_baseObject = new BaseObject();
  CHECK( m_baseObject->initialize("PhotonWP") );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------ //
StatusCode PhotonObject::fillPhotonContainer(xAOD::Photon* photon_xAOD, 
                                             const xAOD::VertexContainer* primVertex,
                                             const xAOD::EventInfo* eventInfo,                                             
                                             const std::map<TString,float> recoSF,
                                             const std::map<TString,bool> trigMap,
					                                   const MCCorrContainer* trigEff,
                                             std::string sys_name)
{

  auto it = m_photonVectorMap.find(sys_name);

  if( it==m_photonVectorMap.end() ){
    MsgLog::ERROR("PhotonObject::fillPhotonContainer", "Request to get photon for unknown systematic %s",sys_name.c_str() );
    return StatusCode::FAILURE;
  }

  //
  Timer::Instance()->Start( "PhotonObject::fillPhotonContainer" );

  //
  PhotonVariable* photon = new PhotonVariable();

  const xAOD::Vertex* vtx = photon_xAOD->vertex();
  std::vector<const xAOD::TrackParticle*> tracks;
  if(vtx){
    for(Int_t i = 0; i < vtx->nTrackParticles(); i++){
      const xAOD::TrackParticle* track = vtx->trackParticle(i);
      if(!track) continue;
      tracks.push_back(track);
    }
  }

  // TLV
  photon->SetPtEtaPhiM( photon_xAOD->pt() * m_convertFromMeV,
                        photon_xAOD->eta(),
                        photon_xAOD->phi(),
                        photon_xAOD->m() * m_convertFromMeV );

  if( m_trackObject ){
    for(auto tracktmp : tracks){
      photon->trkLinks.push_back(m_trackObject->fillTrack(tracktmp,primVertex,eventInfo,TrackVariable::TrackType::PHOTONLINK,sys_name));
    }
  }

  // SUSYTools definitions
  photon->signal = cacc_signal.isAvailable(*photon_xAOD) ? cacc_signal(*photon_xAOD) : false;;

  // Tight photon definition
  photon->tight = m_photon_tight->accept(photon_xAOD) ? true : false;

  // Overlap removal
  photon->passOR = cacc_passOR.isAvailable(*photon_xAOD) ? cacc_passOR(*photon_xAOD) : false;;

  // Isolation
  const Root::TAccept isIsoSel = m_baseObject->isoTool->accept(*photon_xAOD);
  photon->IsoFCTightCaloOnly   = isIsoSel.getCutResult("FixedCutTightCaloOnly");
  photon->IsoFixedCutTight     = isIsoSel.getCutResult("FixedCutTight");
  photon->IsoFixedCutLoose     = isIsoSel.getCutResult("FixedCutLoose");

  float topoetcone20 =0.0;
  float topoetcone30 =0.0;
  float topoetcone40 =0.0;
  photon_xAOD->isolationValue(topoetcone20,xAOD::Iso::topoetcone20);
  photon_xAOD->isolationValue(topoetcone30,xAOD::Iso::topoetcone30);
  photon_xAOD->isolationValue(topoetcone40,xAOD::Iso::topoetcone40);

  photon->topoetcone20 = topoetcone20 * m_convertFromMeV;
  photon->topoetcone30 = topoetcone30 * m_convertFromMeV;
  photon->topoetcone40 = topoetcone40 * m_convertFromMeV;

  // SUSY20 topoetcone
  photon->SUSY20_topoetcone20 = cacc_SUSY20_topoetcone20.isAvailable( *photon_xAOD ) ?
    cacc_SUSY20_topoetcone20( *photon_xAOD ) * m_convertFromMeV : -1.0;
  photon->SUSY20_topoetcone30 = cacc_SUSY20_topoetcone30.isAvailable( *photon_xAOD ) ?
    cacc_SUSY20_topoetcone30( *photon_xAOD ) * m_convertFromMeV : -1.0;
  photon->SUSY20_topoetcone40 = cacc_SUSY20_topoetcone40.isAvailable( *photon_xAOD ) ?
    cacc_SUSY20_topoetcone40( *photon_xAOD ) * m_convertFromMeV : -1.0;
  photon->SUSY20_topoetcone20NonCoreCone = cacc_SUSY20_topoetcone20NonCoreCone.isAvailable( *photon_xAOD ) ?
    cacc_SUSY20_topoetcone20NonCoreCone( *photon_xAOD ) * m_convertFromMeV : -1.0;
  photon->SUSY20_topoetcone30NonCoreCone = cacc_SUSY20_topoetcone30NonCoreCone.isAvailable( *photon_xAOD ) ?
    cacc_SUSY20_topoetcone30NonCoreCone( *photon_xAOD ) * m_convertFromMeV : -1.0;
  photon->SUSY20_topoetcone40NonCoreCone = cacc_SUSY20_topoetcone40NonCoreCone.isAvailable( *photon_xAOD ) ?
    cacc_SUSY20_topoetcone40NonCoreCone( *photon_xAOD ) * m_convertFromMeV : -1.0;

  // Trigger decision
  photon->triggerMap = trigMap;

  // Trigger efficiencies
  photon->trigEff = *trigEff;

  // MC corrections
  photon->recoSF = recoSF;

  //
  photon->isConverted = xAOD::EgammaHelpers::isConvertedPhoton( photon_xAOD );

  // see enums at https://gitlab.cern.ch/atlas/athena/blob/21.2/Event/xAOD/xAODEgamma/xAODEgamma/EgammaEnums.h#L243
  photon->conversionType   = (int) xAOD::EgammaHelpers::conversionType( photon_xAOD );

  photon->conversionRadius = (float) xAOD::EgammaHelpers::conversionRadius( photon_xAOD );

  // Save
  it->second->push_back(photon);

  // Stop timer
  Timer::Instance()->Start( "PhotonObject::fillPhotonContainer" );

  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------------------------ //
StatusCode PhotonObject::fillPhotonContainerTruth(xAOD::TruthParticle* photon_xAOD, std::string sys_name)
{

  auto it = m_photonVectorMap.find(sys_name);

  if( it==m_photonVectorMap.end() ){
    MsgLog::ERROR("PhotonObject::fillPhotonContainer", "Request to get photon for unknown systematic %s",sys_name.c_str() );
    return StatusCode::FAILURE;
  }

  //
  PhotonVariable* photon = new PhotonVariable();

  photon->SetPtEtaPhiM( photon_xAOD->pt() * m_convertFromMeV,
            photon_xAOD->eta(),
            photon_xAOD->phi(),
            photon_xAOD->m() * m_convertFromMeV );



  // SUSYTools definitions
  photon->signal = true;

  // Tight photon definition
  photon->tight = true;

  // Overlap removal
  photon->passOR = true;
 
  // Save
  it->second->push_back(photon);

  //
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------ //
const PhotonVector* PhotonObject::getObj(TString sysName)
{

  std::map<TString,PhotonVector*>::iterator it = m_photonVectorMap.find(sysName);

  if(it==m_photonVectorMap.end()){
    MsgLog::WARNING("PhotonObject::getObj","Cannot get muon vector for systematic: %s",sysName.Data() );
    return NULL;
  }

  return it->second;

}
// ------------------------------------------------------------------ //
void PhotonObject::Reset()
{

  std::map<TString,PhotonVector*>::iterator it;
  for(it = m_photonVectorMap.begin(); it != m_photonVectorMap.end(); it++){
    for (PhotonVector::iterator photonItr = it->second->begin(); photonItr != it->second->end(); photonItr++) {
      delete *photonItr;
    }
    it->second->clear();
  }

}
// ------------------------------------------------------------------ //
PhotonObject::~PhotonObject()
{

}
