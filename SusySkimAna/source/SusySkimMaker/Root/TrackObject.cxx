#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/Timer.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/TreeMaker.h"

//
#include <sstream>

// xAOD/RootCore
#include "SusySkimMaker/TrackObject.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/VertexContainer.h"
#include "ElectronPhotonSelectorTools/ElectronSelectorHelpers.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "xAODBase/IParticleHelpers.h"

// ROOT
#include "TMath.h"

TrackObject::TrackObject() : m_convertFromMeV(1.0),
                             m_event(0),
                             m_secondaryTrackContainer(""),
                             m_priSecVtxContainer(""),
                             m_saveEgammaClusters(false),
                             m_truthClassifier(0)
{
  m_primaryTrackContainer.clear();
  m_trackVectorMap.clear();
  m_caloVectorMap.clear();
}
// ------------------------------------------------------------------------- //
StatusCode TrackObject::init(TreeMaker*& treeMaker,xAOD::TEvent* event)
{

  const char* APP_NAME = "TrackObject";

   // Write out Egamma calo clusters
  CentralDB::retrieve(CentralDBFields::WRITEEGAMMACLUSTERS,m_saveEgammaClusters);

  //
  for( auto& sysName : treeMaker->getSysVector() ){
    // Tracks
    TrackVector* tlv  = new TrackVector();
    m_trackVectorMap.insert( std::pair<TString,TrackVector*>(sysName,tlv) );
    // Calo clusters
    if( m_saveEgammaClusters ){
      ObjectVector* calo = new ObjectVector();
      m_caloVectorMap.insert( std::pair<TString,ObjectVector*>(sysName,calo) );
    }
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      MsgLog::INFO("TrackObject::init", "Adding a branch tracklets to skims: %s", sysTree->GetName() );
      std::map<TString,TrackVector*>::iterator trackItr = m_trackVectorMap.find(sysName);
      sysTree->Branch("tracks",&trackItr->second);
      if( m_saveEgammaClusters ){
        MsgLog::INFO("TrackObject::init", "Adding a branch caloClus to skims: %s", sysTree->GetName() );
        std::map<TString,ObjectVector*>::iterator caloItr = m_caloVectorMap.find(sysName);
        sysTree->Branch("caloClus",&caloItr->second);
      }
    }
  }

  //
  CHECK( init_tools() );

  // Set global TEvent object
  m_event = event;

  if( !m_event ){
    MsgLog::ERROR("TrackObject::init","TEvent pointer is not valid!");
    return StatusCode::FAILURE;
  }

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode TrackObject::init_tools()
{

  const char* APP_NAME = "TrackObject";

  MsgLog::INFO("TrackObject::init_tools", "Retrieving fields from CentralDB...");
  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);
  CentralDB::retrieve(CentralDBFields::PRIMARYTRACKCONTANIER,m_primaryTrackContainer);
  CentralDB::retrieve(CentralDBFields::SECONDARYTRACKCONTANIER,m_secondaryTrackContainer);
  CentralDB::retrieve(CentralDBFields::PRISECVTXCONTAINER,m_priSecVtxContainer);

  m_trkSelLooseTool = new InDet::InDetTrackSelectionTool("BaseObjTrackIsoToolLoose");
  CHECK(m_trkSelLooseTool->setProperty("CutLevel"     , "Loose"));
  //CHECK(m_trkSelLooseTool->setProperty("minPt"        , 1000   ));
  //CHECK(m_trkSelLooseTool->setProperty("maxZ0SinTheta", 3.0    ));
  CHECK(m_trkSelLooseTool->initialize());

  m_trkSelTightPrimaryTool = new InDet::InDetTrackSelectionTool("BaseObjTrackIsoToolTightPrimary");
  CHECK(m_trkSelTightPrimaryTool->setProperty("CutLevel"     , "TightPrimary"));
  CHECK(m_trkSelTightPrimaryTool->initialize());

  // Yuya
  m_truthClassifier = new MCTruthClassifier("Track_MCTruth");

  m_smearTool = new InDet::InDetTrackSmearingTool("TrackSmearingTool");
  CHECK(m_smearTool->initialize());

  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
StatusCode TrackObject::fillTrackContainer(const xAOD::VertexContainer* primVertex,
                                           const xAOD::EventInfo* eventInfo,
                                           const xAOD::TruthParticleContainer* truthParticles,
					   const CP::SystematicSet& systSet)
{

  std::string sys_name = systSet.name();

  std::map<TString,TrackVector*>::iterator it = m_trackVectorMap.find(sys_name);

  if( it==m_trackVectorMap.end() ){
    MsgLog::WARNING("TrackObject::fillTrackContainer","Request to get track for unknown systematic %s ", sys_name.c_str() );
    return StatusCode::FAILURE;
  }


  // Save clusters!
  if( m_saveEgammaClusters ){
    saveCaloClusters(sys_name);
  }


  // Check the user actually provided a primary track container name
  // This can happen if SaveTracks is true, but they forgot the name
  if( m_primaryTrackContainer.size()==0 ){
    MsgLog::WARNING("TrackObject::fillTrackContainer","You've requested to save tracks, but never provided a name for CentralDBFields::PRIMARYTRACKCONTANIER");
    return StatusCode::SUCCESS;
  }

  const xAOD::TrackParticleContainer* secondaryTracks = 0;
  if( !m_secondaryTrackContainer.IsWhitespace() ){
    if( m_event->retrieve( secondaryTracks, m_secondaryTrackContainer.Data() ).isFailure() ){
      MsgLog::ERROR("TrackObject::fillTrackContainer","Could not retrieve secondaery track container %s from TEvent!!!",m_secondaryTrackContainer.Data() );
      return StatusCode::FAILURE;
    }
  }

  const xAOD::VertexContainer* secVertex = 0;
  if( !m_priSecVtxContainer.IsWhitespace() ){
    if( m_event->retrieve( secVertex, m_priSecVtxContainer.Data() ).isFailure() ){
      MsgLog::ERROR("TrackObject::fillTrackContainer","Could not retrieve vertex container %s from TEvent!!!", m_priSecVtxContainer.Data() );
      return StatusCode::FAILURE;
    }
  }

  // Now loop over all requested containers
  for( auto primaryTrackContainer : m_primaryTrackContainer ){

    const xAOD::TrackParticleContainer* primaryTracks = 0;
    if( m_event->retrieve( primaryTracks, primaryTrackContainer.Data() ).isFailure() ){
      MsgLog::ERROR("TrackObject::fillTrackContainer","Could not retrieve primary track container %s from TEvent!!!",primaryTrackContainer.Data() );
      continue;
    }

    if( !primaryTracks ){
      MsgLog::ERROR("TrackObject::fillTrackContainer","Despite reading in %s from TEvent, it's still NULL?!?!?!",primaryTrackContainer.Data() );
      continue;
    }

    TrackVariable::TrackType type = TrackVariable::TrackType::UNKNOWN;
    if( primaryTrackContainer=="InDetPixelPrdAssociationTrackParticles"  ) type = TrackVariable::TrackType::PIXELPRD;
    else if( primaryTrackContainer=="InDetPixelThreeLayerTrackParticles" ) type = TrackVariable::TrackType::PIXELTHREELAYER;
    else if( primaryTrackContainer=="InDetTrackParticles"                ) type = TrackVariable::TrackType::STD;
    else if( primaryTrackContainer=="MuonSpectrometerTrackParticles"     ) type = TrackVariable::TrackType::MSONLY;

    const xAOD::TrackParticleContainer* primTrks = nullptr;

    // Apply smearing to track d0, z0 provided you put 
    // TRK_RES_[D0|Z0]_MEAS in systematics file
    if( sys_name.find("TRK_RES") != std::string::npos )
      primTrks = smearTracks(primaryTracks,systSet);  
    else primTrks = primaryTracks;

    // Loop over primary tracks
    for(const auto& track : *primTrks ){

      // Uncorrected skimming
      if( type==TrackVariable::TrackType::PIXELPRD ){
        if( track->pt()<10000.0 ) continue;
      }

      //
      TrackVariable* tracklet = new TrackVariable();
      tracklet->trackType = type;

      // Fill standard tracking information
      fillTrackInfo(tracklet,track,primVertex,eventInfo,truthParticles);

      // Associate the secondary track based on
      // the vertex information
      LinkSecondaryTrack(tracklet,track,secondaryTracks,secVertex,primVertex,eventInfo,truthParticles);
      LinkV0Vertex(tracklet,track,secVertex);

      // Save
      it->second->push_back(tracklet);

    }

  } // Loop over primaryTrackContainers

  // Return gracefully
  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
TrackVariable* TrackObject::fillTrack(const xAOD::TrackParticle* track,
                                      const xAOD::VertexContainer* primVertex,
                                      const xAOD::EventInfo* eventInfo,
                                      TrackVariable::TrackType type,
                                      std::string /*sys_name*/)
{
  if( !track ) return 0;
   TrackVariable* tracklet = new TrackVariable();
  tracklet->trackType = type ;
  fillTrackInfo(tracklet,track,primVertex,eventInfo,0);
  return tracklet;

}
// -------------------------------------------------------------------------- //
void TrackObject::fillTrackInfo(TrackVariable*& trk,const xAOD::TrackParticle* xAODTrk,
                                const xAOD::VertexContainer* primVertex,
                                const xAOD::EventInfo* eventInfo,
                                const xAOD::TruthParticleContainer* truthParticles)
{

  // TLV: Use vertex constrained track parameters when available
  if( cacc_KVUqOverP.isAvailable(*xAODTrk) ){
    float trk_theta = cacc_KVUtheta(*xAODTrk);
    float trk_pt    = fabs( sin(trk_theta) / cacc_KVUqOverP(*xAODTrk) ) ;
    float trk_eta   = -log(tan(trk_theta/2.0));
    float trk_phi   = cacc_KVUphi(*xAODTrk);
    //
    trk->SetPtEtaPhiM( trk_pt * m_convertFromMeV,
                       trk_eta,
                       trk_phi,
                       xAODTrk->m() * m_convertFromMeV );
  }
  else{
    float units = m_convertFromMeV;
     // Units wrong for MS only tracks...?
    //if( trk->trackType == TrackVariable::TrackType::MSONLY ) units *= m_convertFromMeV;
    trk->SetPtEtaPhiM( xAODTrk->pt() * units,
                       xAODTrk->eta(),
                       xAODTrk->phi(),
                       xAODTrk->m() * units );
  }

  // Charge
  trk->q = xAODTrk->charge();

  // Track based isolation
  trk->ptcone20 = cacc_ptcone20.isAvailable(*xAODTrk) ? cacc_ptcone20(*xAODTrk) * m_convertFromMeV : getTrackIsolation(xAODTrk,primVertex,0.2);
  trk->ptcone30 = cacc_ptcone30.isAvailable(*xAODTrk) ? cacc_ptcone30(*xAODTrk) * m_convertFromMeV : getTrackIsolation(xAODTrk,primVertex,0.3);
  trk->ptcone40 = cacc_ptcone40.isAvailable(*xAODTrk) ? cacc_ptcone40(*xAODTrk) * m_convertFromMeV : getTrackIsolation(xAODTrk,primVertex,0.4);

  // Calorimeter isolation
  trk->etcone20Topo = cacc_topoetcone20.isAvailable(*xAODTrk) ? cacc_topoetcone20(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etcone30Topo = cacc_topoetcone30.isAvailable(*xAODTrk) ? cacc_topoetcone30(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etcone40Topo = cacc_topoetcone40.isAvailable(*xAODTrk) ? cacc_topoetcone40(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etclus20Topo = cacc_topoetcone20NonCoreCone.isAvailable(*xAODTrk) ? cacc_topoetcone20NonCoreCone(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etclus30Topo = cacc_topoetcone30NonCoreCone.isAvailable(*xAODTrk) ? cacc_topoetcone30NonCoreCone(*xAODTrk) * m_convertFromMeV : -1.0;
  trk->etclus40Topo = cacc_topoetcone40NonCoreCone.isAvailable(*xAODTrk) ? cacc_topoetcone40NonCoreCone(*xAODTrk) * m_convertFromMeV : -1.0;

  // Impact parameters
  trk->d0     = TrackObject::getD0(xAODTrk);
  trk->z0     = TrackObject::getZ0(xAODTrk,primVertex);
  trk->z0Origin = xAODTrk->z0();
  trk->d0Err  = TrackObject::getD0Err(xAODTrk,eventInfo);
  trk->z0Err  = TrackObject::getZ0Err(xAODTrk);

  trk->z0SinTheta = TrackObject::getZ0(xAODTrk,primVertex) * TMath::Sin(xAODTrk->theta()); // JEFF
  trk->m          = xAODTrk->m() * m_convertFromMeV; // JEFF
  trk->TightPrimary = m_trkSelTightPrimaryTool->accept(xAODTrk); // JEFF

  if (xAODTrk->isAvailable<float>("TRTdEdx") && xAODTrk->isAvailable<float>("pixeldEdx") ){
    trk->TRTdEdx = xAODTrk->auxdata<float>("TRTdEdx"); // SICONG
    trk->pixeldEdx = xAODTrk->auxdata<float>("pixeldEdx"); // SICONG
  }
  trk->index = xAODTrk->index(); // SICONG

  // Secondary vertex impact parameters
  trk->d0SV    = cacc_d0wrtSV.isAvailable(*xAODTrk)    ? cacc_d0wrtSV(*xAODTrk)                            : -1;
  trk->z0SV    = cacc_z0wrtSV.isAvailable(*xAODTrk)    ? cacc_z0wrtSV(*xAODTrk)                            : -1;
  trk->ptSV    = cacc_ptwrtSV.isAvailable(*xAODTrk)    ? cacc_ptwrtSV(*xAODTrk) * m_convertFromMeV         : -1;
  trk->etaSV   = cacc_etawrtSV.isAvailable(*xAODTrk)   ? cacc_etawrtSV(*xAODTrk)                           : -1;
  trk->phiSV   = cacc_phiwrtSV.isAvailable(*xAODTrk)   ? cacc_phiwrtSV(*xAODTrk)                           : -1;
  trk->d0ErrSV = cacc_errD0wrtSV.isAvailable(*xAODTrk) ? sqrt(cacc_errD0wrtSV(*xAODTrk))                   : -1;
  trk->z0ErrSV = cacc_errZ0wrtSV.isAvailable(*xAODTrk) ? sqrt(cacc_errZ0wrtSV(*xAODTrk))                   : -1;
  trk->pErrSV  = cacc_errPwrtSV.isAvailable(*xAODTrk)  ? sqrt(cacc_errPwrtSV(*xAODTrk)) * m_convertFromMeV : -1;

  // Hit information
  xAODTrk->summaryValue(trk->nIBLHits, xAOD::numberOfInnermostPixelLayerHits);
  xAODTrk->summaryValue(trk->nBLayerHits, xAOD::numberOfBLayerHits);
  xAODTrk->summaryValue(trk->nPixLayers, xAOD::numberOfContribPixelLayers);
  xAODTrk->summaryValue(trk->nExpIBLHits, xAOD::expectInnermostPixelLayerHit);
  xAODTrk->summaryValue(trk->nExpBLayerHits, xAOD::expectNextToInnermostPixelLayerHit);
  xAODTrk->summaryValue(trk->nPixHits, xAOD::numberOfPixelHits);
  xAODTrk->summaryValue(trk->nPixDeadSensors, xAOD::numberOfPixelDeadSensors);
  xAODTrk->summaryValue(trk->nPixHoles, xAOD::numberOfPixelHoles);
  xAODTrk->summaryValue(trk->nPixSharedHits, xAOD::numberOfPixelSharedHits);
  xAODTrk->summaryValue(trk->nPixSplitHits, xAOD::numberOfPixelSplitHits);
  xAODTrk->summaryValue(trk->nSCTHits, xAOD::numberOfSCTHits);
  xAODTrk->summaryValue(trk->nSCTHoles, xAOD::numberOfSCTHoles);
  xAODTrk->summaryValue(trk->nSCTSharedHits, xAOD::numberOfSCTSharedHits);
  xAODTrk->summaryValue(trk->nPixOutliers, xAOD::numberOfPixelOutliers);
  xAODTrk->summaryValue(trk->nSCTDeadSensors, xAOD::numberOfSCTDeadSensors);
  xAODTrk->summaryValue(trk->nSCTOutliers, xAOD::numberOfSCTOutliers);
  xAODTrk->summaryValue(trk->nTRTHits, xAOD::numberOfTRTHits);
  xAODTrk->summaryValue(trk->nPixSpoiltHits,xAOD::numberOfPixelSpoiltHits);
  xAODTrk->summaryValue(trk->nGangedFlaggedFakes,xAOD::numberOfGangedFlaggedFakes);
  xAODTrk->summaryValue(trk->pixeldEdx,xAOD::pixeldEdx);

   // Is Tight
  trk->isTight = m_trkSelTightPrimaryTool->accept(*xAODTrk);

  // Is Loose
  trk->isLoose = m_trkSelLooseTool->accept(xAODTrk);

  trk->chiSquared = cacc_chiSquared.isAvailable(*xAODTrk) ? cacc_chiSquared(*xAODTrk) : -1;
  trk->numberDoF = cacc_numberDoF.isAvailable(*xAODTrk) ? cacc_numberDoF(*xAODTrk) : -1;
  trk->fitQuality = (cacc_chiSquared.isAvailable(*xAODTrk) && cacc_numberDoF.isAvailable(*xAODTrk)) ? TMath::Prob(xAODTrk->chiSquared(),xAODTrk->numberDoF()) : -1;
  trk->hitPattern = cacc_hitPattern.isAvailable(*xAODTrk) ? cacc_hitPattern(*xAODTrk) : -1;
  trk->truthMatchProb = cacc_TMP.isAvailable(*xAODTrk) ? cacc_TMP(*xAODTrk) : -1.0;

  // Truth information
  trk->type   = xAOD::TruthHelpers::getParticleTruthType(*xAODTrk);
  trk->origin = xAOD::TruthHelpers::getParticleTruthOrigin(*xAODTrk);

  const xAOD::TruthParticle* truthTrack = xAOD::TruthHelpers::getTruthParticle( *xAODTrk );
  if( truthTrack ){

    // Need the barcode to associate truth link
    trk->barcode = truthTrack->barcode();
    trk->pdgId = truthTrack->pdgId();
    trk->status = truthTrack->status();

    if( truthParticles ){
      for( const auto& p : *truthParticles ){
        if( p->barcode()==truthTrack->barcode() ){

          bool hasDecayVtx = p->hasDecayVtx();
          bool hasProdVtx = p->hasProdVtx();
          const xAOD::TruthVertex* prodVtx = p->prodVtx();
          const xAOD::TruthVertex* decayVtx = p->decayVtx();
          if(prodVtx){
            trk->prodvtx = TVector3(prodVtx->x(),prodVtx->y(),prodVtx->z());
            trk->prodvtxtype = prodVtx->type();
            trk->ishardvtx = isHardScatVrtx(prodVtx);
          }
          if(decayVtx){
            trk->decayvtx = TVector3(decayVtx->x(),decayVtx->y(),decayVtx->z());
            trk->decayvtxtype = decayVtx->type();
          }

          for( unsigned int par=0; par<p->nParents(); par++ ){
            if( !p->parent(par) ) continue;

            trk->npar = p->nParents();
            trk->parbarcode = p->parent(par)->barcode();
            trk->parpdgId = p->parent(par)->pdgId();
            trk->parnchild = p->parent(par)->nChildren();

          }
          break;

        }
      }
    }

    // TLV information
    trk->truthTLV.SetPtEtaPhiM( truthTrack->pt() * m_convertFromMeV,
                                truthTrack->eta(),
                                truthTrack->phi(),
                                truthTrack->m() * m_convertFromMeV );

    if( truthParticles ){
      bool flag = false;
      for( const auto& parchild : *truthParticles ){
        if(trk->barcode == parchild->barcode())continue;
        for( unsigned int npar=0; npar < parchild->nParents(); npar++ ){
          if( !parchild->parent(npar) ) continue;
          if( parchild->parent(npar)->barcode() == trk->parbarcode ){
            trk->parchildpdgId = parchild->pdgId();
            trk->parchildpt = parchild->pt() * m_convertFromMeV;
            trk->parchildbarcode = parchild->barcode();
            flag = true;
            break;
          }
        }
      if(flag) break;
      }
    }
  }

}
// -------------------------------------------------------------------------- //
void TrackObject::LinkSecondaryTrack(TrackVariable*& trk,
                                     const xAOD::TrackParticle* primaryTrack,
                                     const xAOD::TrackParticleContainer* secondaryTracks,
                                     const xAOD::VertexContainer* secVertex,
                                     const xAOD::VertexContainer* primVertex,
                                     const xAOD::EventInfo* eventInfo,
                                     const xAOD::TruthParticleContainer* truthParticles )
{

  // Required
  if( !secondaryTracks || !secVertex ) return;

  //
  for( const auto& vtx : *secVertex ){

    if( vtx->nTrackParticles()<2 ) continue;

    // Hard-coded for the soft-pion reconstruction...
    auto vtx_pionTrk       = vtx->trackParticle(0);
    auto vtx_charginoTrk   = vtx->trackParticle(1);

    if( primaryTrack!=vtx_charginoTrk ) continue;

    // Make and link the associated
    // track to the primary 'trk' object
    auto linkedTrk = trk->makeAssociatedTrack();

    // Fill the standard tracking information
    // for the associated track
    fillTrackInfo(linkedTrk,vtx_pionTrk,primVertex,eventInfo,truthParticles);

    // Save the reconstructed vertex position
    linkedTrk->vtx.SetXYZ( vtx->x(), vtx->y(), vtx->z() );

    // Vertex fit quality
    linkedTrk->vtxQuality =  (vtx->chiSquared() / vtx->numberDoF());

    // Add in vertex truth IPs
    linkedTrk->truth_d0SV = cacc_TP_trk0_d0.isAvailable(*vtx) ? cacc_TP_trk0_d0(*vtx) : 0;
    linkedTrk->truth_z0SV = cacc_TP_trk0_z0.isAvailable(*vtx) ? cacc_TP_trk0_z0(*vtx) : 0;
    trk->truth_d0SV       = cacc_TP_trk1_d0.isAvailable(*vtx) ? cacc_TP_trk1_d0(*vtx) : 0;
    trk->truth_z0SV       = cacc_TP_trk1_z0.isAvailable(*vtx) ? cacc_TP_trk1_z0(*vtx) : 0;

  } // Loop over all secondary vertices

}
// -------------------------------------------------------------------------- //
void TrackObject::LinkV0Vertex(TrackVariable*& trk,
                               const xAOD::TrackParticle* primaryTrack,
                               const xAOD::VertexContainer* secVertex)
{

  // Required
  if( !secVertex ) return;

  //
  for( const auto& vtx : *secVertex ){

    for(Int_t itrk = 0; itrk < vtx->nTrackParticles(); itrk++ ){
      auto vtx_secTrk       = vtx->trackParticle(itrk);

    if(primaryTrack!= vtx_secTrk) continue; //Track is associated with V0 vertex
    trk->isSecTrk = 1;

    } // Loop over all tracks from V0 vertex
  } // Loop over all secondary vertices

}
// -------------------------------------------------------------------------- //
StatusCode TrackObject::saveCaloClusters(std::string sys_name)
{

  std::map<TString,ObjectVector*>::iterator it = m_caloVectorMap.find(sys_name);
  if( it==m_caloVectorMap.end() ){
    MsgLog::WARNING("TrackObject::saveCaloClusters","Request to get caloClus for unknown systematic %s ", sys_name.c_str() );
    return StatusCode::FAILURE;
  }

  // Get xAOD container
  const xAOD::CaloClusterContainer* EgammaClusters = 0;
  if( m_event->retrieve(EgammaClusters, "egammaClusters" ).isFailure() ){
    MsgLog::WARNING("TrackObject::fillTrackContainer","Could not retrieve EgammaClusters!!");
    return StatusCode::SUCCESS;
  }

  // Save
  for( const auto& clus : *EgammaClusters ){
    if( clus->pt()<10000.0 ) continue;
    ObjectVariable* caloClus = new ObjectVariable();
    caloClus->SetPtEtaPhiM( clus->pt()  * m_convertFromMeV,
                            clus->eta(),
                            clus->phi(),
                            clus->m()  * m_convertFromMeV );
    it->second->push_back(caloClus);
  }

  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------------------------------------------------- //
double TrackObject::getTrackIsolation(const xAOD::TrackParticle* track,const xAOD::VertexContainer* vertex,double dR)
{

  const xAOD::Vertex* pV = 0;
  for( const auto& vx : *vertex ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
      pV = vx;
      break;
    }
  }
  if( !pV ){
    MsgLog::ERROR("TrackObject::getTrackIsolation","Cannot get primary vertex. Will not calculate  track based isolation!");
    return -1.0;
  }

  double value = 0;
  for ( size_t i = 0; i < pV->nTrackParticles(); i++ ) {
    const auto* tp = pV->trackParticle(i);
    if ( tp == nullptr ) continue;
    if ( xAOD::P4Helpers::deltaR2(track, tp, false) < dR*dR ) {
      if ( track == tp ) continue;
      if ( xAOD::getOriginalObject(*track) == tp ) continue;
      if ( tp->pt() < 1000.0 ) continue;

      if ( !m_trkSelLooseTool->accept(tp) ) continue;
      value += tp->pt();
    }
  }
  return value*m_convertFromMeV;


}

// -------------------------------------------------------------------------------------------------------- //
const xAOD::TrackParticleContainer* TrackObject::smearTracks(const xAOD::TrackParticleContainer* primaryTracks, const CP::SystematicSet& systSet)
{
  
  // Make a shallow copy of the track particle container
  std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > shallow_xAOD_copy = xAOD::shallowCopyContainer(*primaryTracks);

  // Inform the tool about the systematic you want to use   
  if ( m_smearTool->applySystematicVariation(systSet) != CP::SystematicCode::Ok) {
   MsgLog::WARNING("TrackObject::smearTracks","You want to smear tracks, but the smearing tool can't apply systematic variation");
   return shallow_xAOD_copy.first;
  }

  // Now smear tracks in the copied container
  for ( xAOD::TrackParticle* trk : *(shallow_xAOD_copy.first) ) {
    if ( m_smearTool->applyCorrection(*trk) != CP::CorrectionCode::Ok ) {
      MsgLog::WARNING("TrackObject::smearTracks","You want to smear tracks, but the smearing tool has encountered an error");
      break; 
    }
  }

  return shallow_xAOD_copy.first;

}

// -------------------------------------------------------------------------- //
bool TrackObject::isHardScatVrtx(const xAOD::TruthVertex* pVert)
{

  if (pVert == nullptr) return false;

  const xAOD::TruthVertex* pV = pVert;
  int numOfPartIn = 0;
  int pdg = 0 ;
  int NumOfParton = 0;

  do {
    pVert = pV;
    numOfPartIn = pVert->nIncomingParticles();
    pdg = numOfPartIn != 0 && pVert->incomingParticle(0) != nullptr ? pVert->incomingParticle(0)->pdgId() : 0;
    pV = numOfPartIn != 0 && pVert->incomingParticle(0) != nullptr && pVert->incomingParticle(0)->hasProdVtx() ? pVert->incomingParticle(0)->prodVtx() : nullptr;

  } while (numOfPartIn == 1 && (abs(pdg) < 81 || abs(pdg) > 100) && pV != nullptr);


  if (numOfPartIn == 2) {
    for (unsigned int ipIn = 0; ipIn < pVert->nIncomingParticles(); ipIn++) {
      if (!pVert->incomingParticle(ipIn))
        continue;
      if (abs(pVert->incomingParticle(ipIn)->pdgId()) < 7 || pVert->incomingParticle(ipIn)->pdgId() == 21)
        NumOfParton++;
    }
    if (NumOfParton == 2)
      return true;
  }

  return false;

}

// -------------------------------------------------------------------------- //
float TrackObject::getPtErr(const xAOD::TrackParticle* track)
{

  //
  if( !track ) return 0.0;

  return TMath::Sqrt( xAOD::TrackingHelpers::pTErr2( track ) );

}
// -------------------------------------------------------------------------- //
float TrackObject::getD0(const xAOD::TrackParticle* track)
{

  return track->d0();

}
// -------------------------------------------------------------------------- //
float TrackObject::getD0Err(const xAOD::TrackParticle* track,const xAOD::EventInfo* eventInfo)
{

  /*
    Calculate the d0 uncertainty, taking into account the uncertainty on the beam spot
  */

  double sigma2_d0 = track->definingParametersCovMatrixVec().at(0);

  float sigma2_beamSpotd0 = xAOD::TrackingHelpers::d0UncertaintyBeamSpot2(track->phi(),
                                                                          eventInfo->beamPosSigmaX(),
                                                                          eventInfo->beamPosSigmaY(),
                                                                          eventInfo->beamPosSigmaXY() );

  float d0Err = TMath::Sqrt(sigma2_d0 + sigma2_beamSpotd0);
  return d0Err;

}
// -------------------------------------------------------------------------- //
float TrackObject::getZ0(const xAOD::TrackParticle* track,const xAOD::VertexContainer* vertex)
{

  // Get primary vertex
  const xAOD::Vertex* pV = 0;

  for( const auto& vx : *vertex ) {
    if(vx->vertexType() == xAOD::VxType::PriVtx){
      pV = vx;
      break;
    }
  }

  if( !pV ){
    std::cout << "<TrackObject::getZ0> WARNING Cannot get primary vertex (PV), z0 will not be extrapolated to the PV" << std::endl;
    return track->z0();
  }
  else{
    // Extrapolate back to PV
    double z0_exPV = track->z0() + track->vz() - pV->z();
    return z0_exPV;
  }

}
// -------------------------------------------------------------------------- //
float TrackObject::getZ0Err(const xAOD::TrackParticle* track)
{

  /*
    Index
      => d0 uncertainty 0
      => z0 uncertainty 2
      => qOverP uncertainty 14
  */

  std::vector<float> CovMatrixVec = track->definingParametersCovMatrixVec();

  const int idx = 2;

  if(CovMatrixVec.size()==15) return TMath::Sqrt(CovMatrixVec[idx]);
  else                        return (-100.0);

}
// -------------------------------------------------------------------------- //
uint8_t TrackObject::getNPixHitsPlusDeadSensors(const xAOD::TrackParticle* track)
{
  // Note that this works just as well for muons, despite being
  // provided in the ElectronPhotonSelectorTools package
  return ElectronSelectorHelpers::numberOfPixelHitsAndDeadSensors(track);
}
// -------------------------------------------------------------------------- //
bool TrackObject::getPassBL(const xAOD::TrackParticle* track)
{
  // Note that this works just as well for muons, despite being
  // provided in the ElectronPhotonSelectorTools package
  return ElectronSelectorHelpers::passBLayerRequirement(track);
}
// ------------------------------------------------------------------------- //
const TrackVector* TrackObject::getObj(TString sysName)
{

  std::map<TString,TrackVector*>::iterator it = m_trackVectorMap.find(sysName);

  if(it==m_trackVectorMap.end()){
    MsgLog::WARNING("TrackObject::getObj","WARNING Cannot get track vector for systematic %s",sysName.Data() );
    return NULL;
  }

  return it->second;

}
// ------------------------------------------------------------------------- //
const ObjectVector* TrackObject::getCaloObj(TString sysName)
{

  std::map<TString,ObjectVector*>::iterator it = m_caloVectorMap.find(sysName);

  if(it==m_caloVectorMap.end()){
    MsgLog::WARNING("TrackObject::getObj","WARNING Cannot get calo vector for systematic %s",sysName.Data() );
    return NULL;
  }

  return it->second;

}
// ------------------------------------------------------------------------- //
void TrackObject::Reset()
{

  std::map<TString,TrackVector*>::iterator it;
  for(it = m_trackVectorMap.begin(); it != m_trackVectorMap.end(); it++){
    for (TrackVector::iterator muItr = it->second->begin(); muItr != it->second->end(); muItr++) {
      delete *muItr;
    }
    it->second->clear();
  }

  std::map<TString,ObjectVector*>::iterator itC;
  for(itC = m_caloVectorMap.begin(); itC != m_caloVectorMap.end(); itC++){
    for (ObjectVector::iterator objItr = itC->second->begin(); objItr != itC->second->end(); objItr++) {
      delete *objItr;
    }
    itC->second->clear();
  }


}
