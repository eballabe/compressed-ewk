#include "SusySkimMaker/TruthEvent.h"

TruthEvent::TruthEvent()
{

}
// -------------------------------------------------------------------------------- //
TruthEvent::TruthEvent(const TruthEvent &rhs):
  ObjectVariable(rhs),
  TTbarTLVs(rhs.TTbarTLVs),
  TauTLVs(rhs.TauTLVs),
  decayMode(rhs.decayMode),
  Tau1decayMode(rhs.Tau1decayMode),
  Tau2decayMode(rhs.Tau2decayMode),
  truthMll(rhs.truthMll),
  polWeight(rhs.polWeight),
  nAntiKt4TruthJets20(rhs.nAntiKt4TruthJets20),
  Zdecays(rhs.Zdecays),
  Wdecays(rhs.Wdecays),
  nTruthTauFromW(rhs.nTruthTauFromW),
  TruthTauFromW_DMV(rhs.TruthTauFromW_DMV),
  truthFatjets(rhs.truthFatjets),
  truthJets(rhs.truthJets),
  truthJets_label(rhs.truthJets_label),
  partons(rhs.partons),
  truthZbosons(rhs.truthZbosons)
{
}
// --------------------o------------------------------------------------------------ //
TruthEvent& TruthEvent::operator=(const TruthEvent &rhs)
{
  if (this != &rhs) {
    ObjectVariable::operator=(rhs);
    TTbarTLVs = rhs.TTbarTLVs;
    TauTLVs = rhs.TauTLVs;
    decayMode = rhs.decayMode;
    Tau1decayMode = rhs.Tau1decayMode;
    Tau2decayMode = rhs.Tau2decayMode;
    truthMll = rhs.truthMll;
    polWeight = rhs.polWeight;
    nAntiKt4TruthJets20 = rhs.nAntiKt4TruthJets20;
    Zdecays  = rhs.Zdecays;
    Wdecays  = rhs.Wdecays;
    nTruthTauFromW  = rhs.nTruthTauFromW;
    TruthTauFromW_DMV  = rhs.TruthTauFromW_DMV;
    truthFatjets = rhs.truthFatjets;
    truthJets = rhs.truthJets;
    truthJets_label = rhs.truthJets_label;
    partons = rhs.partons;
    truthZbosons = rhs.truthZbosons;
  }
  return *this;
}
// -------------------------------------------------------------------------------- //
const TLorentzVector* TruthEvent::getTTbarTLV(const TString name)
{

  auto tlv = this->TTbarTLVs.find(name);

  if( tlv != this->TTbarTLVs.end() ) return tlv->second;
  else{
    std::cout << "<TruthEvent::getTTbarTLV> Could not find TLorentzVector for " << name << std::endl;
    return NULL;
  }

}
// -------------------------------------------------------------------------------- //
const TLorentzVector* TruthEvent::getTauTLV(const TString name)
{

  auto tlv = this->TauTLVs.find(name);

  if( tlv != this->TauTLVs.end() ) return tlv->second;
  else{
    std::cout << "<TruthEvent::getTauTLV> Could not find TLorentzVector for " << name << std::endl;
    return NULL;
  }

}
