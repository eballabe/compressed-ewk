#include "SusySkimMaker/TauVariable.h"


TauVariable::TauVariable()
{

  setDefault(nTrack,-1);
  setDefault(loose,false);
  setDefault(medium,false);
  setDefault(tight,false);

}
// -------------------------------------------------------------------------------- //
TauVariable::TauVariable(const TauVariable &rhs):
  LeptonVariable(rhs),
  nTrack(rhs.nTrack),
  loose(rhs.loose),
  medium(rhs.medium),
  tight(rhs.tight)
{
}
// -------------------------------------------------------------------------------- //
TauVariable& TauVariable::operator=(const TauVariable &rhs)
{

  if(this != &rhs){
    LeptonVariable::operator=(rhs);
    nTrack    = rhs.nTrack;
    loose     = rhs.loose;
    medium    = rhs.medium;
    tight     = rhs.tight;
  }

  return *this;

}
