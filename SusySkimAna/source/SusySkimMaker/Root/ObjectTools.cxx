#include "SusySkimMaker/ObjectTools.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/Timer.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/Systematics.h"


//
ObjectTools::ObjectTools() : m_toolsInit(false),
                 m_eventObject(0),
                 m_metObject(0),
                 m_muonObject(0),
                 m_electronObject(0),
                 m_jetObject(0),
                 m_fatjetObject(0),
                 m_trackjetObject(0),
                 m_tauObject(0),
                 m_photonObject(0),
			           m_trackObject(0),
                 m_v0Object(0), // JEFF
                 m_VSIObject(0), // SICONG
	      		     m_truthObject(0),
	      		     m_trigTool(0),
	      		     m_lumi(1.0),
	      		     m_isData(false),
	      		     m_truthonly(false),
	      		     m_dsid(0),
	      		     m_excludeAdditionalLeptons(false),
	      		     m_filterBaseline(true),
	      		     m_filterOR(false),
	      		     m_doSampleOR(true),
	      		     m_doSampleOR_ZjetZgam(false),
	      		     m_doSampleOR_WplvWmqq(false),
	      		     m_requireTrigData(false),
	      		     m_requireGRLData(false),
	      		     m_nLepSkimFilter(0),

                 m_nPhotonSkimFilter(0),
                 m_doTriggerSFs(true),
                 m_usePhotonsInMET(false),
                 m_useTausInMET(false),
                 m_useNearbyLepIsoCorr(false),
                 m_doBTagging(true),
                 m_saveGlobalDileptonTrigSFs(true),
                 m_saveGlobalPhTrigSF(true),
                 m_singleEleTrigSFChains("SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"),
                 m_singleMuTrigSFChains2015("HLT_mu20_iloose_L1MU15_OR_HLT_mu40"),
                 m_singleMuTrigSFChains201618("HLT_mu26_ivarmedium_OR_HLT_mu50"),
                 //m_nearbyLepType("baseline"),
                 m_evtProperties(0),
                 //m_tTbarNNLOTool(0),
                 //m_nearbyLepIsoCorr(0),
                 m_sysState("")
{

  m_sherpaOverlapEvents.clear();
}
// -------------------------------------------------------------------- //
void ObjectTools::createTools()
{

  // Alreadty done
  if( m_toolsInit ) return;

  // Create all default tools this classes needs
  m_trigTool       = new TriggerTools(true);
  m_eventObject    = new EventObject();
  m_metObject      = new MetObject();
  m_muonObject     = new MuonObject();
  m_photonObject   = new PhotonObject();
  m_electronObject = new ElectronObject();
  m_jetObject      = new JetObject();
  m_tauObject      = new TauObject();

  // Class is ready to use all tools
  m_toolsInit = true;

}
// -------------------------------------------------------------------- //
StatusCode ObjectTools::init(xAOD::TEvent* event,TreeMaker*& treeMaker,MetaData*& metaData,
                 float nEvents,bool isData, bool isAf2, bool truthOnly, int dsid,
                 bool doFatjet, bool doTrackjet)
{

  const char* APP_NAME = "ObjectTools";

  m_truthonly = truthOnly;
  m_isData    = isData;
  m_dsid      = dsid;

  // If user didnt call
  if( !m_toolsInit ) createTools();

  // Trigger tools. Load maps
  if( !m_truthonly ){
    CHECK( m_trigTool->initialize(event) );
  }
  else{
    Info("ObjectTools::init>", "Running in truth-only mode, not initialising the trigger tools!" );
  }

  //
  CentralDB::retrieve(CentralDBFields::EXCLUDEADDLEP,m_excludeAdditionalLeptons);
  CentralDB::retrieve(CentralDBFields::FILTERBASELINE,m_filterBaseline);
  CentralDB::retrieve(CentralDBFields::FILTEROR,m_filterOR);
  CentralDB::retrieve(CentralDBFields::SKIMNLEP,m_nLepSkimFilter);
  CentralDB::retrieve(CentralDBFields::SKIMNPHOTON,m_nPhotonSkimFilter);
  CentralDB::retrieve(CentralDBFields::DOTRIGSF,m_doTriggerSFs);
  CentralDB::retrieve(CentralDBFields::DOSAMPLEOR,m_doSampleOR);
  CentralDB::retrieve(CentralDBFields::DOSAMPLEORZJETZGAM,m_doSampleOR_ZjetZgam);
  CentralDB::retrieve(CentralDBFields::DOSAMPLEORWPLVWMQQ,m_doSampleOR_WplvWmqq);
  CentralDB::retrieve(CentralDBFields::USEPHOTONSINMET,m_usePhotonsInMET);
  CentralDB::retrieve(CentralDBFields::USETAUSINMET,m_useTausInMET);
  CentralDB::retrieve(CentralDBFields::REQTRIGDATA,m_requireTrigData);
  CentralDB::retrieve(CentralDBFields::REQGRLDATA,m_requireGRLData);
  CentralDB::retrieve(CentralDBFields::USENEARBYLEPISOCORR,m_useNearbyLepIsoCorr);
  CentralDB::retrieve(CentralDBFields::DOBTAGGING,m_doBTagging);
  CentralDB::retrieve(CentralDBFields::SAVEGLOBALDILEPTONTRIGSFS,m_saveGlobalDileptonTrigSFs);
  CentralDB::retrieve(CentralDBFields::SAVEGLOBALPHOTONTRIGSF,m_saveGlobalPhTrigSF);
  CentralDB::retrieve(CentralDBFields::SINGLEELETRIGSFCHAINS,m_singleEleTrigSFChains);
  CentralDB::retrieve(CentralDBFields::SINGLEMUTRIGSFCHAINS2015,m_singleMuTrigSFChains2015);
  CentralDB::retrieve(CentralDBFields::SINGLEMUTRIGSFCHAINS201618,m_singleMuTrigSFChains201618);

  // Event tools, load GRL, PRW, cross sections
  CHECK( m_eventObject->init(treeMaker,nEvents,isData,isAf2) );
  CHECK( m_metObject->init(treeMaker) );
  CHECK( m_muonObject->init(treeMaker, isData) );
  CHECK( m_photonObject->init(treeMaker) );
  CHECK( m_electronObject->init(treeMaker, isData) );
  CHECK( m_jetObject->init(treeMaker, isData) );
  CHECK( m_tauObject->init(treeMaker) );

  // Truth info
  if( !isData ){
    m_truthObject = new TruthObject();
    CHECK( m_truthObject->init(treeMaker, m_dsid) );
  }

  // Toggles TrackObject container to be written out
  if( CentralDB::retrieve(CentralDBFields::SAVETRACKS ) ){
    m_trackObject = new TrackObject();
    CHECK( m_trackObject->init(treeMaker,event) );
    
    m_v0Object = new V0Object(); // JEFF
    CHECK( m_v0Object->init(treeMaker,event) );
    
    m_VSIObject = new VSIObject(); // SICONG
    CHECK( m_VSIObject->init(treeMaker,event) );

    // Save links
    if( CentralDB::retrieve(CentralDBFields::SAVETRACKLINKS) ){
      m_electronObject->setTrackObject(m_trackObject);
      m_muonObject->setTrackObject(m_trackObject);
      m_photonObject->setTrackObject(m_trackObject);
    }

  }

  // Toggles fat jet JetObject container to be written out
  Info("ObjectTools::init", "doFatjet: %i", doFatjet);
  if( doFatjet ) {
    m_fatjetObject   = new JetObject();
    CHECK( m_fatjetObject->init(treeMaker,isData,"fatjets") );
  }

  // Toggles track jet JetObject container to be written out
  Info("ObjectTools::init", "doTrackjet: %i", doTrackjet);
  if( doTrackjet ) {
    m_trackjetObject = new JetObject();
    CHECK( m_trackjetObject->init(treeMaker,isData,"trackjets") );
  }

  //
  m_evtProperties = new EventProperties();
  m_evtProperties->Reset();

  // (obsolete! migrated to CP::IsolationCloseByCorrectionTool in the ST)
  //if( m_useNearbyLepIsoCorr && !m_truthonly ){
  //  CHECK( init_nearbyLepIsoCorr() );
  //}

  //
  Info("ObjectTools","All object tools initialized successfully!");

  // Keep around for now
  (void)metaData;

  // Return gracefully
  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------- //
void ObjectTools::setSysState(std::string sysState)
{

  // Store globally
  m_sysState = sysState;

  // TODO : Now each tool


}
// -------------------------------------------------------------------- //
void ObjectTools::setSystematicSet(const CP::SystematicSet sysSet)
{

  m_muonObject->setSystematicSet(sysSet);

  //std::cout << "Setting systematic state: " << sysSet.name() << std::endl;


}
// -------------------------------------------------------------------- //
//                          XAOD METHODS
// -------------------------------------------------------------------- //
bool ObjectTools::get_xAOD_event(ST::SUSYObjDef_xAOD& susyObj,
                                 const xAOD::EventInfo* eventInfo,
                                 const xAOD::TruthEventContainer* truthEvents,
                                 const xAOD::VertexContainer* primVertex,
                                 const xAOD::JetContainer* jets,
                                 const xAOD::MuonContainer* muons,
                                 const xAOD::ElectronContainer* electrons,
                                 const xAOD::PhotonContainer* photons)
{
  // Filtering, if configured by the user
  if( !passObjectFilter() ) return false;

  // Get SUSY final state code
  int pdgid1=0;
  int pdgid2=0;

  if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
  // Retrieve SUSY final state particle IDs from decorators set at derivation level if available
    if (cacc_SUSYPID1.isAvailable(*eventInfo) && cacc_SUSYPID2.isAvailable(*eventInfo)){
      pdgid1 = cacc_SUSYPID1(*eventInfo);
      pdgid1 = cacc_SUSYPID2(*eventInfo);
    }
    // Deactivate for now as ST complains about an empty TruthBSM container for all SM samples ...
    // else if( susyObj.FindSusyHP(pdgid1,pdgid2) != StatusCode::SUCCESS ){}
  }

  // Merge the emulated trigger decisions and the ones from the TDT
  // If triggers exist in both maps, the ones from the emulation are taken
  // TODO: This needs to go into the trigger tools, since the map stored in MetTriggers
  // is not consistent with this...!
  std::map<TString, bool> HLTEvtDec = this->getEmulatedTriggerMap(susyObj);
  std::map<TString, bool> HLTEvtDecTDT = m_trigTool->getHLTEvtDec();
  HLTEvtDec.insert(HLTEvtDecTDT.begin(), HLTEvtDecTDT.end());

  float met_L1 = m_trigTool->get_met_L1();
  float met_HLT_pufit = m_trigTool->get_met_HLT_pufit();
  float met_HLT_cell = m_trigTool->get_met_HLT_cell();
  float leadJetPt_L1 = m_trigTool->get_leadJetPt_L1();
  float leadJetPt_HLT = m_trigTool->get_leadJetPt_HLT();

  // Fill event trees
  m_eventObject->fillEventContainer(eventInfo,
                                    jets,
                                    muons,
                                    truthEvents,
                                    primVertex,
                                    photons,
                                    m_trigTool->getL1EvtDec(),
                                    HLTEvtDec,
                                    m_trigTool->getTriggerPrescale(),
                                    met_L1,
                                    met_HLT_pufit,
                                    met_HLT_cell,
                                    leadJetPt_L1,
                                    leadJetPt_HLT,
                                    doSherpaVjetsNjetsWeight(eventInfo,susyObj),
				    m_globalDiLepTrigSF,
				    m_globalMultiLepTrigSF,
                                    m_globalPhotonTrigSF,
                                    pdgid1,pdgid2,
                                    m_sysState);
 // Return gracefully
  return true;

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_met(ST::SUSYObjDef_xAOD& susyObj,
                   xAOD::MissingETContainer* met,
                   xAOD::MissingETContainer* metTrack,
                   xAOD::MissingETContainer* met_leptons_invis,
                   xAOD::MissingETContainer* met_muons_invis,
                   xAOD::MissingETContainer* met_electrons_invis,
                   xAOD::MissingETContainer* met_photons_invis,
                   const xAOD::ElectronContainer* elec,
                   const xAOD::TauJetContainer* taujet,
                   const xAOD::MuonContainer* muon,
                   const xAOD::JetContainer* jet,
                   const xAOD::PhotonContainer* photon,
                   const xAOD::MissingETContainer* truthMET,
                   MetVariable::MetFlavor flavor)
{

  // Start!
  Timer::Instance()->Start( "ObjectTools::get_xAOD_met" );

  /*
    Common configurations for all MET rebuilding
  */
  if( !m_usePhotonsInMET ) photon = 0;
  if( !m_useTausInMET    ) taujet = 0;

  /*
     Triggers. These will go into all containers unfortunately
  */
  const auto& trigMap = m_trigTool->matchMetTriggers();

  /*
     Standard MET Reconstruction
  */
  if( met ){
    if( susyObj.GetMET( *met,jet,elec,muon,photon,taujet,true,true ) == StatusCode::SUCCESS ){

      // Calculate MET significance (only available from Rel 21.2.16)
      double metSignif=-1.;
      susyObj.GetMETSig(*met,metSignif,true,true);

      // Fill container
      m_metObject->fillMetContainer(met,truthMET,trigMap,metSignif,flavor,m_sysState);

    }
    else{
      MsgLog::WARNING("ObjectTools::get_xAOD_met","Problem with DEFAULT MET rebuilding! MET containers will not be filled!");
    }
  }

  /*
    Track MET
  */

  if( metTrack ){
    if( susyObj.GetTrackMET( *metTrack,jet,elec,muon ) == StatusCode::SUCCESS ) {

      // update MET flavour with TRACK for different WPs
      if(flavor == MetVariable::MetFlavor::LOOSE)flavor = MetVariable::MetFlavor::TRACKLOOSE;
      else if(flavor == MetVariable::MetFlavor::TIGHTER)flavor = MetVariable::MetFlavor::TRACKTIGHTER;
      else if(flavor == MetVariable::MetFlavor::TENACIOUS)flavor = MetVariable::MetFlavor::TRACKTENACIOUS;
      else flavor = MetVariable::MetFlavor::TRACK;

      // Fill container
      m_metObject->fillMetContainer(metTrack,truthMET,trigMap,0,flavor,m_sysState);

    }
    else{
      MsgLog::WARNING("ObjectTools::get_xAOD_met","Problem with TRACK MET rebuilding! MET containers will not be filled!");
    }
  }

  /*
    Invisble leptons
  */

  if( met_leptons_invis ){

    // Add all leptons to invisible particles
    ConstDataVector<xAOD::IParticleContainer> invis = getInvisibleParticlesForMET(muon,elec,0);

    if( susyObj.GetMET( *met_leptons_invis,jet,elec,muon,photon,taujet,true,true,invis.asDataVector()) == StatusCode::SUCCESS ){

      // Fill container
      m_metObject->fillMetContainer(met_leptons_invis,truthMET,trigMap,0,MetVariable::MetFlavor::INVISIBLE_LEP,m_sysState);

    }
    else{
      Warning("ObjectTools::get_xAOD_met","Problem with INVISIBLE_LEP MET rebuilding! MET containers will not be filled!");
    }
  }

  /*
    Only invisible muons
  */
  if( met_muons_invis ){

    // Only muons as invisible particles
    ConstDataVector<xAOD::IParticleContainer> invis_muons = getInvisibleParticlesForMET(muon,0,0);

    if( susyObj.GetMET( *met_muons_invis,jet,elec,muon,photon,taujet,true,true,invis_muons.asDataVector()) == StatusCode::SUCCESS ){

      // Fill container
      m_metObject->fillMetContainer(met_muons_invis,truthMET,trigMap,0,MetVariable::MetFlavor::INVISIBLE_MUON,m_sysState);

    }
    else{
      Warning("ObjectTools::get_xAOD_met","Problem with INVISIBLE_MUON MET rebuilding! MET containers will not be filled!");
    }
  }

  /*
    Only invisible electrons
  */
  if( met_electrons_invis ){

    // Only muons as invisible particles
    ConstDataVector<xAOD::IParticleContainer> invis_electrons = getInvisibleParticlesForMET(0,elec,0);

    if( susyObj.GetMET( *met_electrons_invis,jet,elec,muon,photon,taujet,true,true,invis_electrons.asDataVector()) == StatusCode::SUCCESS ){

      // Fill container
      m_metObject->fillMetContainer(met_electrons_invis,truthMET,trigMap,0,MetVariable::MetFlavor::INVISIBLE_ELECTRON,m_sysState);

    }
    else{
      Warning("ObjectTools::get_xAOD_met","Problem with INVISIBLE_ELECTRON MET rebuilding! MET containers will not be filled!");
    }
  }

  /*
    Only invisible photons
  */
  if( met_photons_invis ){

    // Only muons as invisible particles
    ConstDataVector<xAOD::IParticleContainer> invis_photons = getInvisibleParticlesForMET(0,0,photon);

    if( susyObj.GetMET( *met_photons_invis,jet,elec,muon,photon,taujet,true,true,invis_photons.asDataVector()) == StatusCode::SUCCESS ){

      // Fill container
      m_metObject->fillMetContainer(met_photons_invis,truthMET,trigMap,0,MetVariable::MetFlavor::INVISIBLE_PHOTON,m_sysState);

    }
    else{
      Warning("ObjectTools::get_xAOD_met","Problem with INVISIBLE_PHOTON MET rebuilding! MET containers will not be filled!");
    }
  }


  // Finished!
  Timer::Instance()->End( "ObjectTools::get_xAOD_met" );

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_muons(xAOD::MuonContainer* muons,
				 const xAOD::VertexContainer* vertices,
				 const xAOD::EventInfo* eventInfo)
{

  //
  for( const auto& mu : *muons ) {

    // Only fill output containers with objects passing baseline selection
    if( !passBaselineSelection(mu) ) continue;

    // Reconstruction efficiency scale-factors
    const auto& recoWeights = getWeightsFromMap( m_muonSFs,getIdx(mu) );

    // Trig decision
    const auto& trigMap = m_trigTool->matchLeptonTriggers(mu);

    // Trigger efficienies
    auto trigEff = getWeightsFromMap( m_muonTrigEffs,getIdx(mu) );

    // Fill custom container
    m_muonObject->fillMuonContainer(mu,vertices,eventInfo,trigMap,recoWeights,trigEff,m_sysState);

  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_electrons(xAOD::ElectronContainer* electrons,
                       const xAOD::VertexContainer* vertices,
                                           const xAOD::EventInfo* eventInfo)
{

  for( const auto& ele : *electrons ) {

    // Only fill output containers with objects passing baseline selection
    if( !passBaselineSelection(ele) ) continue;

    // Reconstruction efficiency scale-factors
    const auto& recoWeights = getWeightsFromMap(m_electronSFs,getIdx(ele) );

    // Trigger matching
    const auto& trigMap = m_trigTool->matchLeptonTriggers(ele);

    // Trigger efficienies
    auto trigEff = getWeightsFromMap(m_eleTrigEffs,getIdx(ele) );

    // Fill custom skims
    m_electronObject->fillElectronContainer(ele,vertices,eventInfo,trigMap,recoWeights,trigEff,m_sysState);

  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_muons_NearbyLepIsoCorr(xAOD::MuonContainer* muons)
{

  if( !m_useNearbyLepIsoCorr ) return;

  // Keep track of the indices corresponding to the MuonVector that the
  // skim will ultimately store
  unsigned int index_baseline_mu = 0;

  for( const auto& mu : *muons ) {

    // Only fill output containers with objects passing baseline selection
    if( !passBaselineSelection(mu) ) continue;

    // Fill custom container with NearbyLepIsoCorr vars
    m_muonObject->fillMuonContainer_NearbyLepIsoCorr(mu,index_baseline_mu,m_sysState);
    ++index_baseline_mu;
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_electrons_NearbyLepIsoCorr(xAOD::ElectronContainer* electrons)
{

  if( !m_useNearbyLepIsoCorr ) return;

  // Keep track of the indices corresponding to the ElectronVector that the
  // skim will ultimately store
  unsigned int index_baseline_ele = 0;

  for( const auto& ele : *electrons ) {

    // Only fill output containers with objects passing baseline selection
    if( !passBaselineSelection(ele) ) continue;

    // Fill custom container with NearbyLepIsoCorr vars
    m_electronObject->fillElectronContainer_NearbyLepIsoCorr(ele,index_baseline_ele,m_sysState);
    ++index_baseline_ele;
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_taus(xAOD::TauJetContainer* taus,
                      const xAOD::VertexContainer* vertices)
{

  //
  if( !taus ) return;

  for( const auto& tau : *taus ) {

    // Only fill output containers with objects passing baseline selection
    if( !passBaselineSelection(tau) ) continue;

    // Reconstruction scale factors
    const auto& recoWeights = getWeightsFromMap(m_tauSFs,getIdx(tau) );

    // Fill tau container
    m_tauObject->fillTauContainer(tau,vertices,m_sysState,recoWeights);

  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_jets(xAOD::JetContainer* jets,
                xAOD::JetContainer* fatjets,
                xAOD::JetContainer* trackjets,
                const xAOD::VertexContainer* vertices)
{

  // Standard jets
  for( const auto& jet : *jets ) {

    // Only fill output containers with objects passing baseline selection
    if( !passBaselineSelection(jet) ) continue;

    // B-tagging and JVT SF weights
    const auto& FTWeights = getWeightsFromMap(m_jetSFs, getIdx(jet) );
    const auto& JVTWeights = getWeightsFromMap(m_jetJvtSFs,getIdx(jet) );

    // Trigger decision for Jet triggers
    const auto& trigMap = m_trigTool->matchJetTriggers(jet);

    // Fill JetVariable container
    m_jetObject->fillJetContainer(jet,vertices,m_sysState,FTWeights,JVTWeights,trigMap);

  }

  // Fat jets
  if( fatjets ){
    if( m_fatjetObject ){
      for( const auto& fatjet : *fatjets )  m_fatjetObject->fillFatjetContainer(fatjet,trackjets,vertices,m_sysState);
    }else{
      std::cout << "ObjectTools::get_xAOD_jets  ERROR  fatjetObject is not initialized! Skip." << std::endl;
    }
  }

  // Track jets
  if( trackjets ){
    if( m_trackjetObject ){
      for( const auto& trackjet : *trackjets ){
	const auto& FTWeights = getWeightsFromMap(m_trackjetSFs, getIdx(trackjet) );
	m_trackjetObject->fillTrackjetContainer(trackjet,m_sysState,FTWeights);
      }
    }else{
      std::cout << "ObjectTools::get_xAOD_jets  ERROR  fatjetObject is not initialized! Skip." << std::endl;
    }
  }


}
// --------------------------------------------------------------------------------------------- //
StatusCode ObjectTools::get_xAOD_photons(xAOD::PhotonContainer* photons,
                                         const xAOD::VertexContainer* vertices,
                                         const xAOD::EventInfo* eventInfo)
{

  const char* APP_NAME = "ObjectTools";

  for( const auto& photon : *photons ) {

    // Filter baseline photons from SUSYTools
    if( !passBaselineSelection(photon) ) continue;

    // Trigger decision for Photon triggers
    const auto& trigMap = m_trigTool->matchPhotonTriggers(photon);

    // Reconstruction SFs
    const auto& recoWeights = getWeightsFromMap(m_photonSFs,getIdx(photon) );

    // Trigger efficienies
    auto trigEff = getWeightsFromMap( m_photonTrigEffs,getIdx(photon) );

    // Fill outout container
    CHECK( m_photonObject->fillPhotonContainer(photon,vertices,eventInfo,recoWeights,trigMap,trigEff,m_sysState) );

  }

  // Return gracefully
  return StatusCode::SUCCESS;

}
// --------------------------------------------------------------------------------------------- //
bool ObjectTools::passBaselineSelection(const xAOD::IParticle* p)
{

  // Baseline skimming for all objects
  if( m_filterBaseline && !cacc_baseline(*p)  ) return false;

  // OR skimming for all objects by Taus
  if ( p->type() != xAOD::Type::Tau && cacc_passOR.isAvailable(*p) ){
    if( m_filterOR && !cacc_passOR(*p)  ) return false;
  }

  // Keep track of number of leptons in the event
  if ( p->type() == xAOD::Type::Muon || p->type() == xAOD::Type::Electron ){
    if( cacc_baseline(*p) ) m_evtProperties->nBaseLeptons++;
    if( cacc_signal(*p)   ) m_evtProperties->nSignalLeptons++;
  }

  // Keep track of the number of photons in the event
  if ( p->type() == xAOD::Type::Photon ){
    if( cacc_baseline(*p) ) m_evtProperties->nBasePhotons++;
    if( cacc_signal(*p)   ) m_evtProperties->nSignalPhotons++;
  }

  // Desired object!
  return true;

}
// --------------------------------------------------------------------------------------------- //
bool ObjectTools::passObjectFilter() const
{

  // Skim on the number of leptons, if configured
  if( m_nLepSkimFilter>0 ){
    if( m_evtProperties->nBaseLeptons < m_nLepSkimFilter ){
      return false;
    }
  }

  // Skim on the number of photons, if configured
  if( m_nPhotonSkimFilter>0 ){
    if( m_evtProperties->nBasePhotons < m_nPhotonSkimFilter ){
      return false;
    }
  }

  // Desired event!
  return true;

}
// -------------------------------------------------------------------- //
//                        TRUTH XAOD METHODS
// -------------------------------------------------------------------- //
bool ObjectTools::get_xAOD_event_truth(const xAOD::EventInfo* eventInfo,
                                       const xAOD::TruthParticleContainer* truthParticles,
                                       const xAOD::TruthEventContainer* truthEvents)
{

  // Fill event trees
  m_eventObject->fillEventContainerTruth(eventInfo,truthParticles,truthEvents,m_sysState);

  return true;

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_met_truth(xAOD::MissingETContainer* met)
{

  m_metObject->fillMetContainerTruth(met,m_sysState);

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_muons_truth(xAOD::TruthParticleContainer* muons,
                       const float ptcut,const float etacut)
{
  for (xAOD::TruthParticle* muo : *muons) {

    // baseline selection
    if (muo->pt() < ptcut) continue;
    if (fabs(muo->eta()) >= etacut) continue;

    // Truth selection
    if (fabs(muo->pdgId()) != 13) continue;
    // TODO: final state status code 1 for all generators?
    if (muo->status() != 1) continue;

    // Fill custom skims
    m_muonObject->fillMuonContainerTruth(muo,m_sysState);
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_photons_truth(xAOD::TruthParticleContainer* photons,
                                       const float ptcut,const float etacut)
{

  for (xAOD::TruthParticle* photon : *photons) {

    // Baseline selection
    if (photon->pt() < ptcut             ) continue;
    if (fabs(photon->eta()) >= etacut    ) continue;

    // Truth selection
    if (fabs(photon->pdgId()) != 22      ) continue;

    // Only photons from the Higgs!
    //if( photon->auxdata<unsigned int>("classifierParticleOrigin") != 14 ) continue;

    // Fill!
    m_photonObject->fillPhotonContainerTruth(photon,m_sysState);

  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_electrons_truth(xAOD::TruthParticleContainer* electrons,
                       const float ptcut,const float etacut)
{
  for (xAOD::TruthParticle* ele : *electrons) {

    // baseline selection
    if (ele->pt() < ptcut) continue;
    if (fabs(ele->eta()) >= etacut) continue;

    // Truth selection
    if (fabs(ele->pdgId()) != 11) continue;
    // TODO: final state status code 1 for all generators?
    if (ele->status() != 1) continue;

    // Fill custom skims
    m_electronObject->fillElectronContainerTruth(ele,m_sysState);
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_jets_truth(xAOD::JetContainer* jets,
                      const float ptcut,const float etacut)
{

  for( const auto& jet : *jets ) {

    // // Don't consider jets failing overlap
    // if( !jet->auxdata< bool >("passOR" ) ) continue;

    // baseline selection
    if (jet->pt() < ptcut) continue;
    if (fabs(jet->eta()) >= etacut) continue;

    m_jetObject->fillJetContainerTruth(jet,m_sysState);

  }

}
// -------------------------------------------------------------------- //
void ObjectTools::get_xAOD_fatjets_truth(xAOD::JetContainer* fatjets,
                     const float ptcut,const float etacut)
{

  if(!m_fatjetObject){
    std::cout << "ObjectTools::get_xAOD_fatjets_truth  ERROR  fatjetObject is not initialized! Exit." << std::endl;
    return;
  }

  for( const auto& fatjet : *fatjets ) {

    // baseline selection
    if (fatjet->pt() < ptcut) continue;
    if (fabs(fatjet->eta()) >= etacut) continue;

    m_fatjetObject->fillFatjetContainerTruth(fatjet,m_sysState);

  }

}
// -------------------------------------------------------------------- //
void ObjectTools::doWeights(ST::SUSYObjDef_xAOD& susyObj,
			    xAOD::MuonContainer* muons,
			    xAOD::ElectronContainer* electrons,
			    xAOD::TauJetContainer* taus,
			    xAOD::JetContainer* jets,
			    xAOD::JetContainer* trackjets,
			    xAOD::PhotonContainer* photons,
			    std::vector<ST::SystInfo> sysWeights)
{

  Timer::Instance()->Start( "ObjectTools::doWeights" );

  // Ensure we are only doing all weight systematics for the nominal systematic set
  if( m_sysState != "" ) sysWeights.clear();

  // Add in nominal set
  ST::SystInfo infodef;
  CP::SystematicSet defaultSet;

  infodef.systset           = defaultSet;
  infodef.affectsKinematics = false;
  infodef.affectsWeights    = false;
  infodef.affectsType       = ST::SystObjType::Unknown;

  sysWeights.push_back(infodef);

  for(const auto& wSys : sysWeights){

    // Key for value
    TString sys = wSys.systset.name();

    // Configure tools to process systematics (except nominal, we are already configured for the default set)
    if( sys != ""){
      if(susyObj.applySystematicVariation(wSys.systset) != CP::SystematicCode::Ok ){
        MsgLog::ERROR("ObjectTools::doWeights","Cannot configure SUSYTools for systematic variation %s!",sys.Data());
        abort();
      }
    }

    // ========================================================== //
    //                Muon weight systematics
    // ========================================================== //

    if(sys=="" || wSys.affectsType == ST::SystObjType::Muon){

      // TODO: fix hack in robust way (get tool?)
      bool affectsTrigger = ( sys=="" ||  sys.Contains("MUON_EFF_Trig") );
      bool affectsReco    = ( sys=="" ||  !sys.Contains("MUON_EFF_Trig") );

      // Retrieve and store:
      //  -- Reconstruction Data/MC scale factors
      //  -- Efficiencies for all requested SF chains in data and MC
      for( const auto& mu : *muons ){

    // Only consider baseline muons
    if( !passBaselineSelection(mu) ) continue;

    // Reconstruction scale factors
    if( affectsReco ){

          // SFs only for signal muons
	  float SF = cacc_signal(*mu) ? susyObj.GetSignalMuonSF(*mu) : 1.0;

	  // Add this SF to global map
	  addWeightToMap( m_muonSFs,getIdx(mu),sys,SF );

        } // Affects reco

	// Trigger scale factors
	// Keep this method wrapped under m_doTriggerSFs
	if( affectsTrigger && m_doTriggerSFs ){
	  TString SFChains = susyObj.treatAsYear()==2015 ? m_singleMuTrigSFChains2015 :
	    m_singleMuTrigSFChains201618;
	  float mc_eff   = susyObj.GetMuonTriggerEfficiency(*mu,SFChains.Data(),false);
	  float data_eff = susyObj.GetMuonTriggerEfficiency(*mu,SFChains.Data(),true);
	  float sf = mc_eff==0. ? 1. : data_eff/mc_eff;

	  // Instead of relying on applying the pT thresholds ourselves
	  // Don't save any efficiencies that don't have pT>pT_threshold*1.05
	  // since they are not considered in the SF calculation
	  if( mc_eff>0.0 || data_eff>0.0 )
	    addMCCorrection(m_muonTrigEffs,getIdx(mu),"singleMuonTriggerEff",sys,sf,data_eff,mc_eff);
	} // Affects trigger
      } // Loop over muons
    } // Affecting muons

    // ========================================================== //
    //                Electron weight systematics
    // ========================================================== //

    if(sys=="" || wSys.affectsType == ST::SystObjType::Electron){

     bool affectsTrigger = ( sys=="" ||  sys.Contains("EL_EFF_Trigger") );
     bool affectsReco    = ( sys=="" ||  !sys.Contains("EL_EFF_Trigger") );

     for( const auto& ele : *electrons ){

       // Only consider baseline electrons
       if( !passBaselineSelection(ele) ) continue;

       if( affectsReco ){
	 // Get SF, no trigger SF
	 float SF = cacc_signal(*ele) ? susyObj.GetSignalElecSF(*ele,true,true,false,true) : 1.0;

	 // Add this SF to global map
	 addWeightToMap( m_electronSFs,getIdx(ele),sys,SF );
       }

       // Loop over all requested trigger chains
       // Egamma tool only gives us MC efficiency, multiple by SF to get the data eff
       // This will likely overcount some systematics
       if( affectsTrigger && m_doTriggerSFs ){

	 float mc_eff    = susyObj.GetEleTriggerEfficiency(*ele,m_singleEleTrigSFChains.Data());
	 float data_eff  = mc_eff * susyObj.GetEleTriggerEfficiencySF(*ele,m_singleEleTrigSFChains.Data());
	 float sf = mc_eff==0. ? 1. : data_eff/mc_eff;

	 // Don't save null results
	 if( mc_eff>0.0 || data_eff>0.0 )
	   addMCCorrection(m_eleTrigEffs,getIdx(ele),"singleElectronTriggerEff",sys,sf,data_eff,mc_eff);

       } // Affects trigger
      } // Loop over electrons
    } // Affecting electrons

    // ========================================================== //
    //                Tau weight systematics
    // ========================================================== //
    if( taus && (sys == "" || wSys.affectsType == ST::SystObjType::Tau) ){

      for( const auto& tau : *taus ){

    // Get SF
    float SF = 1.0; // susyObj.GetSignalTauSF(*tau);

    // Add this SF to global map
    addWeightToMap( m_tauSFs,getIdx(tau),sys,SF );

      } // Loop over taus

    } // Affecting taus

    // ========================================================== //
    //              Jet btag/JVT weight systematics
    // ========================================================== //

    if(sys == "" || wSys.affectsType==ST::SystObjType::BTag || wSys.affectsType==ST::SystObjType::Jet){

      // Do b-tagging weights and decorate the jet with individual SFs
      if( m_doBTagging ){
        susyObj.BtagSF(jets);
        if(trackjets) susyObj.BtagSF_trkJet(trackjets);
      }

      // JVT scale-factors
      // Note we only rely on the decoration
      susyObj.JVT_SF(jets);

      for( const auto& jet : *jets ){

    // Get jet-by-jet SFs
    float SF_btagging = cacc_effscalefact(*jet);

        // Only consider jets which SUSYTools runs through the JvtEff tool
        float SF_jvt = 1.0;
        if( ST::acc_signal_less_JVT(*jet) && cacc_passOR(*jet) ){
          SF_jvt = cacc_jvtscalefact.isAvailable(*jet) ? cacc_jvtscalefact(*jet) : 1.0;
        }

    // Add SFs to global maps
    addWeightToMap( m_jetSFs,getIdx(jet),sys,SF_btagging );
        addWeightToMap( m_jetJvtSFs,getIdx(jet),sys,SF_jvt );

      } // Loop over jets

      // Track jet btag SFs
      if(trackjets){
	for( const auto& trackjet : *trackjets ){
	  // Get jet-by-jet SFs
	  float SF_btagging = cacc_effscalefact(*trackjet);
	  // Add SFs to global maps
	  addWeightToMap( m_trackjetSFs,getIdx(trackjet),sys,SF_btagging );
	} // Loop over track jets
      }

    } // Affecting jet systematics

    // ========================================================== //
    //             Photon weight systematics
    // ========================================================== //

    if( photons && (sys == "" || wSys.affectsType==ST::SystObjType::Photon) ){

      bool affectsTrigger = ( sys=="" ||  sys.Contains("PH_EFF_TRIGGER") );
      bool affectsReco    = ( sys=="" ||  !sys.Contains("PH_EFF_TRIGGER") );

      // Reconstruction,isolation weights
      for( const auto& photon : *photons ){

	// Only consider signal photons
	if( !passBaselineSelection(photon) ) continue;

	if( affectsReco ){
	  double sf_weight = cacc_signal(*photon) ? susyObj.GetSignalPhotonSF(*photon,true,true,false) : 1.0;
	  addWeightToMap(m_photonSFs,getIdx(photon),sys,sf_weight );
	}

	// Trigger scale factors
	// Keep this method wrapped under m_doTriggerSFs
	if( affectsTrigger && m_doTriggerSFs ){
	  float sf   = photon->pt()/1000.<100. ? susyObj.GetSignalPhotonSF(*photon,false,false,true) : 1.;  // No SF is supported above 100GeV.
	  addMCCorrection(m_photonTrigEffs,getIdx(photon),"singlePhotonTriggerEff",sys,sf,sf,1.);
	} // Affects trigger
      } // Loop over photons

    } // Affecting photon systematics

    // ========================================================== //
    //             Event-level trigger SF systematics
    // ========================================================== //

    if( electrons && muons && (sys=="" || sys.BeginsWith("MUON_EFF_Trig") || sys.BeginsWith("EL_EFF_Trigger")) && m_saveGlobalDileptonTrigSFs ){
      double sf_diLep  = susyObj.GetTriggerGlobalEfficiencySF(*electrons,*muons,"diLepton");
      addWeightToMap(m_globalDiLepTrigSF, sys, sf_diLep );

      double sf_multiLep  = susyObj.GetTriggerGlobalEfficiencySF(*electrons,*muons,"multiLepton");
      addWeightToMap(m_globalMultiLepTrigSF, sys, sf_multiLep );
    }

    if( photons && (sys=="" || sys.BeginsWith("PH_EFF_TRIGGER_Uncertainty")) && m_saveGlobalPhTrigSF ){
      double sf_weight = susyObj.GetTriggerGlobalEfficiencySF(*photons);
      addWeightToMap(m_globalPhotonTrigSF, sys, sf_weight );
    }
    // Event-level trigger SF systematics

    // ========================================================== //

    // Reset tools to nominal set
    if( sys!= "" ){
      if( susyObj.resetSystematics() != CP::SystematicCode::Ok ){
    std::cout << "ERROR::Cannot reset SUSYTools systematics" << std::endl;
    abort();
      }
    }

  } // Loop over wSys

  Timer::Instance()->End( "ObjectTools::doWeights" );

}
// -------------------------------------------------------------------- //
void ObjectTools::addWeightToMap(std::map<int,std::map<TString,float>>& objMap,
                       int idx,TString sys,float SF)

{

  // Loop over object with idx in the map
  auto objIdx = objMap.find(idx);

  // Check if index has been processed
  if( objIdx != objMap.end() ){
    objIdx->second.insert( std::pair<TString,float>(sys,SF) );
  }
  // First time we run over this idx
  else{
    std::map<TString,float> recoSF_temp;
    recoSF_temp.insert( std::pair<TString,float>(sys,SF) );
    objMap.insert( std::pair<int,std::map<TString,float>>(idx,recoSF_temp) );
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::addWeightToMap(std::map<TString,float>& objMap,
				 TString sys,float SF)

{

  // Loop over object with sys in the map
  auto objSys = objMap.find(sys);

  // Check if index has been processed
  if( objSys != objMap.end() ){
    objSys->second = SF;
  }
  // First time we run over this idx
  else{
    objMap.insert( std::pair<TString,float>(sys,SF) );
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::addMCCorrection(std::map<int,MCCorrContainer*>& trigMap,int idx,
                                  TString instance, TString sys, float sf,float data_eff,float mc_eff)
{

  auto objIdx = trigMap.find(idx);

  // Existing container
  if( objIdx != trigMap.end() ){
    // Save SFs
    if( sf>0.0 ){
      objIdx->second->addSF(instance,sys,sf);
    }
    // Save efficiencies, works in the case only MC or data efficiency passed
    if( data_eff>0.0 || mc_eff>0.0 ){
      objIdx->second->addEff(instance,sys,mc_eff,data_eff);
    }
  }
  // Make a new container and save it
  else{
    MCCorrContainer* trigEff = new MCCorrContainer();
    //
    if( sf>0.0 ){
      trigEff->addSF(instance,sys,sf);
    }
    if( data_eff>0.0 || mc_eff>0.0 ){
      trigEff->addEff(instance,sys,mc_eff,data_eff);
    }
    trigMap.insert( std::pair<int,MCCorrContainer*>(idx,trigEff) );
  }


}
// -------------------------------------------------------------------- //
MCCorrContainer* ObjectTools::getWeightsFromMap(std::map<int,MCCorrContainer*>& trigMap, int idx)
{

  auto objIdx = trigMap.find(idx);

  // Found something!
  if( objIdx != trigMap.end() ) return objIdx->second;
  else{
    MCCorrContainer* empty = new MCCorrContainer();

    // Save into the map, which will clean up the memory we allocated above
    trigMap.insert( std::pair<int,MCCorrContainer*>(idx,empty) );

    return empty;
  }

}
// -------------------------------------------------------------------- //
const std::map<TString,float> ObjectTools::getWeightsFromMap(std::map<int,std::map<TString,float>>& objMap, int idx)
{

  std::map<TString,float> empty;
  if(m_isData) return empty;
  // Loop over object with idx in the map
  auto objIdx = objMap.find(idx);

  if(objIdx != objMap.end()) return objIdx->second;
  else{
    std::cout << "<ObjectTools::getWeightFromMap> WARNING Cannot find weight map for idx: " << idx << std::endl;
    return empty;
  }

}
// ---------------------------------------------------------------------------------------------------------------------------- //
bool ObjectTools::isMatchedToTau(const TLorentzVector* particle, const xAOD::TruthParticleContainer* truthParticles)
{

  for (const auto& iter: *truthParticles) {
    if ( !(iter) ) continue;
    int pdgid = (iter)->pdgId();

    if ( ( abs(pdgid)==15 ) && (iter)->status()!=3  && (iter)->pt() > 10000.0 ){
      if( particle->DeltaR( iter->p4() ) < 0.4 ) return true;
    }
  }
  return false;

}
// ------------------------------------------------------------------------------------- //
bool ObjectTools::passEventFiltering(const xAOD::TruthParticleContainer* truthParticles,
                                     const xAOD::EventInfo* eventInfo,
                                     const xAOD::VertexContainer* vertices)
{

  // Require at least one PV
  if( m_eventObject->getNVtx(vertices,2)==0 ) return false;

  // Skimming for data
  if( m_isData ){

    // Require GRL if requested
    if(m_requireGRLData) {
      if (!m_eventObject->passGRL(eventInfo->runNumber(), eventInfo->lumiBlock())) return false;
    }

    // Trigger skimming
    if( m_requireTrigData && !m_trigTool->hasEvtHLTBit() ) {
      return false;
    }

  }

  // Rest of cuts are applied only for simulation
  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return true;

  // Perform overlap removal between filtered and inclusive samples
  // Please always protected the filtering under this flag
  if( m_doSampleOR ){

    // ttbar and single top MET OR
    if( !passTtbarMetOverlap(truthParticles,eventInfo) ) return false;

    // HT
    //if( !passTtbarHtOverlap(eventInfo) ) return false;

  }

  // Turn this on as a workaround for the following sample:
  // (has all decay modes instead of just the ones it should)
  // mc15_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq
  if (m_doSampleOR_WplvWmqq) {
    if (!passWplvWmqqOverlap(truthParticles, eventInfo)) return false;
  }

  // Z+jet and Z+gamma OR
  if( m_doSampleOR_ZjetZgam ){
    if( !passZjetZgammaOverlap(truthParticles,eventInfo) ) return false;
  }

  // Return gracefully
  return true;

}
// ---------------------------------------------------------------------------------------------------------------------------- //
bool ObjectTools::passTtbarHtOverlap(const xAOD::EventInfo* eventInfo)
{

  bool doOverlap=true;

  // Overlap for nominal samples only
  if( eventInfo->mcChannelNumber()==410000 || eventInfo->mcChannelNumber()==407012) doOverlap=true;  // only let them pass if ht <600
  if( eventInfo->mcChannelNumber()==410013 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==410014 ) doOverlap=true;

  // these are the ht filtered samples which are inclusive in MET, these do not need to be ht filtered though
  if( eventInfo->mcChannelNumber()==407009 ) doOverlap=false; //600-1000
  if( eventInfo->mcChannelNumber()==407010 ) doOverlap=false; //1000-1500
  if( eventInfo->mcChannelNumber()==407011 ) doOverlap=false; //>1500

  //
  if( !doOverlap ) return true;

  float ht = 0.0;
  //
  if( cacc_GenFiltHT.isAvailable( *eventInfo ) ){
    ht = cacc_GenFiltHT( *eventInfo ) / 1000.0;
    // std::cout<<"Found gen ht:"<<ht<<std::endl;
  }
  else{
    Warning("passTtbarHtOverlap","No GenFiltHT decoration, will not perform HT overlap removal!");
    return true;
  }

  // Apply cut
  if ( ht > 600.0 ) return false;

  // Passes!
  return true;


}
// ---------------------------------------------------------------------------------------------------------------------------- //
bool ObjectTools::passTtbarMetOverlap(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo)
{

  bool doOverlap=false;

  // Overlap for nominal samples only
  if( eventInfo->mcChannelNumber()==410000 || eventInfo->mcChannelNumber()==407012) doOverlap=true;
  // Powheg+Pythia: rad Hi
  if( eventInfo->mcChannelNumber()==410001 || eventInfo->mcChannelNumber()==407032) doOverlap=true;
  // Powheg+Pythia: rad Low
  if( eventInfo->mcChannelNumber()==410002 || eventInfo->mcChannelNumber()==407036) doOverlap=true;
  // Powheg+Herwig
  if( eventInfo->mcChannelNumber()==410004 || eventInfo->mcChannelNumber()==407040) doOverlap=true;

  // Single top
  if( eventInfo->mcChannelNumber()==410013 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==410014 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==407058 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==407052 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==410062 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==410063 ) doOverlap=true;

  // these are the ht filtered samples which are inclusive in MET
  if( eventInfo->mcChannelNumber()==407009 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==407010 ) doOverlap=true;
  if( eventInfo->mcChannelNumber()==407011 ) doOverlap=true;

  //
  if( !doOverlap ) return true;

  float GenFiltMET = 0.0;

  // Priority given to the *correct* decoration
  // Not sure when this will arrive
  if( cacc_GenFiltMET.isAvailable( *eventInfo ) ){
    GenFiltMET = cacc_GenFiltMET( *eventInfo );
  }
  // Reconstruct from what's available in the truth record.
  // Note this is a approxiation since some particles are slimmed away
  else{

    TLorentzVector neutrinoMET = TLorentzVector(0.,0.,0.,0.);
    for (const auto& particle: *truthParticles) {
      if (particle->isNeutrino()){
    auto particle_mother = particle->parent(0);
    //this is to avoid double-counting since there are usually two copies of the W in Powheg+Pythia6
    if (particle_mother->nParents()>0 && particle_mother->parent(0)->isTop()) continue;
    if (particle_mother->isW() && particle_mother->nParents()==0){ //there are a few cases where the W has no parent.
      neutrinoMET += particle->p4();
    }
    while (particle_mother->nParents()>0){
      if (particle_mother->parent(0)->isTop()) break;
      if (particle_mother->isW() && fabs(particle->parent(0)->parent(0)->pdgId()) < 100){ //get rid of semi-leptonic B decays with this cut.
        neutrinoMET += particle->p4();
        break;
      }
      particle_mother = particle_mother->parent(0);
    }
      }
    }

    //
    GenFiltMET = neutrinoMET.Pt();

  }

  /*
    The following samples are met filtered - since the GenFiltMET
    quantity is not nescessarily perfect, remove the < 200 GeV events

    mc15_13TeV.407012.PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH
    mc15_13TeV.407032.PhPyEG_CT10_P2012radHiC6L1_ttbarMET200_hdamp345_down_nonAH
    mc15_13TeV.407036.PhPyEG_CT10_P2012radLoC6L1_ttbarMET200_hdamp172_up_nonAH
    mc15_13TeV.407040.PhHppEG_CT10_UE5C6L1_ttbarMET200_hdamp172p5_nonAH
    mc15_13TeV.407058.PowhegPythiaEvtGen_P2012CT10_Wt_DS_inclusive_tbar_MET200
    mc15_13TeV.407052.PowhegPythiaEvtGen_P2012CT10_Wt_DS_inclusive_top_MET200
  */
  // MEV
  if( eventInfo->mcChannelNumber()==407012 ||
      eventInfo->mcChannelNumber()==407032 ||
      eventInfo->mcChannelNumber()==407036 ||
      eventInfo->mcChannelNumber()==407040 ||
      eventInfo->mcChannelNumber()==407058 ||
      eventInfo->mcChannelNumber()==407052 ){
    if ( GenFiltMET < 200000.0 ) return false;
    else return true;
  }
  // The rest of the samples where this code gets executed are inclusive samples
  // For these, the GenFiltMET > 200 GeV is removed
  else{
    if ( GenFiltMET > 200000.0 ) return false;
    else return true;
  }

  // Passes overlap
  return true;

}
// ---------------------------------------------------------------------------------------------------------------------------- //
bool ObjectTools::passZjetZgammaOverlap(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo)
{

  // Temp, make compiler shutup
  (void)truthParticles;
  (void)eventInfo;

  // Not currently implemented!
  return true;

  /*
  // Z+jet vs. Zgamma overlap removal:
  // if photon in Z+jet event, then veto the event

  // Of course, skip this if it is data
  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return true;

  unsigned int dsid = eventInfo->mcChannelNumber();

  if(364100 <= dsid && dsid <= 364141){
    //std::cout << "dsid " << dsid << std::endl;

    // Loop over the truthParticles and look for truth photons
    for(const auto& tp : *truthParticles){

      //std::cout << "tp " << tp << " pdgID " << tp->absPdgId() << " pt " << tp->pt() << " status " << tp->status() << std::endl;

      if((tp->absPdgId()) == 22 && tp->pt() > 10000 && std::abs(tp->eta()) <= 2.5 && tp->status() == 1){
        // Looks like a truth photon with pT > 10 GeV in the final state!
        // Now look at truth vertices

        bool found = false;
        const xAOD::TruthVertex* prodvtx = particle->prodVtx();
        for (unsigned int moth=0; moth<prodvtx->nIncomingParticles(); moth++) {
          const xAOD::TruthParticle* mother= prodvtx->incomingParticle(moth);
          //if (  m_isSherpa ) {
          for (unsigned int ref=0; ref<leptonsFromWZwithTau.size(); ref++) {
            const xAOD::TruthParticle* lepFromWZ = leptonsFromWZwithTau.at(ref);
            if ( lepFromWZ == mother ) {
              found = true;
              if ( !(xAOD::P4Helpers::isInDeltaR(*particle, *lepFromWZ, 0.1)) ){
                hasFSRPhotonLargeDeltaR = true;
                break;
              }
            }
          }
        }

        // Veto this event.
        return false;
      }
    }
  }

  return true;
  */
}
// ------------------------------------------------------------------------------------- //
bool ObjectTools::passSherpaPtSliceOR(const xAOD::TruthParticleContainer* truthParticles,
                        const xAOD::EventInfo* eventInfo)
{

  if( !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) return true;

  unsigned int dsid = eventInfo->mcChannelNumber();
  unsigned int eventNumber = eventInfo->eventNumber();

  // Check we are running over any of the above samples
  float vBPt_cut = -1.0;
  bool isW       = false;

  if( dsid >= 167740 && dsid <= 167748 ){
    isW=true;
    vBPt_cut=40.0;
  }

  if( dsid >= 167749 && dsid <= 167760 ){
    isW=false;
    vBPt_cut=70.0;
  }

  // Not running over inclusive sample
  if( vBPt_cut<0.0 ) return true;


  std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > truthP_shallowCopy = xAOD::shallowCopyContainer( *truthParticles );
  xAOD::TruthParticleContainer::iterator truthP_itr  = (truthP_shallowCopy.first)->begin();
  xAOD::TruthParticleContainer::iterator truthP_end  = (truthP_shallowCopy.first)->end();

  const int kInteresingStatus(3);

  TLorentzVector* l1 = 0;
  TLorentzVector* l2 = 0;

  int pdgId_1 = 0;
  int pdgId_2 = 0;

  for( ; truthP_itr != truthP_end; ++truthP_itr ) {
    xAOD::TruthParticle* truth = (xAOD::TruthParticle*)(*truthP_itr);
    //
    const int &p = truth->pdgId();
    const int &s = truth->status();
    // Check for final state leptons
    bool goodLepton = (s==kInteresingStatus && (abs(p)>=11 && abs(p)<=16));
    if(goodLepton){
      //std::cout << "PDG:    " << p << std::endl;
      if( !l1 ){
    l1 = new TLorentzVector();
    l1->SetPtEtaPhiM(truth->pt(),truth->eta(),truth->phi(),truth->m());
    pdgId_1 = p;
      }
      else if ( !l2 ){
    l2 = new TLorentzVector();
    l2->SetPtEtaPhiM(truth->pt(),truth->eta(),truth->phi(),truth->m());

    pdgId_2 = p;

    // We found two good leptons,
    break;
      }

    } // end if(goodLepton)
  } // loop

  // Clean up
  delete truthP_shallowCopy.first;
  delete truthP_shallowCopy.second;

  if( !l1 && !l2 ){
    std::cout << " <ObjectTools::passSherpaPtSliceOR> WARNING::Cannot find two leptons at truth level. " << std::endl;
    return true;
  }

  // For V+jet events, we need opposite signed leptons
  if(pdgId_1 * pdgId_2 > 0) return true;

  if(isW){
    // W+jets events must have same flavor
    if(abs( abs(pdgId_1)- abs(pdgId_2) ) != 1 ) return true;
  }
  else{
    // Z+jet events must have same flavor and opposite sign
    if( pdgId_1 != -pdgId_2 ) return true;
  }

  // VB pT in GeV
  float pT = (*l1+*l2).Pt() / 1000.0;

  // We should veto this event. Save the result.
  if(pT>vBPt_cut){

    //
    auto evt_it = m_sherpaOverlapEvents.find(dsid);

    // Already vetoed an event from this DSID? Add to the list.
    if( evt_it != m_sherpaOverlapEvents.end() ){
      evt_it->second.push_back( std::pair<unsigned int,float>(eventNumber,pT) );
    }
    else{
      std::vector< std::pair<unsigned int,float> > vec;
      vec.push_back( std::pair<unsigned int,float>(eventNumber,pT) );
      //
      m_sherpaOverlapEvents.insert( std::pair< unsigned int,std::vector<std::pair<unsigned int,float>> >(dsid,vec) );
    }

    return false;
  }

  return true;

}
// -------------------------------------------------------------------- //
bool ObjectTools::passWplvWmqqOverlap(const xAOD::TruthParticleContainer* truthParticles, const xAOD::EventInfo* eventInfo) {

  // Only for the WplvWmqq
  if( eventInfo->mcChannelNumber()!=363360) return true;

  int nWs = 0;
  for (auto truthParticle: *truthParticles) {
    if (truthParticle->isW()) {
      nWs++;
      if (truthParticle->nChildren() > 0) {
        if (truthParticle->charge() > 0 && !truthParticle->child(0)->isLepton()) {
          return false;
        } if (truthParticle->charge() < 0 && !truthParticle->child(0)->isQuark()) {
          return false;
        }
      } else {
        std::cout << "ERROR::Can't find a decay for the W-boson in the WplvWmqq sample" << std::endl;
        abort();
      }
    }
  }
  if (nWs != 2) {
    std::cout << "ERROR::Found " << nWs << "W Bosons instead of 2! in WplvWmqq sample" << std::endl;
    abort();
  }
  return true;
}
// -------------------------------------------------------------------- //
ConstDataVector<xAOD::IParticleContainer> ObjectTools::getInvisibleParticlesForMET(const xAOD::MuonContainer* muon,
                                                                                   const xAOD::ElectronContainer* elec,
                                                                                   const xAOD::PhotonContainer* phot,
                                                                                   bool requireBaseline)
{

  //
  ConstDataVector<xAOD::IParticleContainer> invs(SG::VIEW_ELEMENTS);

  // Muons
  if( muon ){
    for( const auto& mu : *muon ){
      if( requireBaseline && !passBaselineSelection(mu) ) continue;
      invs.push_back(mu);
    }
  }

  // Electrons
  if( elec ){
    for( const auto& ele : *elec ){
      if( requireBaseline && !passBaselineSelection(ele) ) continue;
      invs.push_back(ele);
    }
  }

  // Photons
  if( phot ){
    for( const auto& pho : *phot ){
      if( requireBaseline && !passBaselineSelection(pho) ) continue;
      invs.push_back(pho);
    }
  }

  return invs;

}
// ------------------------------------------------------------------- //
void ObjectTools::get_objects(Objects*& obj)
{

  // TODO: Once Object class migrate, get ride of sysName
  TString sysName = TString( m_sysState );

  if( obj->skimObjectsExist() ){
    get_objects(obj,
        obj->skimEvt,
        obj->skimMet,
        obj->skimMuons,
        obj->skimElectrons,
        obj->skimJets,
        obj->skimFatjets,
        obj->skimTrackjets,
        obj->skimTaus,
                obj->skimPhotons,
                obj->skimV0s, // JEFF
                obj->skimVSIs, // SICONG
                obj->skimTracks,
                obj->skimCalo,
        obj->skimTruthEvent,
        obj->skimTruthEvent_Vjets,
        obj->skimTruthEvent_VV,
        obj->skimTruthEvent_VVV,
        obj->skimTruthEvent_TT,
        obj->skimTruthEvent_XX,
        obj->skimTruthEvent_GG,
                obj->skimTruth);
    return;
  }

  // Get all internally filled objects
  const MuonVector* mu              = m_muonObject->getObj(sysName);
  const ElectronVector* ele         = m_electronObject->getObj(sysName);
  const JetVector* jet              = m_jetObject->getObj(sysName);
  const TauVector* tau              = m_tauObject->getObj(sysName);
  const MetVector* met              = m_metObject->getObj(sysName);
  const EventVariable* evt          = m_eventObject->getEvt(sysName);
  const PhotonVector* photons       = m_photonObject->getObj(sysName);

  const TruthVector* truthParticle  = 0;
  const TruthEvent* truthEvent      = 0;
  const TruthEvent_Vjets* truthEvent_Vjets = 0;
  const TruthEvent_VV*    truthEvent_VV    = 0;
  const TruthEvent_VVV*   truthEvent_VVV    = 0;
  const TruthEvent_TT*    truthEvent_TT    = 0;
  const TruthEvent_XX*    truthEvent_XX    = 0;
  const TruthEvent_GG*    truthEvent_GG    = 0;

  if( m_truthObject ){
    truthParticle = m_truthObject->getObj(sysName);
    truthEvent    = m_truthObject->getTruthEvent(sysName);
    truthEvent_Vjets = m_truthObject->getTruthEvent_Vjets(sysName);
    truthEvent_VV    = m_truthObject->getTruthEvent_VV(sysName);
    truthEvent_VVV    = m_truthObject->getTruthEvent_VVV(sysName);
    truthEvent_TT    = m_truthObject->getTruthEvent_TT(sysName);
    truthEvent_XX    = m_truthObject->getTruthEvent_XX(sysName);
    truthEvent_GG    = m_truthObject->getTruthEvent_GG(sysName);

     // When running over systematics
     // Point the truth container to nominal, which is the only
     // container that exists (truth classification only run for nominal)
    if( !truthEvent ){
      truthEvent = m_truthObject->getTruthEvent("");
      truthEvent_Vjets = m_truthObject->getTruthEvent_Vjets("");
      truthEvent_VV    = m_truthObject->getTruthEvent_VV("");
      truthEvent_VVV    = m_truthObject->getTruthEvent_VVV("");
      truthEvent_TT    = m_truthObject->getTruthEvent_TT("");
      truthEvent_XX    = m_truthObject->getTruthEvent_XX("");
      truthEvent_GG    = m_truthObject->getTruthEvent_GG("");
    }

  }

  // Tracks
  const TrackVector* tracks = 0;
  const ObjectVector* calo  = 0;
  if( m_trackObject ){
    tracks = m_trackObject->getObj(sysName);
    calo   = m_trackObject->getCaloObj(sysName);
  }

  // V0s // JEFF
  const V0Vector* v0s = 0;
  if( m_v0Object ){
    v0s = m_v0Object->getObj(sysName);
  }
  // VSI // SICONG
  const VSIVector* vsiVec = 0;
  if( m_VSIObject ){
    vsiVec = m_VSIObject->getObj(sysName);
  }
  // Fat jet
  const JetVector* fatjet = 0;
  if( m_fatjetObject ){
    fatjet = m_fatjetObject->getObj(sysName);
  }

  // Track jet
  const JetVector* trackjet = 0;
  if( m_trackjetObject ){
    trackjet = m_trackjetObject->getObj(sysName);
  }

  // Fill object containers and select objects
  get_objects(obj,evt,met,mu,ele,jet,fatjet,trackjet,tau,photons, v0s, vsiVec, tracks,calo, // JEFF //SICONG
	      truthEvent,
	      truthEvent_Vjets,
	      truthEvent_VV,
	      truthEvent_VVV,
	      truthEvent_TT,
	      truthEvent_XX,
	      truthEvent_GG,
	      truthParticle);
}
// ------------------------------------------------------------------- //
void ObjectTools::get_objects(Objects*& obj,
    const EventVariable* skimEvt,
    const MetVector* skimMet,
    const MuonVector* skimMuons,
    const ElectronVector* skimElectrons,
    const JetVector* skimJets,
    const JetVector* skimFatjets,
    const JetVector* skimTrackjets,
    const TauVector* skimTaus,
    const PhotonVector* skimPhotons,
    const V0Vector* skimV0s, // JEFF
    const VSIVector* skimVSIs, // SICONG
    const TrackVector* skimTracks,
    const ObjectVector* skimCalo,
    const TruthEvent* skimTruthEvent,
    const TruthEvent_Vjets* skimTruthEvent_Vjets,
    const TruthEvent_VV* skimTruthEvent_VV,
    const TruthEvent_VVV* skimTruthEvent_VVV,
    const TruthEvent_TT* skimTruthEvent_TT,
    const TruthEvent_XX* skimTruthEvent_XX,
    const TruthEvent_GG* skimTruthEvent_GG,
    const TruthVector* skimTruth)
{

  // Clear all internal Objects vectors
  obj->clear();

  // Read in objects and sort by pT
  obj->readObjects(skimEvt,
                   skimMet,
                   skimMuons,
                   skimElectrons,
                   skimJets,
                   skimFatjets,
                   skimTrackjets,
                   skimTaus,
                   skimPhotons,
                   skimTracks,
                   skimCalo,
                   skimV0s, // JEFF
                   skimVSIs, // SICONG
                   skimTruthEvent,
                   skimTruthEvent_Vjets,
                   skimTruthEvent_VV,
                   skimTruthEvent_VVV,
                   skimTruthEvent_TT,
                   skimTruthEvent_XX,
                   skimTruthEvent_GG,
                   skimTruth);

  // Classify objects according to what the user configured, i.e. apply cuts to signal objects
  obj->classifyObjects();

}
// -------------------------------------------------------------------- //
ObjectTools::WeightType ObjectTools::getWeightTypeFromString(TString typeString)
{

  if     (typeString=="GEN"         ) return WeightType::GEN;
  else if(typeString=="PILEUP"      ) return WeightType::PILEUP;
  else if(typeString=="EVT"         ) return WeightType::EVT;
  else if(typeString=="LEP"         ) return WeightType::LEP;
  else if(typeString=="TRIG"        ) return WeightType::TRIG;
  else if(typeString=="BTAG"        ) return WeightType::BTAG;
  else if(typeString=="JVT"         ) return WeightType::JVT;
  else if(typeString=="PILEUP_UP"   ) return WeightType::PILEUP_UP;
  else if(typeString=="PILEUP_DOWN" ) return WeightType::PILEUP_DOWN;
  else if(typeString=="PHOTON"      ) return WeightType::PHOTON;
  else{
    std::cout << "<ObjectTools::getWeightTypeFromString> WARNING Unknown WeightType : " <<  typeString << std::endl;
    return WeightType::UNKNOWN;
  }

}
// -------------------------------------------------------------------- //
void ObjectTools::init_tTbarNNLOTool(int DSID)
{

 (void)DSID;

/*
 * M.G. Commented for now, until transitioned to CMake
  m_tTbarNNLOTool = new NNLOReweighter( DSID );
  m_tTbarNNLOTool->Init();
*/

}
// -------------------------------------------------------------------- //
double ObjectTools::getTtbarNNLOWeight(float topPtInMEV, int DSID, bool doExtended)
{

  (void)topPtInMEV;
  (void)DSID;
  (void)doExtended;

  double weight = 1.0;
  return weight;

/*
 *  * M.G. Commented for now, until transitioned to CMake
 *
  // Only for ttbar PowhegPythia6 inclusive and MET200 sample
  bool doWeights=false;
  if( DSID == 410000 || DSID == 407012 ) doWeights=true;
  else doWeights = false;

  if( !doWeights ) return weight;

  if( !m_tTbarNNLOTool ) init_tTbarNNLOTool( DSID );

  // Do it!
  if( doExtended ){
    weight = m_tTbarNNLOTool->GetExtendedTopPtWeight( topPtInMEV );
  }
  else{
    weight = m_tTbarNNLOTool->GetTopPtWeight( topPtInMEV );
  }

  //
  return weight;
*/
}
// -------------------------------------------------------------------- //
float ObjectTools::doSherpaVjetsNjetsWeight(const xAOD::EventInfo* eventInfo,
                                            ST::SUSYObjDef_xAOD& susyObj )
{


  // Rest of cuts are applied only for simulation
  bool doWeights = false;
  if( eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ) {

    // W+jets
    unsigned int dsid = eventInfo->mcChannelNumber();
    if( dsid>=363331 && dsid<=363354 ) doWeights = true;
    if( dsid>=363436 && dsid<=363483 ) doWeights = true;

    // Z+jets
    if( dsid>=363102 && dsid<=363122 ) doWeights = true;
    if( dsid>=363361 && dsid<=363435 ) doWeights = true;

  }

  float sherpaNJetsWeight_nom = 1.0;
  if( doWeights ){
    sherpaNJetsWeight_nom  = susyObj.getSherpaVjetsNjetsWeight();
  }

  // Finished!
  return sherpaNJetsWeight_nom;

}
// -------------------------------------------------------------------- //
std::map<TString,bool> ObjectTools::getEmulatedTriggerMap(ST::SUSYObjDef_xAOD& susyObj)
{
  std::map<TString,bool> emulatedTriggers;

  // For now hardcoded
  emulatedTriggers["HLT_xe110_mht_L1XE50"] = susyObj.IsMETTrigPassed("HLT_xe110_mht_L1XE50");

  return emulatedTriggers;
}
// -------------------------------------------------------------------- //
double ObjectTools::getWeight(Objects* obj,TString sys,int type, bool useTrackJets, TString trigChains)
{

  double weight = 1.0;

  if(obj==NULL){
    std::cout << "<ObjectTools::getWeight> WARNING Objects pointer is null, returning a weight of 1.0" << std::endl;
    return weight;
  }

  // This is data, no weights
  if( !obj->evt.isMC ) return weight;

  double nEvt = obj->evt.nEvents;

  if(nEvt<=0){
    //std::cout << "<ObjectTools::getWeight> WARNING Number of events to normalize to is less or equal to zero! Setting to 1" << std::endl;
    nEvt=1.0;
  }

  // Lumi weight
  if(type & WeightType::GEN){
    weight *= (obj->evt.xsec * m_lumi)  / nEvt;
  }

  // Pileup weight
  if( !m_truthonly && (( type &  WeightType::PILEUP) ||
               ( type &  WeightType::PILEUP_UP) ||
               ( type &  WeightType::PILEUP_DOWN) )){

      if( type &  WeightType::PILEUP)
        weight *= obj->evt.pileUp;
      if( type &  WeightType::PILEUP_UP)
        weight *= obj->evt.pileUp_sysUp;
      if( type &  WeightType::PILEUP_DOWN)
        weight *= obj->evt.pileUp_sysDown;

  }


  // MC generator weight
  if(type & WeightType::EVT){
    weight *= obj->evt.mcWeight;
  }

  // Lepton reconstruction weights
  if( !m_truthonly && (type & WeightType::LEP)){

    // Electron and muon reco weights
    for(unsigned int i=0; i<obj->baseElectrons.size(); i++){
      weight *= obj->baseElectrons[i]->getRecoSF(sys);
    }
    for(unsigned int i=0; i<obj->baseMuons.size(); i++){
      weight *= obj->baseMuons[i]->getRecoSF(sys);
    }

  }

  // Lepton trigger weights
  // Does nothing at the moment except when one uses di-leptonic and photon triggers
  // User should call ConfigMgr trigger analysis method
  if( !m_truthonly && ( type & WeightType::TRIG)){
  
    // Lepton trigger
    if(trigChains.Contains("singleLep")){
      for(unsigned int i=0; i<obj->baseElectrons.size(); i++)
	weight *= obj->baseElectrons[i]->getTrigSF("singleElectronTriggerEff",sys);
      for(unsigned int i=0; i<obj->baseMuons.size(); i++)
	weight *= obj->baseMuons[i]->getTrigSF("singleMuonTriggerEff",sys);
    }
    else if(trigChains.Contains("diLep"))     weight *= obj->evt.getGlobalDiLepTrigSF(sys);
    else if(trigChains.Contains("multiLep"))  weight *= obj->evt.getGlobalMultiLepTrigSF(sys);

    // Photon trigger
    else if(trigChains.Contains("singlePhoton")){
      for(unsigned int i=0; i<obj->basePhotons.size(); i++)
	weight *= obj->basePhotons[i]->getTrigSF("singlePhotonTriggerEff",sys);
    }
    else if(trigChains.Contains("diPhoton"))    weight *= obj->evt.getGlobalPhotonTrigSF(sys);

  } // Trigger

  // B-tagging weights
  if( !m_truthonly && (type & WeightType::BTAG) && !useTrackJets){
    // B-tagging scale factors
    for( const auto& jet : obj->cJets ){
      weight *= jet->getFtSF(sys);
    }
  }

  //  B-tagging weights (based on track jets)
  if( !m_truthonly && (type & WeightType::BTAG) && useTrackJets){
    // B-tagging scale factors
    for( const auto& trackjet : obj->trackjets) {
      weight *= trackjet->getFtSF(sys);
    }
  }

  if( !m_truthonly && (type & WeightType::JVT)){

    // Loop over entire jet container,
    // such that we pick up the inefficiency SF
    // (cJets requires ST::signal, which has JVT requirements)
    for( const auto& jet : obj->jets ){
      weight *= jet->getJVTSF(sys);
    }

  } // JVT SFs

  /*
  if( !m_truthonly && (type & WeightType::BOSONTAG)){

    // Loop over entire signal fatjet container,
    // such that we pick up the inefficiency SF
    // (cJets requires ST::signal, which has JVT requirements)
    for( const auto& fatjet : obj->cFatjets ){
      weight *= fatjet->getWTAGSF(sys);
    }

  } // Boson tagging SFs
  */

  if( !m_truthonly && (type & WeightType::PHOTON)){

    // Reconstruction and isolation SFs
    for( const auto& photon: obj->basePhotons ){
      weight *= photon->getRecoSF(sys);
    }

  } // Photon SFs

  return weight;

}
// ------------------------------------------------------------------------------------- //
void ObjectTools::get_xAOD_truth(const xAOD::TruthParticleContainer* truthParticles,
				 const xAOD::TruthParticleContainer* truthElectrons,
				 const xAOD::TruthParticleContainer* truthMuons,
				 const xAOD::TruthParticleContainer* truthTaus,
				 const xAOD::TruthParticleContainer* truthPhotons,
				 const xAOD::TruthParticleContainer* truthNeutrinos,
				 const xAOD::TruthParticleContainer* truthBosonWDP,
				 const xAOD::TruthParticleContainer* truthZBosons,
				 const xAOD::TruthParticleContainer* truthTopWDP,
				 const xAOD::TruthParticleContainer* truthBSMWDP,
				 const xAOD::TruthParticleContainer* truthTauWDP,
				 const xAOD::JetContainer* truthJets,
				 const xAOD::JetContainer* truthFatjets,
                                 const xAOD::EventInfo* eventInfo)
{

  // Pointer is only created when instructed to run over truth
  if( !m_truthObject || m_sysState!="" ) return;

  //
  Timer::Instance()->Start( "ObjectTools::get_xAOD_truth" );

  // Truth event
  m_truthObject->fillTruthEventContainer(truthParticles, truthElectrons, truthMuons, truthTaus, truthPhotons, truthNeutrinos, truthBosonWDP, truthZBosons, truthTopWDP, truthBSMWDP, truthTauWDP, truthJets, truthFatjets, eventInfo);



  // Truth particles
  // only possible if TruthParticles container exists in input, not the case for
  // most derivations now so skip to avoid warnings
  if (truthParticles) {
    m_truthObject->fillTruthParticleContainer(truthParticles,truthJets, truthBSMWDP); //this line is a problem
  }

  //
  Timer::Instance()->End( "ObjectTools::get_xAOD_truth" );

}
/*
// -------------------------------------------------------------------- //
bool ObjectTools::init_nearbyLepIsoCorr()
{
  //
  // Obsolete! migrated to CP::IsolationCloseByCorrectionTool in the ST)
  //

  const char* APP_NAME = "ObjectTools";

  // Take the IsoTool that SUSYTools put into the ToolStore
  ToolHandle<CP::IIsolationSelectionTool> IsoTool = asg::ToolStore::get<CP::IsolationSelectionTool>("ToolSvc.IsoTool");

  // Tool is documented at
  // https://gitlab.cern.ch/jojungge/NearbyLepIsoCorrection/blob/master/README
  m_nearbyLepIsoCorr = new NearLep::IsoCorrection("LeptonIsoCorrection");
  CHECK( m_nearbyLepIsoCorr->setProperty("IsolationTool", IsoTool) );

  // Use "signal" leptons from ST as input. Note this means that to make
  // use of the NearbyLepIsoCorrection, your SUSYTools should set
  // "SigLep.RequireIso: 0", since the "signal" requirement will otherwise
  // include the isolation requirement as well!
  CHECK( m_nearbyLepIsoCorr->setProperty("InputQualityDecorator", m_nearbyLepType) );

  // Decorator for leptons which pass the corrected isolation criteria
  CHECK( m_nearbyLepIsoCorr->setProperty("IsolationDecorator", "isol_corr") );

  // In principle, decorator for signal leptons which pass the corrected
  // isolation criteria. In reality, this isn't working for me right now,
  // so I instead check signal && isol_corr for now, which should
  // give identical results
  CHECK( m_nearbyLepIsoCorr->setProperty("SignalDecorator", "signal_nearby") );

  // Use the track and calo isolation corrections
  CHECK( m_nearbyLepIsoCorr->setProperty("ApplyPtConeCorrection", true) );
  CHECK( m_nearbyLepIsoCorr->setProperty("ApplyTopoEtConeCorrection", true) );

  // Need to turn this on to correct ones with larger cone than what is used for calculating isolation for signal leptons
  CHECK( m_nearbyLepIsoCorr->setProperty("AlwaysCorrect", true) );

  // Initialize the tool
  CHECK( m_nearbyLepIsoCorr->initialize() );

  return true;
}
*/
// -------------------------------------------------------------------- //
bool ObjectTools::applyNearbyLepIsoCorr(ST::SUSYObjDef_xAOD& susyObj,
					xAOD::ElectronContainer* electrons,
                                        xAOD::MuonContainer* muons)
{
  const char* APP_NAME = "ObjectTools";

  if( !m_useNearbyLepIsoCorr ) return true;

  // Apply the correction to the isolation variables
  susyObj.NearbyLeptonCorrections(electrons, muons);
  //CHECK( m_nearbyLepIsoCorr->CorrectIsolation(electrons, muons) ); // obsolete

  return true;
}
// -------------------------------------------------------------------- //
void ObjectTools::DecorateWithIdx(xAOD::IParticleContainer* p)
{
  if(!p) return;

  //
  if( !p ) return;

  unsigned int pIdx = 1;
  for( const auto& particle : *p ){
    particle->auxdata< int >("idx") = pIdx;
    pIdx++;
  }

}
// -------------------------------------------------------------------- //
int ObjectTools::getIdx(const xAOD::IParticle* p)
{

  int idx = p->auxdata< int >("idx");
  return idx;

}
// -------------------------------------------------------------------- //
void ObjectTools::Reset_perEvt()
{

  // Trigger tool
  if(!m_truthonly)
    m_trigTool->Reset_perEvt();

  // Object classes only set once per event
  m_eventObject->Reset();
  m_metObject->Reset();
  m_muonObject->Reset();
  m_photonObject->Reset();
  m_electronObject->Reset();
  m_jetObject->Reset();
  m_tauObject->Reset();

  // Conditional resets
  if( m_truthObject ){
    m_truthObject->Reset();
  }

  if( m_trackObject ){
    m_trackObject->Reset();
  }

  if( m_v0Object ){// JEFF
    m_v0Object->Reset();
  }
  if( m_VSIObject ){// SICONG
    m_VSIObject->Reset();
  }
  if( m_fatjetObject ){
    m_fatjetObject->Reset();
  }

  if( m_trackjetObject ){
    m_trackjetObject->Reset();
  }

  // Reset weight maps
  // Will move to Systematics class
  m_muonSFs.clear();
  m_electronSFs.clear();
  m_photonSFs.clear();
  m_tauSFs.clear();
  m_jetSFs.clear();
  m_jetJvtSFs.clear();
  m_trackjetSFs.clear();

  m_globalDiLepTrigSF.clear();
  m_globalMultiLepTrigSF.clear();
  m_globalPhotonTrigSF.clear();

  // Clean memory
  for(auto& it : m_muonTrigEffs){
    delete it.second;
  }
  m_muonTrigEffs.clear();

 for(auto& it : m_eleTrigEffs){
    delete it.second;
  }
  m_eleTrigEffs.clear();

 for(auto& it : m_photonTrigEffs){
    delete it.second;
  }
  m_photonTrigEffs.clear();

}
// -------------------------------------------------------------------- //
void ObjectTools::Reset_perSys()
{

  if(!m_truthonly)
    m_trigTool->Reset_perSys();

  //if(m_truthObject) m_truthObject->Reset();

  // Skimming is systematic dependent
  m_evtProperties->Reset();

}
// -------------------------------------------------------------------- //
void ObjectTools::LoadEvtTriggers()
{
  if(!m_truthonly)
    m_trigTool->LoadEvtTriggers();
}
// -------------------------------------------------------------------- //
void ObjectTools::Log(MetaData* metaData)
{

  // Log all object classes
  m_eventObject->Log(metaData);

  // Any events lost from truth level sherpa overlap removal
  metaData->sherpaOverlapEvents = m_sherpaOverlapEvents;

  // Log all other properties from this class
  metaData->log("ObjectTools","Target luminosity",m_lumi);
  metaData->log("ObjectTools","Filter baseline objects",m_filterBaseline);
  metaData->log("ObjectTools","Exclude additional leptons from MET",m_excludeAdditionalLeptons);
  metaData->log("ObjectTools","Filter objects failing overlap removal",m_filterOR);
  metaData->log("ObjectTools","Filter number of leptons (applied to skims too)",m_nLepSkimFilter);
  metaData->log("ObjectTools","Save trigger scale-factors",m_doTriggerSFs);
  metaData->log("ObjectTools","Perform inclusive/MET filtered sample OR",m_doSampleOR);
  metaData->log("ObjectTools","Perform Z+jet/Z+gamma sample OR",m_doSampleOR_ZjetZgam);
  metaData->log("ObjectTools","Require any trigger to fire (apply only to data)",m_requireTrigData);
  metaData->log("ObjectTools","Require GRL (only applied to data)",m_requireGRLData);


}
// -------------------------------------------------------------------- //
ObjectTools::~ObjectTools()
{

  // Clean up memory
  if(m_eventObject      ) delete m_eventObject;
  if(m_metObject        ) delete m_metObject;
  if(m_muonObject       ) delete m_muonObject;
  if( m_photonObject    ) delete m_photonObject;
  if( m_trackObject     ) delete m_trackObject;
  if( m_v0Object        ) delete m_v0Object;
  if(m_electronObject   ) delete m_electronObject;
  if(m_jetObject        ) delete m_jetObject;
  if(m_fatjetObject     ) delete m_fatjetObject;
  if(m_trackjetObject   ) delete m_trackjetObject;
  if(m_tauObject        ) delete m_tauObject;
  if(m_truthObject      ) delete m_truthObject;
  if(m_trigTool         ) delete m_trigTool;
  if(m_evtProperties    ) delete m_evtProperties;
  //if( m_tTbarNNLOTool   ) delete m_tTbarNNLOTool;
  //if(m_nearbyLepIsoCorr ) delete m_nearbyLepIsoCorr;

  m_sherpaOverlapEvents.clear();

}
