#include "SusySkimMaker/V0Variable.h"
#include "SusySkimMaker/MsgLog.h"

// -------------------------------------------------------------------------------- //
V0Variable::V0Variable() : truthLink(0)
{

  trackType = TrackType::UNKNOWN;

  //
  setDefault(Kshort_mass,0);
  setDefault(Lambda_mass,0);
  setDefault(Lambdabar_mass,0);
  setDefault(Kshort_massError,0);
  setDefault(Lambda_massError,0);
  setDefault(Lambdabar_massError,0);
  setDefault(Rxy,0);
  setDefault(RxyError,0);
  setDefault(pT,0);
  setDefault(pTError,0);
  setDefault(px,0);
  setDefault(py,0);
  setDefault(pz,0);
  setDefault(track1Pt,0);
  setDefault(track2Pt,0);
  setDefault(track1Eta,0);
  setDefault(track2Eta,0);
  setDefault(track1Phi,0);
  setDefault(track2Phi,0);
  setDefault(track1TightPrimary,0);
  setDefault(track2TightPrimary,0);
  setDefault(track1Barcode,0);
  setDefault(track2Barcode,0);


  setDefault(q,-0);
  setDefault(d0,-1.0);
  setDefault(z0,-1.0);
  setDefault(d0Err,-1.0);
  setDefault(z0Err,-1.0);
  setDefault(ptcone20,0.0);
  setDefault(ptcone40,0.0);
  setDefault(ptcone40,0.0);
  setDefault(nIBLHits,0);
  setDefault(nPixHits,0);
  setDefault(nIBLHits,0);
  setDefault(nPixLayers,0);
  setDefault(nExpBLayerHits,0);
  setDefault(nPixHoles,0);
  setDefault(nPixOutliers,0);
  setDefault(nGangedFlaggedFakes,0);
  setDefault(nSCTHits,0);
  setDefault(nSCTHoles,0);
  setDefault(nSCTSharedHits,0);
  setDefault(nSCTOutliers,0);
  setDefault(nTRTHits,0);
  setDefault(nPixSpoiltHits,0);
  setDefault(type,-1);
  setDefault(origin,-1);
  setDefault(etcone20Topo,-1);
  setDefault(etclus20Topo,-1);
  setDefault(etclus40Topo,-1);
  setDefault(fitQuality,-1);
  setDefault(truthProVtx,-1);
  setDefault(vtxQuality,-1);
 

 
  //
  m_associatedTrack.clear();

}
// -------------------------------------------------------------------------------- //
V0Variable::~V0Variable()
{
 
  std::vector<V0Variable*>::iterator iter;
  for (iter = m_associatedTrack.begin(); iter != m_associatedTrack.end(); iter++) {
    delete *iter;
  }

}
// -------------------------------------------------------------------------------- //
V0Variable::V0Variable(const V0Variable &rhs):
  ObjectVariable(rhs),

  Kshort_mass(rhs.Kshort_mass),
  Lambda_mass(rhs.Lambda_mass),
  Lambdabar_mass(rhs.Lambdabar_mass),
  Kshort_massError(rhs.Kshort_massError),
  Lambda_massError(rhs.Lambda_massError),
  Lambdabar_massError(rhs.Lambdabar_massError),
  Rxy(rhs.Rxy),
  RxyError(rhs.RxyError),
  pT(rhs.pT),
  pTError(rhs.pTError),
  px(rhs.px),
  py(rhs.py),
  pz(rhs.pz),
  track1Pt(rhs.track1Pt),
  track2Pt(rhs.track2Pt),
  track1Eta(rhs.track1Eta),
  track2Eta(rhs.track2Eta),
  track1Phi(rhs.track1Phi),
  track2Phi(rhs.track2Phi),
  track1TightPrimary(rhs.track1TightPrimary),
  track2TightPrimary(rhs.track2TightPrimary),
  track1Barcode(rhs.track1Barcode),
  track2Barcode(rhs.track2Barcode),


  trackType(rhs.trackType),
  q(rhs.q),
  d0(rhs.d0),
  z0(rhs.z0),
  d0Err(rhs.d0Err),
  z0Err(rhs.z0Err),
  ptcone20(rhs.ptcone20),
  ptcone30(rhs.ptcone30),
  ptcone40(rhs.ptcone40),
  nIBLHits(rhs.nIBLHits),
  nPixLayers(rhs.nPixLayers),
  nExpBLayerHits(rhs.nExpBLayerHits),
  nPixHits(rhs.nPixHits),
  nPixHoles(rhs.nPixHoles),
  nPixOutliers(rhs.nPixOutliers),
  nSCTHits(rhs.nSCTHits),
  nSCTHoles(rhs.nSCTHoles),
  nSCTSharedHits(rhs.nSCTSharedHits),
  nSCTOutliers(rhs.nSCTOutliers),
  nTRTHits(rhs.nTRTHits),
  nPixSpoiltHits(rhs.nPixSpoiltHits),
  nGangedFlaggedFakes(rhs.nGangedFlaggedFakes),
  type(rhs.type),
  origin(rhs.origin),
  etcone20Topo(rhs.etcone20Topo),
  etclus20Topo(rhs.etclus20Topo),
  etclus40Topo(rhs.etclus40Topo),
  fitQuality(rhs.fitQuality),
  truthProVtx(rhs.truthProVtx),
  vtx(rhs.vtx),
  vtxQuality(rhs.vtxQuality),
  truthLink(rhs.truthLink),
  m_associatedTrack(rhs.m_associatedTrack)

{

}
// -------------------------------------------------------------------------------- //
V0Variable& V0Variable::operator=(const V0Variable &rhs)
{

  if (this != &rhs) {
    ObjectVariable::operator=(rhs);

    Kshort_mass         = rhs.Kshort_mass;
    Lambda_mass         = rhs.Lambda_mass;
    Lambdabar_mass      = rhs.Lambdabar_mass;
    Kshort_massError    = rhs.Kshort_massError;
    Lambda_massError    = rhs.Lambda_massError;
    Lambdabar_massError = rhs.Lambdabar_massError;
    Rxy                 = rhs.Rxy;
    RxyError            = rhs.RxyError;
    pT                  = rhs.pT;
    pTError             = rhs.pTError;
    px                  = rhs.px;
    py                  = rhs.py;
    pz                  = rhs.pz;
    track1Pt            = rhs.track1Pt;
    track2Pt            = rhs.track2Pt;
    track1Eta           = rhs.track1Eta;
    track2Eta           = rhs.track2Eta;
    track1Phi           = rhs.track1Phi;
    track2Phi           = rhs.track2Phi;
    track1TightPrimary  = rhs.track1TightPrimary;
    track2TightPrimary  = rhs.track2TightPrimary;
    track1Barcode       = rhs.track1Barcode;
    track2Barcode       = rhs.track2Barcode;
      
      


    trackType             = rhs.trackType;
    q                     = rhs.q;
    d0                    = rhs.d0;
    z0                    = rhs.z0;
    d0Err                 = rhs.d0Err;
    z0Err                 = rhs.z0Err;
    ptcone20              = rhs.ptcone20;
    ptcone30              = rhs.ptcone30;
    ptcone40              = rhs.ptcone40;
    nIBLHits              = rhs.nIBLHits;
    nPixLayers            = rhs.nPixLayers;
    nExpBLayerHits        = rhs.nExpBLayerHits;
    nPixHits              = rhs.nPixHits;
    nPixHoles             = rhs.nPixHoles;
    nPixOutliers          = rhs.nPixOutliers;
    nSCTHits              = rhs.nSCTHits;
    nSCTHoles             = rhs.nSCTHoles;
    nSCTSharedHits        = rhs.nSCTSharedHits;
    nSCTOutliers          = rhs.nSCTOutliers;
    nTRTHits              = rhs.nTRTHits;
    nPixSpoiltHits        = rhs.nPixSpoiltHits;
    nGangedFlaggedFakes   = rhs.nGangedFlaggedFakes;
    type                  = rhs.type;
    origin                = rhs.origin;
    etcone20Topo          = rhs.etcone20Topo;
    etclus20Topo          = rhs.etclus20Topo;
    etclus40Topo          = rhs.etclus40Topo;
    fitQuality            = rhs.fitQuality;
    truthProVtx           = rhs.truthProVtx;
    vtx                   = rhs.vtx;
    vtxQuality            = rhs.vtxQuality;
    truthLink             = rhs.truthLink;
    m_associatedTrack     = rhs.m_associatedTrack;
  }

  return *this;

}
// -------------------------------------------------------------------------------- //
V0Variable* V0Variable::makeAssociatedTrack()
{

  V0Variable* trk = new V0Variable();
  m_associatedTrack.push_back(trk);

  return trk;

}
// -------------------------------------------------------------------------------- //
V0Variable* V0Variable::getAssociatedTrack(int idx)
{

  if( idx>this->nAssociatedTracks() ){
    MsgLog::ERROR("TrackVariable::getAssociatedTrack","Invalid index %i. Returning a NULL object",idx);
    return 0;
  }

  //
  return m_associatedTrack[idx];

}
// -------------------------------------------------------------------------------- //
void V0Variable::print() const
{

    MsgLog::INFO("TrackVariable::print","Track pT %f eta %f and phi %f",this->Pt(), this->Eta(), this->Phi() );
    MsgLog::INFO("TrackVariable::print","Track d0 %f +/- %f and z0 %f +/- %f",this->d0, this->d0Err, this->z0, this->z0Err );
    MsgLog::INFO("TrackVariable::print","Track d0Sig %f and z0*sin(theta) %f",abs(this->d0/this->d0Err), this->z0*sin(this->Theta()) );
    MsgLog::INFO("TrackVariable::print","Track number of TRT hits %i",this->nTRTHits);
    MsgLog::INFO("TrackVariable::print","Track number of pixel hits %i, holes %i, and outliers hits %i",this->nPixHits, this->nPixHoles,this->nPixOutliers);
    MsgLog::INFO("TrackVariable::print","Track number of pixel layers %i and b-layer hits %i",this->nPixLayers,this->nExpBLayerHits);
    MsgLog::INFO("TrackVariable::print","Track number of SCT hits %i, holes %i, shared hits %i and outliers hits %i",this->nSCTHits, this->nSCTHoles,this->nSCTSharedHits,this->nSCTOutliers);
    MsgLog::INFO("TrackVariable::print","Track ptcone20 %f, ptcone30 %f, and ptcone40 %f",this->ptcone20,this->ptcone30,this->ptcone40);
    MsgLog::INFO("TrackVariable::print","Track fit quality %f",this->fitQuality);
    MsgLog::INFO("TrackVariable::print","Track vertex associated radius %f and quality %f",this->vtx.Perp(),this->vtxQuality);
    MsgLog::INFO("TrackVariable::print","Truth type %i and origin  %i",this->type,this->origin);

}



