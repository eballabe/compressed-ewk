#include "SusySkimMaker/BaseObject.h"
#include "SusySkimMaker/StatusCodeCheck.h"

#include "IFFTruthClassifier/IFFTruthClassifier.h"

BaseObject::BaseObject() : isoTool(0),
			   IFFClassifier(0),
			   isoLowPtPLVTool(0),
			   havePLVWPsRemoved(false),
			   havePLIVWPsRemoved(false)
{

}
// -------------------------------------------------------------------- //
StatusCode BaseObject::initialize(TString stream)
{

  const char* APP_NAME = "BaseObject";

  // IFF truth classification tool
  TString IFFClassifierToolName = "IFFClassifier_"+stream;
  IFFClassifier = new IFFTruthClassifier((std::string)IFFClassifierToolName.Data());
  CHECK( IFFClassifier->initialize() );

  // Isolation selection tool
  TString IsoToolName = "IsoTool_"+stream;
  isoTool = new CP::IsolationSelectionTool( (std::string)IsoToolName.Data() );
  CHECK( isoTool->initialize() );

  if(stream == "ElectronWP"){
    
    //Old WPs
    CHECK( isoTool->addElectronWP("FCHighPtCaloOnly") );
    CHECK( isoTool->addElectronWP("FCLoose") );
    CHECK( isoTool->addElectronWP("FCTight") );
    CHECK( isoTool->addMuonWP("FCLoose_FixedRad") );
    CHECK( isoTool->addMuonWP("FCTight_FixedRad") );

    CHECK( isoTool->addElectronWP("HighPtCaloOnly") );
    CHECK( isoTool->addElectronWP("Loose_VarRad") );
    CHECK( isoTool->addElectronWP("Tight_VarRad") );
    CHECK( isoTool->addElectronWP("TightTrackOnly_VarRad") );
    CHECK( isoTool->addElectronWP("TightTrackOnly_FixedRad") );
    CHECK( isoTool->addElectronWP("PflowLoose") ); 
    CHECK( isoTool->addElectronWP("PflowTight") );
    CHECK( isoTool->addElectronWP("PLVLoose") );
    CHECK( isoTool->addElectronWP("PLVTight") );
    CHECK( isoTool->addElectronWP("PLImprovedTight") );
    CHECK( isoTool->addElectronWP("PLImprovedVeryTight") );
  }

  else if(stream == "MuonWP"){

    //Old WPs
    CHECK( isoTool->addMuonWP("FixedCutHighPtTrackOnly") );
    CHECK( isoTool->addMuonWP("FCTightTrackOnly") );
    CHECK( isoTool->addMuonWP("FCLoose") );
    CHECK( isoTool->addMuonWP("FCTight") );
    CHECK( isoTool->addMuonWP("FCLoose_FixedRad") );
    CHECK( isoTool->addMuonWP("FCTight_FixedRad") );

    CHECK( isoTool->addMuonWP("HighPtTrackOnly") );
    CHECK( isoTool->addMuonWP("TightTrackOnly_VarRad") );
    CHECK( isoTool->addMuonWP("TightTrackOnly_FixedRad") );
    CHECK( isoTool->addMuonWP("Loose_VarRad") );
    CHECK( isoTool->addMuonWP("Loose_FixedRad") );
    CHECK( isoTool->addMuonWP("Tight_VarRad") );
    CHECK( isoTool->addMuonWP("Tight_FixedRad") );
    CHECK( isoTool->addMuonWP("PflowLoose_VarRad") );
    CHECK( isoTool->addMuonWP("PflowLoose_FixedRad") );
    CHECK( isoTool->addMuonWP("PflowTight_VarRad") );
    CHECK( isoTool->addMuonWP("PflowTight_FixedRad") );
    CHECK( isoTool->addMuonWP("PLVLoose") );
    CHECK( isoTool->addMuonWP("PLVTight") );
    CHECK( isoTool->addMuonWP("PLImprovedTight") );
    CHECK( isoTool->addMuonWP("PLImprovedVeryTight") );
  }

  else if(stream == "PhotonWP"){
    CHECK( isoTool->addPhotonWP("FixedCutLoose") );
    CHECK( isoTool->addPhotonWP("FixedCutTight") );
  }

  // IsolationLowPtPLVTool for LowPtPLV score calculation
  TString IsoLowPtPLVToolName = "IsoLowPtPLVTool_"+stream;
  isoLowPtPLVTool = new CP::IsolationLowPtPLVTool((std::string)IsoLowPtPLVToolName.Data());
  CHECK( isoLowPtPLVTool->initialize() );

  return true;

}
// ----------------------------------------------------------------------- //
void BaseObject::disablePLVWPs(xAOD::Type::ObjectType type){

  if(havePLVWPsRemoved) return;

  std::cout << "<BaseObject::removePLVWPs>  WARNING  PLV score seems not contained in the file. Will disable the PLV WPs evaluation." << std::endl;

  // Dump the non-PLV registered WPs
  auto isoWPs = 
    type == xAOD::Type::Muon ? 
    isoTool->getMuonWPs() :
    isoTool->getElectronWPs();

  std::vector<std::string> s_isoWPs;
  for(auto isoWP : isoWPs){
    if(!TString(isoWP->name()).BeginsWith("PLV")) s_isoWPs.push_back(isoWP->name());      
  }

  // Clear 
  if(type == xAOD::Type::Muon)  isoTool->clearMuonWPs();
  else                          isoTool->clearElectronWPs();

  // Re-register the non-PLV WPs
  for(auto s_isoWP : s_isoWPs) isoTool->addWP(s_isoWP,type);

  // Make sure this operation only happens once
  havePLVWPsRemoved = true;
 
}
// ----------------------------------------------------------------------- //
void BaseObject::disablePLIVWPs(xAOD::Type::ObjectType type){

  if(havePLIVWPsRemoved) return;

  std::cout << "<BaseObject::removePLVWPs>  WARNING  PLIV score seems not contained in the file. Will disable the PLIV WPs evaluation." << std::endl;

  // Dump the non-PLV registered WPs
  auto isoWPs =
    type == xAOD::Type::Muon ?
    isoTool->getMuonWPs() :
    isoTool->getElectronWPs();

  std::vector<std::string> s_isoWPs;
  for(auto isoWP : isoWPs){
    if(!TString(isoWP->name()).BeginsWith("PLI")) s_isoWPs.push_back(isoWP->name());
  }

  // Clear
  if(type == xAOD::Type::Muon)  isoTool->clearMuonWPs();
  else                          isoTool->clearElectronWPs();

  // Re-register the non-PLV WPs
  for(auto s_isoWP : s_isoWPs) isoTool->addWP(s_isoWP,type);

  // Make sure this operation only happens once
  havePLIVWPsRemoved = true;

}
// ----------------------------------------------------------------------- //
BaseObject::~BaseObject()
{
  if( isoTool ) delete isoTool;
}
