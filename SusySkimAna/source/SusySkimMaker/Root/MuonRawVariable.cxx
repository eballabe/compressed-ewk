#include "SusySkimMaker/MuonRawVariable.h"


MuonRawVariable::MuonRawVariable()
{


}
/*

  Function here (or in the initalization of this class), define named for each variable, 
  so we can look them up later on 

 */
// -------------------------------------------------------------------------------- //
MuonRawVariable::MuonRawVariable(const MuonRawVariable &rhs):
  MuonVariable(rhs)
{

}
// -------------------------------------------------------------------------------- //
MuonRawVariable& MuonRawVariable::operator=(const MuonRawVariable &rhs)
{
  if (this != &rhs) {
    MuonVariable::operator=(rhs);
  }
  return *this;
}
// -------------------------------------------------------------------------------- //
MuonRawVariable& MuonRawVariable::operator=(MuonVariable &rhs)
{
  if (this != &rhs) {
    MuonVariable::operator=(rhs);
  }
  return *this;
}

