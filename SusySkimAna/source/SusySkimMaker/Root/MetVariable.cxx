#include "SusySkimMaker/MetVariable.h"


MetVariable::MetVariable()
{

  setDefault(Et,0.0);
  setDefault(phi,-99.0);
  setDefault(px,0.0);
  setDefault(py,0.0);
  setDefault(Et_truth,0.0);
  setDefault(px_truth,0.0);
  setDefault(py_truth,0.0);
  setDefault(Et_ele,0.0);
  setDefault(phi_ele,-99.0);
  setDefault(px_ele,0.0);
  setDefault(Et_muon,0.0);
  setDefault(phi_muon,-99.0);
  setDefault(px_muon,0.0);
  setDefault(py_muon,0.0);
  setDefault(Et_jet,0.0);
  setDefault(phi_jet,-99.0);
  setDefault(px_jet,0.0);
  setDefault(py_jet,0.0);
  setDefault(Et_soft,0.0);
  setDefault(phi_soft,-99.0);
  setDefault(px_soft,0.0);
  setDefault(py_soft,0.0);
  setDefault(Et_muons_invis,0.0);
  setDefault(phi_muons_invis,-99.0);
  setDefault(px_muons_invis,0.0);
  setDefault(py_muons_invis,0.0);
  setDefault(metSignif,0.0);

  this->flavor = MetFlavor::DEFAULT;

}
// -------------------------------------------------------------------------------- //
MetVariable::MetVariable(const MetVariable &rhs):
  ObjectVariable(rhs),
  flavor(rhs.flavor),
  Et(rhs.Et),
  phi(rhs.phi),
  px(rhs.px),
  py(rhs.py),
  Et_truth(rhs.Et_truth),
  px_truth(rhs.px_truth),
  py_truth(rhs.py_truth),
  Et_ele(rhs.Et_ele),
  phi_ele(rhs.phi_ele),
  px_ele(rhs.px_ele),
  py_ele(rhs.py_ele),
  Et_muon(rhs.Et_muon),
  phi_muon(rhs.phi_muon),
  px_muon(rhs.px_muon),
  py_muon(rhs.py_muon),
  Et_jet(rhs.Et_jet),
  phi_jet(rhs.phi_jet),
  px_jet(rhs.px_jet),
  py_jet(rhs.py_jet),
  Et_soft(rhs.Et_soft),
  phi_soft(rhs.phi_soft),
  px_soft(rhs.px_soft),
  py_soft(rhs.py_soft),
  Et_muons_invis(rhs.Et_muon),
  phi_muons_invis(rhs.phi_muon),
  px_muons_invis(rhs.px_muon),
  py_muons_invis(rhs.py_muon),
  metSignif(rhs.metSignif)
{

  // Apply default values
  Reset();

}
// -------------------------------------------------------------------------------- //
MetVariable& MetVariable::operator=(const MetVariable &rhs)
{
  if(this != &rhs){
    ObjectVariable::operator=(rhs);
    flavor      = rhs.flavor;
    Et          = rhs.Et;
    phi         = rhs.phi;
    px          = rhs.px;
    py          = rhs.py;

    Et_truth  = rhs.Et_truth;
    px_truth  = rhs.px_truth;
    py_truth  = rhs.py_truth;

    Et_ele    = rhs.Et_ele;
    phi_ele   = rhs.phi_ele;
    px_ele    = rhs.px_ele;
    py_ele    = rhs.py_ele;

    Et_jet    = rhs.Et_jet;
    phi_jet   = rhs.phi_jet;
    px_jet    = rhs.px_jet;
    py_jet    = rhs.py_jet;

    Et_muon    = rhs.Et_muon;
    phi_muon   = rhs.phi_muon;
    px_muon    = rhs.px_muon;
    py_muon    = rhs.py_muon;

    Et_soft    = rhs.Et_soft;
    phi_soft   = rhs.phi_soft;
    px_soft    = rhs.px_soft;
    py_soft    = rhs.py_soft;

    Et_muons_invis    = rhs.Et_muons_invis;
    phi_muons_invis   = rhs.phi_muons_invis;
    px_muons_invis    = rhs.px_muons_invis;
    py_muons_invis    = rhs.py_muons_invis;

    metSignif   = rhs.metSignif;
  }
  return *this;
}
// -------------------------------------------------------------------------------- //
TLorentzVector MetVariable::getTruthTLV() const
{

  TLorentzVector tv;
  tv.SetPxPyPzE( this->px_truth,this->py_truth,0,this->Et_truth);
  return tv;

}
// -------------------------------------------------------------------------------- //
void MetVariable::Reset()
{

  this->Et  = 0;
  this->phi = 0;
  this->px  = 0;
  this->py  = 0;
  this->SetPxPyPzE(0,0,0,0);

  this->Et_truth = 0;
  this->px_truth = 0;
  this->py_truth = 0;

  this->Et_ele  = 0;
  this->phi_ele = 0;
  this->px_ele  = 0;
  this->py_ele  = 0;

  this->Et_muon  = 0;
  this->phi_muon = 0;
  this->px_muon  = 0;
  this->py_muon  = 0;

  this->Et_jet  = 0;
  this->phi_jet = 0;
  this->px_jet  = 0;
  this->py_jet  = 0;

  this->Et_soft  = 0;
  this->phi_soft = 0;
  this->px_soft  = 0;
  this->py_soft  = 0;

  this->Et_muons_invis  = 0;
  this->phi_muons_invis = 0;
  this->px_muons_invis  = 0;
  this->py_muons_invis  = 0;

  this->metSignif = 0;
  
}
// -------------------------------------------------------------------------------- //
MetVariable::~MetVariable()
{

}


