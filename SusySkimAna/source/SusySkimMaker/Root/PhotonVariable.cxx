#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/PhotonVariable.h"

// ----------------------------------------------------------------------- //
PhotonVariable::PhotonVariable()
{
  setDefault(IsoFCTightCaloOnly,false);
  setDefault(IsoFixedCutLoose,false);
  setDefault(IsoFixedCutTight,false);
  setDefault(topoetcone20,-1.0);
  setDefault(topoetcone30,-1.0);
  setDefault(topoetcone40,-1.0);
  setDefault(SUSY20_topoetcone20,-1.0);
  setDefault(SUSY20_topoetcone30,-1.0);
  setDefault(SUSY20_topoetcone40,-1.0);
  setDefault(SUSY20_topoetcone20NonCoreCone,-1.0);
  setDefault(SUSY20_topoetcone30NonCoreCone,-1.0);
  setDefault(SUSY20_topoetcone40NonCoreCone,-1.0);
  setDefault(signal,false);
  setDefault(tight,false);
  setDefault(passOR,false);
  setDefault(isConverted,false);
  setDefault(conversionRadius,-999.0);
  setDefault(conversionType,-1);
}
// ----------------------------------------------------------------------- //
PhotonVariable::PhotonVariable(const PhotonVariable& rhs) :
  ObjectVariable(rhs),
  IsoFCTightCaloOnly(rhs.IsoFCTightCaloOnly),
  IsoFixedCutLoose(rhs.IsoFixedCutLoose),
  IsoFixedCutTight(rhs.IsoFixedCutTight),
  topoetcone20(rhs.topoetcone20),
  topoetcone30(rhs.topoetcone30),
  topoetcone40(rhs.topoetcone40),
  SUSY20_topoetcone20(rhs.SUSY20_topoetcone20),
  SUSY20_topoetcone30(rhs.SUSY20_topoetcone30),
  SUSY20_topoetcone40(rhs.SUSY20_topoetcone40),
  SUSY20_topoetcone20NonCoreCone(rhs.SUSY20_topoetcone20NonCoreCone),
  SUSY20_topoetcone30NonCoreCone(rhs.SUSY20_topoetcone30NonCoreCone),
  SUSY20_topoetcone40NonCoreCone(rhs.SUSY20_topoetcone40NonCoreCone),
  signal(rhs.signal),
  tight(rhs.tight),
  passOR(rhs.passOR),
  isConverted(rhs.isConverted),
  conversionRadius(rhs.conversionRadius),
  conversionType(rhs.conversionType),
  recoSF(rhs.recoSF)
{

}
// ----------------------------------------------------------------------- //
PhotonVariable& PhotonVariable::operator=(const PhotonVariable& rhs)
{

  if( this != &rhs ){
    ObjectVariable::operator=(rhs);
    IsoFCTightCaloOnly       = rhs.IsoFCTightCaloOnly;
    IsoFixedCutLoose         = rhs.IsoFixedCutLoose;
    IsoFixedCutTight         = rhs.IsoFixedCutTight;
    topoetcone20             = rhs.topoetcone20;
    topoetcone30             = rhs.topoetcone30;
    topoetcone40             = rhs.topoetcone40;
    SUSY20_topoetcone20 = rhs.SUSY20_topoetcone20;
    SUSY20_topoetcone30 = rhs.SUSY20_topoetcone30;
    SUSY20_topoetcone40 = rhs.SUSY20_topoetcone40;
    SUSY20_topoetcone20NonCoreCone = rhs.SUSY20_topoetcone20NonCoreCone;
    SUSY20_topoetcone30NonCoreCone = rhs.SUSY20_topoetcone30NonCoreCone;
    SUSY20_topoetcone40NonCoreCone = rhs.SUSY20_topoetcone40NonCoreCone;
    signal                   = rhs.signal;
    tight                    = rhs.tight;
    passOR                   = rhs.passOR;
    isConverted              = rhs.isConverted;
    conversionRadius         = rhs.conversionRadius;
    conversionType           = rhs.conversionType;
    recoSF                   = rhs.recoSF;
  }

  return *this;

}
// ----------------------------------------------------------------------- //
float PhotonVariable::getRecoSF(const TString sys)
{

  auto it = this->recoSF.find(sys);

  if(it == this->recoSF.end()){

    auto nom = this->recoSF.find("");

    if( nom == this->recoSF.end() ){
      std::cout << " <PhotonVariable::getRecoSF> WARNING Request to reconstruction SF, but it doesn't exist for systematic: " << sys << " or nominal" << std::endl;
      return 1.0;
    }
    else{
      return nom->second;
    }

  }
  else{
    return it->second;
  }

}
// -------------------------------------------------------------------------------- //
double PhotonVariable::getTrigSF(const TString instance, const TString sys)
{
  double sf=1.;
  this->trigEff.getSF(instance,sys,sf);

  return sf; 
}
// ----------------------------------------------------------------------- //
void PhotonVariable::print() const
{

  MsgLog::INFO("PhotonVariable::print","Listing photon properties...");
  MsgLog::INFO("PhotonVariable::print","pT  :   %f",this->Pt() );
  MsgLog::INFO("PhotonVariable::print","Eta :   %f",this->Eta() );
  MsgLog::INFO("PhotonVariable::print","Phi :   %f",this->Phi());

}
// ----------------------------------------------------------------------- //
PhotonVariable::~PhotonVariable()
{
}
