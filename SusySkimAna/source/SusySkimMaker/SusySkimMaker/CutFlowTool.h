#ifndef NTUPLEMAKER_CUTFLOWTOOL_H
#define NTUPLEMAKER_CUTFLOWTOOL_H

#include <iostream>
#include <map>

#include "TString.h"
#include "TH1.h"
#include "TFile.h" 
#include <iomanip> 

class CutFlowTool
{

 private:
  void EventFill(TString stream_full,TString cutName,double weight);
  void PrintHist(TString name,TH1F* hist);
  TString getWeightedName(TString stream);
  TString getUnWeightedName(TString stream);

 public:
  CutFlowTool();
  ~CutFlowTool(){};

  void defineCutFlow(TString stream, TFile* outputFile, bool unweightedHist=true,TString sys=""); 
  void bookCut(TString stream_full,TString cutName,double weight=1.0,bool initialize=false);
  TH1F* GetCutFlowHist(TString stream);
  void Print(TString stream="");
  void Write();

  /**
   * Merge two cutflows defined in this class. The ordering matters, ie
   * stream_one will be put first, and stream two appended to the last bin of 
   * stream one. Returns <weighted,unweighted> pair of histograms. This should
   * probably be called at the end of your job
   */ 
  std::vector<std::pair<TH1*,TH1*>> mergeCutFlows(TString stream_one, TString stream_two);

  /**
   * Systematic list -- creates a cutflow for each entry in the cutflow
   */ 
  void setSysList(std::vector<TString> sysList){m_sysList=sysList;}

  ///
  /// Set global systematic state 
  ///
  void setSysState(TString sysState){m_sysState=sysState;}

 protected:

  ///
  /// Global systematic state
  ///
  TString                    m_sysState;
  std::vector<TString>       m_sysList;
  std::map<TString,TH1F*>    m_cutFlowMap;


};

#endif
