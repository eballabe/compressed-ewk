#ifndef SusySkimMaker_CheckGridSubmission_h
#define SusySkimMaker_CheckGridSubmission_h

// Rootcore include(s)
#include "SusySkimMaker/EventObject.h"

// C++ include(s)
#include <fstream>  
#include <iostream>
#include <utility> 

// Forward declaration(s)
class StatusCode;

using namespace std;

class CheckGridSubmission 
{

 public:
  CheckGridSubmission();
  ~CheckGridSubmission(){};

  ///
  /// Initialize this class for usage
  ///
  StatusCode initialize();

  ///
  /// Simple checks that our grid jobs will be able to find important information,
  /// E.g. cross sections, number of events, etc
  ///
  bool checkSamples(std::vector<TString> samples, bool isData, bool isAf2);

  // Data structures
  struct AMISampleStatus
  {
    AMISampleStatus() : prodsysStatus(""), 
                        nEvents(-1), 
                        sampleReady(true),
                        dsid(0) {}

    TString prodsysStatus;
    float nEvents;
    bool sampleReady;
    unsigned int dsid;

  };

  struct RucioSampleStatus
  {
    RucioSampleStatus() : nEvents(-1) {}
    float nEvents;
  };

  struct JobStatus{

    std::vector<float> crossSections;
    std::vector<TString> samples;
    std::vector<AMISampleStatus> amiResults;
    std::vector<RucioSampleStatus> rucioResults;
    std::vector<bool> validPRWProfile;
    std::vector<bool> isData;

    void addStatus(TString sample,
                   float crossSection,
                   AMISampleStatus amiResult,
                   RucioSampleStatus rucioResult,
                   bool _isData,
                   bool _validPRWProfile){
      samples.push_back(sample);
      crossSections.push_back(crossSection);
      amiResults.push_back(amiResult);
      rucioResults.push_back(rucioResult);
      isData.push_back(_isData);
      validPRWProfile.push_back(_validPRWProfile);
    }

    void printStatus(int i) {
      if ( amiResults[i].dsid==0) {
        std::cout << "        - Nothing to check for period containers" << std::endl;
        return;
      }
      if (!(isData[i])) {
        std::cout << "        - Cross section:            " << crossSections[i] << std::endl;
      }
      std::cout << "        - Passing PRW checks:       " << validPRWProfile[i] << std::endl;      
      std::cout << "        - Number of events (AMI):   " << amiResults[i].nEvents << std::endl;
      std::cout << "        - Number of events (rucio): ";
      if (rucioResults[i].nEvents > 0) {
        std::cout << amiResults[i].nEvents << std::endl;
      } else {
        std::cout << "[not checked]" << std::endl;
      }
      std::cout << "        - AMI prodsysStatus :       " << amiResults[i].prodsysStatus << std::endl;
    }

    int size(){ return samples.size(); }

  };

  //
  JobStatus* getGoodJobs(){ return m_goodJobs; }
  JobStatus* getBadJobs(){ return m_badJobs; }

  void skipPRWCheck(bool skip) { m_disablePRW = skip; };
  bool getCheckStatus(){return m_checkStatus;}

 private:

  ///
  /// Retrieve information from AMI
  ///
  AMISampleStatus getAMIInfo(TString sample, bool isData);

  ///
  /// Retrieve information from Rucio
  ///
  RucioSampleStatus getRucioInfo(TString sample);

  ///
  /// Check that the sample has PRW profiles
  ///
  bool checkPRW(EventObject* evt, unsigned int sampleID, TString sample, bool isData);

  ///
  /// Try to final the final state for a signal process, and then cache that value
  /// Returns -1 when a cross section cannot be found
  ///
  float getXSec(EventObject* evt,unsigned int sampleID,unsigned int& cached_finalState);

  /// From a sample, get the corresponding ID
  unsigned int getSampleID(TString sample);

 /// Methods for initialization 
 private:
  ///
  /// Reads in rtag <-> run number map
  ///
  StatusCode readRtagRunNumbers();

  /// Check for a grid proxy, and set m_checkGridInfo true if a proxy is found, 
  /// otherwise set it to false;
  ///
  StatusCode checkGridProxy();
  

 private:
  
  // Flags
  TString m_scope;

  /// Don't check PRW, steered through deep.conf
  bool m_disablePRW;

  /// Initialized as false, when calling checkSamples(...),
  /// if all checks succeed, set to true.
  bool m_checkStatus;

  ///
  /// True if a proxy is found 
  /// False otherwise
  bool m_checkGridInfo;

  ///
  /// Fill in readRtagRunNumbers
  ///
  std::map<TString,int> m_rTagRunNumbers;

  ///
  /// Keep track of any improperly prepared samples found in these checks
  ///
  JobStatus* m_badJobs;
  JobStatus* m_goodJobs;



};

#endif
