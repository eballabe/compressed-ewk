#ifndef SusySkimMaker_PhotonObject_h
#define SusySkimMaker_PhotonObject_h

// Framework includes
#include "SusySkimMaker/BaseObject.h"
#include "SusySkimMaker/PhotonVariable.h"
#include "AsgTools/StatusCode.h"
#include "SusySkimMaker/TrackObject.h"

// xAOD EDM includes
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include <ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h>
#include <ElectronPhotonSelectorTools/egammaPIDdefs.h>

// Forward declarations
class TreeMaker;
class TString;

//
typedef std::vector<PhotonVariable*> PhotonVector;

class PhotonObject
{

 public:

  ///
  /// Constructor
  ///
  PhotonObject();

  ///
  /// Destructor
  ///
  virtual ~PhotonObject();

  ///
  /// Initialization method. Required
  ///
  StatusCode init(TreeMaker*& treeMaker);

  ///
  /// Fills the output skim photon variable
  ///
  StatusCode fillPhotonContainer( xAOD::Photon* photon_xAOD, 
          const xAOD::VertexContainer* primVertex,
          const xAOD::EventInfo* eventInfo,
				  const std::map<TString,float> recoSF, 
				  const std::map<TString,bool> trigMap, 
				  const MCCorrContainer* trigEff,
				  std::string sysName );

  ///
  /// For filliing track links
  ///
  void setTrackObject(TrackObject* trackObject){ m_trackObject=trackObject; }

  ///
  /// Fills the output skim information from a truth particle input
  ///
  StatusCode fillPhotonContainerTruth(xAOD::TruthParticle* photon_xAOD, std::string sys_name="");

  ///
  /// Photon selector tools
  ///
  AsgPhotonIsEMSelector *m_photon_tight;

  ///
  /// Interface to isolation tools and in principle anything else common to leptons
  ///
  BaseObject* m_baseObject;

  ///
  /// Accessor
  ///
  const PhotonVector* getObj(TString sysName);

  ///
  /// Reset. Should be called after each event
  ///
  void Reset();

 protected:

  ///
  /// How to convert from MeV
  ///
  float m_convertFromMeV;

  ///
  /// TrackObject tool
  ///
  TrackObject* m_trackObject;

  /// 
  /// Global map to store all systematic variations
  ///
  std::map<TString,PhotonVector*>   m_photonVectorMap;

};

#endif
