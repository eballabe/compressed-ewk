#ifndef SusySkimMaker_BaseObject_h
#define SusySkimMaker_BaseObject_h

#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationSelection/IsolationLowPtPLVTool.h"
#include "IFFTruthClassifier/IFFTruthClassifier.h"

class BaseObject
{

 public:

  ///
  /// Default constructor
  ///
  BaseObject();

  ///
  /// Destructor
  ///
  virtual ~BaseObject();

  ///
  /// Initialize function, should always be called
  ///
  StatusCode initialize(TString stream);

  ///
  /// Disable the PLV WP evaluation in IsolationSelectionTool
  /// 
  void disablePLVWPs(xAOD::Type::ObjectType type);

  ///
  /// Disable the PLIV WP evaluation in IsolationSelectionTool
  ///
  void disablePLIVWPs(xAOD::Type::ObjectType type);

  ///
  /// Isolation tool
  ///
  CP::IsolationSelectionTool* isoTool;

  ///
  /// IFF lepton origin classifier tool
  ///
  IFFTruthClassifier *IFFClassifier;

  ///
  /// For LowPtPLV score calculation
  ///
  CP::IsolationLowPtPLVTool* isoLowPtPLVTool;

  ///
  /// Have the PLV WPs been disabled because the file doesn't have the PLV variables?
  ///
  bool havePLVWPsRemoved;

  ///
  /// Have the PLIV WPs been disabled because the file doesn't have the PLV variables?
  ///
  bool havePLIVWPsRemoved;

};

#endif
