#ifndef SusySkimMaker_OBSERVABLES_H
#define SusySkimMaker_OBSERVABLES_H

// ROOT includes
#include "TLorentzVector.h"
#include "MetVariable.h"

// Forward declarations
class Objects;
class MuonVariable;
class ElectronVariable;
class LeptonVariable;
class TrackVariable;

class Observables
{

 public:
  Observables();
  ~Observables();

  //
  struct RecoBoson {
    TLorentzVector parent;
    std::vector<LeptonVariable*> children;
  };

  struct FatJet {
    TLorentzVector jet;
    int nSubJets;
    std::vector<TLorentzVector> pSubJets; // 4-vectors of the subjets
  };

  typedef enum {AMT2, AMT2B, MT2LBLB, AMT2BWEIGHT, MT2TAU, MT2LJ, MT2LL} mt2type;


  bool reconstructZZ(const Objects* obj, RecoBoson& Zleading, RecoBoson& Zsubleading, bool useBaseline=false);
  bool reconstructWZ(const Objects* obj, RecoBoson& Wboson, RecoBoson& Zboson, bool useBaseline=false);
  bool reconstructSSWW(const Objects* obj);
  std::vector <TLorentzVector> solveInvisiblePz_2Bdecay(const TLorentzVector &p1, TLorentzVector p2_pt, const double &mX, const double &m1, const double &m2);
  TLorentzVector reconstructNeutrino(const Objects* obj, const LeptonVariable* lep, float WMass, int solution_bit);

  bool isSFOS(LeptonVariable* lep1,LeptonVariable* lep2);
  bool isSF(LeptonVariable* lep1,LeptonVariable* lep2);
  bool isOS(LeptonVariable* lep1,LeptonVariable* lep2);
  float getMeff(const Objects* obj, unsigned int njet = 0, float minjetpt = 30.);
  float getTruthMeff(const Objects* obj, unsigned int njet = 0, float minjetpt = 30.);
  float getMeffRel(const Objects* obj, unsigned int njet = 0, float minjetpt = 30.);
  float getMetRel(const Objects* obj, float DeltaPhi);
  float getMt(const Objects* obj, bool useBaseline=false, unsigned int lepindex=0, bool useTrackMET=false);
  float getMt(const MetVariable& met, LeptonVariable* lep);
  float getMtWg(const Objects* obj, unsigned int photon_idx);
  float getTruthMt(const Objects* obj);
  float getHt(const Objects* obj,unsigned int njet = 0, float minjetpt = 30.);
  float getTruthHt(const Objects* obj,unsigned int njet = 0, float minjetpt = 30.);
  float getMt2(const Objects* obj, mt2type type, float trial_invis=0., bool useBaseline=false, bool useTrackMET=false);
  float getMct(const Objects* obj);
  float getMct2(const Objects* obj);
  float getMinMtb(const Objects* obj, float minjetpt);
  float getAplanarity(const Objects* obj, unsigned int njet, unsigned int nlep, float minjetpt, bool useBaseline=false);
  float getTopness(const Objects* obj);
  float getMinDrBJet(const Objects* obj, const TLorentzVector* ref, float minjetpt);
  float getMinDrJet(const Objects* obj, const TLorentzVector* ref, float minjetpt);
  float getMll(const Objects* obj, bool useBaseline=false);
  float getMlj(const Objects* obj, unsigned int njet = 1, bool useBaseline=false );
  float getDeltaPhiNearest(const Objects* obj, const TLorentzVector* ref, bool useBaseline=false);
  float getHadWMass(const Objects* obj,float& wPt);
  int getNBJets(const Objects*obj, float jet_threshold=30.0);
  int getNJets(const Objects*obj, float jet_threshold=30.0);
  int getNTruthJets(const Objects*obj, float jet_threshold=30.0);
  int getNTruthBJets(const Objects*obj, float jet_thresholdi=30.0);
  int getTTbar2LTagger(const Objects* obj, float& WMass1, float& WMass2, float& TopMass1, float& TopMass2, float& ttReso);

  std::vector<FatJet> getReclusteredTrimmedFatJets(const Objects* obj, float R, float fcut);

  float getMSqTauTau(const Objects* obj, int version=2, bool useTrackMET=false);
  float getRll(const Objects* obj);
  float getDPhiJ1Met(const Objects* obj, bool useTrackMET=false);
  float getDPhiJNMet(const Objects* obj, unsigned int jetIndex, float jet_threshold=30.0, bool useTrackMET=false);
  float getDPhibJetMet(const Objects* obj);

  float getDPhiTrkMet(int iTrk, const Objects* obj, bool useTrackMET=false); // JEFF

  float getDPhilMet(const Objects* obj);
  float getminDPhiJetMet(const Objects* obj, int maxNJet, float jet_threshold=30.0, bool useTrackMET=false);
  float getminDPhiJetInvisibleMet(const Objects* obj, int maxNJet, float jet_threshold=30.0, MetVariable::MetFlavor met_flavor=MetVariable::MetFlavor::INVISIBLE_LEP);
  float getminDPhiJetDileptonMet(const Objects* obj, int maxNJet, float jet_threshold=30.0, bool useTrackMET=false);
  float getminDPhiJetPhotonMet(const Objects* obj, int maxNJet, float jet_threshold=30.0, bool useTrackMET=false);
  float getminDPhibJetMet(const Objects* obj);
  float getminDPhilMet(const Objects* obj);
  float getRjlOverEl(const Objects* obj);
  float getLepCosThetaLab(const Objects* obj);
  float getLepCosThetaCoM(const Objects* obj);
  double getLepPhotonTopoEtclus(const Objects* obj, TrackVariable* track, double deltaR = 0.4, bool useBaseline=false, bool useOnlyCore=false);
  double getLepPhotonTrackPt(const Objects* obj, TrackVariable* track, double deltaR = 0.4, bool useBaseline=false, bool usePhotonConv=false);
  double getLepPhotonMinDR(const Objects* obj, TrackVariable* track, bool useBaseline=false);
  bool isMuonTrk(const Objects* obj, TrackVariable* track, bool useBaseline=false);
  bool isElectronTrk(const Objects* obj, TrackVariable* track, bool useBaseline=false);
  bool isPhotonTrk(const Objects* obj, TrackVariable* track, bool useBaseline=false);

  // Some cleaning checks
  float EnergyWeightedTime(const Objects* obj,unsigned int nJet);
  float minDeltaPhiMetPileUpJets(const Objects* obj);
  float averageTiming(const Objects* obj,unsigned int nJet);
  float passDeadTile(const Objects* obj,float& eta, float& phi);

  // Reweighting tools
  double getLifetimeWeight(const Objects* obj, double genTau, double rwtTau);

  // Casting functions
  MuonVariable* cast_to_muon(LeptonVariable* lep);
  ElectronVariable* cast_to_electron(LeptonVariable* lep);

  void setDbgLevel(int dbg){m_dbg=dbg;}

protected:
  int m_dbg;

  bool m_useRapidity = true; // this enable rapidity in the ROOT::TLorentzVector::deltaR

};

#endif
