#ifndef SusySkimMaker_JetVariable_h
#define SusySkimMaker_JetVariable_h

// Rootcore
//#include "SusySkimMaker/ObjectVariable.h"
#include "SusySkimMaker/TruthVariable.h"

class JetVariable : public ObjectVariable
{

 public:

  ///
  /// Default Constructor
  ///
  JetVariable();

  ///
  /// Desctructor
  ///
  virtual ~JetVariable(){};

  ///
  /// Copy constructor
  ///
  JetVariable(const JetVariable&);

  ///
  /// Assignment operator
  ///
  JetVariable& operator=(const JetVariable&);

  // JVT
  float jvt;

  //
  // B-tagging
  //  => MV2 information: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTaggingBenchmarks#MV2c20_tagger_antikt4topoem_jets
  //  => Rel 21 information: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21
  float mv2c10;
  float dl1;
  float dl1r;
  float btagEff;

  ///
  /// Result of b-jet classification from SUSYTools
  bool bjet;

  // Jet classification, as coming from SUSYTools methods
  bool signal;
  bool bad;
  bool badTile;
  bool passOR;
  bool passDRcut;

  ///
  /// B-tagging and mis-identification scale factors, key=systematic name, value=scale factor
  /// Note that you should call getFtSF(...) to retrieve a given scale factor
  ///
  std::map<TString,float> FT_SF;

  ///
  /// JVT Data/MC scale-factors. Same format as b-tagging SFs, except you should call getJVTSF(...)
  ///
  std::map<TString,float> JVT_SF;

  // Truth information
  // Is there a B-Hadron in this jet ? => label it 5  (b)
  // Is there a C-Hadron in this jet? => label it 4  (c)
  // Is there a tau in this jet =? label it 15  (tau)
  // Otherwise label it 0  (light)
  int truthLabel;

  /// Number of tracks associated to jet with pT > 500 MEV
  int nTrk;

  float tileEnergy;
  float EMFrac;
  float HECFrac; 
  float LArQuality;
  float HECQuality; 
  float Timing; 
  float sumpttrk;
  float FracSamplingMax;
  float NegativeE;
  float AverageLArQF;
  float FracSamplingMaxIndex;
  float Width;
  
  // Substructure variables
  float D2;
  float Tau32;

  // (Ghost-)associated track jets
  std::vector<TLorentzVector> GATrackJets;
  int nGATrackJets;
  int nGATrackBJets60Eff;
  int nGATrackBJets70Eff;
  int nGATrackBJets77Eff;
  int nGATrackBJets85Eff;
  int nGATrackBJets60Eff_DL1r;
  int nGATrackBJets70Eff_DL1r;
  int nGATrackBJets77Eff_DL1r;
  int nGATrackBJets85Eff_DL1r;

  // Flags for the boosted boson tagging result
  //  Nominal 50% eff WP
  bool passWTag50;
  bool passWTag50_mass;
  bool passWTag50_sub;
  bool passWTag50_nTrk;
  bool passZTag50;
  bool passZTag50_mass;
  bool passZTag50_sub;
  bool passZTag50_nTrk;
  float bjt_wsf50;
  float bjt_zsf50;

  //  Inclusive 50% eff WP
  bool passWTagIncl50;
  bool passWTagIncl50_mass;
  bool passWTagIncl50_sub;
  bool passWTagIncl50_nTrk;
  bool passZTagIncl50;
  bool passZTagIncl50_mass;
  bool passZTagIncl50_sub;
  bool passZTagIncl50_nTrk;
  float bjt_wsfIncl50;
  float bjt_zsfIncl50;
  // Aux variables to compute the boson tagging SF systematics
  float Nominal_bjt_weffIncl50;
  float Nominal_bjt_zeffIncl50;
  float Nominal_pt;
  float Nominal_eta;
  float Nominal_phi;
  float Nominal_m;

  //  Nominal 80% eff WP
  bool passWTag80;
  bool passWTag80_mass;
  bool passWTag80_sub;
  bool passWTag80_nTrk;
  bool passZTag80;
  bool passZTag80_mass;
  bool passZTag80_sub;
  bool passZTag80_nTrk;
  float bjt_wsf80;
  float bjt_zsf80;

  // Xbb scores
  float XbbScoreQCD;
  float XbbScoreTop;
  float XbbScoreHiggs;


 //
 // Methods to retrieve and/or process the above variables
 // Note that you should protect each method with a //! after its declaration,
 // such that it doesn't get written
 //
 public:

  /// 
  /// Truth link
  ///
  mutable TruthVariable* truthLink;

  //
  bool hasTruthLink(); //!

  ///
  /// Method to get the flavor tagging SF for a given systematic.
  /// If the systematic is not found, it searches for the nominal 
  /// value and returns this, if found. In the event the flavor
  /// tagging SF is not found for the systematic or nominal, the
  /// function return 1.0
  ///
  float getFtSF(const TString sys); //! 

  ///
  /// Same forgot as b-tagging SF
  ///
  float getJVTSF(const TString sys); //!

  ///
  /// Print event into log file.
  ///
  void dumpJet(); //!

  ClassDef(JetVariable,7);

};

#endif
