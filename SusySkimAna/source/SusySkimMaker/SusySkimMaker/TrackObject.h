#ifndef SusySkimMaker_TrackObject_h
#define SusySkimMaker_TrackObject_h

/*
 *  Base class for any object which utilizes xAOD::TrackParticle
 *  Can use the methods in this class to get information about the
 *  track parameters, ie d0, z0, etc
 *
 *  INFO: Being extended to store Tracks
 *
 **/

#include "xAODRootAccess/TEvent.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "SusySkimMaker/TrackVariable.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "InDetTrackSystematicsTools/InDetTrackSmearingTool.h"
#include "PATInterfaces/SystematicVariation.h"

// MC Truth
#include "MCTruthClassifier/MCTruthClassifier.h"

// Forward dceclarations
class TreeMaker;

// Typedefs
typedef std::vector<TrackVariable*> TrackVector;
typedef std::vector<ObjectVariable*> ObjectVector;

//
class TrackObject
{

 public:
   TrackObject();
   virtual ~TrackObject() {};

   ///
   /// Initialization method for this class. Connects each TrackletVariable in global m_muonVectorMap map
   /// to output trees found in TreeMaker class
   ///
   StatusCode init(TreeMaker*& treeMaker,xAOD::TEvent* event);


   ///
   /// Fill
   ///
   StatusCode fillTrackContainer(const xAOD::VertexContainer* primVertex,
                                 const xAOD::EventInfo* eventInfo,
                                 const xAOD::TruthParticleContainer* truthParticles,
				 const CP::SystematicSet& systSet);


   ///
   /// Standalone fill
   ///
   TrackVariable* fillTrack(const xAOD::TrackParticle* track,
                            const xAOD::VertexContainer* primVertex,
                            const xAOD::EventInfo* eventInfo,
                            TrackVariable::TrackType type,
                            std::string sys_name="");


   static bool isHardScatVrtx(const xAOD::TruthVertex* pVert);

   /*
    * Get uncertainty on pT measurement, taking into account correlations with other track parameters
    **/
   static float getPtErr(const xAOD::TrackParticle* track);

   /*
    * Impact parameter d0 and its error
    **/
   static float getD0(const xAOD::TrackParticle* track);
   static float getD0Err(const xAOD::TrackParticle* track,const xAOD::EventInfo* eventInfo);

   /*
    * Impact parameter z0 and its error
    **/
   static float getZ0(const xAOD::TrackParticle* track,const xAOD::VertexContainer* primVertex);
   static float getZ0Err(const xAOD::TrackParticle* track);

   /*
    * Track hits
    **/
   static uint8_t getNPixHitsPlusDeadSensors(const xAOD::TrackParticle* track);
   static bool getPassBL(const xAOD::TrackParticle* track);


   ///
   const TrackVector* getObj(TString sysName);
   const ObjectVector* getCaloObj(TString sysName);

   ///
   /// Clear internal TrackVectors inside
   ///
   void Reset();

 protected:

   ///
   /// Initialize all tools needed for this class
   ///
   StatusCode init_tools();

   ///
   /// Fill standard track information
   ///
   void fillTrackInfo(TrackVariable*& trk,const xAOD::TrackParticle* xAODTrk,
                      const xAOD::VertexContainer* primVertex,
                      const xAOD::EventInfo* eventInfo,
                      const xAOD::TruthParticleContainer* truthParticles);

   ///
   /// Find the secondary track that was
   /// associated to primary track using
   // the VertexContainer
   void LinkSecondaryTrack(TrackVariable*& trk,
                           const xAOD::TrackParticle* primaryTrack,
                           const xAOD::TrackParticleContainer* secondaryTracks,
                           const xAOD::VertexContainer* secVertex,
                           const xAOD::VertexContainer* primVertex,
                           const xAOD::EventInfo* eventInfo,
                           const xAOD::TruthParticleContainer* truthParticles );

   void LinkV0Vertex(TrackVariable*& trk,
                     const xAOD::TrackParticle* primaryTrack,
                     const xAOD::VertexContainer* secVertex );



    double getTrackIsolation(const xAOD::TrackParticle* track,
                             const xAOD::VertexContainer* vertex,
                             double dR);

  ///
  /// Save calorimeter clusters
  ///
  StatusCode saveCaloClusters(std::string sys_name);

  //
  // Apply smearing to track d0, z0 in xAOD::TrackContainer
  //
  const xAOD::TrackParticleContainer* smearTracks(const xAOD::TrackParticleContainer* primaryTrack,
						 const CP::SystematicSet& systSet);

   ///
   /// Global std::map to store TrackletVectors for all systematic uncertainties
   ///
   std::map<TString,TrackVector*>  m_trackVectorMap;
   std::map<TString,ObjectVector*>  m_caloVectorMap;
   
   ///
   /// MC Truth classifier objects
   ///
   MCTruthClassifier*             m_truthClassifier;

 private:
   ///
   /// Convert units from MeV
   ///
   float m_convertFromMeV;

   ///
   /// TEvent object
   ///
   xAOD::TEvent* m_event;

   ///
   /// Primary track container name, read in the CentralDB field
   ///
   std::vector<TString> m_primaryTrackContainer;

   ///
   /// Secondary track container name, read in from CentralDB field
   ///
   TString m_secondaryTrackContainer;

   ///
   /// Vertex container name, that is used to associate a secondary
   /// track to the primary track container
   ///
   TString m_priSecVtxContainer;

   ///
   /// Write out egamma clusters
   ///
   bool m_saveEgammaClusters;

   //
   // Track selection tool
   //
   InDet::InDetTrackSelectionTool* m_trkSelLooseTool;
   InDet::InDetTrackSelectionTool* m_trkSelTightPrimaryTool;

   //
   // Track smearing tool
   //
   InDet::InDetTrackSmearingTool* m_smearTool;

};

#endif
