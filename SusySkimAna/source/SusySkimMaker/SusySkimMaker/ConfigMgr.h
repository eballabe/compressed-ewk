#ifndef SusySkimMaker_ConfigMgr_h
#define SusySkimMaker_ConfigMgr_h

// RootCore
#include "SusySkimMaker/BaseHeader.h"
#include "SusySkimMaker/Systematics.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/HistMaker.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// C++
#include <map>
#include <utility> 
#include <vector>

/*
  Top level class. Initializes all other tools and holds all parameters which define the job
*/

///
///
class CentralDB;
class StatusCode;
///
///

class ConfigMgr
{

 public:

  ///
  /// Constructor
  ///
  ConfigMgr();

  ///
  /// Destructor
  ///
  ~ConfigMgr();

 public:

  void Log();

  ///
  ///
  /// Initialize tools for running in over xAODs and store output into skims.
  /// Note that some of the tools requires flags in this class to be set (i.e. isData)
  /// so you should call this at the end of configuring this tool.
  /// 
  StatusCode create_xAOD_tools();
  StatusCode init_xAOD_running(TFile* skimFile,
                               xAOD::TEvent* event,
                               float nEvents,
                               int writeSkims,
                               int writeTrees,
                               int isData,
                               int isAf2,
                               TString sysSetName,
                               bool truthOnly,
			       int  dsid,
			       bool doFatjets=false,
			       bool doTrackjets=false,
                               TString SUSYTools="");

  ///
  /// Initialize for running over the skims
  ///  
  bool init_skim_running(TFile*& file);

  ///
  /// TreeMaker tool: used to create output trees and cutflow histograms.
  /// 
  TreeMaker* treeMaker;

  ///
  /// Histogram stream
  ///
  HistMaker* histMaker;

  ///
  /// Cutflow tool
  ///
  CutFlowTool* cutflow;

  ///
  /// Meta data logger
  ///
  MetaData* metaData;

  ///
  /// Central data-base. Reads in the configuration for the run and used to communicate it to all
  /// other classes
  ///
  CentralDB* centralDB;

  ///
  /// Objects class: used to store objects and apply cuts to them
  ///
  Objects* obj;

  ///
  /// ObjectTools class: used to get xAOD objects and interface them to Object classes
  ///
  ObjectTools* objectTools;

  ///
  /// Class to handle systematics
  ///
  Systematics* systematics;
  
  ///
  /// SUSYTools: main tool to select, calibrate and apply systematic variables to xAOD objects
  ///
  ST::SUSYObjDef_xAOD  susyObj;
  ST::SUSYObjDef_xAOD  susyObjLoose;
  ST::SUSYObjDef_xAOD  susyObjTighter;
  ST::SUSYObjDef_xAOD  susyObjTenacious;


  std::vector<TString> sysWeights;
  std::vector<TString> sysLeptonWeights;
  std::vector<TString> sysMuonTrigWeights;
  std::vector<TString> sysJetWeights;

  std::vector<ST::SystInfo> sysInfoKinematics;
  std::vector<ST::SystInfo> sysInfoWeights;

  ///
  /// Configure tools to use these trigger chains.
  /// Notifys Systematics and ObjectTools.
  /// Main use is for trigger SFs.
  /// When this is used, rather than calling in standalone mode
  /// is that the systematics are handled in an automatic way
  /// Note you can call this funciton any number of times, if you are
  /// e.g. taking an OR between trigger bits and need SF1*SF2, etc
  ///
  void addTriggerAna(TString SF,int run_start=-1,int run_end=-1, TString branchName="");
  void doTriggerAna();

  // Give asscess to the lepton which was matched 
  std::vector<LeptonVariable*> getTrigAnaMatchedLepton(TString branchName);

  ///
  /// Set systematic state of tools. Stores global systematic state and distributes it 
  /// to all other tools
  ///
  void setSysState(TString sysState);
  void setSystematicSet(const CP::SystematicSet sysSet);

  ///
  /// Return global systematic state of this and all tools this classes uses
  ///
  const TString getSysState() const { return m_sysState; }

  ///
  /// Do weight systematics
  ///
  void doWeightSystematics(bool useTrackJets=false, TString trigChains="");

  ///
  /// Reset tools which depend on events, and not systematic uncertainties. Fill an event counter
  ///
  void Reset_perEvt();

  ///
  /// Reset tools which depend on systematics.
  ///
  void Reset_perSys();

  /// 
  /// Return global event counter. Filled in Reset_perEvt()
  ///
  const TH1F* getEvtCounter(){return m_eventCounter;}

  /// 
  /// Return global counter for original AOD sumOfWeights. Set by xAODEventLooper::fileExecute()
  ///
  const TH1F* getSOWHist(){return m_sumOfWeights;}

  /// 
  /// Return global counter for original AOD nEvents. Set by xAODEventLooper::fileExecute()
  ///
  const TH1F* getNEventsHist(){return m_origNEvents;}


  ///
  /// Return tree storing the CutBookKeeper info
  ///
  const TTree* getCBKTree(){return m_CBKTree;}

  ///
  /// Return the SUSYTools configuration file used to init susyObj
  ///
  TString getSUSYToolsConfigFile() const { return m_SUSYToolsConfigFile; }

  ///
  /// Fill eventCounter files original totalSOW
  ///
  void fillEvtCounterOrigSOW(float origSOW);

  ///
  /// Fill eventCounter files original totalNEvents
  ///
  void fillEvtCounterOrigNEvents(float origNEvents);

  ///
  /// Fill the CutBookKeeper tree from the CutBookKeepers
  ///
  void fillCBKTree(const xAOD::CutBookkeeperContainer* cbks, TString inputFile="");

  ///
  /// Return if the file is data
  ///
  bool isData(){ return m_isData; }

  ///
  /// Return the DSID (mcChannelNumber) of the file
  ///
  int  getDSID(){   return m_dsid; }

  ///
  /// Get/Set the derivation name for the sample
  ///
  TString getDerivName() { return m_derivName; } 
  void    setDerivName(TString derivName) { m_derivName = derivName; } 


 public:
  struct TriggerAna{
    int run_start;
    int run_end;
    TString trigChainSF;
    TString branchName;
    //LeptonVariable* matchedLepton;
    std::vector<LeptonVariable*> matchedLeptons;
    std::vector<TString> splitTrigChain;
  };

 protected:
   TString                                       m_sysState;
   TString                                       m_SUSYToolsConfigFile;
   TH1F*                                         m_eventCounter;
   TH1F*                                         m_sumOfWeights;
   TH1F*                                         m_origNEvents;
   bool                                          m_isData;
   int                                           m_dsid;
   TString                                       m_derivName;
   TTree*                                        m_CBKTree;

 
   //
   std::map<TString,std::vector<TriggerAna*>> m_anaTrig;

   // Global map to keep track of which input files were used 
   // to fill the CBK TTree, so it can detect duplicates upstream 
   // of the users merge
   std::map<TString,bool> m_cbkInputFiles;


};


#endif
