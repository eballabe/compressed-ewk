#ifndef SusySkimMaker_JetObject_h
#define SusySkimMaker_JetObject_h

// Rootcore
#include "SusySkimMaker/JetVariable.h"
#include "SusySkimMaker/BaseHeader.h"

// xAOD
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/JetAttributes.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
// We should migrate this too, but requires source code chanegs
//#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "BoostedJetTaggers/SmoothedWZTagger.h"

// MC Truth
#include "MCTruthClassifier/MCTruthClassifier.h"
#include "xAODTracking/VertexContainer.h"

// b-tagging emulation
#include <TRandom3.h>

typedef std::vector<JetVariable*> JetVector;

class TreeMaker;

class JetObject
{

 private:

  ///
  /// Initialize tools needed for this class.
  ///
  StatusCode init_tools(bool isData, TString jetType="jets"); 

 public:

  ///
  /// Default constructor
  ///
  JetObject();

  ///
  /// Destructor
  ///
  virtual ~JetObject();

  ///
  /// Initialization method, see MuonObject
  ///
  StatusCode init(TreeMaker*& treeMaker, bool isData=false, TString jetType="jets");

  ///
  /// Fill JetVariable members
  ///
  void fillJetContainer(xAOD::Jet* jet_xAOD,const xAOD::VertexContainer* vertices,std::string sys_name,std::map<TString,float> FT_SF,std::map<TString,float> JVT_SF,std::map<TString,bool> trigMap);

  ///
  /// Fill JetVariable members from fat jets
  ///
  void fillFatjetContainer(xAOD::Jet* fatjet_xAOD, 
			   const xAOD::JetContainer* trackjets_xAOD,
			   const xAOD::VertexContainer* vertices, 
			   std::string sys_name);

  ///
  /// Fill JetVariable members from track jets
  ///
  void fillTrackjetContainer(xAOD::Jet* trackjet_xAOD,
			     std::string sys_name, 
			     std::map<TString,float> FT_SF);

  ///
  /// Fill JetVariable members from truth jets
  ///
  void fillJetContainerTruth(xAOD::Jet* jet_xAOD,std::string sys_name="");

  ///
  /// Fill JetVariable members from truth fat jets
  ///
  void fillFatjetContainerTruth(xAOD::Jet* fatjet_xAOD,std::string sys_name="");

  ///
  /// Fill into JetVariable 'default' variables: i.e. Timing, LAr/HEC Quality, etc
  ///
  void fillDetailedJetVariables(xAOD::Jet* jet_xAOD, JetVariable*& jet);

  ///
  /// Number of tracks associated to this jet with pT>500 MeV
  ///
  int getNumTrkPt500(xAOD::Jet* jet_xAOD,const xAOD::VertexContainer* vertices);
  int getFatjetNumTrkPt500(xAOD::Jet* jet_xAOD,const xAOD::VertexContainer* vertices);

  ///
  /// Truth classification for b-jets
  ///
  bool isTruthBJet(xAOD::Jet* jet_xAOD, double bTagEfficiency);

  ///
  /// Get b-jet efficiency
  ///
  double getBTagEfficiency(xAOD::Jet* jet_xAOD);

  /// 
  /// Returns a const JetVector for a given systematic. 
  /// A null pointer is returned in the event nothing is found
  ///
  const JetVector* getObj(TString sysName);

  ///
  /// Fill b-tagging information. First check that the btagging container exists,
  /// and if it doesn't fill with -1 
  ///
  void fillBTaggingInfo(xAOD::Jet* jet_xAOD, JetVariable*& jetOut);
  void fillTrackjetBTaggingInfo(xAOD::Jet* fatjet_xAOD, JetVariable*& fatjet);
  bool fillFatjetBTaggingInfo(xAOD::Jet* fatjet_xAOD, JetVariable*& fatjet);
  void fillFatjetBTaggingInfo_DRMatch(xAOD::Jet* fatjet_xAOD, 
				      JetVariable*& fatjet,
				      const xAOD::JetContainer* trackjets_xAOD,
				      const float DRthresh);
  void fillFatjetBTaggingInfo_Impl(const xAOD::Jet* trackjet_xAOD, JetVariable*& fatjet);

  ///
  /// Clear all vectors in m_jetVectorMap
  ///
  void Reset();

 protected:

  ///
  /// Global std::map used to store all JetVectors for
  /// provided systematics
  ///
  std::map<TString,JetVector*>   m_jetVectorMap;

  ///
  /// Conversion factor from MeV to desired units
  ///
  float                          m_convertFromMeV;

  ///
  /// MC Truth classifier objects
  ///
  MCTruthClassifier*             m_truthClassifier;

  ///
  /// B-tagging efficiency tool to get efficiencies for b-tagging emulation at truth level
  ///
  asg::AnaToolHandle<IBTaggingEfficiencyTool> m_btagEffTool;

  ///
  /// B-tagging emulation random number
  ///
  TRandom3*                      m_rnd;

  ///
  /// Turn on/off b-tagging emulation at truth level. Controlled by deep.conf
  ///
  bool                           m_doTruthBtaggingEmulation;

  ///
  /// Write out additional variables, typically only for specific studies
  /// so turned off by default. Can be steering by deep.conf
  ///
  bool                           m_writeDetailedSkim;

  /// When false don't store any b-tagging information
  /// Useful when the derivation doesn't store b-tagging information
  /// Default true
  /// Steered by deep.conf
  bool                            m_doBTagging;

  /// Retrieve/fill b-tagging scores?
  /// If yes, you need to further specify the btagging container names as well.
  /// Default is false
  /// Steered by deep.conf
  /// Example and further instruction can be found in CentralDBFields.h
  bool    m_doBTagScores;
  bool    m_skipMV2c10Scores;
  TString m_MV2c10ContainerName;
  TString m_DL1rContainerName;
  TString m_MV2c10ContainerName_trkJet;
  TString m_DL1rContainerName_trkJet;
  

  /// DR threshold for matching trackjets to a fat jet
  /// Default 1.0
  /// Steered by deep.conf
  float                           m_DRThresh_fatjet_trackjets;

  //
  // BTaggingSelectionTool for calculating btag weights
  //

  BTaggingSelectionTool *m_bTagSelTool_MV2c10;
  BTaggingSelectionTool *m_bTagSelTool_DL1r;
  BTaggingSelectionTool *m_bTagSelTool_trkJet_MV2c10;
  BTaggingSelectionTool *m_bTagSelTool_trkJet_DL1r;

/*
  asg::AnaToolHandle<IBTaggingSelectionTool> m_bTagSelTool_MV2c10;
  asg::AnaToolHandle<IBTaggingSelectionTool> m_bTagSelTool_DL1r;
  asg::AnaToolHandle<IBTaggingSelectionTool> m_bTagSelTool_trkJet_MV2c10;
  asg::AnaToolHandle<IBTaggingSelectionTool> m_bTagSelTool_trkJet_DL1r;
*/

  //
  // BoostedBosonTagger selection tools
  //
  SmoothedWZTagger *m_WTaggerTool_Eff50;
  SmoothedWZTagger *m_ZTaggerTool_Eff50;
  SmoothedWZTagger *m_WTaggerTool_InclEff50;
  SmoothedWZTagger *m_ZTaggerTool_InclEff50;
  SmoothedWZTagger *m_WTaggerTool_Eff80;
  SmoothedWZTagger *m_ZTaggerTool_Eff80;

  //
  // Is this data?
  //
  bool m_isData;

};

#endif
