#ifndef SusySkimMaker_EventVariable_h
#define SusySkimMaker_EventVariable_h

/*

  Author: Matthew Gignac, University of British Columbia
  Date: 2015/09/19

 */

// C++
#include <vector>
#include <iostream>
#include <map>

// ROOT
#include "TObject.h"
#include "TLorentzVector.h"
#include "TVector3.h"

//
#include "SusySkimMaker/MCCorrections.h"

class EventVariable
{

 public:

  ///
  /// Constructor
  ///
  EventVariable();

  ///
  /// Destructor
  ///
  virtual ~EventVariable(){};

  ///
  // Copy constructor
  ///
  EventVariable(const EventVariable&);

  ///
  /// Assignment operator
  ///
  EventVariable& operator=(const EventVariable&);

  // Event cleaning variables
  enum EventCleaning {
    LAr=0,
    Tile=1,
    IncompleteEvt=2,
    SCT=3,
    GRL=4,
    PrimVtx=5,
    BadJet=6,
    BadMuon=7,
    CosmicMuon=8,
    EvtLooseBadJet=9,
    EvtTightBadJet=10,
    NEvtCleaning=11,
    EmulEvtTightBad1Jet=12,
    EmulEvtTightBad2Jet=13,
  };

  // What type of input xAODs we ran over
  enum Stream {
    Unknown=0,
    Simulation=1,
    Truth=2,
    Data=3
  };

  ///
  /// General information
  ///
  int runNumber;
  int randomRunNumber;
  unsigned long long evtNumber;
  int lumiBlock;
  int dsid;
  int timestamp;

  ///
  /// Beam position
  ///
  TVector3 beampos;

  ///
  /// Number of vertices, filled by calling EventObject::getNVtx(...), requiring
  /// at least two tracks associated to a given vertex.
  ///
  int nVtx;

  ///
  /// Number of truth classfied hard-scattered jets.
  /// Filled from EventObject::getNumHSJets(...);
  /// Used GhostTruthAssociationLink, if available, and requires the jets to have pT>10 GeV.
  /// If Link is unavailable, variable is not filled
  int numHSJets;

  ///
  /// Final state, primarily used for SUSY signal samples. Defaulted to zero for all other backgrounds
  /// and data samples.
  ///
  int FS;

  ///
  /// PDF information
  ///
  float x1;
  float x2;
  float pdf1;
  float pdf2;
  float scalePDF;
  int id1;
  int id2;

  ///
  /// Corresponds to EventInfo::actualInteractionsPerCrossing
  ///
  float mu;
  float avg_mu;
  float actual_mu;

  // Beamspot info
  float beamSpotX;
  float beamSpotY;
  float beamSpotZ;
  float beamSpotXerr;
  float beamSpotYerr;
  float beamSpotZerr;
  float beamSpotXYerr;
  float beamTiltXZ;
  float beamTiltYZ;


  // Primary vertex information
  TVector3 privtx;
  float privtxErrX;
  float privtxErrY;
  float privtxErrZ;
  float privtxCorXY;
  float privtxCorXZ;
  float privtxCorYZ;
  float privtxFitQ;

  // Pileup vertex information
  std::vector<TVector3> pileupvtx;

  // Photon classified vertex
  TVector3 phvtx;
  float phvtxErrZ;
  float phvtxFitQ;

  ///
  /// Pileup weights, and their systematics.
  /// TODO: Would like to handle these systematics differently..
  ///
  float pileUp;
  float pileUp_sysUp;
  float pileUp_sysDown;
  unsigned long long PRWHash;


  float mcWeight;
  float xsec;
  float xsec_uncert;

  float nEvents;
  float sumOfWeightsPerEvent;

  float runLumi;

  // TODO: Phase out, replace with Stream
  bool isMC;

  // See above
  Stream stream;

  // Will replace evtCleaning variable
  std::map<EventCleaning,bool> eventCleaning;

  // Maps of trigger decisions
  std::map<TString,bool> evtL1Trigger;
  std::map<TString,bool> evtHLTTrigger;
  std::map<TString,float> evtTriggerPrescale;

  // Emulated trigger values
  float met_L1;
  float met_HLT_pufit;
  float met_HLT_cell;
  float leadJetPt_L1;
  float leadJetPt_HLT;

  // Vgamma OR
  bool isVgammaOverlap;

  /// Sherpa v2.2 n-jet weights. Only should be applied to Sherpa v2.2 samples
  float sherpaNJetWeight;

  // Event level trigger SF calculated by TrigGlobalEfficiencyCorrection
  std::map<TString,float> globalDiLepTrigSF;
  std::map<TString,float> globalMultiLepTrigSF;
  std::map<TString,float> globalPhotonTrigSF;

  // Info for event generators
  float GenHt;
  float GenMET;

  // MC Corrections (e.g. LHE3 event weights)
  MCCorrContainer MCCorr;

 ///
 /// All these methods should be protected with //! at the end,
 /// such that they are not written to disk; only used to help
 /// store/retrieve variables that are written
 ///
 public:

  ///
  /// Add a event cleaning cut to eventCleaning map.
  ///
  void addEvtCleaning( EventCleaning var, bool value );

  ///
  /// Get a given event cleaning cut, not written to disk
  /// Note the first function will be phased out
  ///
  bool passEvtCleaning( EventCleaning var ); //!

  ///
  /// Return L1 event level trigger decision for a given chain
  ///
  bool getL1EvtTrigDec(const std::string trigChainName); //!

  ///
  /// Return HLT event level trigger decision for a given chain
  ///
  bool getHLTEvtTrigDec(const std::string trigChainName); //!

  ///
  /// Return trigger prescale for a given chain
  ///
  float getTriggerPrescale(const std::string trigChainName); //!

  ///
  /// Return event-level trigger SF for a given systematics
  ///
  float getGlobalDiLepTrigSF(const TString &sys); //!
  float getGlobalMultiLepTrigSF(const TString &sys); //!
  float getGlobalPhotonTrigSF(const TString &sys); //!

  ///
  /// Get Sherpa V+jets weights
  ///
  float getSherpaVjetsNjetsWeight(int var=0);  //!

  ///
  /// Stream accessors
  ///
  bool isData() { return this->stream == Stream::Data ? true : false; } //!
  bool isSimulation() { return this->stream == Stream::Simulation ? true : false; } //!
  bool isTruth() { return this->stream == Stream::Truth ? true : false; } //!

  ClassDef(EventVariable, 7);

};

#endif
