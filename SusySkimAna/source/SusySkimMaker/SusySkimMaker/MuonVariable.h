#ifndef SusySkimMaker_MuonVariable_h
#define SusySkimMaker_MuonVariable_h

// Rootcore includes(s)
#include "SusySkimMaker/LeptonVariable.h"

class MuonVariable: public LeptonVariable 
{

 public:

  ///
  /// Default constructor
  ///
  MuonVariable();

  ///
  /// Destructor
  ///
  virtual ~MuonVariable(){};

  ///
  /// Copy constructor 
  ///
  MuonVariable(const MuonVariable&);

  ///
  /// Assignment operator
  ///
  MuonVariable& operator=(const MuonVariable&);

  ///
  /// Isolation variable: Sum of the eT of tracks within a radius of \DeltaR = 0.3.
  ///
  float etcone30;

  /// 
  /// Quality of the muon
  ///   0 - tight
  ///   1 - medium
  ///   2 - loose
  ///   3 - very loose
  /// More information: https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionTool
  ///
  int quality;

  ///
  /// For the muon LowPtWP
  /// (adding this bool is nicer than making quality a bit more complicated)
  ///
  bool lowPtWp;

  ///
  /// Simple functions to parse the quality
  ///
  bool passesTight()     const { return ((quality % 10) <= 0); }
  bool passesMedium()    const { return ((quality % 10) <= 1); }
  bool passesLoose()     const { return ((quality % 10) <= 2); }
  bool passesVeryLoose() const { return ((quality % 10) <= 3); }
  bool passesHighPt()    const { return (quality >= 10); }
  bool passesLowPtWP()   const { return lowPtWp; }

  ///
  /// Result of SUSYTools::IsBadMuon()
  ///
  bool bad;

  ///
  /// Result of SUSYTools::IsCosmicMuon()
  ///
  bool cosmic;

  /// 
  /// Always returns false, these are muons
  ///
  bool isEle() const { return false; } //!

  ///
  /// Always returns true, as these are muons
  ///
  bool isMu()  const { return true; }  //!

  int nPrecLayers;
  int nPrecHoleLayers;
  int nGoodPrecLayers;

  float idPt;
  float idTheta;
  float mePt;
  float meTheta;
  float idQoverPErr;
  float meQoverPErr;
  float cbQoverPErr;
  float matchQ;

  float ParamEnergyLoss;
  float MeasEnergyLoss;


  ClassDef(MuonVariable, 1);

};

#endif
