//Class Header

#include <PATInterfaces/SystematicSet.h>

#include <xAODEgamma/EgammaxAODHelpers.h>

#include <xAODBase/IParticleHelpers.h>

#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/Muon.h>

#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/Electron.h>

#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/Photon.h>

#include <xAODCaloEvent/CaloCluster.h>

#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/TrackParticle.h>

#include <xAODTracking/VertexContainer.h>
#include <xAODTracking/Vertex.h>

#include <NearbyLepIsoCorrection/NearbyLepIsoCorrection.h>
#include <FourMomUtils/xAODP4Helpers.h>

//Tools includes:
#include <IsolationSelection/IsolationSelectionTool.h>
#include <IsolationSelection/IsolationWP.h>

#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>

#include <xAODPrimitives/tools/getIsolationAccessor.h>
#include <xAODPrimitives/IsolationHelpers.h>

#define SET_DUAL_TOOL( TOOLHANDLE, TOOLTYPE, TOOLNAME )                \
  ASG_SET_ANA_TOOL_TYPE(TOOLHANDLE, TOOLTYPE);                        \
  TOOLHANDLE.setName(TOOLNAME);

namespace NearLep {
    IsoCorrection::IsoCorrection(const std::string &Name) :
                asg::AsgTool(Name),
                m_Initialized(false),
                m_UseIsoSignal(true),
                m_CopyIsoDecorator(false),
                m_CorrectPtCones(true),
                m_UseSecondaryTracks(true),
                m_CorrectTopoEtCones(true),
                m_CorrectTopoEtCore(false),
                m_ResetOriginalCone(false),
                m_AlwaysCorrectCones(false),
                m_ISO_ETCONECUT(50.),
                m_MaxDR(0.4),
                m_ElContainer(nullptr),
                m_MuContainer(nullptr),
                m_PrimVtx(nullptr),
                m_isoTool("IsoTool"),
                m_TrkSel("TrackSelectionTool"),
                m_dec_Isolation(),
                m_dec_Signal(),
                m_dec_NonIsoSignal(),
                m_dec_PassOR(),
                m_dec_ConesSaved(),
                m_dec_PtConesRecal(),
                // m_dec_OriginalPt(),
                m_dec_OriginalCones(),
                m_dec_CorrectedCones(),
                m_dec_CopyIsolation(),
                m_NonIsol(""),
                m_SignalDec(""),
                m_IsoDec(""),
                m_ORdec(""),
                m_PreDecOrig(""),
                m_PreDecCorr(""),
                m_VtxContName(""),
                m_ISO_PTCONES_int(),
                m_ISO_TOPOETCONES_int(),
                m_ISO_PTCONES(),
                m_ISO_TOPOETCONES(),
                m_ISO_CONES(),
                m_ExcludedTracks() {
        
        m_ISO_PTCONES.push_back(xAOD::Iso::IsolationType::ptcone40);
        m_ISO_PTCONES.push_back(xAOD::Iso::IsolationType::ptvarcone40);
        m_ISO_PTCONES.push_back(xAOD::Iso::IsolationType::ptcone30);
        m_ISO_PTCONES.push_back(xAOD::Iso::IsolationType::ptvarcone30);
        m_ISO_PTCONES.push_back(xAOD::Iso::IsolationType::ptcone20);
        m_ISO_PTCONES.push_back(xAOD::Iso::IsolationType::ptvarcone20);
 
        m_ISO_TOPOETCONES.push_back(xAOD::Iso::IsolationType::topoetcone40);
        m_ISO_TOPOETCONES.push_back(xAOD::Iso::IsolationType::topoetcone30);
        m_ISO_TOPOETCONES.push_back(xAOD::Iso::IsolationType::topoetcone20);

        declareProperty("ConsideredPtCones", m_ISO_PTCONES_int);
        declareProperty("ConsideredTopoCones", m_ISO_TOPOETCONES_int);

        declareProperty("VertexContainerName", m_VtxContName = "PrimaryVertices"); //Name of the VertexContainer
        declareProperty("InputQualityDecorator", m_NonIsol = "NonIsoSignal");
        declareProperty("SignalDecorator", m_SignalDec = "signal");
        declareProperty("IsolationDecorator", m_IsoDec = "isol");
        declareProperty("OverlapDecorator", m_ORdec = "passOR");

        declareProperty("PreFixOrignalCones", m_PreDecOrig = "ORIG_");
        declareProperty("PreFixCorrectedCones", m_PreDecCorr = "CORR_");

        declareProperty("RequireIsoSignal", m_UseIsoSignal = true);

        declareProperty("UseCopiedIsoDecorator", m_CopyIsoDecorator = false);
        declareProperty("UseOverlapDecorator", m_UsePassORdecorator = false);
        declareProperty("Restore", m_ResetOriginalCone = false);

        declareProperty("ApplyPtConeCorrection", m_CorrectPtCones = true);
        declareProperty("UseSecondaryTracks", m_UseSecondaryTracks = true);
        declareProperty("AlwaysCorrect", m_AlwaysCorrectCones = false);

        declareProperty("ApplyTopoEtConeCorrection", m_CorrectTopoEtCones = true);
        declareProperty("UseClustersWithinCore", m_CorrectTopoEtCore = false);

        declareProperty("IsolationTool", m_isoTool);
        m_TrkSel.declarePropertyFor(this, "TrkSelTool", "The track selection tool for correction");

    }
    StatusCode IsoCorrection::initialize() {
        if (m_Initialized) {
            ATH_MSG_WARNING("The tool is already initialized");
            return StatusCode::SUCCESS;
        }
        if (m_NonIsol == m_SignalDec) {
            ATH_MSG_ERROR("The decorator name for the leptons used by the IsolationCorrectionTool must not be the same as the final signal decorator");
            return StatusCode::FAILURE;
        }

        if (m_PreDecOrig == m_PreDecCorr) {
            ATH_MSG_ERROR("The PreFixes of the copies of the original Isolation Cones and of the Corrected Isolation Cones must not be the same");
            return StatusCode::FAILURE;
        }

        if (m_ResetOriginalCone) m_CopyIsoDecorator = true;
        m_dec_Isolation = std::unique_ptr < CharDecor > (new CharDecor(m_IsoDec));
        if (m_CopyIsoDecorator) m_dec_CopyIsolation = std::unique_ptr < CharDecor > (new CharDecor(m_PreDecCorr + m_IsoDec));
        if (m_UsePassORdecorator) m_dec_PassOR = std::unique_ptr < CharDecor > (new CharDecor(m_ORdec));

        m_dec_NonIsoSignal = std::unique_ptr < CharDecor > (new CharDecor(m_NonIsol));
        m_dec_Signal = std::unique_ptr < CharDecor > (new CharDecor(m_SignalDec));
        m_dec_ConesSaved = std::unique_ptr < CharDecor > (new CharDecor(m_PreDecOrig + "Saved"));
        // m_dec_OriginalPt = std::unique_ptr < FloatDecor > (new FloatDecor(m_PreDecOrig + "LepPt"));

        if (!m_TrkSel.isUserConfigured()) {
            ATH_MSG_INFO("Setup new instance of the TrackSelectionTool using the AnaToolHandle");
            SET_DUAL_TOOL(m_TrkSel, InDet::InDetTrackSelectionTool, "TrackSelectionTool");
            ATH_CHECK(m_TrkSel.setProperty("maxZ0SinTheta", 3.0));
            ATH_CHECK(m_TrkSel.setProperty("minPt", 1000.));
            ATH_CHECK(m_TrkSel.setProperty("CutLevel", "Loose"));
        }
        ATH_CHECK(m_TrkSel.retrieve());
        ATH_CHECK(m_isoTool.retrieve());

        m_Initialized = true;
        TransferIsolationVariables(m_ISO_PTCONES_int, m_ISO_PTCONES);
        TransferIsolationVariables(m_ISO_TOPOETCONES_int, m_ISO_TOPOETCONES);
        //No cones were initialized. Get them from the isolation tool
        if (m_ISO_PTCONES.empty() && m_ISO_TOPOETCONES.empty()) {
            CP::IsolationSelectionTool* Tool = dynamic_cast<CP::IsolationSelectionTool*>(m_isoTool.operator->());
            ExtractIsolationVariables(Tool->getElectronWPs());
            ExtractIsolationVariables(Tool->getMuonWPs());
            //Cut names are insane... Extraction not possible -> Just use everything
            if (m_ISO_PTCONES.empty() && m_ISO_TOPOETCONES.empty()) {
                ATH_MSG_INFO("The used WP is not telling any useful information. Take every iso variable");
                for (int i = 1; i <= 47; ++i) {
                    m_ISO_PTCONES_int.push_back(i);
                    m_ISO_TOPOETCONES_int.push_back(i);
                }
                TransferIsolationVariables(m_ISO_PTCONES_int, m_ISO_PTCONES);
                TransferIsolationVariables(m_ISO_TOPOETCONES_int, m_ISO_TOPOETCONES);
            }
        }
        //Kick everything out of the vectors which does not fit
        FilterIsolationTypes(m_ISO_PTCONES, &IsoCorrection::TrackIso);
        FilterIsolationTypes(m_ISO_TOPOETCONES, &IsoCorrection::TopoEtIso);
        //Order the thing
//        OrderIsolationTypes(m_ISO_PTCONES, xAOD::Iso::IsolationFlavour::ptvarcone);
        OrderIsolationTypes(m_ISO_PTCONES, xAOD::Iso::IsolationFlavour::ptcone);
        OrderIsolationTypes(m_ISO_TOPOETCONES, xAOD::Iso::IsolationFlavour::topoetcone);
        for (const auto I : m_ISO_PTCONES)
            m_ISO_CONES.push_back(I);
        for (const auto I : m_ISO_TOPOETCONES)
            m_ISO_CONES.push_back(I);

        for (const auto I : m_ISO_CONES) {
            std::string Cone = xAOD::Iso::toString(I);
            m_dec_CorrectedCones.insert(CorrectionDecorator(I, std::unique_ptr < FloatDecor > (new FloatDecor(m_PreDecCorr + Cone))));
            m_dec_OriginalCones.insert(CorrectionDecorator(I, std::unique_ptr < FloatDecor > (new FloatDecor(m_PreDecOrig + Cone))));

            if (xAOD::Iso::coneSize(I) > m_MaxDR) m_MaxDR = xAOD::Iso::coneSize(I);
        }
        if (m_ISO_CONES.empty()) {
            ATH_MSG_ERROR("No isolation cones");
            return StatusCode::FAILURE;
        }
        ATH_MSG_INFO("###############################################################################################################");
        ATH_MSG_INFO("						Print Options");
        ATH_MSG_INFO("###############################################################################################################");
        ATH_MSG_INFO("Apply always the correction regardless if the lepton is already isolated: " << BoolToString(m_AlwaysCorrectCones));
        ATH_MSG_INFO("Apply PtConeX correction: " << BoolToString(m_CorrectPtCones));
        if (m_CorrectPtCones) ATH_MSG_INFO("Use secondary tracks associated with electrons: " << BoolToString(m_UseSecondaryTracks));
        ATH_MSG_INFO("Apply TopoEtCone correction: " << BoolToString(m_CorrectTopoEtCones));
        if (m_CorrectTopoEtCones) ATH_MSG_INFO("Use topological electron clusters within the core cone (0.1): " << BoolToString(m_CorrectTopoEtCore));

        ATH_MSG_INFO("Maximal size of the IsoCones: " << m_MaxDR);
        ATH_MSG_INFO("###############################################################################################################");
        ATH_MSG_INFO("Input isolation decorator: " << m_IsoDec);
        ATH_MSG_INFO("Input signal decorator: " << m_NonIsol);
        ATH_MSG_INFO("Use objects that pass OverlapRemoval: " << BoolToString(m_UsePassORdecorator) << (m_UsePassORdecorator ? " (" + m_ORdec + ")" : ""));
        ATH_MSG_INFO("###############################################################################################################");
        ATH_MSG_INFO("Track isolation variables to correct:");
        for (const auto I : m_ISO_PTCONES)
            ATH_MSG_INFO("        *** " << xAOD::Iso::toString(I));
        ATH_MSG_INFO("Topocluster isolation variables to correct:");
        for (const auto I : m_ISO_TOPOETCONES)
            ATH_MSG_INFO("        *** " << xAOD::Iso::toString(I));
        ATH_MSG_INFO("###############################################################################################################");

        ATH_MSG_INFO("Output isolation decorator: " << (m_CopyIsoDecorator ? m_PreDecCorr : "") + m_IsoDec);
        ATH_MSG_INFO("Output signal decorator: " << m_SignalDec);

        ATH_MSG_INFO("###############################################################################################################");
        ATH_MSG_INFO("						Tool successfully intialized");
        ATH_MSG_INFO("###############################################################################################################");
        return StatusCode::SUCCESS;
    }

    StatusCode IsoCorrection::SaveIsolation(const xAOD::Electron* E) {
        ATH_CHECK(CopyOriginalValue(E));
        ATH_CHECK(LoadIsolation(E, StoredCone::Original));
        return StatusCode::SUCCESS;
    }
    StatusCode IsoCorrection::SaveIsolation(const xAOD::Muon* M) {
        ATH_CHECK(CopyOriginalValue(M));
        ATH_CHECK(LoadIsolation(M, StoredCone::Original));
        return StatusCode::SUCCESS;
    }
    StatusCode IsoCorrection::LoadIsolation(const xAOD::Electron* E, StoredCone T) {
        return UpdateDecorators(E, T);
    }
    StatusCode IsoCorrection::LoadIsolation(const xAOD::Muon* M, StoredCone T) {
        return UpdateDecorators(M, T);
    }
    StatusCode IsoCorrection::CopyOriginalValue(const xAOD::IParticle* P) {
        if (!m_Initialized) {
            ATH_MSG_ERROR("The tool is not initialized yet");
            return StatusCode::FAILURE;
        }
        if (m_dec_CopyIsolation) (*m_dec_CopyIsolation)(*P) = (*m_dec_Isolation)(*P);
        if (m_dec_ConesSaved->isAvailable(*P) && (*m_dec_ConesSaved)(*P) == 1) return StatusCode::SUCCESS;
        for (const auto I : m_ISO_CONES) {
            SG::AuxElement::Accessor<float>* Acc = xAOD::getIsolationAccessor(I);
            if (Acc == nullptr || !Acc->isAvailable(*P)) continue;
            (*(m_dec_OriginalCones.at(I)))(*P) = (*Acc)(*P);
            ATH_CHECK(SaveConeValue(P, I, (*Acc)(*P), StoredCone::Corrected));
        }
        (*m_dec_ConesSaved)(*P) = 1;
        return StatusCode::SUCCESS;
    }
    const xAOD::TrackParticle* IsoCorrection::Track(const xAOD::IParticle* P) const {
        const xAOD::TrackParticle* T = dynamic_cast<const xAOD::TrackParticle*>(P);
        if (T != nullptr) return T;
        if (P->type() == xAOD::Type::ObjectType::Electron) {
            const xAOD::Electron* El = dynamic_cast<const xAOD::Electron*>(P);
            T = xAOD::EgammaHelpers::getOriginalTrackParticle(El);
            if (T == nullptr) {
                ATH_MSG_WARNING("Could not find the Original InDet track of the electron. Use the GSF track instead");
                T = El->trackParticle();
            }
        } else if (P->type() == xAOD::Type::ObjectType::Muon) {
            const xAOD::Muon* Mu = dynamic_cast<const xAOD::Muon*>(P);
            T = Mu->trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
            if (T == nullptr) {
                ATH_MSG_DEBUG("The muon has no InDet track. Return the next primary track");
                T = Mu->primaryTrackParticle();
            }
        }
        if (T == nullptr) ATH_MSG_WARNING("IsoCorrection::Track(): Could not find an associated track to the Particle");
        return T;
    }
    const xAOD::CaloCluster* IsoCorrection::Cluster(const xAOD::IParticle* P) const {
        const xAOD::CaloCluster* C = nullptr;
        if (!P) return nullptr;
        else if (P->type() == xAOD::Type::ObjectType::CaloCluster) return dynamic_cast<const xAOD::CaloCluster*>(P);
        else if (P->type() == xAOD::Type::ObjectType::Electron) {
            const xAOD::Electron* El = dynamic_cast<const xAOD::Electron*>(P);
            C = El->caloCluster();
        } else if (P->type() == xAOD::Type::ObjectType::Muon) {
            const xAOD::Muon* Mu = dynamic_cast<const xAOD::Muon*>(P);
            C = Mu->cluster();
        }
        return C;
    }
    const xAOD::Vertex* IsoCorrection::RetrieveBestVtx() const {
        const xAOD::VertexContainer* Verticies = nullptr;
        if (evtStore()->contains < xAOD::VertexContainer > (m_VtxContName)) {
            if (evtStore()->retrieve(Verticies, m_VtxContName).isFailure()) {
                ATH_MSG_ERROR("IsoCorrection::RetriveBestVtx(): Unable to retrieve VertexContainer " << m_VtxContName);
                return nullptr;
            } else if (Verticies->size() > 0) {
                for (const auto& V : *Verticies) {
                    if (V->vertexType() == xAOD::VxType::VertexType::PriVtx) return V;
                }
            }
        }
        ATH_MSG_WARNING("The vertex collection " << m_VtxContName << " does not  contain any vertex");
        return nullptr;
    }

    const xAOD::IParticle* IsoCorrection::TrackIsoRefPart(const xAOD::IParticle* P) const {
        if (!P) {
            ATH_MSG_ERROR("IsoCorrection::TrackIsoRefPart(): No particle given");
            return nullptr;
        }
        if (P->type() == xAOD::Type::ObjectType::Muon) return Track(P);
        return P;
    }
    double IsoCorrection::ConeSize(const xAOD::IParticle* P, xAOD::Iso::IsolationType Cone) const {
        if (!m_Initialized) {
            ATH_MSG_ERROR("IsoCorrection::ConeSize(): The tool is not initialized yet. Return 1.");
            return 1.;
        }
        double ConeDR = xAOD::Iso::coneSize(Cone);
        if (PtVarIso(Cone)) {
            double MiniIso = MiniIsoPT / GetPtForConeSize(P);
            if (MiniIso < ConeDR) return MiniIso;
        }
        return ConeDR;
    }
    float IsoCorrection::GetIsoVar(const xAOD::IParticle* P, xAOD::Iso::IsolationType Iso, StoredCone T) const {
        if (!IsolationAvailable(P, Iso, T)) {
            ATH_MSG_DEBUG("IsoCorrection::GetIsoVar(): Isolation " << PromptStoredCone(T) << " " << xAOD::Iso::toString(Iso) << " not available. Return 1.e15");
            return 1.e15;
        } else if (T == StoredCone::Original) return (*(m_dec_OriginalCones.at(Iso)))(*P);
        else if (T == StoredCone::Corrected) return (*(m_dec_CorrectedCones.at(Iso)))(*P);
        return 1.e15;
    }
    bool IsoCorrection::IsolationAvailable(const xAOD::IParticle* P, xAOD::Iso::IsolationType Iso, StoredCone T) const {
        if (!m_Initialized) {
            ATH_MSG_ERROR("IsoCorrection::IsolationAvailable(): The tool is not initialized yet");
            return false;
        }
        CorrectionDecorMap::const_iterator Itr;
        if (T == StoredCone::Original) {
            Itr = m_dec_OriginalCones.find(Iso);
            if (Itr == m_dec_OriginalCones.end()) {
                ATH_MSG_ERROR("IsoCorrection::IsolationAvailable(): No original decorator found for Isolation type " << xAOD::Iso::toString(Iso));
                return false;
            }
        } else if (T == StoredCone::Corrected) {
            Itr = m_dec_CorrectedCones.find(Iso);
            if (Itr == m_dec_CorrectedCones.end()) {
                ATH_MSG_ERROR("IsoCorrection::IsolationAvailable(): No correction decorator found for Isolation type " << xAOD::Iso::toString(Iso));
                return false;
            }
        }
        return Itr->second->isAvailable(*P);
    }
    double IsoCorrection::DeltaR2(const xAOD::IParticle* P, const xAOD::IParticle* P1) const {
        if (!P || !P1) {
            ATH_MSG_WARNING("IsoCorrection::DeltaR2(): One of the given Particles points to nullptr return 1.e4");
            return 1.e4;
        }
        if (IsSame(P, P1)) return 0.;
        double dPhi = xAOD::P4Helpers::deltaPhi(P, P1);
        double dEta = P->eta() - P1->eta();
        return dEta * dEta + dPhi * dPhi;
    }
    bool IsoCorrection::Overlap(const xAOD::IParticle* P, const xAOD::IParticle* P1, double dR) const {
        if (!P || !P1) {
            ATH_MSG_ERROR("IsoCorrection::Overlap(): One of the given particles is a nullptrL pointer. Return false");
            return false;
        }
        return (!IsSame(P, P1) && DeltaR2(P, P1) < (dR * dR));
    }
    StatusCode IsoCorrection::CorrectIsolation(xAOD::ElectronContainer* Electrons, xAOD::MuonContainer* Muons) {
        if (!m_Initialized) {
            ATH_MSG_ERROR("IsoCorrection::CorrectIsolation(): The tool is not initialized yet");
            return StatusCode::FAILURE;
        }
        m_ElContainer = &Electrons;
        m_MuContainer = &Muons;
        m_PrimVtx = RetrieveBestVtx();
        if (!m_PrimVtx) return StatusCode::FAILURE;
        ATH_CHECK(IsoConeCorrection(Electrons));
        ATH_CHECK(IsoConeCorrection(Muons));

        return StatusCode::SUCCESS;
    }
    StatusCode IsoCorrection::SetSystematic(const CP::SystematicSet&) {
        // if (m_CopyIsoDecorator) {
        // if (m_dec_CopyIsolation != nullptr) delete m_dec_CopyIsolation;
        // m_dec_CopyIsolation = new SG::AuxElement::Decorator<char>(m_PreDecCorr + Set.name() + m_IsoDec);
        // }
        // for (CorrectionDecorMap::iterator M = m_dec_CorrectedCones.begin(); M != m_dec_CorrectedCones.end(); ++M) {
        // if (M->second != nullptr) {
        // delete M->second;
        // std::string Cone = xAOD::Iso::toString(M->first);
        // SG::AuxElement::Decorator<float>* NewDec = new SG::AuxElement::Decorator<float>(m_PreDecCorr + Set.name() + Cone);
        // M->second = NewDec;
        // }
        // }
        return StatusCode::SUCCESS;
    }
    StatusCode IsoCorrection::SaveConeValue(const xAOD::IParticle* P, xAOD::Iso::IsolationType I, float Value, StoredCone T) {

        CorrectionDecorMap::const_iterator Itr;
        if (T == StoredCone::Corrected) {
            Itr = m_dec_CorrectedCones.find(I);
            if (Itr != m_dec_CorrectedCones.end()) (*(Itr->second))(*P) = Value;
            else {
                ATH_MSG_ERROR("IsoCorrection::SaveConeValue(): Could not find the Corrected decorator for Cone" << xAOD::Iso::toString(I));
                return StatusCode::FAILURE;
            }
        } else {
            ATH_MSG_ERROR("IsoCorrection::SaveConeValue(): Could not save the cone " << xAOD::Iso::toString(I));
            return StatusCode::FAILURE;
        }
        return StatusCode::SUCCESS;
    }
    bool IsoCorrection::AcceptTrack(const xAOD::TrackParticle* Track, const xAOD::Vertex* PrimVtx) const {
        if (Track == nullptr) return false;
        try {
            if (!m_TrkSel->accept(*Track, PrimVtx)) return false;
        } catch (...) {
            ATH_MSG_ERROR("Could not determine if the Track is accepted");
            return false;
        }
        for (const auto Ex : m_ExcludedTracks)
            if (IsSame(Ex, Track)) return false;
        return true;
    }

    StatusCode IsoCorrection::TrackCorrection(const xAOD::IParticle* P) {
        m_ExcludedTracks.clear();
        //Exclude the Electron tracks apriori
        if (P->type() == xAOD::Type::ObjectType::Electron) m_ExcludedTracks = xAOD::EgammaHelpers::getTrackParticles(dynamic_cast<const xAOD::Electron*>(P));
        //Reset everything
        for (const auto I : m_ISO_PTCONES) {
            float ResetVal = GetIsoVar(P, I, StoredCone::Original);
            ATH_CHECK(SaveConeValue(P, I, ResetVal, StoredCone::Corrected));
        }
        //Loop over the given containers
        for (const auto E : **m_ElContainer) {
            if (!PassNonIsoSignal(E) || IsSame(E, P)) continue;
            if (m_UseSecondaryTracks) {
                std::set<const xAOD::TrackParticle*> Tracks = xAOD::EgammaHelpers::getTrackParticles(E);
                for (const auto T : Tracks)
                    ATH_CHECK(TrackCorrection(P, T));
            } else ATH_CHECK(TrackCorrection(P, Track(E)));
        }
        for (const auto M : **m_MuContainer) {
            if (!PassNonIsoSignal(M) || IsSame(M, P)) continue;
            ATH_CHECK(TrackCorrection(P, Track(M)));
        }
        return StatusCode::SUCCESS;
    }
    StatusCode IsoCorrection::TrackCorrection(const xAOD::IParticle* Lep, const xAOD::TrackParticle* CT) {
        const xAOD::IParticle* T = TrackIsoRefPart(Lep);
        if (!T) {
            ATH_MSG_ERROR("IsoCorrection::TrackCorrection(): Could not correct the PtCones. Either no reference particle found or no Vertex available");
            return StatusCode::FAILURE;
        }
        if (!AcceptTrack(CT, m_PrimVtx) || !Overlap(CT, T, m_MaxDR)) return StatusCode::SUCCESS;
        m_ExcludedTracks.insert(CT); //Avoids double counting of secondary tracks associated with close-by electrons
        for (const auto I : m_ISO_PTCONES) {
            double ConeDR = ConeSize(Lep, I);
            if (Overlap(CT, T, ConeDR)) {
                float NewConeVal = GetIsoVar(Lep, I, StoredCone::Corrected) - CT->pt();
                ATH_CHECK(SaveConeValue(Lep, I, NewConeVal, StoredCone::Corrected));
            } else if (PtIso(I)) return StatusCode::SUCCESS;
        }
        return StatusCode::SUCCESS;
    }

    const xAOD::IParticle* IsoCorrection::ClusterIsoRefPart(const xAOD::IParticle* P) const {
        if (dynamic_cast<const xAOD::Egamma*>(P) != nullptr) return Cluster(P);
        return Track(P);
    }

    StatusCode IsoCorrection::CaloCorrection(const xAOD::IParticle* P) {
        if (!PassNonIsoSignal(P) || PassIsolation(P)) return StatusCode::SUCCESS;
        for (std::vector<xAOD::Iso::IsolationType>::iterator I = m_ISO_TOPOETCONES.begin(); I != m_ISO_TOPOETCONES.end(); ++I) {
            if (!IsolationAvailable(P, *I, StoredCone::Original)) continue;
            ATH_CHECK(SaveConeValue(P, *I, GetIsoVar(P, *I, StoredCone::Original), StoredCone::Corrected));
        }
        ATH_CHECK(CaloCorrection(P, *m_ElContainer));
        // ATH_CHECK ( CaloCorrection( P , *m_MuContainer) );
        return StatusCode::SUCCESS;
    }
    StatusCode IsoCorrection::CaloCorrection(const xAOD::IParticle* NonIso, const xAOD::IParticleContainer* C) {
        const xAOD::IParticle* R = ClusterIsoRefPart(NonIso);
        if (R == nullptr) {
            ATH_MSG_ERROR("IsoCorrection::CaloCorrection(): Could not retrieve the reference particle for TopoEtCone correction");
            return StatusCode::FAILURE;
        }
        for (xAOD::IParticleContainer::const_iterator P = C->begin(); P != C->end(); ++P) {
            if (!PassNonIsoSignal(*P) || IsSame(*P, NonIso) || DeltaR2(NonIso, *P) > 2. * m_MaxDR * m_MaxDR) continue;
            for (std::vector<xAOD::Iso::IsolationType>::iterator I = m_ISO_TOPOETCONES.begin(); I != m_ISO_TOPOETCONES.end(); ++I) {
                if (!IsolationAvailable(NonIso, *I, StoredCone::Corrected)) continue;
                float ConeVal = GetIsoVar(NonIso, *I, StoredCone::Corrected);
                float Correction = TopoEtCorrection(NonIso, *P, *I);
                if (TMath::Abs(Correction) > 0. && TMath::Abs(ConeVal) > 0. && TMath::Abs(Correction / ConeVal) < m_ISO_ETCONECUT) ConeVal -= Correction;
                ATH_CHECK(SaveConeValue(NonIso, *I, ConeVal, StoredCone::Corrected));
            }
        }
        return StatusCode::SUCCESS;
    }
    float IsoCorrection::TopoEtCorrection(const xAOD::IParticle* R, const xAOD::IParticle* S, xAOD::Iso::IsolationType I) {
        float dR2 = TMath::Power(ConeSize(R, I), 2);
        const xAOD::Egamma* ES = dynamic_cast<const xAOD::Egamma*>(S);
        if (ES != nullptr) {
            float Correction = 0.;
            for (size_t c = 0; c < ES->nCaloClusters(); ++c) {
                float N_C_dR = DeltaR2(R, ES->caloCluster(c));
                if (!m_CorrectTopoEtCore && N_C_dR < CaloCoreDR2) continue;
                else if (m_CorrectTopoEtCore && (IsSame(R, ES->caloCluster(c)) || dR2 < N_C_dR)) continue;
                Correction += ClusterEtMinusTile(ES->caloCluster(c));
            }
            return Correction;
        }
        return 0.;
    }
    float IsoCorrection::ClusterEtMinusTile(const xAOD::CaloCluster* C) const {
        float Et = 0.;
        if (C != nullptr) {
            try {
                Et = C->p4(xAOD::CaloCluster::State::UNCALIBRATED).Et();
                if (Et < 0.) return 0.;
                Et = Et - C->eSample(xAOD::CaloCluster::CaloSample::TileGap3) / TMath::CosH(C->p4(xAOD::CaloCluster::State::UNCALIBRATED).Eta());
                return Et;
            } catch (...) {
                ATH_MSG_DEBUG("Could not retrieve the uncalibrated Cluster state");
                Et = C->p4().Et();
            }
        } else ATH_MSG_DEBUG("No CaloCluster was given. Return 0.");
        return Et;
    }

    IsoCorrection::~IsoCorrection() {
        ATH_MSG_INFO("Desctructor called");
    }
    void IsoCorrection::PromptParticleInformation(const xAOD::IParticle* Part, std::string AddInfo) const {
        if (Part == nullptr) {
            Warning("IsoCorrection", "PromptParticleInformation: Could not find the particle");
            return;
        }
        std::string PartType;
        if (Part->type() == xAOD::Type::ObjectType::Electron) PartType = "Electron";
        else if (Part->type() == xAOD::Type::ObjectType::Muon) PartType = "Muon";
        else if (Part->type() == xAOD::Type::ObjectType::Photon) PartType = "Photon";
        else if (Part->type() == xAOD::Type::ObjectType::TrackParticle) PartType = "Track";
        else if (Part->type() == xAOD::Type::ObjectType::CaloCluster) PartType = "CaloCluster";
        Info("IsoCorrection", "Particle Info (%s):			m: %.4f	pt: %.2f GeV	eta: %.4f	phi: %.4f	%s", PartType.c_str(), Part->p4().M() / 1.e3, (Part->pt() / 1.e3), TMath::Abs(Part->eta()), Part->phi(), AddInfo.c_str());
    }
    std::string IsoCorrection::BoolToString(bool Boolean) const {
        if (Boolean) return "true";
        return "false";
    }
    bool IsoCorrection::PassIsolation(const xAOD::IParticle* P) const {
        if (m_dec_CopyIsolation) return (*m_dec_CopyIsolation)(*P);
        return (*m_dec_Isolation)(*P);
    }

    void IsoCorrection::FilterIsolationTypes(std::vector<xAOD::Iso::IsolationType> & Cones, IsoSelect Selector) {
        std::vector<xAOD::Iso::IsolationType> DuplicateFree;
        for (const auto Cone : Cones) {
            if (!IsIsolationTypeInVector(Cone, DuplicateFree) && (this->*Selector)(Cone)) DuplicateFree.push_back(Cone);
        }
        Cones.clear();
        Cones = DuplicateFree;
    }
    void IsoCorrection::OrderIsolationTypes(std::vector<xAOD::Iso::IsolationType>& Cones, xAOD::Iso::IsolationFlavour Flavour) {
        std::vector<xAOD::Iso::IsolationType> DuplicateFree = Cones;
        //First create a vector which is free of duplicates
        Cones.clear();
        //In case of multiple isolation types are stored in the vector -> Sort only the relevant out
        //Sort the desired type first
        while (Cones.size() != DuplicateFree.size()) {
            double MaxSize = -1;
            xAOD::Iso::IsolationType Sorted = xAOD::Iso::IsolationType::numIsolationTypes;
            for (const auto Cone : DuplicateFree) {
                if (xAOD::Iso::coneSize(Cone) >= MaxSize && !IsIsolationTypeInVector(Cone, Cones)) {
                    if (xAOD::Iso::coneSize(Cone) == xAOD::Iso::coneSize(Sorted) && xAOD::Iso::isolationFlavour(Sorted) == Flavour) continue;
                    MaxSize = xAOD::Iso::coneSize(Cone);
                    Sorted = Cone;
                }
            }
            Cones.push_back(Sorted);
        }
    }
    bool IsoCorrection::IsIsolationTypeInVector(xAOD::Iso::IsolationType Iso, const std::vector<xAOD::Iso::IsolationType>& Vector) const {
        for (const auto& Test : Vector)
            if (Iso == Test) return true;
        return false;
    }
    bool IsoCorrection::PtIso(xAOD::Iso::IsolationType Iso) const {
        return xAOD::Iso::IsolationFlavour::ptcone == xAOD::Iso::isolationFlavour(Iso);
    }
    bool IsoCorrection::PtVarIso(xAOD::Iso::IsolationType Iso) const {
        return xAOD::Iso::IsolationFlavour::ptvarcone == xAOD::Iso::isolationFlavour(Iso);
    }
    bool IsoCorrection::TrackIso(xAOD::Iso::IsolationType Iso) const {
        return PtIso(Iso) || PtVarIso(Iso);
    }
    bool IsoCorrection::EtIso(xAOD::Iso::IsolationType Iso) const {
        return xAOD::Iso::IsolationFlavour::etcone == xAOD::Iso::isolationFlavour(Iso);
    }
    bool IsoCorrection::TopoEtIso(xAOD::Iso::IsolationType Iso) const {
        return xAOD::Iso::IsolationFlavour::topoetcone == xAOD::Iso::isolationFlavour(Iso);
    }
    bool IsoCorrection::PassNonIsoSignal(const xAOD::IParticle* P) const {
        return (*m_dec_NonIsoSignal)(*P) && (!m_dec_PassOR || (*m_dec_PassOR)(*P));
    }
    bool IsoCorrection::PassSignal(const xAOD::IParticle* P) const {
        return (*m_dec_Signal)(*P);
    }
    bool IsoCorrection::IsSame(const xAOD::IParticle* P, const xAOD::IParticle* P1) const {
        return P == P1;
    }
    float IsoCorrection::GetPtForConeSize(const xAOD::IParticle* P) const {
        if (P->type() == xAOD::Type::ObjectType::Muon) {
            return GetPtForConeSize(TrackIsoRefPart(P));
        } else if (P->type() == xAOD::Type::ObjectType::TrackParticle) {
            return P->pt();
        }
        const xAOD::IParticle* OR = xAOD::getOriginalObject(*P);
        if (!OR) {
            ATH_MSG_DEBUG("No reference from the shallow copy container could be found");
            return P->pt();
        }
        return OR->pt();

    }
    void IsoCorrection::ExtractIsolationVariables(const std::vector<CP::IsolationWP*> & WPs) {
        for (auto wp : WPs) {
            for (unsigned int i = 0; i < wp->getAccept().getNCuts(); i++) {
                xAOD::Iso::IsolationFlavour flavour = xAOD::Iso::IsolationFlavour::numIsolationFlavours;
                TString name = wp->getAccept().getCutName(i);
                //lower all chars
                name.ToLower();
                ATH_MSG_DEBUG("Found isolation cut " << name.Data());
                if (name.Contains("ptcone")) flavour = xAOD::Iso::IsolationFlavour::ptcone;
                else if (name.Contains("ptvarcone")) flavour = xAOD::Iso::IsolationFlavour::ptvarcone;
                else if (name.Contains("topoetcone")) flavour = xAOD::Iso::IsolationFlavour::topoetcone;
                else if (name.Contains("etcone")) flavour = xAOD::Iso::IsolationFlavour::etcone;
                float size = atof(name(name.Length() - 2, name.Length()).Data()) / 100.;
                xAOD::Iso::IsolationType Iso = xAOD::Iso::isolationType(flavour, xAOD::Iso::coneSize(size));
                if (TrackIso(Iso)) m_ISO_PTCONES.push_back(Iso);
                else if (TopoEtIso(Iso)) m_ISO_TOPOETCONES.push_back(Iso);
            }
        }
    }
    void IsoCorrection::TransferIsolationVariables(std::vector<int> &IntVec, std::vector<xAOD::Iso::IsolationType>& IsoVec) {
        for (auto& Number : IntVec) {
            if (Number % 2 == 1) continue;
            if (Number > 6 && Number < 12) continue;
            if (Number > 16 && Number < 22) continue;
            if (Number > 26 && Number < 32) continue;
            if (Number > 36 && Number < 42) continue;
            if (Number > 46) continue;
            IsoVec.push_back(static_cast<xAOD::Iso::IsolationType>(Number));
        }
        IntVec.clear();
    }

    template<typename Part> StatusCode IsoCorrection::UpdateDecorators(const Part* P, StoredCone T) {
        if (!m_Initialized) {
            ATH_MSG_ERROR("IsoCorrection::UpdateDecorators(): The tool is not initialized yet");
            return StatusCode::FAILURE;
        }
        Part* PART = const_cast<Part*>(P);
        if (!PART) {
            ATH_MSG_ERROR("IsoCorrection::UpdateDecorators(): Could not overwrite the decorators");
            return StatusCode::FAILURE;
        }
        for (const auto& I : m_ISO_CONES) {
            if (!IsolationAvailable(P, I, T)) continue;
            PART->setIsolation(GetIsoVar(P, I, T), I);
        }
        bool PassIso = m_isoTool->accept(*P);
        if (m_dec_CopyIsolation && T != StoredCone::Original) (*m_dec_CopyIsolation)(*P) = PassIso;
        else (*m_dec_Isolation)(*P) = PassIso;

        if (!m_dec_Signal->isAvailable(*P) || !m_dec_NonIsoSignal->isAvailable(*P)) {
            ATH_MSG_DEBUG("Particles are not yet decorated");
            return StatusCode::SUCCESS;
        }
        if (m_UseIsoSignal) (*m_dec_Signal)(*P) = (PassNonIsoSignal(P) && PassIso);
        return StatusCode::SUCCESS;
    }
    template<typename Cont> StatusCode IsoCorrection::IsoConeCorrection(const Cont* C) {
        typename Cont::const_iterator P;
        for (P = C->begin(); P != C->end(); ++P) {
            ATH_CHECK(SaveIsolation(*P));
            if (!PassNonIsoSignal(*P) || (!m_AlwaysCorrectCones && PassIsolation(*P))) continue;
            if (m_CorrectPtCones) {
                ATH_CHECK(TrackCorrection(*P));
                ATH_CHECK(UpdateDecorators(*P, StoredCone::Corrected));
            }
            if (m_CorrectTopoEtCones) {
                ATH_CHECK(CaloCorrection(*P));
                ATH_CHECK(UpdateDecorators(*P, StoredCone::Corrected));
            }
            if (m_ResetOriginalCone) ATH_CHECK(UpdateDecorators(*P, StoredCone::Original));
        }
        return StatusCode::SUCCESS;
    }
}
