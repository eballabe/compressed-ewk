#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
import os, string, pickle, copy
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess
import argparse
from math import sqrt, log

parser = argparse.ArgumentParser(description = 'Setting plot parameters')

parser.add_argument(      '--doPullPlot',     default = False,      action = 'store_true',             help = 'Make pull plot')
parser.add_argument('-v', '--variable',       default = 'MET',      action = 'store',      type = str, help = 'variable')
parser.add_argument('-r', '--region',         default = 'CR_B',     action = 'store',      type = str, help = 'region')
parser.add_argument('-w', '--workspace',      default = ' ',        action = 'store',      type = str, help = 'workspace_name')
parser.add_argument('-o', '--outputDir',      default = 'MyPlots/', action = 'store',      type = str, help = 'outputDir')
parser.add_argument('-b', '--doBlind',        default = 'False',    action = 'store',      type = str, help = 'doBlind')
parser.add_argument(      '--doPreselection', default = False,      action = 'store_true',             help = 'doPreselection')
parser.add_argument(      '--doNminus1',      default = False,      action = 'store_true',             help = 'do N-1 Plots')
parser.add_argument(      '--doAfterFit',     default = 'False',    action = 'store',      type = str, help = 'doAfterFit')
parser.add_argument('-s', '--scale',          default = 'log',      action = 'store',      type = str, help = 'scale: log or linear')

args = parser.parse_args()

doPullPlot     = args.doPullPlot
variable       = args.variable
region         = args.region
wsfilename     = args.workspace
outputDir      = args.outputDir
doBlind        = args.doBlind
doPreselection = args.doPreselection
doNminus1      = args.doNminus1
doAfterFit     = args.doAfterFit
scale          = args.scale

addSignal      = False
cutLeft        = False
cutRight       = True
PlotsForPaper  = False

doAfterFit     = False if doAfterFit == 'False' else True
doBeforeFit    = True  if doAfterFit == False else False
doBlind        = False if doBlind == 'False' else True  
doSignificance = False if addSignal == False  else True

Signal_cuts={ "SR_A"      : "MET > 600. && TrackD0Sig > 20. && TrackPt > 1.5 && TrackPt < 5. && DPhiTrackMET < 0.5 && nIBLHits > 0 && nLep == 0 && nPhoton_base == 0",
	     	  "SR_A_8_20" : "MET > 600. && TrackD0Sig > 8. && TrackD0Sig < 20. && TrackPt > 1.5 && TrackPt < 5. && DPhiTrackMET < 0.5 && nIBLHits > 0 && nLep == 0 && nPhoton_base == 0",
	          "SR_A_4_8"  : "MET > 600. && TrackD0Sig > 4. && TrackD0Sig < 8. && TrackPt > 1.5 && TrackPt < 5. && DPhiTrackMET < 0.5 && nIBLHits > 0 && nLep == 0 && nPhoton_base == 0",
	     	}

if addSignal == True:
    Signal_cut = Signal_cuts[region]
    Signal_Tree_Name = 'Higgsino_NoSys'
        
    Signal_Tree_File_150p5_150p25_150 = '/storage/alesala/ntuples_processed/ntuples_v4/higgsino_150.5_150.25_150.root'
    Signal_Tree_File_150p7_150p35_150 = '/storage/alesala/ntuples_processed/ntuples_v4/higgsino_150.7_150.35_150.root'
    Signal_Tree_File_151_150p5_150    = '/storage/alesala/ntuples_processed/ntuples_v4/higgsino_151_150.5_150.root'
    Signal_Tree_File_151p5_150p75_150 = '/storage/alesala/ntuples_processed/ntuples_v4/higgsino_151.5_150.75_150.root'
    Signal_Tree_File_152_151_150      = '/storage/alesala/ntuples_processed/ntuples_v4/higgsino_152_151_150.root'
    Signal_Tree_File_153_151p5_150    = '/storage/alesala/ntuples_processed/ntuples_v4/higgsino_153_151.5_150.root'

if doPreselection == True:
	Plots = {  'cuts'                   : {'PlotNBins' : 1,  'PlotBinLow' : 0,    'PlotBinHigh' : 1,  'titleX' : 'Region'},
			   'MET'                    : {'PlotNBins' : 20, 'PlotBinLow' : 300,  'PlotBinHigh' : 400,  'titleX' : 'E_{T}^{miss} [GeV]'},
               'TrackD0Sig'             : {'PlotNBins' : 20, 'PlotBinLow' : 4.,   'PlotBinHigh' : 8.,   'titleX' : 'S_{d_{0}}'},
               'TrackPt'                : {'PlotNBins' : 20, 'PlotBinLow' : 1.5,  'PlotBinHigh' : 5.,   'titleX' : 'p_{T}^{track} [GeV]'},
	       	   'DPhiJetMET'             : {'PlotNBins' : 32, 'PlotBinLow' : 0.,   'PlotBinHigh' : 3.14, 'titleX' : '#left|#Delta#phi(p_{T}^{lead jet},E^{miss}_{T})#right|'},
	           'mu'                     : {'PlotNBins' : 30, 'PlotBinLow' : 10.,  'PlotBinHigh' : 70,   'titleX' : '#mu'},
	           'mT'                     : {'PlotNBins' : 20, 'PlotBinLow' : 30.,  'PlotBinHigh' : 100,  'titleX' : 'm_{T} [GeV]'},
	           'DiLeptonMass'           : {'PlotNBins' : 17, 'PlotBinLow' : 81.5, 'PlotBinHigh' : 98.5, 'titleX' : 'm_{ll} [GeV]'},
	           'dRBaseLepPhotonTrack'   : {'PlotNBins' : 25, 'PlotBinLow' : 0.,   'PlotBinHigh' : 1.,   'titleX' : '#DeltaR(#gamma, track)'},
	           'dRSignalLepPhotonTrack' : {'PlotNBins' : 25, 'PlotBinLow' : 0.,   'PlotBinHigh' : 1., 'titleX' : '#DeltaR(#gamma, track)'},
        	}
elif doNminus1 == True:
    Plots = {  'MET'        : {'PlotNBins' : 35, 'PlotBinLow' : 300,  'PlotBinHigh' : 1000,  'titleX' : 'E_{T}^{miss} [GeV]'},
               'TrackD0Sig' : {'PlotNBins' : 35, 'PlotBinLow' : 2.,  'PlotBinHigh' : 72.,   'titleX' : 'S_{d_{0}}'},
               'TrackD0'    : {'PlotNBins' : 40, 'PlotBinLow' : 2.,  'PlotBinHigh' : 80.,   'titleX' : 'd_{0} [mm]'},
               'TrackPt'    : {'PlotNBins' : 35, 'PlotBinLow' : 1.,  'PlotBinHigh' : 8.,   'titleX' : 'p_{T}^{track} [GeV]'},
               'DPhiTrackMET' : {'PlotNBins' : 2, 'PlotBinLow' : 0.,  'PlotBinHigh' : 1.,   'titleX' : '#left|#Delta#phi(p_{T}^{track},E^{miss}_{T})#right|'},
        }
else:
    raise Exception("No Plots dictionary selected - BREAK")
            

def main():

    # Where's the pickle file?
    pickleFilename = wsfilename.replace("ABCD_combined_BasicMeasurement_model_afterFit.root", "MyYieldsTable_{}.pickle".format(variable))
    #pickleFilename = (("Plots/SR_A_{}").format(method) + ("/results/ABCD_noSyst/MyYieldsTable_{}_{}.pickle".format(region,variable)))
    
    # Used as plot title
    #region = ""

    # Samples to stack on top of eachother in each region
    if "gammaJets" in region:  samples = "GJets,dijet,Zjets,Wtaunu,Wmunu,Wenu,VV,ttbar,top,Ztautau"
    elif "multijet" in region: samples = "dijet,GJets,Zjets,Wtaunu,Wmunu,Wenu,VV,ttbar,top,Ztautau"
    else: 					   samples = "Zjets,GJets,dijet,Wtaunu,Wmunu,Wenu,VV,ttbar,top,Ztautau"

    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    if not os.path.exists(pickleFilename):
        print("pickle filename %s does not exist" % pickleFilename)
        print("will proceed to run yieldstable again")
        
        # Run YieldsTable.py with all regions and samples requested, -P command is MANDATORY for multibin plot
        cmd = "YieldsTable.py -b -c %s -s %s -w %s -P -o %s" % (",".join(regionList), samples, wsfilename, pickleFilename.replace(".pickle",".tex"))
        print(cmd)
        subprocess.call(cmd, shell=True)

    if not os.path.exists(pickleFilename):
        print("pickle filename %s still does not exist" % pickleFilename)
        return
    
    if doPullPlot == False:
        regionList = []
        NBins = Plots[variable]['PlotNBins']
        for bin in range(NBins):
            regionList.append("{}_{}_bin{}".format(region,variable,bin))

    # Open the pickle and make the multibin plot
    MymakePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)


# Build a dictionary that remaps region names
def renameRegions():

    myRegionDict = {}

    NBins = Plots[variable]['PlotNBins']
    for bin in range(NBins):
        myRegionDict["{}_{}_bin{}".format(region,variable,bin)] = "{}".format(bin)

    # Remap region names using the old name as index, e.g.:
    myRegionDict[region]                     = region
    myRegionDict["SR_A"]                     = "SR_A_Sd0_20plus"
    myRegionDict["SR_A_8_20"]                = "SR_A_Sd0_8_20" 
    myRegionDict["SR_A_4_8"]                 = "SR_A_Sd0_4_8" 
    myRegionDict["CR_B"]                     = "CR_B_Sd0_20plus"
    myRegionDict["CR_B_8_20"]                = "CR_B_Sd0_8_20" 
    myRegionDict["CR_B_4_8"]                 = "CR_B_Sd0_4_8" 
    myRegionDict["CR_Wlnu_300_400"]          = "CR_Wl#nu_MET_300_400"
    myRegionDict["CR_Wlnu_600plus"]          = "CR_Wl#nu_MET_600plus" 
    myRegionDict["CR_taunu_had_lep_300_400"] = "CR_W#tau#nu_had_lep_MET_300_400"
    myRegionDict["CR_taunu_had_lep_600plus"] = "CR_W#tau#nu_had_lep_MET_600plus"
    myRegionDict["CR_multijet_300_400"]      = "CR_dijet_MET_300_400"
    myRegionDict["CR_multijet_500plus"]      = "CR_dijet_MET_500plus" 
    myRegionDict["VR_gammaJets"]             = "VR_#gamma-jets_Sd0_20plus"
    myRegionDict["VR_gammaJets_8_20"]        = "VR_#gamma-jets_Sd0_8_20" 
    myRegionDict["VR_gammaJets_4_8"]         = "VR_#gamma-jets_Sd0_4_8" 
    myRegionDict["CR_B_gammaJets"]           = "CR_B_#gamma-jets_Sd0_20plus"
    myRegionDict["CR_B_gammaJets_8_20"]      = "CR_B_#gamma-jets_Sd0_8_20" 
    myRegionDict["CR_B_gammaJets_4_8"]       = "CR_B_#gamma-jets_Sd0_4_8" 

    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():

    if doPullPlot:
	    regionList = ["CR_B","CR_B_8_20", "CR_B_4_8", "CR_C", "CR_D", "CR_Wlnu_300_400", "CR_Wlnu_600plus", "CR_taunu_had_lep_300_400", "CR_taunu_had_lep_600plus", "CR_C_tracks", "VR_D_tracks", "CR_B_gammaJets", "CR_B_gammaJets_8_20", "CR_B_gammaJets_4_8", "CR_multijet_300_400", "CR_multijet_500plus", "VR_gammaJets", "VR_gammaJets_8_20", "VR_gammaJets_4_8"]
    else:
        regionList = ["{}_{}".format(region,variable)] # here bins are not specified

    return regionList

# Define the colors for the pull bars
def getRegionColor(name): 
	
    if "SR_A" in name:                           return kRed
	elif "CR_B" in name and "gamma" not in name: return kRed
	elif "CR_C" in name:                         return kRed
	elif "CR_D" in name:                         return kRed
	elif "VR_D" in name:                         return kRed
	elif "Wl" in name:                           return kOrange-3
	elif "had_lep" in name:                      return kYellow
	elif "dijet" in name:                        return kBlue-9
	elif "gamma" in name:                        return kBlue-4
    else:
        print("cannot find color for region (",name,")")

	return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    
    if sample == "ttbar":         return kGray+1
    if sample == "top":           return kGray+2
    if sample == "dijet":         return kBlue-9 
    if sample == "GJets":         return kBlue-4
    if sample == "VV":      	  return TColor.GetColor("#ffca99")
    if sample == "Znunu":         return kRed
    if sample == "Zee":           return kYellow-7 
    if sample == "Zmumu":         return kOrange+9
    if sample == "Zjets":         return kRed
    if sample == "Ztautau":       return TColor.GetColor("#cdffff") 
    if sample == "Wenu":      	  return kRed+3
    if sample == "Wmunu":         return kOrange-3
    if sample == "Wtaunu":        return kYellow
    else:
        print("cannot find color for sample (",sample,")")
    
    return 1

def PoissonError(obs):
    """
    Return the Poisson uncertainty on a number

    @param obs The number to calculate the uncertainty for
    reference from ATLAS Statistics Forum: http://www.pp.rhul.ac.uk/~cowan/atlas/ErrorBars.pdf
    """
    posError = TMath.ChisquareQuantile(1. - (1. - 0.68) / 2., 2. * (obs + 1.)) / 2. - obs
    negError = obs - TMath.ChisquareQuantile((1. - 0.68) / 2., 2. * obs) / 2
    symError = abs(posError - negError) / 2.
    return (posError, negError, symError)

def MakeBox(color = 4, offset = 0, pull = -1, horizontal = False, doPreFit = False, error = 0):
    graph = TGraphErrors(1) if doPreFit else TGraph(4)
    if horizontal:
        if doPreFit:
            graph.SetPoint(0, 0.5 + offset, pull)
            graph.SetPointError(0, 0., error)
        else:
            graph.SetPoint(0, 0.1 + offset, 0);
            graph.SetPoint(1, 0.1 + offset, pull);
            graph.SetPoint(2, 0.9 + offset, pull);
            graph.SetPoint(3, 0.9 + offset, 0);
    else:
        if doPreFit:
            graph.SetPoint(0, pull, 0.3 + offset)
            graph.SetPoint(1, pull, 0.7 + offset)
        else:
            graph.SetPoint(0, 0, 0.3 + offset);
            graph.SetPoint(1, pull, 0.3 + offset);
            graph.SetPoint(2, pull, 0.7 + offset);
            graph.SetPoint(3, 0, 0.7 + offset);
    graph.SetFillColor(color);
    graph.SetLineColor(color);
    graph.SetLineWidth(2);
    graph.SetLineStyle(1);
    graph.SetMarkerStyle(20);
    return graph

def MymakePullPlot(pickleFilename, regionList, samples, renamedRegions, outputPrefix, doBlind, outDir = outputDir, plotSignificance = ""):

    try:
        picklefile = open(pickleFilename,'rb')
    except IOError:
        print("Cannot open pickle %s, continuing to next" % pickleFilename)
        return

    mydict = pickle.load(picklefile)

    results1 = []

    for region in mydict["names"]:
        index = mydict["names"].index(region)
        try:
            nbObs = mydict["nobs"][index]
        except:
            nbObs = 0

        if doAfterFit == True:
            nbExp   = mydict["TOTAL_FITTED_bkg_events"][index]
            nbExpEr = mydict["TOTAL_FITTED_bkg_events_err"][index]

        elif doBeforeFit == True:
            nbExp   = mydict["TOTAL_MC_EXP_BKG_events"][index]
            nbExpEr = mydict["TOTAL_MC_EXP_BKG_err"][index]
        else:
            print('Please choose before/after fit')

        pEr     = PoissonError(nbObs)
        totEr   = sqrt(nbExpEr*nbExpEr+pEr[2]*pEr[2])
        totErDo = sqrt(nbExpEr*nbExpEr+pEr[1]*pEr[1])
        totErUp = sqrt(nbExpEr*nbExpEr+pEr[0]*pEr[0])

        pEr_pull     = PoissonError(nbExp)
        totEr_pull   = sqrt(nbExpEr*nbExpEr+pEr_pull[2]*pEr_pull[2])
        totErDo_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[1]*pEr_pull[1])
        totErUp_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[0]*pEr_pull[0])
            
        plot_pulls = True
        if plot_pulls == True:
            #print ("plot pull = (obs-exp)/err in the bottom panel")
            if (nbObs - nbExp) > 0 and totErUp_pull != 0:
                pull = (nbObs - nbExp) / totErUp_pull
            if (nbObs - nbExp) <= 0 and totErDo_pull != 0:
                pull = (nbObs - nbExp) / totErDo_pull
            #print (pull)
            if -0.02 < pull < 0: pull = -0.02 ###ATT: ugly
            if 0 < pull < 0.02:  pull = 0.02  ###ATT: ugly

        nbExpComponents = []
        for sam in samples.split(","):
            if doAfterFit == True:
                nbExpComponents.append((sam, mydict["Fitted_events_"+sam][index] ))
            elif doBeforeFit == True:
                nbExpComponents.append((sam, mydict["MC_exp_events_"+sam][index] ))

        results1.append((region, pull, nbObs, nbExp, nbExpEr, totEr, nbExpComponents))

    #pull
    if plotSignificance == "":
        if scale == "log":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, 0.12)
        elif scale == "linear":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, 0.12, logy = False)
    else:
        if scale == "log":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, minimum = 0.05)
        elif scale == "linear":
            MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, minimum = 0.05, logy = False)

    # return the results array in case you want to use this somewhere else
    return results1

def MakeHist(regionList, renamedRegions, results, hdata, hbkg, hbkgUp, hbkgDown, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents):
    max = 0
    min = 99999999999.
    for counter in range(len(regionList)):#loop over all the regions
        nObs = 0
        nExp = 0
        nExpEr = 0
        nExpTotEr = 0
        nExpStatEr = 0
        nExpStatErUp = 0
        nExpStatErDo = 0
        nExpTotErUp = 0
        nExpTotErDo = 0
        pull = 0

        name = regionList[counter].replace(" ", "")
        if name in renamedRegions.keys():
            name = renamedRegions[name]

        for info in results: #extract the information
            if regionList[counter] in info[0]:
                nObs   = info[2]
                nExp   = info[3]
                nExpEr = info[4]

                if nExp > 0:
                    nExpStatEr = sqrt(nExp)
                pEr = PoissonError(nObs)
                nExpStatErUp = pEr[0]
                nExpStatErDo = pEr[1]

                if name.find("CR") < 0:
                    nExpTotEr   = sqrt(nExpEr*nExpEr)
                    nExpTotErUp = sqrt(nExpEr*nExpEr)
                    nExpTotErDo = sqrt(nExpEr*nExpEr)
                else:
                    nExpTotEr = nExpEr
        
                if (nObs-nExp) >= 0 and nExpTotErUp != 0:
                    pull = (nObs - nExp) / nExpTotErUp
                if (nObs-nExp) <= 0 and nExpTotErDo != 0:
                    pull = (nObs - nExp) / nExpTotErDo
                if nObs == 0 and nExp == 0:
                    pull = 0
                    nObs = -100
                    nPred = -100
                    nExpEr = 0
                    nExpTotEr = 0
                    nExpStatEr = 0
                    nExpStatErUp = 0
                    nExpStatErDo = 0
                    nExpTotErUp = 0
                    nExpTotErDo = 0

                #bkg components
                compInfo = info[6]
                for i in range(len(compInfo)):
                    hbkgComponents[i].SetBinContent(counter + 1, compInfo[i][1])

                break

        if nObs > max: max = nObs
        if (nExp + nExpTotErUp) > max: max = nExp + nExpTotErUp
        if nObs < min and nObs != 0:   min = nObs
        if nExp < min and nExp != 0:   min = nExp

        if doPullPlot:
            bin_width = 1.
        else:
            bin_width = (Plots[variable]['PlotBinHigh'] - Plots[variable]['PlotBinLow']) / Plots[variable]['PlotNBins']

        graph_bkg.SetPoint(counter, hbkg.GetBinCenter(counter + 1), nExp)
        graph_bkg.SetPointError(counter, bin_width, bin_width, nExpStatErDo, nExpStatErUp)

        graph_bkg2.SetPoint(counter, hbkg.GetBinCenter(counter + 1), nExp)
        graph_bkg2.SetPointError(counter, bin_width / 2., bin_width / 2., nExpEr, nExpEr)

        graph_bkg3.SetPoint(counter, hbkg.GetBinCenter(counter + 1), nExp)
        graph_bkg3.SetPointError(counter, bin_width / 2., bin_width / 2., 0., 0.)

        if  nObs > 0: # remove this condition to also show upper limits data bars in bins with 0 observed events
            graph_data.SetPoint(counter, hbkg.GetBinCenter(counter + 1), nObs)
            graph_data.SetPointError(counter, 0., 0., PoissonError(nObs)[1], PoissonError(nObs)[0])

        if not nObs > 0:
            nObs = 0
        
        print("{}  nObs = {}, nObsErUp = {}, nObsErDown = {}, nExp = {}, nExpEr = {}".format(info[0], nObs, PoissonError(nObs)[0], PoissonError(nObs)[1], nExp, nExpEr))

        graph_pull.SetPoint(counter, hbkg.GetBinCenter(counter + 1), pull)
        graph_pull.SetPointError(counter, 0., 0., 0., 0.)

        if doBlind == False:
            hdata.SetBinContent(counter + 1, nObs)
            hdata.SetBinError(counter + 1, sqrt(nObs))

        hbkg.SetBinContent(counter + 1, nExp)
        hbkg.SetBinError(counter + 1, nExpEr)

        hbkgUp.SetBinContent(counter + 1, nExp + nExpTotErUp)
        hbkgDown.SetBinContent(counter + 1, nExp - nExpTotErDo)

    return

# Methods from PullPlotUtils
def MyMakeHistPullPlot(samples, regionList, outFileNamePrefix, hresults, renamedRegions, doBlind, outDir = outputDir, minimum = 0.1, maximum = None, logy = True):
    print("========================================", outFileNamePrefix)
    ROOT.gStyle.SetOptStat(0000);
    Npar = len(regionList)
 
    if doPullPlot:
        BinHigh = Npar
        BinLow  = 0.
    else:
        BinHigh = Plots[variable]['PlotBinHigh']
        BinLow  = Plots[variable]['PlotBinLow']

    hdata = TH1F(outFileNamePrefix, outFileNamePrefix, Npar, BinLow, BinHigh)
    hdata.GetYaxis().SetTitle("Entries")
    hdata.GetYaxis().SetTitleSize(0.065)
    hdata.GetYaxis().SetTitleOffset(0.8)
    hdata.GetXaxis().SetLabelSize(0.0)
    hdata.GetYaxis().SetLabelSize(0.05)
    hdata.SetMarkerStyle(20)
    hdata.SetMarkerColor(kBlack)

    hbkg = TH1F(" ", " ", Npar, BinLow, BinHigh)
    hbkg.GetYaxis().SetTitle("Entries")
    hbkg.GetYaxis().SetTitleSize(0.065)
    hbkg.GetYaxis().SetTitleOffset(0.8)
    hbkg.GetXaxis().SetLabelSize(0.0)
    hbkg.GetYaxis().SetLabelSize(0.05)
    hbkg.SetLineColor(1)
    hbkg.SetLineWidth(2)

    hbkgUp = TH1F("hbkgUp", "hbkgUp", Npar, BinLow, BinHigh)
    hbkgUp.SetLineStyle(0)
    hbkgUp.SetLineWidth(0)

    hbkgDown = TH1F("hbkgDown", "hbkgDown", Npar, BinLow, BinHigh);
    hbkgDown.SetLineStyle(0)
    hbkgDown.SetLineWidth(0)

    hbkgComponents = []
    samples.replace(" ", "") #remove spaces, and split by comma => don't split by ", "
    for sam in samples.split(","):
        h = TH1F("hbkg" + sam, "hbkg" + sam, Npar, BinLow, BinHigh)
        if PlotsForPaper == True:
            if sam in ["Zjets", "Zttjets", "VVV", "other"]:
                h.SetLineColor(TColor.GetColor('#41ab5d'))
            else:
                h.SetLineWidth(0)
        if PlotsForPaper == False:
            h.SetLineWidth(0)
        h.SetFillColor(getSampleColor(sam))
        hbkgComponents.append(h)

    if doPullPlot:
        graph_bkg  = TGraphAsymmErrors(Npar)
        graph_bkg2 = TGraphAsymmErrors(Npar)
        graph_bkg3 = TGraphAsymmErrors(Npar)
        graph_data = TGraphAsymmErrors(Npar)
        graph_pull = TGraphAsymmErrors(Npar)
    else:
        graph_bkg  = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
        graph_bkg2 = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
        graph_bkg3 = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
        graph_data = TGraphAsymmErrors(Plots[variable]['PlotNBins'])
    
    graph_bkg.SetFillColor(1)
    graph_bkg2.SetFillColor(1)
    graph_bkg2.SetLineWidth(2)
    graph_bkg2.SetFillStyle(3004)
    graph_bkg3.SetFillColor(kCyan+1)
    graph_data.SetFillColor(kCyan+1)

    MakeHist(regionList,  renamedRegions,  hresults, hdata, hbkg, hbkgDown, hbkgUp, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents)

    if addSignal == False:
        myleg = TLegend(0.5,0.60,0.71,0.86)
    else:
        myleg = TLegend(0.35,0.50,0.58,0.86)
    myleg.SetNColumns(2)
    myleg.SetBorderSize(0)
    myleg.SetFillStyle(0)
    myleg.SetTextSize(0.031)
    if doBlind == False:
        myleg.AddEntry(graph_data, renamedRegions.get("data", "Data"), "p")
    myleg.AddEntry(graph_bkg2, renamedRegions.get("bkg2", "SM"), "fl")
    
    # Add background in same order as above
    for h, sample in zip(hbkgComponents, samples.split(",")):
      if PlotsForPaper == True:
          if sample not in ["Zjets","Zttjets","VVV","other"]:
              myleg.AddEntry(h, renamedRegions.get(sample, sample), "f")
          if sample == "Zjets":
              myleg.AddEntry(h, "Others", "f")
      else:
          if sample == "VV":
              myleg.AddEntry(h, "VV", "f")
          elif sample == "ttbar":
              myleg.AddEntry(h, "t#bar{t}", "f")
          elif sample == "top":
              myleg.AddEntry(h, "single top", "f")
          elif "Zjets" in sample:
              myleg.AddEntry(h, "Z+jets", "f")
          elif "Znunu" in sample:
              myleg.AddEntry(h, "Z(#rightarrow#nu#nu)+jets", "f")
          elif sample == "Ztautau":
              myleg.AddEntry(h, "Z(#rightarrow#tau#tau)+jets", "f")
          elif sample == "Zee":
              myleg.AddEntry(h, "Z(#rightarrowee)+jets", "f")
          elif sample == "Zmumu":
              myleg.AddEntry(h, "Z(#rightarrow#mu#mu)+jets", "f")
          elif sample == "Wenu":
              myleg.AddEntry(h, "W(#rightarrowe#nu)+jets", "f")
          elif sample == "Wmunu":
              myleg.AddEntry(h, "W(#rightarrow#mu#nu)+jets", "f")
          elif "Wtaunu" in sample:
              myleg.AddEntry(h, "W(#rightarrow#tau#nu)+jets", "f")	
          elif "GJets" in sample:
              myleg.AddEntry(h, "#gamma-jets", "f")	
          else:
              myleg.AddEntry(h, renamedRegions.get(sample, sample), "f")
 
    c = TCanvas("c" + outFileNamePrefix, outFileNamePrefix, 600, 400);
    upperPad = ROOT.TPad("upperPad", "upperPad", 0.001, 0.30, 0.995, 0.995)
    lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.001, 0.000, 0.995, 0.295)

    upperPad.SetFillColor(0)
    upperPad.SetBorderMode(0)
    upperPad.SetBorderSize(2)
    upperPad.SetTicks()
    upperPad.SetTopMargin(0.1)
    upperPad.SetRightMargin(0.11)
    upperPad.SetBottomMargin(0.0025)
    upperPad.SetLeftMargin(0.12)
    upperPad.SetFrameBorderMode(0)
    upperPad.SetFrameBorderMode(0)
    if logy: upperPad.SetLogy()
    upperPad.Draw()
    
    if doPullPlot:
        lowerPad.SetGridx()
        lowerPad.SetGridy()
        lowerPad.SetFillColor(0)
        lowerPad.SetTickx(1)
        lowerPad.SetTicky(1)
    lowerPad.SetBorderMode(0)
    lowerPad.SetBorderSize(2)
    lowerPad.SetTopMargin(0.035)
    lowerPad.SetRightMargin(0.11)
    lowerPad.SetBottomMargin(0.35)
    lowerPad.SetLeftMargin(0.12)
    lowerPad.Draw()

    c.SetFrameFillColor(ROOT.kWhite)

    upperPad.cd()

    if logy:
        if minimum: hbkg.SetMinimum(minimum)
        if doPullPlot: hbkg.SetMaximum(1E8)
        elif 'SR_A' in region:
            hbkg.SetMaximum(1E3)
        elif 'CR_B' in region or 'CR_C' in region or 'CR_D' in region:
            hbkg.SetMaximum(1E6)
        elif 'CR_Wlnu' in region or 'CR_taunu_had_lep' in region:
            hbkg.SetMaximum(1E4)
        elif 'CR_B_multijet' in region or 'CR_gammaJets' in region:
            hbkg.SetMaximum(1E5)
        else:
            hbkg.SetMaximum(1E8)
    else:
        hbkg.SetMaximum(hdata.GetMaximum() * 2) 
        hbkg.SetMinimum(0.001) # to avoid an halved 0 appearing
        hbkg.GetYaxis().SetMaxDigits(3)

    hbkg.Draw("hist")
    hbkgUp.Draw("hist,same")
    hbkgDown.Draw("hist,same")

    myleg.Draw()
    stack = THStack("stack", "stack")
    for h in reversed(hbkgComponents):
        stack.Add(h)
    stack.Draw("same")

    graph_data.SetMarkerStyle(20)
    graph_data.SetMarkerSize(0.8)
    if doBlind == False:
        graph_data.Draw("P")
    graph_bkg2.Draw("2")

    if doPullPlot: hbkg.GetXaxis().SetNdivisions(-len(regionList))
    hbkg.Draw("hist,same,axis")
   
    if addSignal:

        d1 = ROOT.RDataFrame(Signal_Tree_Name, Signal_Tree_File_150p5_150p25_150)
        d2 = ROOT.RDataFrame(Signal_Tree_Name, Signal_Tree_File_150p7_150p35_150)
        d3 = ROOT.RDataFrame(Signal_Tree_Name, Signal_Tree_File_151_150p5_150)
        d4 = ROOT.RDataFrame(Signal_Tree_Name, Signal_Tree_File_151p5_150p75_150)
        d5 = ROOT.RDataFrame(Signal_Tree_Name, Signal_Tree_File_152_151_150)
        d6 = ROOT.RDataFrame(Signal_Tree_Name, Signal_Tree_File_153_151p5_150)
    
        h1 = d1.Filter(Signal_cut).Define("weight", "1000*eventWeight*genWeight*pileupWeight*jvtWeight*leptonWeight*leptonTrigWeight*photonWeight*140068.94").Histo1D(("hsignal", "hsignal", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']), variable, "weight")
        h2 = d2.Filter(Signal_cut).Define("weight", "1000*eventWeight*genWeight*pileupWeight*jvtWeight*leptonWeight*leptonTrigWeight*photonWeight*140068.94").Histo1D(("hsignal", "hsignal", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']), variable, "weight")
        h3 = d3.Filter(Signal_cut).Define("weight", "1000*eventWeight*genWeight*pileupWeight*jvtWeight*leptonWeight*leptonTrigWeight*photonWeight*140068.94").Histo1D(("hsignal", "hsignal", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']), variable, "weight")
        h4 = d4.Filter(Signal_cut).Define("weight", "1000*eventWeight*genWeight*pileupWeight*jvtWeight*leptonWeight*leptonTrigWeight*photonWeight*140068.94").Histo1D(("hsignal", "hsignal", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']), variable, "weight")
        h5 = d5.Filter(Signal_cut).Define("weight", "1000*eventWeight*genWeight*pileupWeight*jvtWeight*leptonWeight*leptonTrigWeight*photonWeight*140068.94").Histo1D(("hsignal", "hsignal", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']), variable, "weight")
        h6 = d6.Filter(Signal_cut).Define("weight", "1000*eventWeight*genWeight*pileupWeight*jvtWeight*leptonWeight*leptonTrigWeight*photonWeight*140068.94").Histo1D(("hsignal", "hsignal", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh']), variable, "weight")

        hsignal_150p5_150p25 = TH1F("h_150.5_150.25_150", "h_150.5_150.25_150", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_150p7_150p35 = TH1F("h_150.7_150.35_150", "h_150.7_150.35_150", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_151_150p5    = TH1F("h_151_150.5_150", "h_151_150.5_150", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_151p5_150p75 = TH1F("h_151.5_150.75_150", "h_151.5_150.75_150", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_152_151      = TH1F("h_152_151_150", "h_152_151_150", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        hsignal_153_151p5    = TH1F("h_153_151.5_150", "h_153_151.5_150", Plots[variable]['PlotNBins'], Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])

        Signal_array = [hsignal_150p5_150p25, hsignal_150p7_150p35, hsignal_151_150p5, hsignal_151p5_150p75, hsignal_152_151, hsignal_153_151p5]

        for i in range(Npar):
            if i == Npar-1: #Last bin, catch the overflows
                hsignal_150p5_150p25.SetBinContent(i+1, h1.GetBinContent(i+1)+h1.GetBinContent(i+2)) 
                hsignal_150p7_150p35.SetBinContent(i+1, h2.GetBinContent(i+1)+h2.GetBinContent(i+2)) 
                hsignal_151_150p5.SetBinContent(i+1, h3.GetBinContent(i+1)+h3.GetBinContent(i+2)) 
                hsignal_151p5_150p75.SetBinContent(i+1, h4.GetBinContent(i+1)+h4.GetBinContent(i+2))
                hsignal_152_151.SetBinContent(i+1, h5.GetBinContent(i+1)+h5.GetBinContent(i+2)) 
                hsignal_153_151p5.SetBinContent(i+1, h6.GetBinContent(i+1)+h6.GetBinContent(i+2)) 
            else:
                hsignal_150p5_150p25.SetBinContent(i+1, h1.GetBinContent(i+1))
                hsignal_150p7_150p35.SetBinContent(i+1, h2.GetBinContent(i+1))
                hsignal_151_150p5.SetBinContent(i+1, h3.GetBinContent(i+1))
                hsignal_151p5_150p75.SetBinContent(i+1, h4.GetBinContent(i+1))
                hsignal_152_151.SetBinContent(i+1, h5.GetBinContent(i+1))
                hsignal_153_151p5.SetBinContent(i+1, h6.GetBinContent(i+1))

        #hsignal_150p5_150p25.SetLineStyle(2)
        #hsignal_150p7_150p35.SetLineStyle(3)
        #hsignal_151_150p5.SetLineStyle(4)
        #hsignal_151p5_150p75.SetLineStyle(9)
        #hsignal_152_151.SetLineStyle(6)
        #hsignal_153_151p5.SetLineStyle(7)

        hsignal_150p5_150p25.SetLineColor(3)
        hsignal_150p7_150p35.SetLineColor(4)
        hsignal_151_150p5.SetLineColor(6)
        hsignal_151p5_150p75.SetLineColor(8)
        hsignal_152_151.SetLineColor(9)
        hsignal_153_151p5.SetLineColor(21)

        hsignal_150p5_150p25.Draw("same")
        hsignal_150p7_150p35.Draw("same")
        hsignal_151_150p5.Draw("same")
        hsignal_151p5_150p75.Draw("same")
        hsignal_152_151.Draw("same")
        hsignal_153_151p5.Draw("same")

        if doBlind == False:
            graph_data.Draw("P") # redraw on top of sig

        myleg2 = TLegend(0.59, 0.55, 0.68, 0.88)
        myleg2.SetNColumns(1)
        myleg2.SetBorderSize(0)
        myleg2.SetFillStyle(0)
        myleg2.SetTextSize(0.032)
        myleg2.AddEntry(hsignal_150p5_150p25, "m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (150.5,150.25,150) GeV", "l")
        myleg2.AddEntry(hsignal_150p7_150p35, "m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (150.7,150.35,150) GeV", "l")
        myleg2.AddEntry(hsignal_151_150p5,    "m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (151,150.5,150) GeV", "l")
        myleg2.AddEntry(hsignal_151p5_150p75, "m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (151.5,150.75,150) GeV", "l")
        myleg2.AddEntry(hsignal_152_151,      "m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (152,151,150) GeV", "l")
        myleg2.AddEntry(hsignal_153_151p5,    "m(#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = (153,151.5,150) GeV", "l")
        myleg2.Draw()

    lumiText = TLatex()
    lumiText.SetNDC()
    lumiText.SetTextAlign( 11 )
    lumiText.SetTextFont( 42 )
    lumiText.SetTextSize( 0.05 )
    lumiText.SetTextColor( 1 )
    #lumiText.SetTextSize( 0.05 )   change size if needed
    lumiText.DrawLatex(0.17, 0.81, "#bf{#it{ATLAS}} Internal")
    if doPullPlot:
	    lumiText.DrawLatex(0.15, 0.75, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}".format(getattr(MyMakeHistPullPlot, 'luminosity', 140.0)))
    else:
        if 'SR_A' in region or 'CR_B' in region or 'CR_C' in region or 'CR_D' in region or 'jets' in region:
            lumiText.DrawLatex(0.15, 0.75, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}".format(getattr(MyMakeHistPullPlot, 'luminosity', 140.0)))
            if "4_8" in region or "8_20" in region:           lumiText.DrawLatex(0.18, 0.70, "{}".format(renamedRegions[region])) 
            if "gammaJets" in region or "multijet" in region: lumiText.DrawLatex(0.16, 0.70, "{}".format(renamedRegions[region]))
            else:                                             lumiText.DrawLatex(0.20, 0.70, "{}".format(renamedRegions[region])) 
        elif 'CR_Wlnu' in region or 'CR_taunu' in region:
            lumiText.DrawLatex(0.15, 0.75, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}".format(getattr(MyMakeHistPullPlot, 'luminosity', 140.0))) 
            lumiText.DrawLatex(0.19, 0.70, "{}".format(renamedRegions[region])) 
        else:    
            lumiText.DrawLatex(0.15, 0.75, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}".format(getattr(MyMakeHistPullPlot, 'luminosity', 140.0))) 
            lumiText.DrawLatex(0.20, 0.70, "{}".format(renamedRegions[region])) 
        if doAfterFit and doNminus1 == False:
            lumiText.DrawLatex(0.19,0.60, "After Fit")
        elif doBeforeFit and doNminus1 == False:
            lumiText.DrawLatex(0.19,0.60, "Before Fit")

    lowerPad.cd()

    if doSignificance == True:

        significance_array = []

        for hsignal in Signal_array:

            hSignificance     = TH1F(("hSignificance_{}").format(hsignal),    "", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
       	    hSignificance_int = TH1F(("hSignificance_int_{}").format(hsignal),"", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])

            print(hSignificance_int.GetName())

            for i in range(Npar):

                #SM = 0.
                #SM_err = 0.
                #Signal = 0.
                #SM = graph_bkg2.GetPointY(i)
                #SM_err = graph_bkg2.GetErrorY(i)
                #Signal = hsignal.GetBinContent(i+1)

                SM_int = 0.
                SM_int_err = 0.
                Signal_int = 0.

                if cutLeft == True:
                    my_range = range(0, i+1)
                elif cutRight == True:
                    my_range = range(i, Npar)
        
                for k in my_range:           
                    SM_int += graph_bkg2.GetPointY(k)
                    #SM_int_err += graph_bkg2.GetErrorY(k)*graph_bkg2.GetErrorY(k)
                    Signal_int += hsignal.GetBinContent(k+1)
                    #print("k =  {},        SM_int_err = {}    ".format( k+1, sqrt(SM_int_err)))
                
                #SM_int_err = sqrt(SM_int_err)
                SM_int_err = 0.1*SM_int
              
                if ( (SM_int!=0) and not (Signal_int == 0. or SM_int == 0.) ):
                    hSignificance_int.SetBinContent(i+1, atlas_significance((Signal_int+SM_int), SM_int, SM_int_err))
                    print('   BIN: {} - cumulative Signal = {}, cumulative SM = {}, cumulative SMerr = {}, cumulative Z - {}'.format(i+1, Signal_int, SM_int, SM_int_err, atlas_significance((Signal_int+SM_int), SM_int, SM_int_err )))
                else:
                    print('   BIN: {} - cumulative Signal = {}, cumulative SM = {}, cumulative SMerr = {}, cumulative Z - {}'.format(i+1, Signal_int, SM_int_err, SM_int,0.))
                    hSignificance_int.SetBinContent(i+1, 0.)     

                print('########')

            hSignificance_int.SetLineColor(hsignal.GetLineColor())
            #hSignificance_int.SetLineStyle(hsignal.GetLineStyle())

            hSignificance_int.GetXaxis().SetTitle(Plots[variable]['titleX'])
            hSignificance_int.GetXaxis().SetTitleSize(0.12)
            hSignificance_int.GetXaxis().SetTitleOffset(1.3)
            
            if cutLeft == True:
                hSignificance_int.GetYaxis().SetTitle("Cut left Z")
            elif cutRight == True:
                hSignificance_int.GetYaxis().SetTitle("Cut right Z")
            hSignificance_int.GetYaxis().SetTitleSize(0.12)
            hSignificance_int.GetYaxis().SetTitleOffset(0.4)
            hSignificance_int.SetMaximum(2.5)
            hSignificance_int.SetMinimum(0)
            hSignificance_int.GetXaxis().SetLabelSize(0.1)
            hSignificance_int.GetYaxis().SetLabelSize(0.08)
            hSignificance_int.GetXaxis().SetTickLength(0.1)
            
            hSignificance_int.Draw("same")
  
            significance_array.append(hSignificance_int)

        for h in significance_array:
            h.Draw("same")    
        
        #lumiText.DrawLatex(0.65, 0.8, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}, Z(s>0,b>0,#Deltab/b=10%)".format(getattr(MyMakeHistPullPlot,'luminosity',139.0)))
        #lumiText.DrawLatex(0.65, 0.8, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}, Z(s#geq3,b#geq1,#Deltab/b=20%)".format(getattr(MyMakeHistPullPlot,'luminosity',139.0)))

    elif doPullPlot:
        offset = 0.;
        Npar = len(regionList)
        
        frame = TH2F("frame", "", Npar, 0., Npar, 80, -3., 3.)
       
	    # Draw the frame on lower plot where pulls are plotted
        frame.GetXaxis().SetLabelOffset(0.05)
        frame.GetXaxis().SetLabelSize(0.08)
        frame.GetYaxis().SetRangeUser(-3., 3.)
        frame.GetYaxis().SetTitleOffset(0.4)
        frame.GetYaxis().SetTitleSize(0.12)
        frame.GetYaxis().SetLabelSize(0.08)
        frame.GetYaxis().SetNdivisions(4)
        frame.SetYTitle("Significance")
        
        for bin in range(1, hdata.GetNbinsX()+1):
            frame.GetXaxis().SetBinLabel(bin, hdata.GetXaxis().GetBinLabel(bin))

        frame.Draw();
        
        all = []
        counter = 0
        myr = hresults

		# For each region plot the pulls as colored boxes
        for info in myr:
            name = info[0].replace(" ", "")
            name = info[0].replace("_cuts", "")
            
            if "bin0" in name: continue

            if name in list(renamedRegions.keys()):
                name = renamedRegions[name]

            if ((name.find("SR") >= 0)  and doBlind):
                counter += 1
                continue

            for bin in range(1, frame.GetNbinsX()+2):
                if frame.GetXaxis().GetBinLabel(bin) != name: continue
                counter = bin - 1
                break

            frame.GetXaxis().SetBinLabel(counter+1, name);
            
            color = getRegionColor(name)
            
            graph = MakeBox(offset = counter, pull = float(info[1]), color = color, horizontal = True, doPreFit = False, error = info[7] if len(info) >= 8 else 0) 
            graph.Draw("sameLF")
            
            counter += 1
            all.append(graph)

    else:
        hShadow = TH1F("hShadow", "", Npar, Plots[variable]['PlotBinLow'], Plots[variable]['PlotBinHigh'])
        import numpy as np
        ratios = np.empty(0)
        exl = np.empty(0)
        exh = np.empty(0)
        eyl = np.empty(0)
        eyh = np.empty(0)

        for i in range(1, Npar+1):
            xSM = stack.GetStack().Last().GetBinContent(i)
            errxSM = graph_bkg2.GetErrorY(i-1)

            if xSM != 0 and i != Npar+1:

                bkg = hbkg.GetBinContent(i)
                data = hdata.GetBinContent(i)

                if data > 0: # remove this condition to also show upper limits data bars in bins with 0 observed events
                    ratio = data / bkg
                    positions = np.append(positions, hbkg.GetBinCenter(i))
                    ratios = np.append(ratios, ratio)
                    exl = np.append(exl, 0.)
                    exh = np.append(exh, 0.)
                    eyl = np.append(PoissonError(data)[1]/bkg, eyl)
                    eyh = np.append(PoissonError(data)[0]/bkg, eyh)

            if xSM != 0 and i != Npar+1:
                hShadow.SetBinContent(i, 1) 
                hShadow.SetBinError(i, errxSM/xSM)

        graphRatio = TGraphAsymmErrors(len(positions), positions, ratios, exl, exh, eyl, eyh)
        graphRatio.SetMinimum(0.)
        graphRatio.SetMaximum(2.)

        graphRatio.SetMarkerStyle(20)

        hShadow.SetLineColor(0)
        hShadow.SetFillColor(1)
        hShadow.SetFillStyle(3004)
        hShadow.SetLineColor(1)
        hShadow.SetMarkerSize(0)
        hShadow.SetMarkerColor(0)

        hShadow.GetXaxis().SetTitle(Plots[variable]['titleX'])
        hShadow.GetXaxis().SetTitleSize(0.12)
        hShadow.GetXaxis().SetTitleOffset(1.3)
        hShadow.GetYaxis().SetTitle("Data/SM")
        hShadow.GetYaxis().SetTitleSize(0.12)
        #hShadow.GetXaxis().SetNdivisions(505)
        hShadow.GetYaxis().SetNdivisions(505)
        hShadow.GetYaxis().SetTitleOffset(0.4)
        hShadow.GetXaxis().SetLabelSize(0.1)
        hShadow.GetYaxis().SetLabelSize(0.08)
        hShadow.GetXaxis().SetTickLength(0.1)
        hShadow.SetMinimum(0.)
        hShadow.SetMaximum(2.)

        hShadow.Draw("E2")
        if doBlind == False:
            graphRatio.Draw("P,same")
        
        firstbin = hShadow.GetXaxis().GetFirst()
        lastbin  = hShadow.GetXaxis().GetLast()
        xmax = hShadow.GetXaxis().GetBinUpEdge(lastbin) 
        xmin = hShadow.GetXaxis().GetBinLowEdge(firstbin) 

        l  = TLine(xmin, 1.0, xmax, 1.0)
        l2 = TLine(xmin, 0.5, xmax, 0.5)
        l3 = TLine(xmin, 1.5, xmax, 1.5)
        l4 = TLine(xmin, 2.0, xmax, 2.0)
        l5 = TLine(xmin, 2.5, xmax, 2.5)
        l.SetLineWidth(1)
        l.SetLineStyle(2)
        l2.SetLineStyle(3)
        l3.SetLineStyle(3)
        l4.SetLineStyle(3)
        l5.SetLineStyle(3)

        l.Draw("same")
        l2.Draw("same")
        l3.Draw("same")
        l4.Draw("same")
        l5.Draw("same")

    if doBlind:
        string = "Before" if doBeforeFit == True else "After"
        if doPullPlot: 
            region   = "All"
            variable = "Pull"
        if scale == "log":
            c.Print(os.path.join(outDir, "{}_{}_{}_LogScale__blindSR.pdf".format(region, variable, string)))
            #c.Print(os.path.join(outDir, "{}_blindSR.eps".format(variable)))
            #c.Print(os.path.join(outDir, "{}_blindSR.png".format(variable)))
        elif scale == "linear":
            c.Print(os.path.join(outDir, "{}_{}_{}_LinearScale__blindSR.pdf".format(region, variable, string)))
            #c.Print(os.path.join(outDir, "{}_blindSR.eps".format(variable)))
            #c.Print(os.path.join(outDir, "{}_blindSR.png".format(variable)))
    else:
        string = "Before" if doBeforeFit == True else "After"
        if doPullPlot:
            region   = "All"
            variable = "Pull"
        if scale == "log":
            c.Print(os.path.join(outDir, "{}_{}_{}_LogScale.pdf".format(region, variable, string)))
            #c.Print(os.path.join(outDir, "{}.eps".format(variable)))
            #c.Print(os.path.join(outDir, "{}.png".format(variable)))
        elif scale == "linear":
            c.Print(os.path.join(outDir, "{}_{}_{}_LinearScale.pdf".format(region, variable, string)))
            #c.Print(os.path.join(outDir, "{}.eps".format(variable)))
            #c.Print(os.path.join(outDir, "{}.png".format(variable)))

    return


###############
def atlas_significance(nbObs, nbExp, nbExpEr):
    factor1 = nbObs * log( (nbObs * (nbExp + nbExpEr**2)) / (nbExp**2 + nbObs * nbExpEr**2) )
    factor2 = (nbExp**2 / nbExpEr**2) * log( 1 + (nbExpEr**2 * (nbObs - nbExp)) / (nbExp * (nbExp + nbExpEr**2)) )

    if nbObs < nbExp:
        pull  = -sqrt(2 * (factor1 - factor2))
    else:
        pull  = sqrt(2 * (factor1 - factor2))
    return (pull)


### Code for "Plotting the Differences Between Data and Expectation"
### https://arxiv.org/abs/1111.2062
### Code from HistFitter/python/pValue.py
### Georgios Choudalakis and Diego Casadei

import math

def pValuePoissonError(nObs, E, V):
    print ("obs", nObs)
    if E <= 0 or V <= 0:
        print("ERROR in pValuePoissonError(): expectation and variance must be positive. ")
        print("Returning 0.5")

    B = E / V
    A = E * B

    if A > 100:  # need to use logarithms

        stop = nObs
        if nObs > E:
            stop = stop - 1

    # NB: must work in log-scale otherwise troubles!
        logProb = A * math.log(B / (1 + B))
        sum = math.exp(logProb) # P(n=0)
        for u in range(1, stop + 1):
            logProb += math.log((A + u - 1) / (u * (1 + B)))
            sum += math.exp(logProb)

        if nObs > E:  # excess
            return 1 - sum
        else:  # deficit
            return sum
        
    else:
        # Recursive formula: P(nA,B) = P(n-1A,B) (A+n-1)/(n*(1+B))
        p0 = pow(B / (1 + B), A) # P(0A,B)
        nExp = A / B
        if nObs > nExp: # excess
            pLast = p0
            sum = p0
            for k in range(1, nObs):
                p = pLast * (A + k - 1) / (k * (1 + B))
	# cout << Form("Excess: P(%d%8.5g) = %8.5g and sum = %8.5g",k-1,nExp,pLast,sum) << " -> "
                sum = sum + p
                pLast = p
	# cout << Form("P(%d%8.5g) = %8.5g and sum = %8.5g",k,nExp,pLast,sum) << endl      
            return 1 - sum
        else: # deficit
            pLast = p0
            sum = p0
            for k in range(1, nObs + 1):
	# cout << Form("Deficit: P(%d%8.5g) = %8.5g and sum = %8.5g",k-1,nExp,pLast,sum) << " -> "
                p = pLast * (A + k - 1) / (k * (1 + B))
                sum += p
                pLast = p
            return sum

def pja_normal_quantile( p): 

    a = [ -3.969683028665376e+01,     2.209460984245205e+02,     -2.759285104469687e+02,     1.383577518672690e+02,     -3.066479806614716e+01,    2.506628277459239e+00]
    b = [ -5.447609879822406e+01,     1.615858368580409e+02,     -1.556989798598866e+02,     6.680131188771972e+01,     -1.328068155288572e+01, ]
  # b = [ b(1) -> b[0] ,                 b(2) ,                        b(3),                        b(4),                    b(5) -> b[4] ]
    c = [-7.784894002430293e-03,      -3.223964580411365e-01,    -2.400758277161838e+00,     -2.549732539343734e+00,     4.374664141464968e+00,    2.938163982698783e+00,]
  # c = [c(1) -> c[0],                   c(2),                         c(3),                        c(4),                         c(5),                c(6) -> c[5]   ]
    d = [7.784695709041462e-03,        3.224671290700398e-01,     2.445134137142996e+00,      3.754408661907416e+00, ]
  # d = [ d(1) -> d[0],                  d(2),                         d(3),                        d(4) -> d[3] ]

  # Define break-points.
    p_low  = 0.02425
    p_high = 1 - p_low

  # output value
    x=0

  # Rational approximation for lower region.
    if 0 < p and p < p_low:
        q = math.sqrt(-2*math.log(p))
        x = (((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) / ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1)

  # Rational approximation for central region.
    elif p_low <= p and p <= p_high:
        q = p - 0.5
        r = q*q
        x = (((((a[0]*r+a[1])*r+a[2])*r+a[3])*r+a[4])*r+a[5])*q / (((((b[0]*r+b[1])*r+b[2])*r+b[3])*r+b[4])*r+1)
  # Rational approximation for upper region.
    elif p_high < p and p < 1:
        q = math.sqrt(-2*math.log(1-p))
        x = -(((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) / ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1)

    return x

def  pValueToSignificance( p, excess): # excess: bool, False if deficit
  if p < 0 or p > 1:
    print ("ERROR: p-value must belong to [0,1] but input value is ", p)
    return 0

  if excess:
    return pja_normal_quantile(1-p)
  else:
    return pja_normal_quantile(p)

def arxiv_significance(nbObs, nbExp, nbExpEr):
    #calculates significance from https://arxiv.org/abs/1111.2062
    print ("plot significance in the bottom panel")
    pValue = pValuePoissonError(int(nbObs), nbExp, nbExpEr*nbExpEr)
    print ("pval:", pValue)
    if pValue < 0.5:
        pull = pValueToSignificance(pValue, nbObs>nbExp )
        print (pull)
    else:
        pull = 0.0001
        print ("pull at zero!")
    return pull

##############################################

if __name__ == "__main__":
    main()
