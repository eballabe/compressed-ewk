import ROOT
import sys
from ROOT import *

import argparse

parser = argparse.ArgumentParser(description='Parser')
parser.add_argument('-s','--sample', type=str, help='sample')
args = parser.parse_args()

sample = args.sample

infile = TFile.Open("/storage/amurrone/DisplacedTrack/ntuples_Sept2021_TightEvtCleaning/"                           +sample+"/data-tree/"+sample+".root")

tree = infile.Get("tree_NoSys")
tree.Scan("DatasetNumber")

