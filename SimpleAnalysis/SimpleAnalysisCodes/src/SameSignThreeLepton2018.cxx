#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(SameSignThreeLepton2018)

void SameSignThreeLepton2018::Init()
{
  /// validation regions
  addRegions({"WZ4j"});
  addRegions({"VRWZ5j"});
  addRegions({"VRTTV"});
  
  /// nominal signal regions + relaxed signal regions
  addRegions({"Rpc2L1b"});
  addRegions({"Rpc2L2b"});
  addRegions({"Rpc2L0b"});
  addRegions({"Rpc3LSS1b"});
  addRegions({"Rpv2L0b"});
  ///
  addRegions({"Rpc2L1bLoose1"});
  addRegions({"Rpc2L2bLoose1"});
  addRegions({"Rpc2L0bLoose1"});
  addRegions({"Rpv2L0bLoose1"});
  ///
  addRegions({"Rpc2L1bLoose2"});
  addRegions({"Rpc2L2bLoose2"});
  addRegions({"Rpc2L0bLoose2"});
  addRegions({"Rpv2L0bLoose2"});
  ///
  addRegions({"Rpc2L1bLoose3"});
  addRegions({"Rpc2L2bLoose3"});
  addRegions({"Rpc2L0bLoose3"});
  addRegions({"Rpv2L0bLoose3"});
  
}

void SameSignThreeLepton2018::ProcessEvent(AnalysisEvent *event)
{
  auto baselineElectrons = filterCrack(event->getElectrons(10, 2.47, ELooseBLLH|ED0Sigma5|EZ05mm)); 
  auto baselineMuons     = event->getMuons(10, 2.5, MuMedium|MuZ05mm); 
  auto jets              = event->getJets(20., 2.8);  /// JVT cut not applied
  auto metVec            = event->getMET();

  /// Standard SUSY overlap removal      
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return 0.1 + 9.6/muon.Pt(); };
  auto radiusCalcEl   = [] (const AnalysisObject& electron, const AnalysisObject& ) { return 0.1 + 9.6/electron.Pt(); };

  jets               = overlapRemoval(jets, baselineElectrons, 0.2, NOT(BTag85MV2c10)); /// not entirely correct
  jets               = overlapRemoval(jets, baselineMuons, 0.4, LessThan3Tracks); 
  baselineElectrons  = overlapRemoval(baselineElectrons, jets, radiusCalcEl); 
  baselineMuons      = overlapRemoval(baselineMuons, jets, radiusCalcMuon); 
  baselineElectrons  = overlapRemoval(baselineElectrons, baselineMuons,0.01);  

  auto signalElectrons = filterObjects(baselineElectrons, 10, 2.0, EMediumLH|EIsoFCTight);  /// missing ECIDS
  auto signalMuons     = filterObjects(baselineMuons, 10, 2.5, MuD0Sigma3|MuIsoFCTightTrackOnly); 
  
  auto signalLeptons   = signalElectrons+signalMuons;
  AnalysisClass::sortObjectsByPt(signalLeptons);
  auto baselineLeptons = baselineElectrons+baselineMuons;
  AnalysisClass::sortObjectsByPt(baselineLeptons);
  

  int nLeptons = signalLeptons.size();
  int nBaseLeptons = baselineLeptons.size();
  if (nLeptons<2) return;

  auto lep0 = signalLeptons[0];
  auto lep1 = signalLeptons[1];
  
  if (lep0.charge()!=lep1.charge() && nLeptons<3) return;  /// add SS cut and the at least two signal leptons cut
  if(signalLeptons[1].Pt()<20) return; /// add 20,20,10 cut

  /// Variables we'll be cutting on
  double met   = metVec.Et();
  int nJets25  = countObjects(jets, 25.);
  int nJets40  = countObjects(jets, 40.);
  int nBJets20 = countObjects(jets, 20., 2.5, BTag70MV2c10); 
  double meff  = sumObjectsPt(jets) + sumObjectsPt(signalLeptons) + met; 
  double JetPt  = sumObjectsPt(jets);
  double BJetPt = 0;
  for(unsigned int i=0 ; i<jets.size() ; i++){
    if(jets[i].pass(BTag70MV2c10) && fabs(jets[i].Eta())<2.5){
      BJetPt+=jets[i].Pt();
    }
  }
  
  float dRminL0Jet = 999;
  for (unsigned int j=0; j<jets.size(); j++){
    if (jets[j].Pt()>25) {
      float tmp = lep0.DeltaR(jets[j]);
      if(tmp < dRminL0Jet){
        dRminL0Jet = tmp;
      }
    }
  }

  //Variable needed for the Rpc3LSS1b 
  int is3LSS = 0;
  int nPosLep = 0;
  int nNegLep = 0;
  if(nLeptons>2){
    for(int ilep=0;ilep<nLeptons;ilep++){
      if(signalLeptons[ilep].charge()>0) nPosLep++;
      if(signalLeptons[ilep].charge()<0) nNegLep++;
    }
    if (nPosLep>2 || nNegLep>2) { 
      is3LSS = 1;
    }
  }
  
  bool isSS30 = false;
  for(int i=0;i<nLeptons;i++){
    for(int j=i+1;j<nLeptons;j++){
      if(i != j && signalLeptons[i].charge()*signalLeptons[j].charge()>0 && signalLeptons[i].Pt()>30 && signalLeptons[j].Pt()>30){
        isSS30 = true;
		
        break;
      }
      if(isSS30) break;
    }
  }
   
  float mass = 0;
  bool mSFOS=false;
  bool meeZVeto=true;
  if(signalElectrons.size()>1){
    for(size_t i=0;i<signalElectrons.size();i++){
      for(size_t j=i+1;j<signalElectrons.size();j++){
        mass = (signalElectrons[i]+signalElectrons[j]).M();
        if(signalElectrons[i].charge()*signalElectrons[j].charge()<0 && i!=j){
          if (mass>81 && mass<101) mSFOS = true;
        }
        if(signalElectrons[i].charge()*signalElectrons[j].charge()>0 && i!=j){
          if (mass>81 && mass<101) meeZVeto = false;
        }
      }
    }
  }
  if(signalMuons.size()>1){
    for(size_t i=0;i<signalMuons.size();i++){
      for(size_t j=i+1;j<signalMuons.size();j++){
        if(signalMuons[i].charge()*signalMuons[j].charge()<0 && i!=j){
          mass = (signalMuons[i]+signalMuons[j]).M();
          if (mass>81 && mass<101) mSFOS = true;
        }
      }
    }
  }

  bool Rpc2L1b  = nLeptons>=2 && nBJets20>=1 && nJets40>=6 && met/meff>0.25;
  bool Rpc2L2b  = nLeptons>=2 && nBJets20>=2 && nJets25>=6 && met>300 && meff>1400 && met/meff>0.14;
  bool Rpc2L0b  = nLeptons>=2 && nBJets20==0 && nJets40>=6 && met>200 && meff>1000 && met/meff>0.2;
  bool Rpv2L0b  = nLeptons>=2 && nBJets20>=0 && nJets40>=6 && meff>2600.;
  bool Rpc3LSS1b= nLeptons>=3 && nBJets20>=1 && is3LSS==1 && meeZVeto==1 && met/meff>0.14;
  bool isSRVeto = !(Rpc2L1b || Rpc2L2b || Rpc2L0b || Rpv2L0b) ;
  
  bool Rpc2L1bLoose1 = nLeptons>=2 && nBJets20>=1 && nJets25>=6 && met/meff>0.25;
  bool Rpc2L2bLoose1 = nLeptons>=2 && nBJets20>=2 && nJets25>=6 && met>300 && meff>1400 && met/meff>0.14;
  bool Rpc2L0bLoose1 = nLeptons>=2 && nBJets20==0 && nJets25>=6 && met>200 && meff>1000 && met/meff>0.2;
  bool Rpv2L0bLoose1 = nLeptons>=2 && nBJets20>=0 && nJets25>=6 && meff>2600.;
  //
  bool Rpc2L1bLoose2 = nLeptons>=2 && nBJets20>=1 && nJets25>=5 && met/meff>0.14;
  bool Rpc2L2bLoose2 = nLeptons>=2 && nBJets20>=2 && nJets25>=5 && met/meff>0.14;
  bool Rpc2L0bLoose2 = nLeptons>=2 && nBJets20==0 && nJets25>=5 && met/meff>0.14;
  bool Rpv2L0bLoose2 = nLeptons>=2 && nBJets20>=0 && nJets25>=5 && meff>1000.;
  //
  bool Rpc2L1bLoose3  = nLeptons>=2 && nBJets20>=1 && nJets40>=6 && meff>800;
  bool Rpc2L2bLoose3  = nLeptons>=2 && nBJets20>=2 && nJets25>=6 && meff>800;
  bool Rpc2L0bLoose3  = nLeptons>=2 && nBJets20==0 && nJets40>=6 && meff>800;
  bool Rpv2L0bLoose3  = nLeptons>=2 && nBJets20>=0 && nJets40>=6 && meff>800;
 
  bool VRWZ4j = nBaseLeptons==3 && nLeptons==3. && nBJets20==0 && nJets25>=4 && meff>600. && meff<1500. && met>50. && met<250. && mSFOS==1 && isSRVeto; 
  bool VRWZ5j = nBaseLeptons==3 && nLeptons==3. && nBJets20==0 && nJets25>=5 && meff>400. && meff<1500. && met>50. && met<250. && mSFOS==1 && isSRVeto;  
  bool VRTTV  = nLeptons>=2 && isSS30 &&
				nBJets20>=1 && nJets40>=3 && dRminL0Jet>1.1 && met<250. && meff>600. && meff<1500. && BJetPt/JetPt>0.4 && met/meff>0.1 && isSRVeto;;
  
  if(Rpc2L1b) accept("Rpc2L1b");
  if(Rpc2L2b) accept("Rpc2L2b");
  if(Rpc2L0b) accept("Rpc2L0b");
  if(Rpc3LSS1b) accept("Rpc3LSS1b");
  if(Rpv2L0b) accept("Rpv2L0b");
  ///
  if(Rpc2L1bLoose1) accept("Rpc2L1bLoose1");
  if(Rpc2L2bLoose1) accept("Rpc2L2bLoose1");
  if(Rpc2L0bLoose1) accept("Rpc2L0bLoose1");
  if(Rpv2L0bLoose1) accept("Rpv2L0bLoose1");
  ///
  if(Rpc2L1bLoose2) accept("Rpc2L1bLoose2");
  if(Rpc2L2bLoose2) accept("Rpc2L2bLoose2");
  if(Rpc2L0bLoose2) accept("Rpc2L0bLoose2");
  if(Rpv2L0bLoose2) accept("Rpv2L0bLoose2");
  ///
  if(Rpc2L1bLoose3) accept("Rpc2L1bLoose3");
  if(Rpc2L2bLoose3) accept("Rpc2L2bLoose3");
  if(Rpc2L0bLoose3) accept("Rpc2L0bLoose3");
  if(Rpv2L0bLoose3) accept("Rpv2L0bLoose3");
  ///
  if (VRWZ4j) accept("WZ4j");
  if (VRWZ5j) accept("VRWZ5j");
  if (VRTTV) accept("VRTTV");
 

  return;
}
