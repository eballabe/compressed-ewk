#include "SimpleAnalysisFramework/AnalysisClass.h"

/* Simple routine to count leptons, jets and MET */

DefineAnalysis(Basic2016)

void Basic2016::Init()
{
  addRegions({"Leptons1","Leptons2","Leptons3","Leptons4"}); 
  addRegions({"Jets50_2","Jets50_4","Jets50_6","Jets50_8"});
  addRegions({"Met200","Met400","Met600","Met800"});
}

void Basic2016::ProcessEvent(AnalysisEvent *event)
{
  auto baselineElectrons  = event->getElectrons(5, 2.47, ELooseBLLH);
  auto baselineMuons      = event->getMuons(5, 2.4, MuMedium|MuNotCosmic|MuQoPSignificance);
  auto baselineJets       = event->getJets(50., 2.8);
  auto metVec             = event->getMET();
  double met              = metVec.Et();

  auto Leptons = baselineMuons+baselineElectrons;
  
  if (Leptons.size()>=1) accept("Leptons1");
  if (Leptons.size()>=2) accept("Leptons2");
  if (Leptons.size()>=3) accept("Leptons3");
  if (Leptons.size()>=4) accept("Leptons4");

  if (baselineJets.size()>=2) accept("Jets50_2");
  if (baselineJets.size()>=4) accept("Jets50_4");
  if (baselineJets.size()>=6) accept("Jets50_6");
  if (baselineJets.size()>=8) accept("Jets50_8");

  if (met>200) accept("Met200");
  if (met>400) accept("Met400");
  if (met>600) accept("Met600");
  if (met>800) accept("Met800");
  return;
}
