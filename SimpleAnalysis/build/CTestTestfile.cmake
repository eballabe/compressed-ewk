# CMake generated Testfile for 
# Source directory: /storage/ballaben/compressed-ekw/SimpleAnalysis
# Build directory: /storage/ballaben/compressed-ekw/SimpleAnalysis/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("SimpleAnalysisCodes")
subdirs("SimpleAnalysisFramework")
