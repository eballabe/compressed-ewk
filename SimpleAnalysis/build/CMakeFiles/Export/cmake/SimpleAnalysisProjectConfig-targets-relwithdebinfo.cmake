#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "SimpleAnalysisProject::SimpleAnalysisFrameworkLib" for configuration "RelWithDebInfo"
set_property(TARGET SimpleAnalysisProject::SimpleAnalysisFrameworkLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(SimpleAnalysisProject::SimpleAnalysisFrameworkLib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libSimpleAnalysisFrameworkLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libSimpleAnalysisFrameworkLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS SimpleAnalysisProject::SimpleAnalysisFrameworkLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_SimpleAnalysisProject::SimpleAnalysisFrameworkLib "${_IMPORT_PREFIX}/lib/libSimpleAnalysisFrameworkLib.so" )

# Import target "SimpleAnalysisProject::simpleAnalysis" for configuration "RelWithDebInfo"
set_property(TARGET SimpleAnalysisProject::simpleAnalysis APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(SimpleAnalysisProject::simpleAnalysis PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/simpleAnalysis"
  )

list(APPEND _IMPORT_CHECK_TARGETS SimpleAnalysisProject::simpleAnalysis )
list(APPEND _IMPORT_CHECK_FILES_FOR_SimpleAnalysisProject::simpleAnalysis "${_IMPORT_PREFIX}/bin/simpleAnalysis" )

# Import target "SimpleAnalysisProject::slimMaker" for configuration "RelWithDebInfo"
set_property(TARGET SimpleAnalysisProject::slimMaker APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(SimpleAnalysisProject::slimMaker PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/slimMaker"
  )

list(APPEND _IMPORT_CHECK_TARGETS SimpleAnalysisProject::slimMaker )
list(APPEND _IMPORT_CHECK_FILES_FOR_SimpleAnalysisProject::slimMaker "${_IMPORT_PREFIX}/bin/slimMaker" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
