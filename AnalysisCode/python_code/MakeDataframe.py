import uproot
import numpy as np
np.set_printoptions(suppress=True) # print arrays without e notation
np.set_printoptions(threshold=10000)
import pandas as pd
pd.set_option('display.max_columns', None)  
import os.path
import warnings
from pandas.core.common import SettingWithCopyWarning
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)


import argparse
parser = argparse.ArgumentParser(description='Make Dataframse, specify SampleName, inputfile, outputDataframeName')
parser.add_argument('--S', type=str, default='N1C1p', help='Sample Name')
parser.add_argument('--I', type=str, default='/storage/ballaben/samples_DisplacedTrack/N1C1p_151p0_150p5_150p0_MET75.recon_v0.1_mc16_13TeV.500971.MGPy8EG_A14N23LO.root', help='Input File Name')
parser.add_argument('--O', type=str, default='df_N1C1p.pickle', help='Output Dataframe Name')
args = parser.parse_args()


SampleName = args.S
inputfile = uproot.open(args.I)
outputName = args.O

features_to_read = ['EventNumber','eventWeight','trkPt','trkZ0','trkD0','trkD0Err','trkparpdgId','trkPhi','trkEta','trkZ0SinTheta','trkisTight','jetPt','jetPhi','jetEta','met_Et','met_Phi','nLep_base','IsMETTrigPassed'] 


inputfile = inputfile['tree_NoSys;1']
inputfile.keys()

LeadingJet_pTCut = 250.
LeadingJet_EtaCut = 2.4
DPhiJetMET_Cut = 0.4

dictionary = {} #dictionary

for feature in features_to_read:
    dictionary[feature] = np.array(inputfile[feature]) #fill the dictionary

df = pd.DataFrame(dictionary) 

df['Sample'] = np.full(df.index.size,SampleName)
df['njet'] = np.full(df.index.size,0) 
df['LeadingJetPt'] = np.full(df.index.size,0.) 
df['LeadingJetEta'] = np.full(df.index.size,0.) 
df['DPhiJetMET'] = np.full(df.index.size, 0.)
df['ntracks'] = np.full(df.index.size,0) 
df['IsBaseline_trk'] = np.empty(df.index.size, dtype=object)
df['ntracks_baseline'] = np.full(df.index.size, 0)
df['IsAny_trk'] = np.empty(df.index.size, dtype=object)
df['IsIsolated_trk'] = np.empty(df.index.size, dtype=object)
df['minDeltaR_BaselineAny_trk'] = np.empty(df.index.size, dtype=object)
df['ntracks_isolated'] = np.full(df.index.size, 0)
df['IsDisplaced_trk'] = np.empty(df.index.size, dtype=object)
df['ntracks_displaced'] = np.full(df.index.size, 0)
df['DPhiTrackMET'] = np.empty(df.index.size, dtype=object)
df['IsMETAligned_track'] = np.empty(df.index.size, dtype=object)
df['ntracks_METAligned'] = np.full(df.index.size, 0)




def get_dphi(a, b):
    dphi = a-b
    if(dphi<-1*np.pi): dphi= 2*np.pi+dphi
    if(dphi>np.pi): dphi=2*np.pi-dphi
    return(dphi)

def deltaR(selectedTrack_eta, selectedTrack_Phi, anyTrack_eta, anyTrack_Phi):
    if (  len(anyTrack_eta) != len(anyTrack_Phi) ):
        raise Exception('Different lists sizes')
    if len(anyTrack_eta) == 0: 
        deltaR_min = 0 
        IsIsolated_trk = True
    elif len(anyTrack_eta) != 0: 
        if ( (len(anyTrack_eta) == 1) and ( selectedTrack_eta == anyTrack_eta[0]) and (selectedTrack_Phi == anyTrack_Phi[0]) ):
            deltaR_min = 0
            IsIsolated_trk = True
        else:
            array_dRs = []
            for i in range(len(anyTrack_eta)):
                if ( ( selectedTrack_eta == anyTrack_eta[i]) and (selectedTrack_Phi == anyTrack_Phi[i]) ):
                    continue
                else:
                    array_dRs.append( np.sqrt ( (selectedTrack_eta - anyTrack_eta[i])**2 + (selectedTrack_Phi - anyTrack_Phi[i])**2 ) )
            deltaR_min = min(array_dRs)
            IsIsolated_trk = (deltaR_min > 0.3 )
    return IsIsolated_trk,deltaR_min

def get_entries():
    n_entries = df.index.size
    n_entries_SUSY = 0
    n_tracks = 0
    n_tracks_SUSY = 0
    for entry in df.index:
        if ( df['trkparpdgId'][entry].tolist().count(1000023) + df['trkparpdgId'][entry].tolist().count(1000024) != 0 ): 
            n_entries_SUSY += 1
        n_tracks += len(df['trkparpdgId'][entry].tolist())
        n_tracks_SUSY += df['trkparpdgId'][entry].tolist().count(1000023) + df['trkparpdgId'][entry].tolist().count(1000024)
    return n_entries, n_entries_SUSY, n_tracks, n_tracks_SUSY



print('-------')
print('Sample: ',SampleName)


print('-------')
n_entries_0,n_entries_SUSY_0,n_tracks_0,n_tracks_SUSY_0 = get_entries()
print('Dataframe: n_entries_0 = {},  n_entries_SUSY_0 = {},  n_entries_SUSY_0/n_entries_0 = {}'.format(n_entries_0,n_entries_SUSY_0,n_entries_SUSY_0/n_entries_0)) 
print('Dataframe: n_tracks_0 = {},  n_tracks_SUSY_0 = {},  n_tracks_SUSY_0/n_tracks_0 = {}'.format(n_tracks_0,n_tracks_SUSY_0,n_tracks_SUSY_0/n_tracks_0))       
print('-------')


UpTo4Jets_rejected_index = []


print('Total entries = {}'.format(df.index))
for entry in df.index:
    if(entry%100 == 0): print('Processing entry: {}'.format(entry))


    if df['nLep_base'][entry] != 0: continue 
    if df['IsMETTrigPassed'][entry] == False: continue

    df['njet'][entry] = df['jetPt'][entry].size
    if df['njet'][entry] == 0: continue

    df['LeadingJetPt'][entry] = df['jetPt'][entry][0]
    df['LeadingJetEta'][entry] = df['jetEta'][entry][0]
    if ( (df['LeadingJetPt'][entry] < LeadingJet_pTCut) | (np.fabs(df['LeadingJetEta'][entry]) > LeadingJet_EtaCut) ): continue

    if df['njet'][entry] > 4:
        counter = 0
        for jet in range(df['jetPt'][entry].size):
            if ( df['jetPt'][entry][jet] > 30. and np.fabs(df['jetEta'][entry][jet]) < 2.8 ):
                counter += 1
        if counter > 4:
            UpTo4Jets_rejected_index.append(entry)
    if (entry in UpTo4Jets_rejected_index): continue

    DPhiMETJet_i = []
    for jet in range(len(df['jetPt'][entry])): 
        if ( df['jetPt'][entry][jet] > 30. and np.fabs(df['jetEta'][entry][jet]) < 2.8 ):
            DPhiMETJet_i.append(np.fabs(get_dphi(df['met_Phi'][entry],df['jetPhi'][entry][jet])))

    df['DPhiJetMET'][entry] = min(DPhiMETJet_i)
    if df['DPhiJetMET'][entry] < DPhiJetMET_Cut: continue

    df['ntracks'][entry] = df['trkPt'][entry].size
    if df['ntracks'][entry] == 0: continue

    df['IsAny_trk'][entry] = (df['trkPt'][entry] > 1) & (np.fabs(df['trkD0'][entry]) < 1.5) & (np.fabs(df['trkZ0SinTheta'][entry]) < 1.5)

    # Baseline
    df['IsBaseline_trk'][entry] = (df['trkPt'][entry] < 5) & (df['trkPt'][entry] > 1.) & (np.fabs(df['trkEta'][entry]) < 1.5) & (df['trkisTight'][entry] != False) & (np.fabs(df['trkD0'][entry]) < 10) & (np.fabs(df['trkZ0SinTheta'][entry]) < 3.)
    df['ntracks_baseline'][entry] = df['IsBaseline_trk'][entry].tolist().count(True)
    if df['ntracks_baseline'][entry] == 0: continue

    # Isolation
    IsIsolated_trk = []
    DeltaRmin_trk = []
    for track in range(df['IsBaseline_trk'][entry].size):
        if df['IsBaseline_trk'][entry][track] == False:
            IsIsolated_trk.append(False) 
            DeltaRmin_trk.append(0)
        if df['IsBaseline_trk'][entry][track] == True:
            isIsolated, deltaRmin = deltaR( df['trkEta'][entry][track], df['trkPhi'][entry][track],
               df['trkEta'][entry][ [i for i,val in enumerate(df['IsAny_trk'][entry]) if val==True]],
               df['trkPhi'][entry][ [i for i,val in enumerate(df['IsAny_trk'][entry]) if val==True]] )
            IsIsolated_trk.append(isIsolated)
            DeltaRmin_trk.append(deltaRmin)

    df['IsIsolated_trk'][entry] = np.array(IsIsolated_trk)
    df['minDeltaR_BaselineAny_trk'][entry] = np.array(DeltaRmin_trk)
    df['ntracks_isolated'][entry] = df['IsIsolated_trk'][entry].tolist().count(True)
    if df['ntracks_isolated'][entry] == 0: continue

    # Displacement
    df['IsDisplaced_trk'][entry] = (df['IsBaseline_trk'][entry] == True) & (df['IsIsolated_trk'][entry] == True) & (np.fabs(df['trkD0'][entry]/df['trkD0Err'][entry]) > 0.)
    df['ntracks_displaced'][entry] = df['IsDisplaced_trk'][entry].tolist().count(True)
    if df['ntracks_displaced'][entry] == 0: continue

    # MET alignment
    DPhiTrackMET_i = []
    for track in range(len(df['trkPt'][entry])): 
        DPhiTrackMET_i.append( np.fabs( get_dphi( df['met_Phi'][entry], df['trkPhi'][entry][track] ) ) )
    df['DPhiTrackMET'][entry] = np.array(DPhiTrackMET_i)
    df['IsMETAligned_track'][entry] = (np.array(DPhiTrackMET_i) < 1) &  (df['IsDisplaced_trk'][entry] == True)
    df['ntracks_METAligned'][entry] = df['IsMETAligned_track'][entry].tolist().count(True)
    if df['ntracks_METAligned'][entry] == 0: continue

print('Proceeding to apply selection')

LeptonVeto_rejected_index = df[df.nLep_base!=0].index
df.drop(LeptonVeto_rejected_index, inplace=True) 
print('---Lepton-Veto---- All entries with leptons are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY_1 = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY_1 = {}, n_tracks_SUSY_1/n_tracks = {}'.format(n_tracks,n_tracks_SUSY_1,n_tracks_SUSY_1/n_tracks))       
print('-------')


NoJetVeto_rejected_index = df[df.njet==0].index
df.drop(NoJetVeto_rejected_index, inplace=True)
print('---NoJet-Veto---- All entries without any jets are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
print('-------')


LeadingJet_rejected_index = df[(df.LeadingJetPt < LeadingJet_pTCut) | (np.fabs(df.LeadingJetEta) > LeadingJet_EtaCut)].index
df.drop(LeadingJet_rejected_index, inplace=True) 
print('---LeadingJet pT > {} GeV and | LeadingJet Eta | < {} ---- All entries not satisfying these requirements are removed'.format(LeadingJet_pTCut,LeadingJet_EtaCut))
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
print('-------')


for entry in UpTo4Jets_rejected_index:
    df.drop(entry, inplace=True)
print('---Up To 4 Jets veto ---- All entries with more than 4 jets with JetpT > 30 GeV and | JetEta | < 2.4 are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
print('-------')

DPhiJetMET_rejected_index = df[df.DPhiJetMET < DPhiJetMET_Cut].index
df.drop(DPhiJetMET_rejected_index, inplace=True) 
print('---minDPhi(Jet_i, MET) > 0.4 ---- All entries without are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
print('-------')

NoTrackVeto_rejected_index = df[df.ntracks==0].index
df.drop(NoTrackVeto_rejected_index, inplace=True)
print('---NoTrackVeto ---- All entries without any tracks are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))       
print('-------')



NoBaselineTrackVeto_rejected_index = df[df.ntracks_baseline==0].index 
df.drop(NoBaselineTrackVeto_rejected_index, inplace=True)
print('---NoBaselineTrackVeto ---- All entries without any baseline tracks are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))
n_baseline_tracks = 0
n_baseline_tracks_SUSY = 0
for entry in df.index:
    for track in range(len(df['IsBaseline_trk'][entry])):
        if df['IsBaseline_trk'][entry][track] == True:
            n_baseline_tracks += 1
            if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024:
                n_baseline_tracks_SUSY += 1
print('n_baseline_tracks = {}, n_baseline_tracks_SUSY = {}, n_baseline_tracks_SUSY/n_baseline_tracks = {}'.format(n_baseline_tracks,n_baseline_tracks_SUSY,n_baseline_tracks_SUSY/n_baseline_tracks))      
print('-------')


NoIsolatedTrackVeto_rejected_index = df[df.ntracks_isolated==0].index
df.drop(NoIsolatedTrackVeto_rejected_index, inplace=True)
print('---NoIsolatedTrackVeto ---- All entries without any isolated tracks are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))
n_isolated_tracks = 0
n_isolated_tracks_SUSY = 0
for entry in df.index:
    for track in range(len(df['trkPt'][entry])):
        if df['IsIsolated_trk'][entry][track] == True:
            n_isolated_tracks += 1
            if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024:
                n_isolated_tracks_SUSY += 1
print('n_isolated_tracks = {}, n_isolated_tracks_SUSY = {}, n_isolated_tracks_SUSY/n_isolated_tracks = {}'.format(n_isolated_tracks,n_isolated_tracks_SUSY,n_isolated_tracks_SUSY/n_isolated_tracks))      
print('-------')



NoDisplacedTrackVeto_rejected_index = df[df.ntracks_displaced==0].index
df.drop(NoDisplacedTrackVeto_rejected_index, inplace=True)
print('---NoDisplacedTrackVeto ---- All entries without any displaced tracks are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))
n_displaced_tracks = 0
n_displaced_tracks_SUSY = 0
for entry in df.index:
    for track in range(len(df['trkPt'][entry])):
        if df['IsDisplaced_trk'][entry][track] == True:
            n_displaced_tracks += 1
            if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024:
                n_displaced_tracks_SUSY += 1
print('n_displaced_tracks = {}, n_displaced_tracks_SUSY = {}, n_displaced_tracks_SUSY/n_displaced_tracks = {}'.format(n_displaced_tracks,n_displaced_tracks_SUSY,n_displaced_tracks_SUSY/n_displaced_tracks))
print('-------')


NoMETAlignedTrackVeto_rejected_index = df[df.ntracks_METAligned==0].index 
df.drop(NoMETAlignedTrackVeto_rejected_index, inplace=True)
print('---NoMETalignedTrackVeto ---- All entries without any METaligned tracks are removed')
n_entries,n_entries_SUSY,n_tracks,n_tracks_SUSY = get_entries()
print('n_entries = {}, n_entries_SUSY = {}, n_entries_SUSY/n_entries = {}'.format(n_entries,n_entries_SUSY,n_entries_SUSY/n_entries)) 
print('n_tracks = {}, n_tracks_SUSY = {}, n_tracks_SUSY/n_tracks = {}'.format(n_tracks,n_tracks_SUSY,n_tracks_SUSY/n_tracks))
n_METaligned_tracks = 0
n_METaligned_tracks_SUSY = 0
for entry in df.index:
    for track in range(len(df['trkPt'][entry])):
        if df['IsMETAligned_track'][entry][track] == True:
            n_METaligned_tracks += 1
            if df['trkparpdgId'][entry][track] == 1000023 or df['trkparpdgId'][entry][track] == 1000024:
                n_METaligned_tracks_SUSY += 1
print('n_METaligned_tracks = {}, n_METaligned_tracks_SUSY = {}, n_METaligned_tracks_SUSY/n_METaligned_tracks = {}'.format(n_METaligned_tracks,n_METaligned_tracks_SUSY,n_METaligned_tracks_SUSY/n_METaligned_tracks))   
print('-------')

df.to_pickle(outputName)
