import os, sys

samples_list = ["sample_name"]

for filename in samples_list:

	print ("Creating submission files for " + filename)
	
	workPath = "/path/to/this/folder/"

	template_condor = """executable = {}.sh
universe = docker
docker_image = dr.mi.infn.it/alesala/susy-analysis
requirements = (ClusterName != "lagrange-pool") && (ClusterName != "gamma-pool") && HasDocker
should_transfer_files = YES
transfer_input_files = /path/to/input/ntuples.root, /path/to/analysis_new.C, /path/to/analysis_new.h, /path/to/run_analysis.py 
when_to_transfer_output = ON_EXIT
transfer_output_files = DisplacedTrack/{}.root
transfer_ouput_remaps = "{}.root = /path/to/local/machine/folder/where/you/want/to/save/output/{}.root"
arguments      = {}
output         = {}.out
error          = {}.err
log            = {}.log
+JobFlavour    = "tomorrow"
request_memory = 3072
request_cpus = 8
queue"""

	setup = """hostname
source /release_setup.sh
mkdir DisplacedTrack"""

	command = "python run_analysis.py -s 0 -i ./ -o DisplacedTrack/"

	job_file_name = "{}.sh".format(filename)
	job_file = open(workPath + "/" + job_file_name, "w")
	job_file.write("#!/bin/bash\n")
	job_file.write(setup + "\n")
	job_file.write(command +"\n")
	output = "{}.out".format(filename)
	error = "{}.err".format(filename)
	log = "{}.log".format(filename)
	job_file.close()

	submit_file_name = "{}.sub".format(filename)
	submit_file = open(workPath + "/" + submit_file_name, "w")
	tc = template_condor.format(filename, filename, filename, filename, filename, filename)
	submit_file.write(tc + "\n")
