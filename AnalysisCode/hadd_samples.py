import os

path = "/storage/alesala/ntuples_processed/ntuples_v4/"

directories = ["DisplacedTrack_0lep", "DisplacedTrack_1lep", "DisplacedTrack_2lep"]
campaigns = ["MC16a", "MC16d", "MC16e"]
samples = ["data", "diboson", "singletop", "ttbar", "Znunu", "Wenu", "Wmunu", "Wtaunu", "Zee", "Zmumu", "Ztautau", "Gjets", "dijets"]

for directory in directories:
	
	if directory == "DisplacedTrack_0lep":

		# Points at 85 GeV	
		cmd = 'hadd -f ' +  path + 'higgsino_85.7_85.35_85.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*85p7_85p35_85* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_87_86_85.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*87_86_85* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_88_86.5_85.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*88_86p5_85* '
		os.system(cmd)

		# Points at 100 GeV		
		cmd = 'hadd -f ' +  path + 'higgsino_100.7_100.35_100.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*100p7_100p35_100* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_101_100.5_100.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*101_100p5_100* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_101.5_100.75_100.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*101p5_100p75_100* '
		os.system(cmd)		

		cmd = 'hadd -f ' +  path + 'higgsino_102_101_100.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*102_101_100* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_103_101.5_100.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*103_101p5_100* '
		os.system(cmd)

		# Points at 125 GeV
		cmd = 'hadd -f ' +  path + 'higgsino_125.5_125.25_125.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*125p5_125p25_125* '
		os.system(cmd)

		cmd = 'hadd -f ' +  path + 'higgsino_125.7_125.35_125.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*125p7_125p35_125* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_126_125.5_125.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*126_125p5_125* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_126.5_125.75_125.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*126p5_125p75_125* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_127_126_125.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*127_126_125* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_128_126.5_125.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*128_126p5_125* '
		os.system(cmd)
		
		# Points at 150 GeV		
		cmd = 'hadd -f ' +  path + 'higgsino_150.5_150.25_150.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*150p5_150p25_150* '
		os.system(cmd)

		cmd = 'hadd -f ' +  path + 'higgsino_150.7_150.35_150.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*150p7_150p35_150* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_151_150.5_150.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*151_150p5_150* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_151.5_150.75_150.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*151p5_150p75_150* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_152_151_150.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*152_151_150* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_153_151.5_150.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*153_151p5_150* '
		os.system(cmd)
		
		# Points at 175 GeV		
		cmd = 'hadd -f ' +  path + 'higgsino_175.5_175.25_175.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*175p5_175p25_175* '
		os.system(cmd)

		cmd = 'hadd -f ' +  path + 'higgsino_175.7_175.35_175.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*175p7_175p35_175* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_176_175.5_175.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*176_175p5_175* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_176.5_175.75_175.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*176p5_175p75_175* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_177_176_175.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*177_176_175* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_178_176.5_175.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*178_176p5_175* '
		os.system(cmd)
		
		# Points at 200 GeV		
		cmd = 'hadd -f ' +  path + 'higgsino_200.7_200.35_200.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*200p7_200p35_200* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_201_200.5_200.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*201_200p5_200* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_201.5_200.75_200.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*201p5_200p75_200* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_202_201_200.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*202_201_200* '
		os.system(cmd)
		
		# Points at 225 GeV
		cmd = 'hadd -f ' +  path + 'higgsino_225.7_225.35_225.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*225p7_225p35_225* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_226_225.5_225.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*226_225p5_225* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_226.5_225.75_225.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*226p5_225p75_225* '
		os.system(cmd)
		
		cmd = 'hadd -f ' +  path + 'higgsino_227_226_225.root '
		for cp in campaigns:
			cmd += path + directory + '/' + cp + '/*227_226_225* '
		os.system(cmd)
	
	os.system('hadd -f ' + path + directory + '/data.root ' + path + directory + '/Data/data* ')

	cmd = 'hadd -f ' +  path + directory + '/diboson.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/diboson* '
	os.system(cmd)
	
	cmd = 'hadd -f ' +  path + directory + '/ttbar.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/ttbar* '
	os.system(cmd)

	cmd = 'hadd -f ' +  path + directory + '/singletop.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/singletop* '
	os.system(cmd)

	cmd = 'hadd -f ' +  path + directory + '/Znunu.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Znnjets* '
	os.system(cmd)
	
	cmd = 'hadd -f ' +  path + directory + '/Zee.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Zee* '
	os.system(cmd)
	
	cmd = 'hadd -f ' +  path + directory + '/Zmumu.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Zmm* '
	os.system(cmd)
	
	cmd = 'hadd -f ' +  path + directory + '/Ztautau.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Ztt* '
	os.system(cmd)

	cmd = 'hadd -f ' +  path + directory + '/Wenu.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Wen* '
	os.system(cmd)
	
	cmd = 'hadd -f ' +  path + directory + '/Wmunu.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Wmn* '
	os.system(cmd)
	
	cmd = 'hadd -f ' +  path + directory + '/Wtaunu.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Wtn* '
	os.system(cmd)

	cmd = 'hadd -f ' +  path + directory + '/Gjets.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/Gjets* '
	os.system(cmd)

	cmd = 'hadd -f ' +  path + directory + '/dijets.root '
	for cp in campaigns:
		cmd += path + directory + '/' + cp + '/dijet* '
	os.system(cmd)

for sp in samples:
	cmd = 'hadd -f ' + path + sp + '.root '
	for directory in directories:
		cmd += path + directory + '/' + sp + '.root '
	os.system(cmd)

