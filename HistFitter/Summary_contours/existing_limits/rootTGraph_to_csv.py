#!/usr/bin/env python
'''
Reads in TGraph root file
Outputs the (x, y) coordinates of contour as CSV file
'''

# So Root ignores command line inputs so we can use argparse
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import *
import time 
import os, sys, time, argparse, math

#____________________________________________________________________________
def main():
  
  t0 = time.time()
  
  # Parse in arguments
  parser = argparse.ArgumentParser(description='Converts root TGraph into CSV contour.')
  parser.add_argument('-i', '--inFile',  type=str, nargs='?', help='Input root TGraph', default='')
  parser.add_argument('-o', '--outFile', type=str, nargs='?', help='Output x, y coordinates of contour CSV', default='')
  args = parser.parse_args()

  if args.inFile: 
    in_file = args.inFile
  if args.outFile: 
    out_file = args.outFile
  
  #in_file = 'outputGraphs_nominal_DirSlep.root'
  #out_file = 'ATL13TeV_140ifb_2L0JSlepLHRH_exp_mN1_vs_mSlep_1.csv'
  
  print( 'Processing {0}'.format(in_file) )   
  tf = TFile(in_file)
  #tgraph_cont = tf.Get('Exp_0')
  tgraph_cont = tf.Get('Obs_0')
  
  # ------------------------------------------------------
  # Extract the contour and put it into a TGraph (1D)
  n_pts = tgraph_cont.GetN()
  # ------------------------------------------------------
  # Store the x, y coordinates of the contour to file
  # ------------------------------------------------------
  with open(out_file, 'w') as f_out:
    #f_out.write( 'mSlepLH2gen,mN1\n' )
    f_out.write( 'mC1,dMC1N1\n' )
    for i in range(0, n_pts):
      x = Double()
      y = Double()
      tgraph_cont.GetPoint(i, x, y) 
      out_x = x
      out_y = y
      f_out.write( '{0:.4f},{1:.4f}\n'.format(out_x, out_y) ) 
  # ------------------------------------------------------

#_________________________________________________________________________
def mkdir(dirPath):
  '''
  make directory for given input path
  '''
  try:
    os.makedirs(dirPath)
    print 'Successfully made new directory ' + dirPath
  except OSError:
    pass
 
if __name__ == "__main__":
  main()
